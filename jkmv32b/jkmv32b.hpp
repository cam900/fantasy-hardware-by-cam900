/*

==============================================================================

    JKMV32B
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMV32B

Fantasy video hardware

*/

#ifndef JKMV32B_HPP
#define JKMV32B_HPP

#pragma once

#include <algorithm>
#include <array>

namespace jkmv32b
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	template<typename T>
	static const inline T clamp(const T in, const T min, const T max)
	{
		return (in < min) ? min : ((in > max) ? max : in);
	} // clamp

	enum color_format_t : u8
	{
		C_ARGB4444 = 0,
		C_ARGB1555,
		C_RGB565,
		C_ARGB8888
	}; // enum color_format_t

	enum texture_format_t : u8
	{
		CLUT1 = 0,
		CLUT2,
		CLUT4,
		CLUT8,
		GREY1,
		GREY2,
		GREY4,
		GREY8,
		GREY4A,
		GREY8A,
		CLUT4A,
		CLUT8A,
		ARGB4444,
		ARGB1555,
		RGB565,
		ARGB8888
	}; // enum texture_format_t

	enum depth_stencil_format : u8
	{
		D1 = 0,
		D2,
		D4,
		D8,
		D16,
		D32
	}; // enum depth_stencil_format

	static const inline u8 pal_expand(const u8 src, const u8 bit)
	{
		u8 res = bitfield(src, 0, bit);
		switch (bit)
		{
			case 1: res = (res & 1) ? 0xff : 0; break;
			case 2: res = (res << 6) | (res << 4) | (res << 2) | res; break;
			case 3: res = (res << 5) | (res << 2) | (res >> 1); break;
			case 4: res = (res << 4) | res; break;
			case 5: res = (res << 3) | (res >> 2); break;
			case 6: res = (res << 2) | (res >> 4); break;
			case 7: res = (res << 1) | (res >> 6); break;
			case 8:
			default: break;
		}
		return res;
	} // pal_expand

	static const inline u32 convert_argb(const color_format_t src_format, const color_format_t dst_format, const u32 argb)
	{
		u32 src;
		switch (src_format)
		{
			case ARGB4444:
				src = (pal_expand(bitfield(argb, 12, 4), 4) << 24)
					| (pal_expand(bitfield(argb, 8, 4), 4) << 16)
					| (pal_expand(bitfield(argb, 4, 4), 4) << 8)
					| (pal_expand(bitfield(argb, 0, 4), 4) << 0);
				break;
			case ARGB1555:
				src = (pal_expand(bitfield(argb, 15, 1), 1) << 24)
					| (pal_expand(bitfield(argb, 10, 5), 5) << 16)
					| (pal_expand(bitfield(argb, 5, 5), 5) << 8)
					| (pal_expand(bitfield(argb, 0, 5), 5) << 0);
				break;
			case RGB565:
				src = (0xff << 24)
					| (pal_expand(bitfield(argb, 11, 5), 5) << 16)
					| (pal_expand(bitfield(argb, 5, 6), 6) << 8)
					| (pal_expand(bitfield(argb, 0, 5), 5) << 0);
				break;
			case ARGB8888:
			default:
				src = argb;
				break;
		}
		u32 dst;
		switch (dst_format)
		{
			case ARGB4444:
				dst = (bitfield(argb, 28, 4) << 12)
					| (bitfield(argb, 20, 4) << 8)
					| (bitfield(argb, 12, 4) << 4)
					| (bitfield(argb, 4, 4) << 0);
				break;
			case ARGB1555:
				dst = (bitfield(argb, 31, 1) << 15)
					| (bitfield(argb, 19, 5) << 10)
					| (bitfield(argb, 11, 5) << 5)
					| (bitfield(argb, 3, 5) << 0);
				break;
			case RGB565:
				dst = (bitfield(argb, 19, 5) << 11)
					| (bitfield(argb, 10, 6) << 5)
					| (bitfield(argb, 3, 5) << 0);
				break;
			case ARGB8888:
			default:
				dst = src;
				break;
		}
		return dst;
	} // convert_argb

	template<typename T>
	static const inline bool get_test_mode(const u8 mode, T src, T dst)
	{
		bool res;
		if (bitfield(mode, 7))
			src = ~src;
		if (bitfield(mode, 6))
			dst = ~dst;
		if (bitfield(mode, 5))
		{
			T tmp = src;
			src = dst;
			dst = tmp;
		}
		switch (bitfield(mode, 0, 4))
		{
			default:
			case 0x00: res = true; break;
			case 0x01: res = (src == dst); break;
			case 0x02: res = (src > dst); break;
			case 0x03: res = (src < dst); break;
			case 0x04: res = (src & dst) == 0; break;
			case 0x05: res = (src & dst) == src; break;
			case 0x06: res = (src | dst) == 0; break;
			case 0x07: res = (src | dst) == src; break;
			case 0x08: res = (src ^ dst) == 0; break;
			case 0x09: res = (src ^ dst) == src; break;
		}
		if (bitfield(mode, 4))
			res = !res;

		return res;
	} // get_test_mode

	template<typename T>
	static const inline T get_write_mode(const u8 mode, const u8 bit, T src, T dst)
	{
		T res;
		if (bitfield(mode, 7))
			src = ~src;
		if (bitfield(mode, 6))
			dst = ~dst;
		if (bitfield(mode, 5))
		{
			T tmp = src;
			src = dst;
			dst = tmp;
		}
		switch (bitfield(mode, 0, 4))
		{
			default:
			case 0x00: res = dst; break;
			case 0x01: res = 0; break;
			case 0x02: res = std::min<T>(src, dst); break;
			case 0x03: res = std::max<T>(src, dst); break;
			case 0x04: res = std::min<s64>(dst + (1 << bit), T(~0)); break;
			case 0x05: res = dst + (1 << bit); break;
			case 0x06: res = std::max<s64>(dst - (1 << bit), 0); break;
			case 0x07: res = dst - (1 << bit); break;
			case 0x08: res = src + dst; break;
			case 0x09: res = src - dst; break;
			case 0x0a: res = src & dst; break;
			case 0x0b: res = src | dst; break;
			case 0x0c: res = src ^ dst; break;
			case 0x0d: res = (src + dst) >> 1; break;
		}
		if (bitfield(mode, 4))
			res = ~res;

		return res;
	} // get_write_mode

	static const inline u8 get_color_fade(const u8 mode, const u8 amount, u8 src, u8 dst)
	{
		s32 res;
		if (bitfield(mode, 7))
			src = ~src;
		if (bitfield(mode, 6))
			dst = ~dst;
		if (bitfield(mode, 5))
		{
			u8 tmp = src;
			src = dst;
			dst = tmp;
		}
		switch (bitfield(mode, 0, 4))
		{
			default:
			case 0x00: res = dst; break;
			case 0x01: res = (src * (256 - amount)) + (dst * amount) >> 8; break;
			case 0x02: res = src + dst; break;
			case 0x03: res = src - dst; break;
			case 0x04: res = src + ((dst * amount) >> 8); break;
			case 0x05: res = src - ((dst * amount) >> 8); break;
			case 0x06: res = (src * dst) >> 8; break;
			case 0x07: res = (src * (dst << 1)) >> 8; break;
			case 0x08: res = (src * ((dst * amount) >> 8)) >> 8; break;
			case 0x09: res = (src * ((dst * amount) >> 7)) >> 8; break;
			case 0x0a: res = std::min<s32>(src, dst); break;
			case 0x0b: res = std::max<s32>(src, dst); break;
			case 0x0c: res = src & dst; break;
			case 0x0d: res = src | dst; break;
			case 0x0e: res = src ^ dst; break;
			case 0x0f: res = (src + dst) >> 1; break;
		}
		res = clamp<s32>(res, 0, 0xff);
		if (bitfield(mode, 4))
			res ^= 0xff;

		return res;
	} // get_color_fade

	static const inline u8 get_blend_mode(const u8 mode, u32 src, u32 dst, u32 consta, u32 constb)
	{
		u32 res;
		if (bitfield(mode, 7))
			src = ~src;
		if (bitfield(mode, 6))
			dst = ~dst;
		if (bitfield(mode, 5))
		{
			u32 tmp = src;
			src = dst;
			dst = tmp;

			tmp = consta;
			consta = constb;
			constb = tmp;
		}
		u32 sa = bitfield(src, 24, 8);
		u32 da = bitfield(dst, 24, 8);
		u32 ca = bitfield(consta, 24, 8);
		u32 sat = std::min<u32>(sa, 0xff - da);
		switch (bitfield(mode, 0, 4))
		{
			default:
			case 0x00: res = 0; break;
			case 0x01: res = src; break;
			case 0x02: res = (sa << 24) | (sa << 16) | (sa << 8) | (sa << 0); break;
			case 0x03: res = consta; break;
			case 0x04: res = (ca << 24) | (ca << 16) | (ca << 8) | (ca << 0); break;
			case 0x05: res = (0xff << 24) | (sat << 16) | (sat << 8) | (sat << 0); break;
		}
		if (bitfield(mode, 4))
			res = ~res;

		return res;
	} // get_blend_mode

	static const inline u8 get_blend_op(const u8 mode, u8 src, u8 dst)
	{
		s32 res;
		if (bitfield(mode, 7))
			src = ~src;
		if (bitfield(mode, 6))
			dst = ~dst;
		if (bitfield(mode, 5))
		{
			u8 tmp = src;
			src = dst;
			dst = tmp;
		}
		switch (bitfield(mode, 0, 4))
		{
			default:
			case 0x00: res = src; break;
			case 0x01: res = 0; break;
			case 0x02: res = src + dst; break;
			case 0x03: res = src - dst; break;
			case 0x04: res = std::min<s32>(src, dst); break;
			case 0x05: res = std::max<s32>(src, dst); break;
			case 0x06: res = (src * dst) >> 8; break;
			case 0x07: res = (src + dst) >> 1; break;
			case 0x08: res = src & dst; break;
			case 0x09: res = src | dst; break;
			case 0x0a: res = src ^ dst; break;
		}
		res = clamp<s32>(res, 0, 0xff);
		if (bitfield(mode, 4))
			res ^= 0xff;

		return res;
	} // get_blend_op

	class color_t
	{
		public:
			color_t()
				: m_a(0)
				, m_r(0)
				, m_g(0)
				, m_b(0)
			{
			} // color_t

			void set_argb(u32 argb)
			{
				m_a = bitfield(argb, 24, 8);
				m_r = bitfield(argb, 16, 8);
				m_g = bitfield(argb, 8, 8);
				m_b = bitfield(argb, 0, 8);
			}

			u32 get_argb()
			{
				return (m_a << 24) | (m_r << 16) | (m_g << 8) | (m_b << 0);
			}

			void set_a(const u8 a)
			{
				m_a = a;
			} // set_a

			u8 a() const
			{
				return m_a;
			} // a

			void set_r(const u8 r)
			{
				m_r = r;
			} // set_r

			u8 r() const
			{
				return m_r;
			} // r

			void set_g(const u8 g)
			{
				m_g = g;
			} // set_g

			u8 g() const
			{
				return m_g;
			} // g

			void set_b(const u8 b)
			{
				m_b = b;
			} // set_b

			u8 b() const
			{
				return m_b;
			} // b

		private:
			u8 m_a = 0;
			u8 m_r = 0;
			u8 m_g = 0;
			u8 m_b = 0;
	}; // color_t

	template<bool BigEndian>
	class jkmv32b_intf_t
	{
		public:
			jkmv32b_intf_t()
			{
			} // jkmv32b_intf_t

			virtual u8 read_byte(u32 addr) { return 0; }

			u16 read_word(u32 addr)
			{
				return BigEndian ? (read_byte(addr + 1) | (u16(read_byte(addr)) << 8))
					: (read_byte(addr) | (u16(read_byte(addr + 1)) << 8));
			} // read_word

			u32 read_dword(u32 addr)
			{
				return BigEndian ? (read_word(addr) | (u32(read_word(addr + 2)) << 16))
					: (read_word(addr + 2) | (u32(read_word(addr)) << 16));
			} // read_dword

			virtual void write_byte(u32 addr, u8 data, u8 mask = ~0) {}

			void write_word(u32 addr, u16 data, u16 mask = ~0)
			{
				if (BigEndian)
				{
					write_byte(addr + 1, bitfield(data, 0, 8), bitfield(mask, 0, 8));
					write_byte(addr, bitfield(data, 8, 8), bitfield(mask, 8, 8));
				}
				else
				{
					write_byte(addr, bitfield(data, 0, 8), bitfield(mask, 0, 8));
					write_byte(addr + 1, bitfield(data, 8, 8), bitfield(mask, 8, 8));
				}
			} // write_word

			void write_dword(u32 addr, u32 data, u32 mask = ~0)
			{
				if (BigEndian)
				{
					write_word(addr, bitfield(data, 0, 16), bitfield(mask, 0, 16));
					write_word(addr + 2, bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				else
				{
					write_word(addr + 2, bitfield(data, 0, 16), bitfield(mask, 0, 16));
					write_word(addr, bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
			} // write_dword

			virtual void blitter_cycle_cb(u64 cycle) {}

	}; // class jkmv32b_intf_t

	template<bool BigEndian, typename T>
	class jkmv32b_t
	{
		private:
			static const u32 CHIP_VERSION = 1;
			
			class clip_t
			{
				public:
					clip_t()
						: m_enable(false)
						, m_left(0)
						, m_right(0)
						, m_top(0)
						, m_bottom(0)
					{
					} // clip_t

					void reset()
					{
						m_enable = false;
						m_left = 0;
						m_right = 0;
						m_top = 0;
						m_bottom = 0;
					}

					// getter, setters
					void set_enable(const bool enable)
					{
						m_enable = enable;
					} // set_enable

					bool enable() const
					{
						return m_enable;
					} // enable

					void set_left(const u16 left, const u16 mask = ~0)
					{
						m_left = (m_left & ~mask) | (left & mask);
					} // set_left

					u16 left() const
					{
						return m_left;
					} // left

					void set_right(const u16 right, const u16 mask = ~0)
					{
						m_right = (m_right & ~mask) | (right & mask);
					} // set_right

					u16 right() const
					{
						return m_right;
					} // right

					void set_top(const u16 top, const u16 mask = ~0)
					{
						m_top = (m_top & ~mask) | (top & mask);
					} // set_top

					u16 top() const
					{
						return m_top;
					} // top

					void set_bottom(const u16 bottom, const u16 mask = ~0)
					{
						m_bottom = (m_bottom & ~mask) | (bottom & mask);
					} // set_bottom

					u16 bottom() const
					{
						return m_bottom;
					} // bottom

					virtual bool check(const u16 x, const u16 y) const
					{
						return true;
					} // check

				private:
					// regisers
					bool m_enable = false;
					u16 m_left = 0;
					u16 m_right = 0;
					u16 m_top = 0;
					u16 m_bottom = 0;
			}; // class clip_t

			class inner_clip_t : public clip_t
			{
				public:
					inner_clip_t()
						: clip_t()
					{
					} // inner_clip_t

					virtual bool check(const u16 x, const u16 y) const override
					{
						return (!this->enable()) ||
							((x >= this->left() && x <= this->right()) && (y >= this->top() && y <= this->bottom()));
					} // check
			}; // class inner_clip_t

			class outer_clip_t : public clip_t
			{
				public:
					outer_clip_t()
						: clip_t()
					{
					} // inner_clip_t

					virtual bool check(const u16 x, const u16 y) const override
					{
						return (!this->enable()) ||
							((x <= this->left() && x >= this->right()) && (y <= this->top() && y >= this->bottom()));
					} // check
			}; // class outer_clip_t

			class blitter_t
			{
				private:
					class texture_t
					{
						private:
							class clut_t
							{
								public:
									clut_t(jkmv32b_t &host)
										: m_host(host)
										, m_addr(0)
										, m_format(0)
									{
									} // clut_t

									void reset()
									{
										m_addr = 0;
										m_format = 0;
									}

									u32 get_clut(const u32 pixel) const
									{
										u32 ret = 0;
										switch (m_format)
										{
											case color_format_t::C_ARGB4444:
												ret = convert_argb(color_format_t::C_ARGB4444,
														color_format_t::C_ARGB8888,
														m_host.intf().read_word(m_addr + (pixel << 1)));
												break;
											case color_format_t::C_ARGB1555:
												ret = convert_argb(color_format_t::C_ARGB1555,
														color_format_t::C_ARGB8888,
														m_host.intf().read_word(m_addr + (pixel << 1)));
												break;
											case color_format_t::C_RGB565:
												ret = convert_argb(color_format_t::C_RGB565,
														color_format_t::C_ARGB8888,
														m_host.intf().read_word(m_addr + (pixel << 1)));
												break;
											case color_format_t::C_ARGB8888:
											default:
												ret = m_host.intf().read_dword(m_addr + (pixel << 2));
												break;
										}
										m_host.add_cycle(1);
										return ret;
									}

									// getter, setters
									void set_addr(const u32 addr, const u32 mask = ~0)
									{
										m_addr = (m_addr & ~mask) | (addr & mask);
									} // set_addr

									u32 addr() const
									{
										return m_addr;
									} // addr

									void set_format(const u8 format, const u8 mask = ~0)
									{
										m_format = (m_format & ~mask) | (format & mask);
									} // set_format

									u8 format() const
									{
										return m_format;
									} // format

								private:
									// structs, classes
									jkmv32b_t &m_host;

									// registers
									u32 m_addr = 0;
									u8 m_format = 0;
							}; // class clut_t

							class tilemap_t
							{
								public:
									tilemap_t()
										: m_enable(false)
										, m_addr(0)
										, m_width(0)
										, m_height(0)
										, m_format(0)
									{
									} // tilemap_t

									void reset()
									{
										m_enable = false;
										m_addr = 0;
										m_width = 0;
										m_height = 0;
										m_format = 0;
									}

									// getter, setters
									void set_enable(const bool enable)
									{
										m_enable = enable;
									} // set_enable

									bool enable() const
									{
										return m_enable;
									} // enable

									void set_addr(const u32 addr, const u32 mask = ~0)
									{
										m_addr = (m_addr & ~mask) | (addr & mask);
									} // set_addr

									u32 addr() const
									{
										return m_addr;
									} // addr

									void set_width(const u8 width, const u8 mask = ~0)
									{
										m_width = (m_width & ~mask) | (width & mask);
									} // set_width

									u8 width() const
									{
										return m_width;
									} // width

									void set_height(const u8 height, const u8 mask = ~0)
									{
										m_height = (m_height & ~mask) | (height & mask);
									} // set_height

									u8 height() const
									{
										return m_height;
									} // height

									void set_format(const u8 format, const u8 mask = ~0)
									{
										m_format = (m_format & ~mask) | (format & mask);
									} // set_format

									u8 format() const
									{
										return m_format;
									} // format

								private:
									// registers
									bool m_enable = false;
									u32 m_addr = 0;
									u8 m_width = 0;
									u8 m_height = 0;
									u8 m_format = 0;
							}; // class tilemap_t

						public:
							texture_t(jkmv32b_t &host)
								: m_host(host)
								, m_tilemap(tilemap_t())
								, m_clut(host)
								, m_enable(false)
								, m_addr(0)
								, m_width(0)
								, m_height(0)
								, m_stride(0)
								, m_format(0)
								, m_wrap_x(false)
								, m_wrap_y(false)
								, m_flip_x(false)
								, m_flip_y(false)
							{
							} // texture_t

							void reset()
							{
								m_tilemap.reset();
								m_clut.reset();
								m_enable = false;
								m_addr = 0;
								m_width = 0;
								m_height = 0;
								m_stride = 0;
								m_format = 0;
								m_wrap_x = false;
								m_wrap_y = false;
								m_flip_x = false;
								m_flip_y = false;
							}

							u32 get_pixel(const u32 base, const u32 x, const u32 y, const u32 xstride, const u32 ystride) const
							{
								const u32 addr = m_swap_xy ? (y | (x << ystride)) : (x | (y << xstride));
								u32 ret = 0;
								switch (m_format)
								{
									case texture_format_t::CLUT1:
										ret = m_clut.get_clut(
											bitfield(m_host.intf().read_byte(base + (addr >> 3)), bitfield(addr, 0, 3)));
										break;
									case texture_format_t::CLUT2:
										ret = m_clut.get_clut(
											bitfield(m_host.intf().read_byte(base + (addr >> 2)), (bitfield(addr, 0, 2) << 1), 2));
										break;
									case texture_format_t::CLUT4:
										ret = m_clut.get_clut(
											bitfield(m_host.intf().read_byte(base + (addr >> 1)), (bitfield(addr, 0, 1) << 2), 4));
										break;
									case texture_format_t::CLUT8:
										ret = m_clut.get_clut(m_host.intf().read_byte(base + addr));
										break;
									case texture_format_t::GREY1:
										ret = pal_expand(bitfield(m_host.intf().read_byte(base + (addr >> 3)), bitfield(addr, 0, 3)), 1);
										ret = (ret << 24) | (ret << 16) | (ret << 8) | (ret << 0);
										break;
									case texture_format_t::GREY2:
										ret = pal_expand(bitfield(m_host.intf().read_byte(base + (addr >> 2)), (bitfield(addr, 0, 2) << 1), 2), 2);
										ret = (ret << 24) | (ret << 16) | (ret << 8) | (ret << 0);
										break;
									case texture_format_t::GREY4:
										ret = pal_expand(bitfield(m_host.intf().read_byte(base + (addr >> 1)), (bitfield(addr, 0, 1) << 2), 4), 4);
										ret = (ret << 24) | (ret << 16) | (ret << 8) | (ret << 0);
										break;
									case texture_format_t::GREY8:
										ret = m_host.intf().read_byte(base + addr);
										ret = (ret << 24) | (ret << 16) | (ret << 8) | (ret << 0);
										break;
									case texture_format_t::GREY4A:
									{
										const u32 pixel = m_host.intf().read_byte(base + addr);
										ret = pal_expand(bitfield(pixel, 0, 4), 4);
										ret = pal_expand(bitfield(pixel, 4, 4), 4)
											| (ret << 16) | (ret << 8) | (ret << 0);
										break;
									}
									case texture_format_t::GREY8A:
									{
										const u32 pixel = m_host.intf().read_word(base + (addr << 1));
										ret = bitfield(pixel, 0, 8);
										ret = bitfield(pixel, 8, 8)
											| (ret << 16) | (ret << 8) | (ret << 0);
										break;
									}
									case texture_format_t::CLUT4A:
									{
										const u32 pixel = m_host.intf().read_byte(base + addr);
										ret = m_clut.get_clut(bitfield(pixel, 0, 4));
										ret = pal_expand(bitfield(pixel, 4, 4), 4)
											| (ret << 16) | (ret << 8) | (ret << 0);
										break;
									}
									case texture_format_t::CLUT8A:
									{
										const u32 pixel = m_host.intf().read_word(base + (addr << 1));
										ret = m_clut.get_clut(bitfield(pixel, 0, 8));
										ret = bitfield(pixel, 8, 8)
											| (ret << 16) | (ret << 8) | (ret << 0);
										break;
									}
									case texture_format_t::ARGB4444:
										ret = convert_argb(color_format_t::C_ARGB4444,
											color_format_t::C_ARGB8888,
											m_host.intf().read_word(base + (addr << 1)));
										break;
									case texture_format_t::ARGB1555:
										ret = convert_argb(color_format_t::C_ARGB1555,
											color_format_t::C_ARGB8888,
											m_host.intf().read_word(base + (addr << 1)));
										break;
									case texture_format_t::RGB565:
										ret = convert_argb(color_format_t::C_RGB565,
											color_format_t::C_ARGB8888,
											m_host.intf().read_word(base + (addr << 1)));
										break;
									case texture_format_t::ARGB8888:
									default:
										ret = m_host.intf().read_word(base + (addr << 2));
										break;
								}
								m_host.add_cycle(1);
								return ret;
							}

							// accessors
							tilemap_t &tilemap()
							{
								return m_tilemap;
							} // tilemap

							clut_t &clut()
							{
								return m_clut;
							} // clut

							// getter, setters
							void set_enable(const bool enable)
							{
								m_enable = enable;
							} // set_enable

							bool enable() const
							{
								return m_enable;
							} // enable

							void set_addr(const u32 addr, const u32 mask = ~0)
							{
								m_addr = (m_addr & ~mask) | (addr & mask);
							} // set_addr

							u32 addr() const
							{
								return m_addr;
							} // addr

							void set_width(const u16 width, const u16 mask = ~0)
							{
								m_width = (m_width & ~mask) | (width & mask);
							} // set_width

							u16 width() const
							{
								return m_width;
							} // width

							void set_height(const u16 height, const u16 mask = ~0)
							{
								m_height = (m_height & ~mask) | (height & mask);
							} // set_height

							u16 height() const
							{
								return m_height;
							} // height

							void set_stride(const u16 stride, const u16 mask = ~0)
							{
								m_stride = (m_stride & ~mask) | (stride & mask);
							} // set_stride

							u16 stride() const
							{
								return m_stride;
							} // stride

							void set_format(const u8 format, const u8 mask = ~0)
							{
								m_format = (m_format & ~mask) | (format & mask);
							} // set_format

							u8 format() const
							{
								return m_format;
							} // format

							void set_wrap_x(const bool wrap_x)
							{
								m_wrap_x = wrap_x;
							} // set_wrap_x

							bool wrap_x() const
							{
								return m_wrap_x;
							} // wrap_x

							void set_wrap_y(const bool wrap_y)
							{
								m_wrap_y = wrap_y;
							} // set_wrap_y

							bool wrap_y() const
							{
								return m_wrap_y;
							} // wrap_y

							void set_flip_x(const bool flip_x)
							{
								m_flip_x = flip_x;
							} // set_flip_x

							bool flip_x() const
							{
								return m_flip_x;
							} // flip_x

							void set_flip_y(const bool flip_y)
							{
								m_flip_y = flip_y;
							} // set_flip_y

							bool flip_y() const
							{
								return m_flip_y;
							} // flip_y

							void set_swap_xy(const bool swap_xy)
							{
								m_swap_xy = swap_xy;
							} // set_swap_xy

							bool swap_xy() const
							{
								return m_swap_xy;
							} // swap_xy

						private:
							// structs, classes
							jkmv32b_t &m_host;
							tilemap_t m_tilemap;
							clut_t m_clut;

							// registers
							bool m_enable = false;
							u32 m_addr = 0;
							u16 m_width = 0;
							u16 m_height = 0;
							u16 m_stride = 0;
							u8 m_format = 0;
							bool m_wrap_x = false;
							bool m_wrap_y = false;
							bool m_flip_x = false;
							bool m_flip_y = false;
							bool m_swap_xy = false;
					}; // class texture_t

					class transform_t
					{
						private:
							class param_t
							{
								public:
									param_t()
										: m_start(0)
										, m_zoom(0)
										, m_tilt(0)
										, m_stretch(0)
									{
									} // param_t

									void reset()
									{
										m_start = 0;
										m_zoom = 0;
										m_tilt = 0;
										m_stretch = 0;
									}

									void set_start(const u32 start, const u32 mask = ~0)
									{
										m_start = (m_start & ~mask) | (start & mask);
									} // set_start

									u32 start() const
									{
										return m_start;
									} // start

									void set_zoom(const u32 zoom, const u32 mask = ~0)
									{
										m_zoom = (m_zoom & ~mask) | (zoom & mask);
									} // set_zoom

									u32 zoom() const
									{
										return m_zoom;
									} // zoom

									void set_tilt(const u32 tilt, const u32 mask = ~0)
									{
										m_tilt = (m_tilt & ~mask) | (tilt & mask);
									} // set_tilt

									u32 tilt() const
									{
										return m_tilt;
									} // tilt

									void set_stretch(const u32 stretch, const u32 mask = ~0)
									{
										m_stretch = (m_stretch & ~mask) | (stretch & mask);
									} // set_stretch

									u32 stretch() const
									{
										return m_stretch;
									} // stretch

								private:
									// registers
									u32 m_start = 0;
									u32 m_zoom = 0;
									u32 m_tilt = 0;
									u32 m_stretch = 0;
							}; // param_t
						public:
							transform_t()
								: m_x(param_t())
								, m_y(param_t())
							{
							} // transform_t

							void reset()
							{
								m_x.reset();
								m_y.reset();
							}

							// getters, setters
							param_t &x()
							{
								return m_x;
							} // x

							param_t &y()
							{
								return m_y;
							} // x

						private:
							// structs, classes
							param_t m_x;
							param_t m_y;
					}; // class transform_t

					class transparent_t
					{
						public:
							transparent_t()
								: m_enable(false)
								, m_color(0)
							{

							} // transparent_t

							void reset()
							{
								m_enable = false;
								m_color = 0;
							}

							// getter, setters
							void set_enable(const bool enable)
							{
								m_enable = enable;
							} // set_enable

							bool enable() const
							{
								return m_enable;
							} // enable

							void set_color(const u32 color, const u32 mask = ~0)
							{
								m_color = (m_color & ~mask) | (color & mask);
							} // set_color

							u32 color() const
							{
								return m_color;
							} // color

						private:
							bool m_enable = false;
							u32 m_color = 0;
					}; // class transparent_t

					class depth_tester_t
					{
						private:
							class buffer_t
							{
								public:
									buffer_t(jkmv32b_t & host)
										: m_host(host)
										, m_addr(0)
										, m_width(0)
										, m_height(0)
										, m_stride(0)
										, m_format(0)
									{
									} // buffer_t

									void reset()
									{
										m_addr = 0;
										m_width = 0;
										m_height = 0;
										m_stride = 0;
										m_format = 0;
									}

									u32 get_data(const u32 x, const u32 y) const
									{
										const u32 addr = (x | (y << m_stride));
										u32 ret = 0;
										switch (m_format)
										{
											case depth_stencil_format::D1:
												ret = bitfield(m_host.intf().read_byte(m_addr + (addr >> 3)), bitfield(addr, 0, 3)) << 31;
												break;
											case depth_stencil_format::D2:
												ret = bitfield(m_host.intf().read_byte(m_addr + (addr >> 2)), (bitfield(addr, 0, 2) << 1)) << 30;
												break;
											case depth_stencil_format::D4:
												ret = bitfield(m_host.intf().read_byte(m_addr + (addr >> 1)), (bitfield(addr, 0, 1) << 2)) << 28;
												break;
											case depth_stencil_format::D8:
												ret = m_host.intf().read_byte(m_addr + addr) << 24;
												break;
											case depth_stencil_format::D16:
												ret = m_host.intf().read_word(m_addr + (addr << 1)) << 16;
												break;
											case depth_stencil_format::D32:
												ret = m_host.intf().read_dword(m_addr + (addr << 2));
												break;
										}
										m_host.add_cycle(1);
										return ret;
									}

									void write_data(const u32 x, const u32 y, const u32 in)
									{
										const u32 addr = (x | (y << m_stride));
										switch (m_format)
										{
											case depth_stencil_format::D1:
												m_host.intf().write_byte(m_addr + (addr >> 3), bitfield(in, 31) << (addr & 7), 0x1 << (addr & 7));
												break;
											case depth_stencil_format::D2:
												m_host.intf().write_byte(m_addr + (addr >> 2), bitfield(in, 30, 2) << ((addr & 3) << 1), 0x3 << ((addr & 3) << 1));
												break;
											case depth_stencil_format::D4:
												m_host.intf().write_byte(m_addr + (addr >> 1), bitfield(in, 28, 4) << ((addr & 1) << 2), 0xf << ((addr & 1) << 2));
												break;
											case depth_stencil_format::D8:
												m_host.intf().write_byte(m_addr + addr, bitfield(in, 24, 8));
												break;
											case depth_stencil_format::D16:
												m_host.intf().write_dword(m_addr + (addr << 1), bitfield(in, 16, 16));
												break;
											case depth_stencil_format::D32:
												m_host.intf().write_dword(m_addr + (addr << 2), in);
												break;
										}
										m_host.add_cycle(1);
									}
	
									// getters, setters
									void set_addr(const u32 addr, const u32 mask = ~0)
									{
										m_addr = (m_addr & ~mask) | (addr & mask);
									} // set_addr

									u32 addr() const
									{
										return m_addr;
									} // addr

									void set_width(const u16 width, const u16 mask = ~0)
									{
										m_width = (m_width & ~mask) | (width & mask);
									} // set_width

									u16 width() const
									{
										return m_width;
									} // width

									void set_height(const u16 height, const u16 mask = ~0)
									{
										m_height = (m_height & ~mask) | (height & mask);
									} // set_height

									u16 height() const
									{
										return m_height;
									} // height

									void set_stride(const u16 stride, const u16 mask = ~0)
									{
										m_stride = (m_stride & ~mask) | (stride & mask);
									} // set_stride

									u16 stride() const
									{
										return m_stride;
									} // stride

									void set_format(const u8 format, const u8 mask = ~0)
									{
										m_format = (m_format & ~mask) | (format & mask);
									} // set_format

									u8 format() const
									{
										return m_format;
									} // format

								private:
									// structs, classes
									jkmv32b_t &m_host;

									u32 m_addr = 0;
									u16 m_width = 0;
									u16 m_height = 0;
									u16 m_stride = 0;
									u8 m_format = 0;
							}; // class buffer_t
						public:
							depth_tester_t(jkmv32b_t &host)
								: m_host(host)
								, m_depth_buffer(host)
								, m_stencil_buffer(host)
								, m_enable(false)
								, m_depth_ref(0)
								, m_stencil_ref(0)
								, m_stencil_ref_mask(0)
								, m_stencil_write_mask(0)
								, m_depth_test(0)
								, m_stencil_test(0)
								, m_depth_write_fail(0)
								, m_depth_write_pass(0)
								, m_stencil_write_sfail(0)
								, m_stencil_write_dpfail(0)
								, m_stencil_write_dppass(0)
							{
							} // depth_tester_t

							void reset()
							{
								m_depth_buffer.reset();
								m_stencil_buffer.reset();

								m_enable = false;
								m_depth_ref = 0;
								m_stencil_ref = 0;
								m_stencil_ref_mask = 0;
								m_stencil_write_mask = 0;
								m_depth_test = 0;
								m_stencil_test = 0;
								m_depth_write_fail = 0;
								m_depth_write_pass = 0;
								m_stencil_write_sfail = 0;
								m_stencil_write_dpfail = 0;
								m_stencil_write_dppass = 0;
							}

							// accessors
							buffer_t &depth_buffer()
							{
								return m_depth_buffer;
							} // depth_buffer

							buffer_t &stencil_buffer()
							{
								return m_stencil_buffer;
							} // stencil_buffer

							// getter, setters
							void set_enable(const bool enable)
							{
								m_enable = enable;
							} // set_enable

							bool enable() const
							{
								return m_enable;
							} // enable

							void set_depth_ref(const u32 depth_ref, const u32 mask = ~0)
							{
								m_depth_ref = (m_depth_ref & ~mask) | (depth_ref & mask);
							} // set_depth_ref

							u32 depth_ref() const
							{
								return m_depth_ref;
							} // depth_ref

							void set_stencil_ref(const u32 stencil_ref, const u32 mask = ~0)
							{
								m_stencil_ref = (m_stencil_ref & ~mask) | (stencil_ref & mask);
							} // set_stencil_ref

							u32 stencil_ref() const
							{
								return m_stencil_ref;
							} // stencil_ref

							void set_stencil_ref_mask(const u32 stencil_ref_mask, const u32 mask = ~0)
							{
								m_stencil_ref_mask = (m_stencil_ref_mask & ~mask) | (stencil_ref_mask & mask);
							} // set_stencil_ref_mask

							u32 stencil_ref_mask() const
							{
								return m_stencil_ref_mask;
							} // stencil_ref_mask

							void set_stencil_write_mask(const u32 stencil_write_mask, const u32 mask = ~0)
							{
								m_stencil_write_mask = (m_stencil_write_mask & ~mask) | (stencil_write_mask & mask);
							} // set_stencil_write_mask

							u32 stencil_write_mask() const
							{
								return m_stencil_write_mask;
							} // stencil_write_mask

							void set_depth_test(const u8 depth_test, const u8 mask = ~0)
							{
								m_depth_test = (m_depth_test & ~mask) | (depth_test & mask);
							} // set_depth_test

							u8 depth_test() const
							{
								return m_depth_test;
							} // depth_test

							void set_stencil_test(const u8 stencil_test, const u8 mask = ~0)
							{
								m_stencil_test = (m_stencil_test & ~mask) | (stencil_test & mask);
							} // set_stencil_test

							u8 stencil_test() const
							{
								return m_stencil_test;
							} // stencil_test

							void set_depth_write_fail(const u8 depth_write_fail, const u8 mask = ~0)
							{
								m_depth_write_fail = (m_depth_write_fail & ~mask) | (depth_write_fail & mask);
							} // set_depth_write_fail

							u8 depth_write_fail() const
							{
								return m_depth_write_fail;
							} // depth_write_fail

							void set_depth_write_pass(const u8 depth_write_pass, const u8 mask = ~0)
							{
								m_depth_write_pass = (m_depth_write_pass & ~mask) | (depth_write_pass & mask);
							} // set_depth_write_pass

							u8 depth_write_pass() const
							{
								return m_depth_write_pass;
							} // depth_write_pass

							void set_stencil_write_sfail(const u8 stencil_write_sfail, const u8 mask = ~0)
							{
								m_stencil_write_sfail = (m_stencil_write_sfail & ~mask) | (stencil_write_sfail & mask);
							} // set_stencil_write_sfail

							u8 stencil_write_sfail() const
							{
								return m_stencil_write_sfail;
							} // stencil_write_sfail

							void set_stencil_write_dpfail(const u8 stencil_write_dpfail, const u8 mask = ~0)
							{
								m_stencil_write_dpfail = (m_stencil_write_dpfail & ~mask) | (stencil_write_dpfail & mask);
							} // set_stencil_write_dpfail

							u8 stencil_write_dpfail() const
							{
								return m_stencil_write_dpfail;
							} // stencil_write_dpfail

							void set_stencil_write_dppass(const u8 stencil_write_dppass, const u8 mask = ~0)
							{
								m_stencil_write_dppass = (m_stencil_write_dppass & ~mask) | (stencil_write_dppass & mask);
							} // set_stencil_write_dppass

							u8 stencil_write_dppass() const
							{
								return m_stencil_write_dppass;
							} // stencil_write_dppass

						private:
							// structs, classes
							jkmv32b_t &m_host;
							buffer_t m_depth_buffer;
							buffer_t m_stencil_buffer;

							// registers
							bool m_enable = false;
							u32 m_depth_ref = 0;
							u32 m_stencil_ref = 0;
							u32 m_stencil_ref_mask = 0;
							u32 m_stencil_write_mask = 0;
							u8 m_depth_test = 0;
							u8 m_stencil_test = 0;
							u8 m_depth_write_fail = 0;
							u8 m_depth_write_pass = 0;
							u8 m_stencil_write_sfail = 0;
							u8 m_stencil_write_dpfail = 0;
							u8 m_stencil_write_dppass = 0;
					}; // class depth_tester_t

					class alpha_tester_t
					{
						public:
							alpha_tester_t()
								: m_enable(false)
								, m_ref(0)
								, m_mode(0)
							{
							} // alpha_tester_t

							void reset()
							{
								m_enable = false;
								m_ref = 0;
								m_mode = 0;
							} // reset

							void set_enable(const bool enable)
							{
								m_enable = enable;
							} // set_enable

							bool enable() const
							{
								return m_enable;
							} // enable

							void set_ref(const u8 ref, const u8 mask = ~0)
							{
								m_ref = (m_ref & ~mask) | (ref & mask);
							} // set_ref

							u8 ref() const
							{
								return m_ref;
							} // ref

							void set_mode(const u8 mode, const u8 mask = ~0)
							{
								m_mode = (m_mode & ~mask) | (mode & mask);
							} // set_mode

							u8 mode() const
							{
								return m_mode;
							} // mode

						private:
							bool m_enable = false;
							u8 m_ref = 0;
							u8 m_mode = 0;
					}; // class alpha_tester_t

					class color_fade_t
					{
						private:
							class color_fade_channel_t
							{
								public:
									color_fade_channel_t()
										: m_fade_target(0)
										, m_fade_amount(0)
										, m_fade_mode(0)
									{
									} // color_fade_channel_t

									void reset()
									{
										m_fade_target = 0;
										m_fade_amount = 0;
										m_fade_mode = 0;
									} // reset

									// getters, setters
									void set_fade_target(const u8 fade_target, const u8 mask = ~0)
									{
										m_fade_target = (m_fade_target & ~mask) | (fade_target & mask);
									} // set_fade_target

									u8 fade_target() const
									{
										return m_fade_target;
									} // fade_target

									void set_fade_amount(const u8 fade_amount, const u8 mask = ~0)
									{
										m_fade_amount = (m_fade_amount & ~mask) | (fade_amount & mask);
									} // set_fade_amount

									u8 fade_amount() const
									{
										return m_fade_amount;
									} // fade_amount

									void set_fade_mode(const u8 fade_mode, const u8 mask = ~0)
									{
										m_fade_mode = (m_fade_mode & ~mask) | (fade_mode & mask);
									} // set_fade_mode

									u8 fade_mode() const
									{
										return m_fade_mode;
									} // fade_mode

								private:
									u8 m_fade_target = 0;
									u8 m_fade_amount = 0;
									u8 m_fade_mode = 0;
							}; // class color_fade_channel_t

						public:
							color_fade_t()
								: m_a(color_fade_channel_t())
								, m_r(color_fade_channel_t())
								, m_g(color_fade_channel_t())
								, m_b(color_fade_channel_t())
								, m_enable(false)
								, m_premultiplied(false)
							{
							} // color_fade_t

							void reset()
							{
								m_a.reset();
								m_r.reset();
								m_g.reset();
								m_b.reset();
								m_enable = false;
								m_premultiplied = false;
							} // reset

							// accessors
							color_fade_channel_t &a()
							{
								return m_a;
							} // a

							color_fade_channel_t &r()
							{
								return m_r;
							} // r

							color_fade_channel_t &g()
							{
								return m_g;
							} // g

							color_fade_channel_t &b()
							{
								return m_b;
							} // b

							// getters, setters
							void set_enable(const bool enable)
							{
								m_enable = enable;
							} // set_enable;

							bool enable() const
							{
								return m_enable;
							} // enable

							void set_premultiplied(const bool premultiplied)
							{
								m_premultiplied = premultiplied;
							} // set_premultiplied;

							bool premultiplied() const
							{
								return m_premultiplied;
							} // premultiplied

						private:
							// structs, classes
							color_fade_channel_t m_a;
							color_fade_channel_t m_r;
							color_fade_channel_t m_g;
							color_fade_channel_t m_b;

							// registers
							bool m_enable = false;
							bool m_premultiplied = false;
					}; // class color_fade_t

					class alpha_blender_t
					{
						public:
							alpha_blender_t()
								: m_enable(false)
								, m_srcblend_c(0)
								, m_dstblend_c(0)
								, m_srcblend_a(0)
								, m_dstblend_a(0)
								, m_blendop_c(0)
								, m_blendop_a(0)
								, m_constcol{0, 0}
							{
							} // alpha_blender_t

							void reset()
							{
								m_enable = false;
								m_srcblend_c = 0;
								m_dstblend_c = 0;
								m_srcblend_a = 0;
								m_dstblend_a = 0;
								m_blendop_c = 0;
								m_blendop_a = 0;
								m_constcol[0] = m_constcol[1] = 0;
							}

							// getters, setters
							void set_enable(const bool enable)
							{
								m_enable = enable;
							} // set_enable

							bool enable() const
							{
								return m_enable;
							} // enable

							void set_srcblend_c(const u8 srcblend_c, const u8 mask = ~0)
							{
								m_srcblend_c = (m_srcblend_c & ~mask) | (srcblend_c & mask);
							} // set_srcblend_c

							u8 srcblend_c() const
							{
								return m_srcblend_c;
							} // srcblend_c

							void set_dstblend_c(const u8 dstblend_c, const u8 mask = ~0)
							{
								m_dstblend_c = (m_dstblend_c & ~mask) | (dstblend_c & mask);
							} // set_dstblend_c

							u8 dstblend_c() const
							{
								return m_dstblend_c;
							} // dstblend_c

							void set_srcblend_a(const u8 srcblend_a, const u8 mask = ~0)
							{
								m_srcblend_a = (m_srcblend_a & ~mask) | (srcblend_a & mask);
							} // set_srcblend_c

							u8 srcblend_a() const
							{
								return m_srcblend_a;
							} // srcblend_a

							void set_dstblend_a(const u8 dstblend_a, const u8 mask = ~0)
							{
								m_dstblend_a = (m_dstblend_a & ~mask) | (dstblend_a & mask);
							} // set_dstblend_a

							u8 dstblend_a() const
							{
								return m_dstblend_a;
							} // dstblend_a

							void set_blendop_c(const u8 blendop_c, const u8 mask = ~0)
							{
								m_blendop_c = (m_blendop_c & ~mask) | (blendop_c & mask);
							} // set_blendop_c

							u8 blendop_c() const
							{
								return m_blendop_c;
							} // blendop_c

							void set_blendop_a(const u8 blendop_a, const u8 mask = ~0)
							{
								m_blendop_a = (m_blendop_a & ~mask) | (blendop_a & mask);
							} // set_blendop_a

							u8 blendop_a() const
							{
								return m_blendop_a;
							} // blendop_a

							void set_constcol(const u8 index, const u32 constcol, const u32 mask = ~0)
							{
								m_constcol[index] = (m_constcol[index] & ~mask) | (constcol & mask);
							} // set_constcol

							u32 constcol(const u8 index) const
							{
								return m_constcol[index];
							} // constcol

						private:
							bool m_enable = false;
							u8 m_srcblend_c = 0;
							u8 m_dstblend_c = 0;
							u8 m_srcblend_a = 0;
							u8 m_dstblend_a = 0;
							u8 m_blendop_c = 0;
							u8 m_blendop_a = 0;
							std::array<u32, 2> m_constcol = {0, 0};
					}; // class alpha_blender_t
				public:
					blitter_t(jkmv32b_t &host)
						: m_host(host)
						, m_texture(host)
						, m_srctrans(transform_t())
						, m_dsttrans(transform_t())
						, m_src_inner_clip(inner_clip_t())
						, m_src_outer_clip(outer_clip_t())
						, m_dst_inner_clip(inner_clip_t())
						, m_dst_outer_clip(outer_clip_t())
						, m_transparent(transparent_t())
						, m_depth_tester(host)
						, m_alpha_tester(alpha_tester_t())
						, m_color_fade(color_fade_t())
						, m_alpha_blender(alpha_blender_t())
						, m_static_color(0)
						, m_x_size(0)
						, m_y_size(0)
						, m_busy(false)
					{
					} // blitter_t

					void reset()
					{
						m_texture.reset();
						m_srctrans.reset();
						m_dsttrans.reset();
						m_src_inner_clip.reset();
						m_src_outer_clip.reset();
						m_dst_inner_clip.reset();
						m_dst_outer_clip.reset();
						m_transparent.reset();
						m_depth_tester.reset();
						m_alpha_tester.reset();
						m_color_fade.reset();
						m_alpha_blender.reset();

						m_static_color = 0;
						m_x_size = 0;
						m_y_size = 0;
					}

					void blit();

					void write_pixel(const u16 sx, const u16 sy, const u16 dx, const u16 dy);

					// accessors
					texture_t &texture()
					{
						return m_texture;
					} // texture

					transform_t &srctrans()
					{
						return m_srctrans;
					} // srctrans

					transform_t &dsttrans()
					{
						return m_dsttrans;
					} // dsttrans

					inner_clip_t &src_inner_clip()
					{
						return m_src_inner_clip;
					} // src_inner_clip

					outer_clip_t &src_outer_clip()
					{
						return m_src_outer_clip;
					} // src_outer_clip

					inner_clip_t &dst_inner_clip()
					{
						return m_dst_inner_clip;
					} // dst_inner_clip

					outer_clip_t &dst_outer_clip()
					{
						return m_dst_outer_clip;
					} // dst_outer_clip

					transparent_t &transparent()
					{
						return m_transparent;
					} // transparent

					depth_tester_t &depth_tester()
					{
						return m_depth_tester;
					} // depth_tester

					alpha_tester_t &alpha_tester()
					{
						return m_alpha_tester;
					} // alpha_tester

					color_fade_t &color_fade()
					{
						return m_color_fade;
					} // color_fade

					alpha_blender_t &alpha_blender()
					{
						return m_alpha_blender;
					} // alpha_blender

					void set_static_color(const u32 static_color, const u32 mask = ~0)
					{
						m_static_color = (m_static_color & ~mask) | (static_color & mask);
					} // set_static_color

					u32 static_color() const
					{
						return m_static_color;
					} // static_color

					void set_x_size(const u16 x_size, const u16 mask = ~0)
					{
						m_x_size = (m_x_size & ~mask) | (x_size & mask);
					} // set_x_size

					u16 x_size() const
					{
						return m_x_size;
					} // x_size

					void set_y_size(const u16 y_size, const u16 mask = ~0)
					{
						m_y_size = (m_y_size & ~mask) | (y_size & mask);
					} // set_y_size

					u16 y_size() const
					{
						return m_y_size;
					} // y_size

					void set_busy(const bool busy)
					{
						m_busy = busy;
					} // set_busy

					bool busy() const
					{
						return m_busy;
					} // busy

				private:
					// structs, classes
					jkmv32b_t &m_host;
					texture_t m_texture;
					transform_t m_srctrans;
					transform_t m_dsttrans;
					inner_clip_t m_src_inner_clip;
					outer_clip_t m_src_outer_clip;
					inner_clip_t m_dst_inner_clip;
					outer_clip_t m_dst_outer_clip;
					transparent_t m_transparent;
					depth_tester_t m_depth_tester;
					alpha_tester_t m_alpha_tester;
					color_fade_t m_color_fade;
					alpha_blender_t m_alpha_blender;

					// registers
					u32 m_static_color = 0;
					u16 m_x_size = 0;
					u16 m_y_size = 0;

					// internal states
					bool m_busy = false;
			}; // class blitter_t

			class bitmap_t
			{
				public:
					bitmap_t(jkmv32b_t &host)
						: m_host(host)
						, m_inner_clip(inner_clip_t())
						, m_outer_clip(outer_clip_t())
						, m_enable(false)
						, m_addr(0)
						, m_width(0)
						, m_height(0)
						, m_stride(0)
						, m_format(0)
						, m_flip_x(false)
						, m_flip_y(false)
						, m_swap_xy(false)
						, m_xoffset(0)
						, m_yoffset(0)
					{
					} // bitmap_t

					void reset()
					{
						m_inner_clip.reset();
						m_outer_clip.reset();

						m_enable = false;
						m_addr = 0;
						m_width = 0;
						m_height = 0;
						m_stride = 0;
						m_format = 0;
						m_flip_x = false;
						m_flip_y = false;
						m_swap_xy = false;
						m_xoffset = 0;
						m_yoffset = 0;
					} // reset

					u32 get_pixel(const u16 x, const u16 y) const;
					void write_pixel(const u16 x, const u16 y, const u32 argb);

					// accessors
					inner_clip_t &inner_clip()
					{
						return m_inner_clip;
					} // inner_clip

					outer_clip_t &outer_clip()
					{
						return m_outer_clip;
					} // outer_clip

					void set_enable(const bool enable)
					{
						m_enable = enable;
					} // set_enable

					bool enable() const
					{
						return m_enable;
					} // enabe

					void set_addr(const u32 addr, const u32 mask = ~0)
					{
						m_addr = (m_addr & ~mask) | (addr & mask);
					} // set_addr

					u32 addr() const
					{
						return m_addr;
					} // addr

					void set_width(const u16 width, const u16 mask = ~0)
					{
						m_width = (m_width & ~mask) | (width & mask);
					} // set_width

					u16 width() const
					{
						return m_width;
					} // width

					void set_height(const u16 height, const u16 mask = ~0)
					{
						m_height = (m_height & ~mask) | (height & mask);
					} // set_height

					u16 height() const
					{
						return m_height;
					} // height

					void set_stride(const u16 stride, const u16 mask = ~0)
					{
						m_stride = (m_stride & ~mask) | (stride & mask);
					} // set_stride

					u16 stride() const
					{
						return m_stride;
					} // stride

					void set_format(const u8 format, const u8 mask = ~0)
					{
						m_format = (m_format & ~mask) | (format & mask);
					} // set_format

					u8 format() const
					{
						return m_format;
					} // format

					void set_flip_x(const bool flip_x)
					{
						m_flip_x = flip_x;
					} // set_flip_x

					bool flip_x() const
					{
						return m_flip_x;
					} // flip_x

					void set_flip_y(const bool flip_y)
					{
						m_flip_y = flip_y;
					} // set_flip_y

					bool flip_y() const
					{
						return m_flip_y;
					} // flip_y

					void set_swap_xy(const bool swap_xy)
					{
						m_swap_xy = swap_xy;
					} // set_swap_xy

					bool swap_xy() const
					{
						return m_swap_xy;
					} // swap_xy

					void set_xoffset(const u16 xoffset, const u16 mask = ~0)
					{
						m_xoffset = (m_xoffset & ~mask) | (xoffset & mask);
					} // set_xoffset

					u16 xoffset() const
					{
						return m_xoffset;
					} // xoffset

					void set_yoffset(const u16 yoffset, const u16 mask = ~0)
					{
						m_yoffset = (m_yoffset & ~mask) | (yoffset & mask);
					} // set_yoffset

					u16 yoffset() const
					{
						return m_yoffset;
					} // yoffset

				private:
					// structs, classes
					jkmv32b_t &m_host;
					inner_clip_t m_inner_clip;
					outer_clip_t m_outer_clip;

					// registers
					bool m_enable = false;
					u32 m_addr = 0;
					u16 m_width = 0;
					u16 m_height = 0;
					u16 m_stride = 0;
					u8 m_format = 0;
					bool m_flip_x = false;
					bool m_flip_y = false;
					bool m_swap_xy = false;
					u16 m_xoffset = 0;
					u16 m_yoffset = 0;
			}; // bitmap_t

		public:
			jkmv32b_t(jkmv32b_intf_t<BigEndian> &intf)
				: m_intf(intf)
				, m_blitter(*this)
				, m_display_buffer(*this)
				, m_draw_buffer(*this)
				, m_reg(0)
				, m_bgcolor(0)
				, m_mem_addr(0)
				, m_cycle(0)
			{
			} // jkmv32b_t

			// host interface
			u32 host_r(const u8 addr);
			void host_w(const u8 addr, const u32 data, const u32 mask);

			void reset()
			{
				m_blitter.reset();
				m_display_buffer.reset();
				m_draw_buffer.reset();
				m_reg = 0;
				m_bgcolor = 0;
				m_mem_addr = 0;
				m_cycle = 0;
			} // reset

			void add_cycle(const u64 cycle)
			{
				m_cycle += cycle;
			} // add_cycle

			u32 read_pixel(const u16 x, const u16 y) const
			{
				return m_display_buffer.get_pixel(x, y);
			} // read_pixel

			// accessors
			jkmv32b_intf_t<BigEndian> &intf()
			{
				return m_intf;
			} // intf

			blitter_t &blitter()
			{
				return m_blitter;
			} // blitter

			bitmap_t &display_buffer()
			{
				return m_display_buffer;
			} // display_buffer

			bitmap_t &draw_buffer()
			{
				return m_draw_buffer;
			} // draw_buffer

			// getters, setters
			void set_bgcolor(const u32 bgcolor, const u32 mask = ~0)
			{
				m_bgcolor = (m_bgcolor & ~mask) | (bgcolor & mask);
			} // set_bgcolor

			u32 bgcolor() const
			{
				return m_bgcolor;
			} // bgcolor

		private:
			// register access handler
			u32 reg_r(const u8 addr, const u8 reg);
			void reg_w(const u8 addr, const u8 reg, const u32 data, const u32 mask = ~0);

			// structs, classes
			jkmv32b_intf_t<BigEndian> &m_intf;
			blitter_t m_blitter;
			bitmap_t m_display_buffer;
			bitmap_t m_draw_buffer;

			// registers
			u8 m_reg = 0;
			u32 m_bgcolor = 0;

			// external memory access
			u16 m_mem_addr = 0;

			// internal states
			u64 m_cycle = 0;
	}; // class jkmv32b_t
} // namespace jkmv32b

#endif // JKMV32B_HPP
