/*

==============================================================================

    JKMV32B
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMV32B

Fantasy video hardware

Features:
- Blitter with:
	- Extended affine transformation
	- Depth/Stencil test
	- Alpha test
	- Color fade
	- Alpha blending
	- Clipping
- Configurable framebuffers with optional clipping
- Max 4 GByte VRAM

*/

#include "jkmv32b.hpp"

namespace jkmv32b
{
	/*
		host interface

		8 bit:
		Address Description
		00      Register select
		04      Register data bit 0-7
		05      Register data bit 8-15
		06      Register data bit 16-23
		07      Register data bit 24-31

		16 bit:
		Address Description
		00      Register select
		04      Register data LSB
		06      Register data MSB

		32 bit:
		Address Description
		00      Register select
		04      Register data

		Register map

		Register Bit                                     Description
		         1111 1111 1111 1111 0000 0000 0000 0000
		         fedc ba98 7654 3210 fedc ba98 7654 3210 
		Memory access:
		00       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx VRAM address
		01       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx VRAM data
		Blitter:
		02       x--- ---- ---- ---- ---- ---- ---- ---- Blitter busy
		         ---- ---- ---- ---- ---- ---- ---- ---x Blitter execute
		03       x--- ---- ---- ---- ---- ---- ---- ---- Texture enable
		         -x-- ---- ---- ---- ---- ---- ---- ---- Transparent(1)/Opaque(0)
		         --x- ---- ---- ---- ---- ---- ---- ---- Depth/Stencil test enable
		         ---x ---- ---- ---- ---- ---- ---- ---- Alpha test enable
		         ---- x--- ---- ---- ---- ---- ---- ---- Color fade enable
		         ---- -x-- ---- ---- ---- ---- ---- ---- Alpha blend enable
		         ---- --x- ---- ---- ---- ---- ---- ---- Premultiplied Alpha
		         ---- ---- x--- ---- ---- ---- ---- ---- Source inner clip enable
		         ---- ---- -x-- ---- ---- ---- ---- ---- Source outer clip enable
		         ---- ---- --x- ---- ---- ---- ---- ---- Target inner clip enable
		         ---- ---- ---x ---- ---- ---- ---- ---- Target outer clip enable
		Texture:
		04       xxxx ---- ---- ---- ---- ---- ---- ---- Texture width (2^x)
		         ---- xxxx ---- ---- ---- ---- ---- ---- Texture height (2^x)
		         ---- ---- xxxx ---- ---- ---- ---- ---- Texture stride (2^x)
		         ---- ---- ---- xxxx ---- ---- ---- ---- Texture format
		         ---- ---- ---- ---- x--- ---- ---- ---- Wraparound X
		         ---- ---- ---- ---- -x-- ---- ---- ---- Wraparound Y
		         ---- ---- ---- ---- --x- ---- ---- ---- Flip X
		         ---- ---- ---- ---- ---x ---- ---- ---- Flip Y
		         ---- ---- ---- ---- ---- x--- ---- ---- Swap X,Y
		         ---- ---- ---- ---- ---- --xx ---- ---- CLUT format
		         ---- ---- ---- ---- ---- ---- xx-- ---- Tile width (2^(2+x))
		         ---- ---- ---- ---- ---- ---- --xx ---- Tile height (2^(2+x))
		         ---- ---- ---- ---- ---- ---- ---- x--- Tilemap enable
		         ---- ---- ---- ---- ---- ---- ---- -xxx Tilemap format
		05       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Texture address
		06       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx CLUT address
		07       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Tilemap address
		08       aaaa aaaa rrrr rrrr gggg gggg bbbb bbbb Static color
		Transformation:
		09       xxxx xxxx xxxx xxxx ---- ---- ---- ---- X draw size
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Y draw size
		10       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Source X start
		11       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Source Y start
		12       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Source X zoom
		13       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Source Y zoom
		14       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Source X tilt
		15       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Source Y tilt
		16       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Source X stretch
		17       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Source Y stretch
		18       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Target X start
		19       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Target Y start
		1a       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Target X zoom
		1b       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Target Y zoom
		1c       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Target X tilt
		1d       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Target Y tilt
		1e       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Target X stretch
		1f       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Target Y stretch
		Clip:
		20       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Source inner clip left
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Source inner clip right
		21       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Source inner clip top
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Source inner clip bottom
		22       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Source outer clip left
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Source outer clip right
		23       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Source outer clip top
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Source outer clip bottom
		24       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Target inner clip left
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Target inner clip right
		25       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Target inner clip top
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Target inner clip bottom
		26       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Target outer clip left
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Target outer clip right
		27       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Target outer clip top
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Target outer clip bottom
		Transparent:
		28       aaaa aaaa rrrr rrrr gggg gggg bbbb bbbb Transparent color
		Depth/Stencil test:
		29       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Depth reference
		2a       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Stencil reference
		2b       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Stencil test mask
		2c       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Stencil write mask
		2d       xxxx xxxx ---- ---- ---- ---- ---- ---- Depth test mode
		         ---- ---- xxxx xxxx ---- ---- ---- ---- Stencil test mode
		         ---- ---- ---- ---- xxxx xxxx ---- ---- Depth write mode (if fail)
		         ---- ---- ---- ---- ---- ---- xxxx xxxx Depth write mode (if pass)
		2e       xxxx xxxx ---- ---- ---- ---- ---- ---- Stencil write mode (Stencil fail)
		         ---- ---- xxxx xxxx ---- ---- ---- ---- Stencil write mode (Stencil pass, Depth fail)
		         ---- ---- ---- ---- xxxx xxxx ---- ---- Stencil write mode (Stencil pass, Depth pass)
		Alpha test:
		2f       xxxx xxxx ---- ---- ---- ---- ---- ---- Alpha test reference
		         ---- ---- xxxx xxxx ---- ---- ---- ---- Alpha test mode
		Color fade:
		30       aaaa aaaa rrrr rrrr gggg gggg bbbb bbbb Color fade target
		31       aaaa aaaa rrrr rrrr gggg gggg bbbb bbbb Color fade amount
		32       aaaa aaaa rrrr rrrr gggg gggg bbbb bbbb Color fade mode
		Alpha blend:
		33       xxxx xxxx ---- ---- ---- ---- ---- ---- Source blend mode (Color)
		         ---- ---- xxxx xxxx ---- ---- ---- ---- Source blend mode (Alpha)
		         ---- ---- ---- ---- xxxx xxxx ---- ---- Target blend mode (Color)
		         ---- ---- ---- ---- ---- ---- xxxx xxxx Target blend mode (Alpha)
		34       aaaa aaaa rrrr rrrr gggg gggg bbbb bbbb Constant color A
		35       aaaa aaaa rrrr rrrr gggg gggg bbbb bbbb Constant color B
		36       xxxx xxxx ---- ---- ---- ---- ---- ---- Blend operation (Color)
		         ---- ---- xxxx xxxx ---- ---- ---- ---- Blend operation (Alpha)
		Draw buffer:
		40       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Draw buffer address
		41       xxxx ---- ---- ---- ---- ---- ---- ---- Draw buffer width (2^x)
		         ---- xxxx ---- ---- ---- ---- ---- ---- Draw buffer height (2^x)
		         ---- ---- xxxx ---- ---- ---- ---- ---- Draw buffer stride (2^x)
		         ---- ---- ---- xx-- ---- ---- ---- ---- Draw buffer format
		         ---- ---- ---- --x- ---- ---- ---- ---- Draw buffer flip X
		         ---- ---- ---- ---x ---- ---- ---- ---- Draw buffer flip Y
		         ---- ---- ---- ---- x--- ---- ---- ---- Draw buffer swap X,Y
		         ---- ---- ---- ---- --x- ---- ---- ---- Display buffer inner clip enable
		         ---- ---- ---- ---- ---x ---- ---- ---- Display buffer outer clip enable
		         ---- ---- ---- ---- ---- ---- ---- ---x Draw buffer enable
		42       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Draw buffer X offset
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Draw buffer Y offset
		44       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Display buffer inner clip left
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Display buffer inner clip right
		45       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Display buffer inner clip top
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Display buffer inner clip bottom
		46       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Display buffer inner clip left
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Display buffer inner clip right
		47       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Display buffer inner clip top
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Display buffer inner clip bottom
		Display buffer:
		48       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Display buffer address
		49       xxxx ---- ---- ---- ---- ---- ---- ---- Display buffer width (2^x)
		         ---- xxxx ---- ---- ---- ---- ---- ---- Display buffer height (2^x)
		         ---- ---- xxxx ---- ---- ---- ---- ---- Display buffer stride (2^x)
		         ---- ---- ---- xx-- ---- ---- ---- ---- Display buffer format
		         ---- ---- ---- --x- ---- ---- ---- ---- Display buffer flip X
		         ---- ---- ---- ---x ---- ---- ---- ---- Display buffer flip Y
		         ---- ---- ---- ---- x--- ---- ---- ---- Display buffer swap X,Y
		         ---- ---- ---- ---- --x- ---- ---- ---- Display buffer inner clip enable
		         ---- ---- ---- ---- ---x ---- ---- ---- Display buffer outer clip enable
		         ---- ---- ---- ---- ---- ---- ---- ---x Display buffer enable
		4a       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Display buffer X offset
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Display buffer Y offset
		4c       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Display buffer inner clip left
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Display buffer inner clip right
		4d       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Display buffer inner clip top
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Display buffer inner clip bottom
		4e       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Display buffer inner clip left
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Display buffer inner clip right
		4f       xxxx xxxx xxxx xxxx ---- ---- ---- ---- Display buffer inner clip top
		         ---- ---- ---- ---- xxxx xxxx xxxx xxxx Display buffer inner clip bottom
		Depth/Stencil buffer:
		50       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Depth buffer address
		51       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Stencil buffer address
		52       xxxx ---- ---- ---- ---- ---- ---- ---- Depth buffer width (2^x)
		         ---- xxxx ---- ---- ---- ---- ---- ---- Depth buffer height (2^x)
		         ---- ---- xxxx ---- ---- ---- ---- ---- Depth buffer stride (2^x)
		         ---- ---- ---- xxx- ---- ---- ---- ---- Depth buffer format
		         ---- ---- ---- ---- xxxx ---- ---- ---- Stencil buffer width (2^x)
		         ---- ---- ---- ---- ---- xxxx ---- ---- Stencil buffer height (2^x)
		         ---- ---- ---- ---- ---- ---- xxxx ---- Stencil buffer stride (2^x)
		         ---- ---- ---- ---- ---- ---- ---- xxx- Stencil buffer format
		Background color:
		53       aaaa aaaa rrrr rrrr gggg gggg bbbb bbbb Background color
		Chip version:
		58       xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Version (Read only)


		Texture format:
		0000 1bpp CLUT
		0001 2bpp CLUT
		0010 4bpp CLUT
		0011 8bpp CLUT
		0100 1bit Greyscale
		0101 2bit Greyscale
		0110 4bit Greyscale
		0111 8bit Greyscale
		1000 4bit Greyscale + 4bit Alpha
		1001 8bit Greyscale + 8bit Alpha
		1010 4bpp CLUT + 4bit Alpha
		1011 8bpp CLUT + 8bit Alpha
		1100 ARGB 4444
		1101 ARGB 1555
		1110 RGB 565
		1111 ARGB 8888

		CLUT/Display/Draw buffer format:
		00 ARGB 4444
		01 ARGB 1555
		10 RGB 565
		11 ARGB 8888

		Depth/Stencil buffer format:
		000 1bit
		001 2bit
		010 4bit
		011 8bit
		100 16bit
		101 32bit

		Tilemap format:
		Format Bit                                     Description
		       1111 1111 1111 1111 0000 0000 0000 0000
		       fedc ba98 7654 3210 fedc ba98 7654 3210 
		000    x--- ---- ---- ---- ---- ---- ---- ---- Flip X
		       -x-- ---- ---- ---- ---- ---- ---- ---- Flip Y
			   --xx xxxx xxxx ---- ---- ---- ---- ---- Color
			   ---- ---- ---- xxxx xxxx xxxx xxxx xxxx Tile index (0 = disable)
		001    xxxx xxxx xxxx ---- ---- ---- ---- ---- Color
		       ---- ---- ---- xxxx xxxx xxxx xxxx xxxx Tile index (0 = disable)
		010    x--- ---- ---- ---- ---- ---- ---- ---- Flip X
		       -x-- ---- ---- ---- ---- ---- ---- ---- Flip Y
			   --xx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Tile index (0 = disable)
		011    xxxx xxxx xxxx xxxx xxxx xxxx xxxx xxxx Tile index (0 = disable)
		100                        x--- ---- ---- ---- Flip X
		                           -x-- ---- ---- ---- Flip Y
		                           --xx ---- ---- ---- Color
		                           ---- xxxx xxxx xxxx Tile index (0 = disable)
		101                        xxxx ---- ---- ---- Color
		                           ---- xxxx xxxx xxxx Tile index (0 = disable)
		110                        x--- ---- ---- ---- Flip X
		                           -x-- ---- ---- ---- Flip Y
								   --xx xxxx xxxx xxxx Tile index (0 = disable)
		111                        xxxx xxxx xxxx xxxx Tile index (0 = disable)

		Depth/Stencil/Alpha test mode:
		1--- ---- Invert source
		-1-- ---- Invert target
		--1- ---- Swap source, target
		---1 ---- Invert result
		---- 0000 Always (1)
		---- 0001 Equals (S = T)
		---- 0010 Greater than (S > T)
		---- 0011 Less than (S < T)
		---- 0100 ANDed zero (S & T == 0)
		---- 0101 ANDed equals (S & T == S)
		---- 0110 ORed zero (S | T == 0)
		---- 0111 ORed equals (S | T == S)
		---- 1000 XORed zero (S ^ T == 0)
		---- 1001 XORed equals (S ^ T == S)

		Depth/Stencil write mode:
		1--- ---- Invert source
		-1-- ---- Invert target
		--1- ---- Swap source, target
		---1 ---- Invert result
		---- 0000 Keep (T)
		---- 0001 Zero (0)
		---- 0010 Min (min(S, T))
		---- 0011 Max (max(S, T))
		---- 0100 Increment (T + 1)
		---- 0101 Increment with wrap (T + 1)
		---- 0110 Decrement (T - 1)
		---- 0111 Decrement with wrap (T - 1)
		---- 1000 Add (S + T)
		---- 1001 Subtract (S - T)
		---- 1010 AND (S & T)
		---- 1011 OR (S | T)
		---- 1100 XOR (S ^ T)
		---- 1101 Average ((S + T) / 2)

		Color fade mode:
		1--- ---- Invert source
		-1-- ---- Invert target
		--1- ---- Swap source, target
		---1 ---- Invert result
		---- 0000 Static (A)
		---- 0001 Interpolate (((S * (256 - A)) + (T * A)) / 256)
		---- 0010 Add (S + T)
		---- 0011 Subtract (S - T)
		---- 0100 Scaled Add (S + ((T * A) / 256))
		---- 0101 Scaled Subtract (S - ((T * A) / 256))
		---- 0110 Multiply ((S * T) / 256)
		---- 0111 2x Multiply ((S * (T * 2)) / 256)
		---- 1000 Scaled Multiply ((S * ((T * A) / 256)) / 256)
		---- 1001 Scaled Multiply ((S * ((T * A) / 128)) / 256)
		---- 1010 Min (min(S, T))
		---- 1011 Max (max(S, T))
		---- 1100 AND (S & T)
		---- 1101 OR (S | T)
		---- 1110 XOR (S ^ T)
		---- 1111 Average ((S + T) / 2)

		Blend mode:
		1--- ---- Invert source
		-1-- ---- Invert target
		--1- ---- Swap source, target and Select constant color
		---1 ---- Invert result
		---- 0000 Zero (0)
		---- 0001 Source color (sA, sR, sG, sB)
		---- 0010 Source alpha (sA, sA, sA, sA)
		---- 0011 Constant color (cA, cR, cG, cB)
		---- 0100 Constant alpha (cA, cA, cA, cA)
		---- 0101 Source alpha saturated (A = min(sA, 1-tA); 1, A, A, A)

		Blend operation:
		1--- ---- Invert source
		-1-- ---- Invert target
		--1- ---- Swap source, target
		---1 ---- Invert result
		---- 0000 Replace (S)
		---- 0001 Zero (0)
		---- 0010 Add (S + T)
		---- 0011 Subtract (S - T)
		---- 0100 Min (min(S, T))
		---- 0101 Max (max(S, T))
		---- 0110 Multiply ((S * T) / 256)
		---- 0111 Average ((S + T) / 2)
		---- 1000 AND (S & T)
		---- 1001 OR (S | T)
		---- 1010 XOR (S ^ T)
	*/

	// Access
	template<bool BigEndian, typename T>
	void jkmv32b_t<BigEndian, T>::reg_w(const u8 addr, const u8 reg, const u32 data, const u32 mask)
	{
		const bool mmsb = bitfield(mask, 24, 8);
		const bool mlsb = bitfield(mask, 16, 8);
		const bool lmsb = bitfield(mask, 8, 8);
		const bool llsb = bitfield(mask, 0, 8);
		const bool msb = bitfield(mask, 16, 16);
		const bool lsb = bitfield(mask, 0, 16);
		switch (reg)
		{
			case 0x00:
				m_mem_addr = (m_mem_addr & ~mask) | (data & mask);
				break;
			case 0x01:
				intf().write_dword(m_mem_addr, data, mask);
				switch (sizeof(T))
				{
					case 1:
						if ((addr & 3) == 3)
						{
							m_mem_addr++;
						}
						break;
					case 2:
						if ((addr & 1) == 1)
						{
							m_mem_addr++;
						}
						break;
					case 4:
						m_mem_addr++;
						break;
				}
				break;
			case 0x02:
				if (llsb)
				{
					if (bitfield(data, 0) && !blitter().busy())
					{
						m_cycle = 0;
						blitter().blit();
						intf().blitter_cycle_cb(m_cycle);
					}
				}
				break;
			case 0x03:
				if (mmsb)
				{
					blitter().texture().set_enable(bitfield(data, 31));
					blitter().transparent().set_enable(bitfield(data, 30));
					blitter().depth_tester().set_enable(bitfield(data, 29));
					blitter().alpha_tester().set_enable(bitfield(data, 28));
					blitter().color_fade().set_enable(bitfield(data, 27));
					blitter().alpha_blender().set_enable(bitfield(data, 26));
					blitter().color_fade().set_premultiplied(bitfield(data, 25));
				}
				if (mlsb)
				{
					blitter().src_inner_clip().set_enable(bitfield(data, 23));
					blitter().src_outer_clip().set_enable(bitfield(data, 22));
					blitter().dst_inner_clip().set_enable(bitfield(data, 21));
					blitter().dst_outer_clip().set_enable(bitfield(data, 20));
				}
				break;
			case 0x04:
				if (mmsb)
				{
					blitter().texture().set_width(bitfield(data, 28, 4), bitfield(mask, 28, 4));
					blitter().texture().set_height(bitfield(data, 24, 4), bitfield(mask, 24, 4));
				}
				if (mlsb)
				{
					blitter().texture().set_stride(bitfield(data, 20, 4), bitfield(mask, 20, 4));
					blitter().texture().set_format(bitfield(data, 16, 4), bitfield(mask, 16, 4));
				}
				if (lmsb)
				{
					blitter().texture().set_wrap_x(bitfield(data, 15));
					blitter().texture().set_wrap_y(bitfield(data, 14));
					blitter().texture().set_flip_x(bitfield(data, 13));
					blitter().texture().set_flip_y(bitfield(data, 12));
					blitter().texture().set_swap_xy(bitfield(data, 11));
					blitter().texture().clut().set_format(bitfield(data, 8, 2), bitfield(mask, 8, 2));
				}
				if (llsb)
				{
					blitter().texture().tilemap().set_width(bitfield(data, 6, 2), bitfield(mask, 6, 2));
					blitter().texture().tilemap().set_height(bitfield(data, 4, 2), bitfield(mask, 4, 2));
					blitter().texture().tilemap().set_enable(bitfield(data, 3));
					blitter().texture().tilemap().set_format(bitfield(data, 0, 3), bitfield(mask, 0, 3));
				}
				break;
			case 0x05:
				blitter().texture().set_addr(data, mask);
				break;
			case 0x06:
				blitter().texture().clut().set_addr(data, mask);
				break;
			case 0x07:
				blitter().texture().tilemap().set_addr(data, mask);
				break;
			case 0x08:
				blitter().set_static_color(data, mask);
				break;
			case 0x09:
				if (msb)
				{
					blitter().set_x_size(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					blitter().set_y_size(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x10:
				blitter().srctrans().x().set_start(data, mask);
				break;
			case 0x11:
				blitter().srctrans().y().set_start(data, mask);
				break;
			case 0x12:
				blitter().srctrans().x().set_zoom(data, mask);
				break;
			case 0x13:
				blitter().srctrans().y().set_zoom(data, mask);
				break;
			case 0x14:
				blitter().srctrans().x().set_tilt(data, mask);
				break;
			case 0x15:
				blitter().srctrans().y().set_tilt(data, mask);
				break;
			case 0x16:
				blitter().srctrans().x().set_stretch(data, mask);
				break;
			case 0x17:
				blitter().srctrans().y().set_stretch(data, mask);
				break;
			case 0x18:
				blitter().dsttrans().x().set_start(data, mask);
				break;
			case 0x19:
				blitter().dsttrans().y().set_start(data, mask);
				break;
			case 0x1a:
				blitter().dsttrans().x().set_zoom(data, mask);
				break;
			case 0x1b:
				blitter().dsttrans().y().set_zoom(data, mask);
				break;
			case 0x1c:
				blitter().dsttrans().x().set_tilt(data, mask);
				break;
			case 0x1d:
				blitter().dsttrans().y().set_tilt(data, mask);
				break;
			case 0x1e:
				blitter().dsttrans().x().set_stretch(data, mask);
				break;
			case 0x1f:
				blitter().dsttrans().y().set_stretch(data, mask);
				break;
			case 0x20:
				if (msb)
				{
					blitter().src_inner_clip().set_left(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					blitter().src_inner_clip().set_right(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x21:
				if (msb)
				{
					blitter().src_inner_clip().set_top(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					blitter().src_inner_clip().set_bottom(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x22:
				if (msb)
				{
					blitter().src_outer_clip().set_left(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					blitter().src_outer_clip().set_right(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x23:
				if (msb)
				{
					blitter().src_outer_clip().set_top(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					blitter().src_outer_clip().set_bottom(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x24:
				if (msb)
				{
					blitter().dst_inner_clip().set_left(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					blitter().dst_inner_clip().set_right(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x25:
				if (msb)
				{
					blitter().dst_inner_clip().set_top(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					blitter().dst_inner_clip().set_bottom(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x26:
				if (msb)
				{
					blitter().dst_outer_clip().set_left(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					blitter().dst_outer_clip().set_right(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x27:
				if (msb)
				{
					blitter().dst_outer_clip().set_top(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					blitter().dst_outer_clip().set_bottom(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x28:
				blitter().transparent().set_color(data, mask);
				break;
			case 0x29:
				blitter().depth_tester().set_depth_ref(data, mask);
				break;
			case 0x2a:
				blitter().depth_tester().set_stencil_ref(data, mask);
				break;
			case 0x2b:
				blitter().depth_tester().set_stencil_ref_mask(data, mask);
				break;
			case 0x2c:
				blitter().depth_tester().set_stencil_write_mask(data, mask);
				break;
			case 0x2d:
				if (mmsb)
				{
					blitter().depth_tester().set_depth_test(bitfield(data, 24, 8), bitfield(mask, 24, 8));
				}
				if (mlsb)
				{
					blitter().depth_tester().set_stencil_test(bitfield(data, 16, 8), bitfield(mask, 16, 8));
				}
				if (lmsb)
				{
					blitter().depth_tester().set_depth_write_fail(bitfield(data, 8, 8), bitfield(mask, 8, 8));
				}
				if (llsb)
				{
					blitter().depth_tester().set_depth_write_pass(bitfield(data, 0, 8), bitfield(mask, 0, 8));
				}
				break;
			case 0x2e:
				if (mmsb)
				{
					blitter().depth_tester().set_stencil_write_sfail(bitfield(data, 24, 8), bitfield(mask, 24, 8));
				}
				if (mlsb)
				{
					blitter().depth_tester().set_stencil_write_dpfail(bitfield(data, 16, 8), bitfield(mask, 16, 8));
				}
				if (lmsb)
				{
					blitter().depth_tester().set_stencil_write_dppass(bitfield(data, 8, 8), bitfield(mask, 8, 8));
				}
				break;
			case 0x2f:
				if (mmsb)
				{
					blitter().alpha_tester().set_ref(bitfield(data, 24, 8), bitfield(mask, 24, 8));
				}
				if (mlsb)
				{
					blitter().alpha_tester().set_mode(bitfield(data, 16, 8), bitfield(mask, 16, 8));
				}
				break;
			case 0x30:
				if (mmsb)
				{
					blitter().color_fade().a().set_fade_target(bitfield(data, 24, 8), bitfield(mask, 24, 8));
				}
				if (mlsb)
				{
					blitter().color_fade().r().set_fade_target(bitfield(data, 16, 8), bitfield(mask, 16, 8));
				}
				if (lmsb)
				{
					blitter().color_fade().g().set_fade_target(bitfield(data, 8, 8), bitfield(mask, 8, 8));
				}
				if (llsb)
				{
					blitter().color_fade().b().set_fade_target(bitfield(data, 0, 8), bitfield(mask, 0, 8));
				}
				break;
			case 0x31:
				if (mmsb)
				{
					blitter().color_fade().a().set_fade_amount(bitfield(data, 24, 8), bitfield(mask, 24, 8));
				}
				if (mlsb)
				{
					blitter().color_fade().r().set_fade_amount(bitfield(data, 16, 8), bitfield(mask, 16, 8));
				}
				if (lmsb)
				{
					blitter().color_fade().g().set_fade_amount(bitfield(data, 8, 8), bitfield(mask, 8, 8));
				}
				if (llsb)
				{
					blitter().color_fade().b().set_fade_amount(bitfield(data, 0, 8), bitfield(mask, 0, 8));
				}
				break;
			case 0x32:
				if (mmsb)
				{
					blitter().color_fade().a().set_fade_mode(bitfield(data, 24, 8), bitfield(mask, 24, 8));
				}
				if (mlsb)
				{
					blitter().color_fade().r().set_fade_mode(bitfield(data, 16, 8), bitfield(mask, 16, 8));
				}
				if (lmsb)
				{
					blitter().color_fade().g().set_fade_mode(bitfield(data, 8, 8), bitfield(mask, 8, 8));
				}
				if (llsb)
				{
					blitter().color_fade().b().set_fade_mode(bitfield(data, 0, 8), bitfield(mask, 0, 8));
				}
				break;
			case 0x33:
				if (mmsb)
				{
					blitter().alpha_blender().set_srcblend_c(bitfield(data, 24, 8), bitfield(mask, 24, 8));
				}
				if (mlsb)
				{
					blitter().alpha_blender().set_srcblend_a(bitfield(data, 16, 8), bitfield(mask, 16, 8));
				}
				if (lmsb)
				{
					blitter().alpha_blender().set_dstblend_c(bitfield(data, 8, 8), bitfield(mask, 8, 8));
				}
				if (llsb)
				{
					blitter().alpha_blender().set_dstblend_a(bitfield(data, 0, 8), bitfield(mask, 0, 8));
				}
				break;
			case 0x34:
			case 0x35:
				blitter().alpha_blender().set_constcol(reg - 0x34, data, mask);
				break;
			case 0x36:
				if (mmsb)
				{
					blitter().alpha_blender().set_blendop_c(bitfield(data, 24, 8), bitfield(mask, 24, 8));
				}
				if (mlsb)
				{
					blitter().alpha_blender().set_blendop_a(bitfield(data, 16, 8), bitfield(mask, 16, 8));
				}
				break;
			case 0x40:
				draw_buffer().set_addr(data, mask);
				break;
			case 0x41:
				if (mmsb)
				{
					draw_buffer().set_width(bitfield(data, 28, 4), bitfield(mask, 28, 4));
					draw_buffer().set_height(bitfield(data, 24, 4), bitfield(mask, 24, 4));
				}
				if (mlsb)
				{
					draw_buffer().set_stride(bitfield(data, 20, 4), bitfield(mask, 20, 4));
					draw_buffer().set_format(bitfield(data, 18, 2), bitfield(mask, 18, 2));
					draw_buffer().set_flip_x(bitfield(data, 17));
					draw_buffer().set_flip_y(bitfield(data, 16));
				}
				if (lmsb)
				{
					draw_buffer().set_swap_xy(bitfield(data, 15));
				}
				if (llsb)
				{
					draw_buffer().set_enable(bitfield(data, 0));
				}
				break;
			case 0x42:
				if (msb)
				{
					draw_buffer().set_xoffset(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					draw_buffer().set_yoffset(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x44:
				if (msb)
				{
					draw_buffer().inner_clip().set_left(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					draw_buffer().inner_clip().set_right(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x45:
				if (msb)
				{
					draw_buffer().inner_clip().set_top(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					draw_buffer().inner_clip().set_bottom(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x46:
				if (msb)
				{
					draw_buffer().outer_clip().set_left(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					draw_buffer().outer_clip().set_right(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x47:
				if (msb)
				{
					draw_buffer().outer_clip().set_top(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					draw_buffer().outer_clip().set_bottom(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x48:
				display_buffer().set_addr(data, mask);
				break;
			case 0x49:
				if (mmsb)
				{
					display_buffer().set_width(bitfield(data, 28, 4), bitfield(mask, 28, 4));
					display_buffer().set_height(bitfield(data, 24, 4), bitfield(mask, 24, 4));
				}
				if (mlsb)
				{
					display_buffer().set_stride(bitfield(data, 20, 4), bitfield(mask, 20, 4));
					display_buffer().set_format(bitfield(data, 18, 2), bitfield(mask, 18, 2));
					display_buffer().set_flip_x(bitfield(data, 17));
					display_buffer().set_flip_y(bitfield(data, 16));
				}
				if (lmsb)
				{
					display_buffer().set_swap_xy(bitfield(data, 15));
				}
				if (llsb)
				{
					display_buffer().set_enable(bitfield(data, 0));
				}
				break;
			case 0x4a:
				if (msb)
				{
					display_buffer().set_xoffset(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					display_buffer().set_yoffset(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x4c:
				if (msb)
				{
					display_buffer().inner_clip().set_left(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					display_buffer().inner_clip().set_right(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x4d:
				if (msb)
				{
					display_buffer().inner_clip().set_top(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					display_buffer().inner_clip().set_bottom(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x4e:
				if (msb)
				{
					display_buffer().outer_clip().set_left(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					display_buffer().outer_clip().set_right(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x4f:
				if (msb)
				{
					display_buffer().outer_clip().set_top(bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				if (lsb)
				{
					display_buffer().outer_clip().set_bottom(bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				break;
			case 0x50:
				blitter().depth_tester().depth_buffer().set_addr(data, mask);
				break;
			case 0x51:
				blitter().depth_tester().stencil_buffer().set_addr(data, mask);
				break;
			case 0x52:
				if (mmsb)
				{
					blitter().depth_tester().depth_buffer().set_width(bitfield(data, 28, 4), bitfield(mask, 28, 4));
					blitter().depth_tester().depth_buffer().set_height(bitfield(data, 24, 4), bitfield(mask, 24, 4));
				}
				if (mlsb)
				{
					blitter().depth_tester().depth_buffer().set_stride(bitfield(data, 20, 4), bitfield(mask, 20, 4));
					blitter().depth_tester().depth_buffer().set_format(bitfield(data, 17, 3), bitfield(mask, 17, 3));
				}
				if (lmsb)
				{
					blitter().depth_tester().stencil_buffer().set_width(bitfield(data, 12, 4), bitfield(mask, 12, 4));
					blitter().depth_tester().stencil_buffer().set_height(bitfield(data, 8, 4), bitfield(mask, 8, 4));
				}
				if (llsb)
				{
					blitter().depth_tester().stencil_buffer().set_stride(bitfield(data, 4, 4), bitfield(mask, 4, 4));
					blitter().depth_tester().stencil_buffer().set_format(bitfield(data, 1, 3), bitfield(mask, 1, 3));
				}
				break;
			case 0x53:
				set_bgcolor(data, mask);
				break;
		}
	} // jkmv32b_t<BigEndian, T>::reg_w

	template<bool BigEndian, typename T>
	u32 jkmv32b_t<BigEndian, T>::reg_r(const u8 addr, const u8 reg)
	{
		u32 ret = 0;
		switch (reg)
		{
			case 0x00:
				ret = m_mem_addr;
				break;
			case 0x01:
				ret = intf().read_dword(m_mem_addr);
				switch (sizeof(T))
				{
					case 1:
						if ((addr & 3) == 3)
						{
							m_mem_addr++;
						}
						break;
					case 2:
						if ((addr & 1) == 1)
						{
							m_mem_addr++;
						}
						break;
					case 4:
						m_mem_addr++;
						break;
				}
				break;
			case 0x02:
				ret |= blitter().busy() ? (1 << 31) : 0;
				break;
			case 0x03:
				ret |= blitter().texture().enable() ? (1 << 31) : 0;
				ret |= blitter().transparent().enable() ? (1 << 30) : 0;
				ret |= blitter().depth_tester().enable() ? (1 << 29) : 0;
				ret |= blitter().alpha_tester().enable() ? (1 << 28) : 0;
				ret |= blitter().color_fade().enable() ? (1 << 27) : 0;
				ret |= blitter().alpha_blender().enable() ? (1 << 26) : 0;
				ret |= blitter().color_fade().premultiplied() ? (1 << 25) : 0;
				ret |= blitter().src_inner_clip().enable() ? (1 << 23) : 0;
				ret |= blitter().src_outer_clip().enable() ? (1 << 22) : 0;
				ret |= blitter().dst_inner_clip().enable() ? (1 << 21) : 0;
				ret |= blitter().dst_outer_clip().enable() ? (1 << 20) : 0;
				break;
			case 0x04:
				ret |= bitfield(blitter().texture().width(), 0, 4) << 28;
				ret |= bitfield(blitter().texture().height(), 0, 4) << 24;
				ret |= bitfield(blitter().texture().stride(), 0, 4) << 20;
				ret |= bitfield(blitter().texture().format(), 0, 4) << 16;
				ret |= blitter().texture().wrap_x() ? (1 << 15) : 0;
				ret |= blitter().texture().wrap_y() ? (1 << 14) : 0;
				ret |= blitter().texture().flip_x() ? (1 << 13) : 0;
				ret |= blitter().texture().flip_y() ? (1 << 12) : 0;
				ret |= blitter().texture().swap_xy() ? (1 << 11) : 0;
				ret |= bitfield(blitter().texture().clut().format(), 0, 2) << 8;
				ret |= bitfield(blitter().texture().tilemap().width(), 0, 2) << 6;
				ret |= bitfield(blitter().texture().tilemap().height(), 0, 2) << 4;
				ret |= blitter().texture().tilemap().enable() ? (1 << 3) : 0;
				ret |= bitfield(blitter().texture().tilemap().format(), 0, 3) << 0;
				break;
			case 0x05:
				ret |= blitter().texture().addr();
				break;
			case 0x06:
				ret |= blitter().texture().clut().addr();
				break;
			case 0x07:
				ret |= blitter().texture().tilemap().addr();
				break;
			case 0x08:
				ret |= blitter().static_color();
				break;
			case 0x09:
				ret |= blitter().x_size() << 16;
				ret |= blitter().y_size();
				break;
			case 0x10:
				ret |= blitter().srctrans().x().start();
				break;
			case 0x11:
				ret |= blitter().srctrans().y().start();
				break;
			case 0x12:
				ret |= blitter().srctrans().x().zoom();
				break;
			case 0x13:
				ret |= blitter().srctrans().y().zoom();
				break;
			case 0x14:
				ret |= blitter().srctrans().x().tilt();
				break;
			case 0x15:
				ret |= blitter().srctrans().y().tilt();
				break;
			case 0x16:
				ret |= blitter().srctrans().x().stretch();
				break;
			case 0x17:
				ret |= blitter().srctrans().y().stretch();
				break;
			case 0x18:
				ret |= blitter().dsttrans().x().start();
				break;
			case 0x19:
				ret |= blitter().dsttrans().y().start();
				break;
			case 0x1a:
				ret |= blitter().dsttrans().x().zoom();
				break;
			case 0x1b:
				ret |= blitter().dsttrans().y().zoom();
				break;
			case 0x1c:
				ret |= blitter().dsttrans().x().tilt();
				break;
			case 0x1d:
				ret |= blitter().dsttrans().y().tilt();
				break;
			case 0x1e:
				ret |= blitter().dsttrans().x().stretch();
				break;
			case 0x1f:
				ret |= blitter().dsttrans().y().stretch();
				break;
			case 0x20:
				ret |= blitter().src_inner_clip().left() << 16;
				ret |= blitter().src_inner_clip().right();
				break;
			case 0x21:
				ret |= blitter().src_inner_clip().top() << 16;
				ret |= blitter().src_inner_clip().bottom();
				break;
			case 0x22:
				ret |= blitter().src_outer_clip().left() << 16;
				ret |= blitter().src_outer_clip().right();
				break;
			case 0x23:
				ret |= blitter().src_outer_clip().top() << 16;
				ret |= blitter().src_outer_clip().bottom();
				break;
			case 0x24:
				ret |= blitter().dst_inner_clip().left() << 16;
				ret |= blitter().dst_inner_clip().right();
				break;
			case 0x25:
				ret |= blitter().dst_inner_clip().top() << 16;
				ret |= blitter().dst_inner_clip().bottom();
				break;
			case 0x26:
				ret |= blitter().dst_outer_clip().left() << 16;
				ret |= blitter().dst_outer_clip().right();
				break;
			case 0x27:
				ret |= blitter().dst_outer_clip().top() << 16;
				ret |= blitter().dst_outer_clip().bottom();
				break;
			case 0x28:
				ret |= blitter().transparent().color();
				break;
			case 0x29:
				ret |= blitter().depth_tester().depth_ref();
				break;
			case 0x2a:
				ret |= blitter().depth_tester().stencil_ref();
				break;
			case 0x2b:
				ret |= blitter().depth_tester().stencil_ref_mask();
				break;
			case 0x2c:
				ret |= blitter().depth_tester().stencil_write_mask();
				break;
			case 0x2d:
				ret |= blitter().depth_tester().depth_test() << 24;
				ret |= blitter().depth_tester().stencil_test() << 16;
				ret |= blitter().depth_tester().depth_write_fail() << 8;
				ret |= blitter().depth_tester().depth_write_pass() << 0;
				break;
			case 0x2e:
				ret |= blitter().depth_tester().stencil_write_sfail() << 24;
				ret |= blitter().depth_tester().stencil_write_dpfail() << 16;
				ret |= blitter().depth_tester().stencil_write_dppass() << 8;
				break;
			case 0x2f:
				ret |= blitter().alpha_tester().ref() << 24;
				ret |= blitter().alpha_tester().mode() << 16;
				break;
			case 0x30:
				ret |= blitter().color_fade().a().fade_target() << 24;
				ret |= blitter().color_fade().r().fade_target() << 16;
				ret |= blitter().color_fade().g().fade_target() << 8;
				ret |= blitter().color_fade().b().fade_target();
				break;
			case 0x31:
				ret |= blitter().color_fade().a().fade_amount() << 24;
				ret |= blitter().color_fade().r().fade_amount() << 16;
				ret |= blitter().color_fade().g().fade_amount() << 8;
				ret |= blitter().color_fade().b().fade_amount();
				break;
			case 0x32:
				ret |= blitter().color_fade().a().fade_mode() << 24;
				ret |= blitter().color_fade().r().fade_mode() << 16;
				ret |= blitter().color_fade().g().fade_mode() << 8;
				ret |= blitter().color_fade().b().fade_mode();
				break;
			case 0x33:
				ret |= blitter().alpha_blender().srcblend_c() << 24;
				ret |= blitter().alpha_blender().srcblend_a() << 16;
				ret |= blitter().alpha_blender().dstblend_c() << 8;
				ret |= blitter().alpha_blender().dstblend_a();
				break;
			case 0x34:
			case 0x35:
				ret |= blitter().alpha_blender().constcol(reg - 0x34);
				break;
			case 0x36:
				ret |= blitter().alpha_blender().blendop_c() << 24;
				ret |= blitter().alpha_blender().blendop_a() << 16;
				break;
			case 0x40:
				ret |= draw_buffer().addr();
				break;
			case 0x41:
				ret |= draw_buffer().width() << 28;
				ret |= draw_buffer().height() << 24;
				ret |= draw_buffer().stride() << 20;
				ret |= draw_buffer().format() << 18;
				ret |= draw_buffer().flip_x() ? (1 << 17) : 0;
				ret |= draw_buffer().flip_y() ? (1 << 16) : 0;
				ret |= draw_buffer().swap_xy() ? (1 << 15) : 0;
				ret |= draw_buffer().enable() ? (1 << 0) : 0;
				break;
			case 0x42:
				ret |= draw_buffer().xoffset() << 16;
				ret |= draw_buffer().yoffset();
				break;
			case 0x44:
				ret |= draw_buffer().inner_clip().left() << 16;
				ret |= draw_buffer().inner_clip().right();
				break;
			case 0x45:
				ret |= draw_buffer().inner_clip().top() << 16;
				ret |= draw_buffer().inner_clip().bottom();
				break;
			case 0x46:
				ret |= draw_buffer().outer_clip().left() << 16;
				ret |= draw_buffer().outer_clip().right();
				break;
			case 0x47:
				ret |= draw_buffer().outer_clip().top() << 16;
				ret |= draw_buffer().outer_clip().bottom();
				break;
			case 0x48:
				ret |= display_buffer().addr();
				break;
			case 0x49:
				ret |= display_buffer().width() << 28;
				ret |= display_buffer().height() << 24;
				ret |= display_buffer().stride() << 20;
				ret |= display_buffer().format() << 18;
				ret |= display_buffer().flip_x() ? (1 << 17) : 0;
				ret |= display_buffer().flip_y() ? (1 << 16) : 0;
				ret |= display_buffer().swap_xy() ? (1 << 15) : 0;
				ret |= display_buffer().enable() ? (1 << 0) : 0;
				break;
			case 0x4a:
				ret |= display_buffer().xoffset() << 16;
				ret |= display_buffer().yoffset();
				break;
			case 0x4c:
				ret |= display_buffer().inner_clip().left() << 16;
				ret |= display_buffer().inner_clip().right();
				break;
			case 0x4d:
				ret |= display_buffer().inner_clip().top() << 16;
				ret |= display_buffer().inner_clip().bottom();
				break;
			case 0x4e:
				ret |= display_buffer().outer_clip().left() << 16;
				ret |= display_buffer().outer_clip().right();
				break;
			case 0x4f:
				ret |= display_buffer().outer_clip().top() << 16;
				ret |= display_buffer().outer_clip().bottom();
				break;
			case 0x50:
				ret |= blitter().depth_tester().depth_buffer().addr();
				break;
			case 0x51:
				ret |= blitter().depth_tester().stencil_buffer().addr();
				break;
			case 0x52:
				ret |= blitter().depth_tester().depth_buffer().width() << 28;
				ret |= blitter().depth_tester().depth_buffer().height() << 24;
				ret |= blitter().depth_tester().depth_buffer().stride() << 20;
				ret |= blitter().depth_tester().depth_buffer().format() << 17;
				ret |= blitter().depth_tester().stencil_buffer().width() << 12;
				ret |= blitter().depth_tester().stencil_buffer().height() << 8;
				ret |= blitter().depth_tester().stencil_buffer().stride() << 4;
				ret |= blitter().depth_tester().stencil_buffer().format() << 1;
				break;
			case 0x53:
				ret |= bgcolor();
				break;
			case 0x54:
				ret |= CHIP_VERSION;
				break;
		}
		return ret;
	} // jkmv32b_t<BigEndian, T>::reg_r

	// host interface
	template<bool BigEndian, typename T>
	void jkmv32b_t<BigEndian, T>::host_w(const u8 addr, const u32 data, const u32 mask)
	{
		switch (sizeof(T))
		{
			case 1:
			{
				const u32 sdata = u32(data) << (bitfield(BigEndian ? ~addr : addr, 0, 2) << 3);
				const u32 smask = u32(mask) << (bitfield(BigEndian ? ~addr : addr, 0, 2) << 3);
				switch (addr & 4)
				{
					case 0:
					{
						if (bitfield(smask, 0, 8))
						{
							m_reg = bitfield(sdata, 0, 8);
						}
						break;
					}
					case 4:
						reg_w(addr, m_reg, sdata, smask);
						break;
				}
				break;
			}
			case 2:
			{
				const u32 sdata = u32(data) << (bitfield(BigEndian ? ~addr : addr, 0) << 4);
				const u32 smask = u32(mask) << (bitfield(BigEndian ? ~addr : addr, 0) << 4);
				switch (addr & 2)
				{
					case 0:
					{
						if (bitfield(smask, 0, 8))
						{
							m_reg = bitfield(sdata, 0, 8);
						}
						break;
					}
					case 2:
						reg_w(addr, m_reg, sdata, smask);
						break;
				}
				break;
			}
			case 4:
				switch (addr & 1)
				{
					case 0:
					{
						if (bitfield(mask, 0, 8))
						{
							m_reg = bitfield(data, 0, 8);
						}
						break;
					}
					case 1:
						reg_w(addr, m_reg, data, mask);
						break;
				}
				break;
		}
	} // jkmv32b_t<BigEndian, T>::host_w

	template<bool BigEndian, typename T>
	u32 jkmv32b_t<BigEndian, T>::host_r(const u8 addr)
	{
		switch (sizeof(T))
		{
			case 1:
				switch (addr & 4)
				{
					case 0:
					{
						if (((BigEndian ? ~addr : addr) & 3) == 0)
						{
							return m_reg;
						}
						break;
					}
					case 4:
					{
						const u8 shift = bitfield(BigEndian ? ~addr : addr, 0, 2) << 3;
						return reg_r(addr, m_reg) >> shift;
						break;
					}
				}
				break;
			case 2:
				switch (addr & 2)
				{
					case 0:
					{
						if (((BigEndian ? ~addr : addr) & 1) == 0)
						{
							return m_reg;
						}
						break;
					}
					case 2:
					{
						const u8 shift = bitfield(BigEndian ? ~addr : addr, 0) << 4;
						return reg_r(addr, m_reg) >> shift;
						break;
					}
				}
				break;
			case 4:
				switch (addr & 1)
				{
					case 0:
						return m_reg;
						break;
					case 1:
						return reg_r(addr, m_reg);
						break;
				}
				break;
		}
		return 0;
	} // jkmv32b_t<BigEndian, T>::host_r

	// Blitter
	template<bool BigEndian, typename T>
	void jkmv32b_t<BigEndian, T>::blitter_t::blit()
	{
		for (u16 y = 0; y < m_y_size; y++)
		{
			for (u16 x = 0; x < m_x_size; x++)
			{
				u32 sx = (srctrans().x().start() +
						((srctrans().x().zoom() + (srctrans().x().stretch() * y)) * x) +
						(srctrans().x().tilt() * y)) >> 16;
				u32 sy = (srctrans().y().start() +
						((srctrans().y().zoom() + (srctrans().y().stretch() * x)) * y) +
						(srctrans().y().tilt() * x)) >> 16;
				const u32 dx = (dsttrans().x().start() +
							((dsttrans().x().zoom() + (dsttrans().x().stretch() * y)) * x) +
							(dsttrans().x().tilt() * y)) >> 16;
				const u32 dy = (dsttrans().y().start() +
							((dsttrans().y().zoom() + (dsttrans().y().stretch() * x)) * y) +
							(dsttrans().y().tilt() * x)) >> 16;

				if (texture().flip_x())
					sx = ((1 << texture().width()) - 1) - sx;

				if (texture().flip_y())
					sy = ((1 << texture().height()) - 1) - sy;

				if (texture().wrap_x())
					sx &= ((1 << texture().width()) - 1);
				else if (sx >= (1 << texture().width()))
					continue;

				if (texture().wrap_y())
					sy &= ((1 << texture().height()) - 1);
				else if (sy >= (1 << texture().height()))
					continue;

				if (src_inner_clip().check(sx, sy) &&
					dst_inner_clip().check(dx, dy) &&
					src_outer_clip().check(sx, sy) &&
					dst_outer_clip().check(dx, dy))
				{
					write_pixel(sx, sy, dx, dy);
				}
			}
		}
	} // jkmv32b_t<BigEndian, T>::blitter_t::blit

	template<bool BigEndian, typename T>
	void jkmv32b_t<BigEndian, T>::blitter_t::write_pixel(const u16 sx, const u16 sy, const u16 dx, const u16 dy)
	{
		const u8 color_shift[16] = {1, 2, 4, 8, 0, 0, 0, 0, 0, 0, 4, 8, 0, 0, 0, 0};
		const s8 tile_shift[16] = {-3, -2, -1, 0, -3, -2, -1, 0, 0, 1, 0, 1, 1, 1, 1, 2};
		const u8 depth_shift[8] = {31, 30, 28, 24, 16, 0, 0, 0};
		u32 pixel = m_static_color;
		if (texture().enable())
		{
			if (texture().tilemap().enable())
			{
				const u32 tx = sx >> (texture().tilemap().width() + 2);
				const u32 ty = sy >> (texture().tilemap().height() + 2);
				u32 px = sx & ((1 << (texture().tilemap().width() + 2)) - 1);
				u32 py = sy & ((1 << (texture().tilemap().height() + 2)) - 1);

				const u32 tileoffs = texture().swap_xy() ?
							ty | (tx << texture().stride()) :
							tx | (ty << texture().stride());
				
				bool tile_fx = false;
				bool tile_fy = false;
				u32 tile_index = 0;
				u32 tile_color = 0;
				u32 tile_data = 0;
				switch (texture().tilemap().format())
				{
					case 0x00:
						tile_data = m_host.intf().read_dword(texture().tilemap().addr() + (tileoffs << 2));
						tile_fx = bitfield(tile_data, 31);
						tile_fy = bitfield(tile_data, 30);
						tile_color = bitfield(tile_data, 20, 10);
						tile_index = bitfield(tile_data, 0, 20);
						break;
					case 0x01:
						tile_data = m_host.intf().read_dword(texture().tilemap().addr() + (tileoffs << 2));
						tile_fx = bitfield(tile_data, 31);
						tile_fy = bitfield(tile_data, 30);
						tile_index = bitfield(tile_data, 0, 30);
						break;
					case 0x02:
						tile_data = m_host.intf().read_dword(texture().tilemap().addr() + (tileoffs << 2));
						tile_color = bitfield(tile_data, 20, 12);
						tile_index = bitfield(tile_data, 0, 20);
						break;
					case 0x03:
						tile_data = m_host.intf().read_dword(texture().tilemap().addr() + (tileoffs << 2));
						tile_index = bitfield(tile_data, 0, 32);
						break;
					case 0x04:
						tile_data = m_host.intf().read_word(texture().tilemap().addr() + (tileoffs << 1));
						tile_fx = bitfield(tile_data, 15);
						tile_fy = bitfield(tile_data, 14);
						tile_color = bitfield(tile_data, 12, 2);
						tile_index = bitfield(tile_data, 0, 12);
						break;
					case 0x05:
						tile_data = m_host.intf().read_word(texture().tilemap().addr() + (tileoffs << 1));
						tile_fx = bitfield(tile_data, 15);
						tile_fy = bitfield(tile_data, 14);
						tile_index = bitfield(tile_data, 0, 14);
						break;
					case 0x06:
						tile_data = m_host.intf().read_word(texture().tilemap().addr() + (tileoffs << 1));
						tile_color = bitfield(tile_data, 12, 4);
						tile_index = bitfield(tile_data, 0, 12);
						break;
					case 0x07:
						tile_data = m_host.intf().read_word(texture().tilemap().addr() + (tileoffs << 1));
						tile_index = bitfield(tile_data, 0, 16);
						break;
				}
				if (tile_index)
				{
					if (tile_fx)
						px = ((1 << (texture().tilemap().width() + 2)) - 1) - px;
					if (tile_fy)
						py = ((1 << (texture().tilemap().height() + 2)) - 1) - py;
					tile_index <<= ((texture().tilemap().width() + 2) + (texture().tilemap().height() + 2));
					if (tile_shift[texture().format()] > 0)
						tile_index <<= tile_shift[texture().format()];
					else if (tile_shift[texture().format()] < 0)
						tile_index >>= -tile_shift[texture().format()];

					pixel = texture().get_pixel(texture().addr() + tile_index,
							px, py,
							(texture().tilemap().width() + 2), (texture().tilemap().height() + 2))
							+ (tile_color << color_shift[texture().format()]);
				}
				else
					return;
			}
			else
				pixel = texture().get_pixel(texture().addr(),
						sx, sy,
						texture().stride(), texture().stride());
		}
		if (((!transparent().enable()) || (transparent().color() != pixel)) &&
			((!alpha_tester().enable()) ||
			(get_test_mode<u8>(alpha_tester().mode(), alpha_tester().ref(), bitfield(pixel, 24, 8)))))
		{
			if (depth_tester().enable())
			{
				const u32 depth = depth_tester().depth_buffer().get_data(dx, dy);
				const u32 stencil = depth_tester().stencil_buffer().get_data(dx, dy);
				const bool dppass = get_test_mode<u32>(depth_tester().depth_test(), depth_tester().depth_ref(), depth);
				const bool stpass = get_test_mode<u32>(depth_tester().stencil_test(), depth_tester().stencil_ref() & depth_tester().stencil_ref_mask(), stencil & depth_tester().stencil_ref_mask());
				if (dppass)
				{
					depth_tester().depth_buffer().write_data(dx, dy,
						get_write_mode(depth_tester().depth_write_pass(),
							depth_shift[depth_tester().depth_buffer().format()],
							depth_tester().depth_ref(), depth));
					if (stpass)
					{
						depth_tester().stencil_buffer().write_data(dx, dy,
							get_write_mode(depth_tester().stencil_write_dppass(),
								depth_shift[depth_tester().stencil_buffer().format()],
								depth_tester().stencil_ref() & depth_tester().stencil_write_mask(), stencil & depth_tester().stencil_write_mask()));
					}
					else
					{
						depth_tester().stencil_buffer().write_data(dx, dy,
							get_write_mode(depth_tester().stencil_write_sfail(),
								depth_shift[depth_tester().stencil_buffer().format()],
								depth_tester().stencil_ref() & depth_tester().stencil_write_mask(), stencil & depth_tester().stencil_write_mask()));
					}
				}
				else
				{
					depth_tester().depth_buffer().write_data(dx, dy,
						get_write_mode(depth_tester().depth_write_fail(),
							depth_shift[depth_tester().depth_buffer().format()],
							depth_tester().depth_ref(), depth));
					if (stpass)
					{
						depth_tester().stencil_buffer().write_data(dx, dy,
							get_write_mode(depth_tester().stencil_write_dpfail(),
								depth_shift[depth_tester().stencil_buffer().format()],
								depth_tester().stencil_ref() & depth_tester().stencil_write_mask(), stencil & depth_tester().stencil_write_mask()));
					}
					else
					{
						depth_tester().stencil_buffer().write_data(dx, dy,
							get_write_mode(depth_tester().stencil_write_sfail(),
								depth_shift[depth_tester().stencil_buffer().format()],
								depth_tester().stencil_ref() & depth_tester().stencil_write_mask(), stencil & depth_tester().stencil_write_mask()));
					}
				}
				if (!dppass || !stpass)
					return;
			}
			if (color_fade().enable())
			{
				u8 sa = bitfield(pixel, 24, 8);
				u8 sr = bitfield(pixel, 16, 8);
				u8 sg = bitfield(pixel, 8, 8);
				u8 sb = bitfield(pixel, 0, 8);
				sa = get_color_fade(color_fade().a().fade_mode(),
					color_fade().a().fade_amount(), sa, color_fade().a().fade_target());
				sr = get_color_fade(color_fade().r().fade_mode(),
					color_fade().r().fade_amount(), sr, color_fade().r().fade_target());
				sg = get_color_fade(color_fade().g().fade_mode(),
					color_fade().g().fade_amount(), sg, color_fade().g().fade_target());
				sb = get_color_fade(color_fade().b().fade_mode(),
					color_fade().b().fade_amount(), sb, color_fade().b().fade_target());
				if (color_fade().premultiplied() && (sa != 0xff))
				{
					sr = (sr * sa) >> 8;
					sg = (sg * sa) >> 8;
					sb = (sb * sa) >> 8;
				}
				pixel = (sa << 24) | (sr << 16) | (sg << 8) | sb;
			}
			if (alpha_blender().enable())
			{
				const u32 target = m_host.draw_buffer().get_pixel(dx, dy);
				const u32 source_c = bitfield(get_blend_mode(alpha_blender().srcblend_c(),
							pixel, target,
							alpha_blender().constcol(0), alpha_blender().constcol(1)), 0, 24);
				const u32 source_a = bitfield(get_blend_mode(alpha_blender().srcblend_a(),
							pixel, target,
							alpha_blender().constcol(0), alpha_blender().constcol(1)), 24, 8);
				const u32 target_c = bitfield(get_blend_mode(alpha_blender().dstblend_c(),
							pixel, target,
							alpha_blender().constcol(0), alpha_blender().constcol(1)), 0, 24);
				const u32 target_a = bitfield(get_blend_mode(alpha_blender().dstblend_a(),
							pixel, target,
							alpha_blender().constcol(0), alpha_blender().constcol(1)), 24, 8);
				u8 sa = source_a;
				u8 sr = bitfield(source_c, 16, 8);
				u8 sg = bitfield(source_c, 8, 8);
				u8 sb = bitfield(source_c, 0, 8);
				const u8 ta = target_a;
				const u8 tr = bitfield(target_c, 16, 8);
				const u8 tg = bitfield(target_c, 8, 8);
				const u8 tb = bitfield(target_c, 0, 8);
				sa = get_blend_op(alpha_blender().blendop_a(), sa, ta);
				sr = get_blend_op(alpha_blender().blendop_c(), sr, tr);
				sg = get_blend_op(alpha_blender().blendop_c(), sg, tg);
				sb = get_blend_op(alpha_blender().blendop_c(), sb, tb);
				pixel = (sa << 24) | (sr << 16) | (sg << 8) | sb;
			}
			m_host.draw_buffer().write_pixel(dx, dy, pixel);
		}
	} // jkmv32b_t<BigEndian, T>::blitter_t::write_pixel

	// Bitmap
	template<bool BigEndian, typename T>
	u32 jkmv32b_t<BigEndian, T>::bitmap_t::get_pixel(const u16 x, const u16 y) const
	{
		const u16 dx = x + xoffset();
		const u16 dy = y + yoffset();
		if (enable() &&
			((dx < (1 << width())) &&
			(dy < (1 << height()))) &&
			(inner_clip().check(dx, dy) &&
			outer_clip().check(dx, dy)))
		{
			const u32 offs = swap_xy() ? (dy | (dx << stride())) : (dx | (dy << stride()));
			u32 ret = 0;
			switch (format())
			{
				case color_format_t::C_ARGB4444:
					ret = convert_argb(color_format_t::C_ARGB4444,
						color_format_t::C_ARGB8888,
						m_host.intf().read_word(addr() + (offs << 1)));
					break;
				case color_format_t::C_ARGB1555:
					ret = convert_argb(color_format_t::C_ARGB1555,
						color_format_t::C_ARGB8888,
						m_host.intf().read_word(addr() + (offs << 1)));
					break;
				case color_format_t::C_RGB565:
					ret = convert_argb(color_format_t::C_RGB565,
						color_format_t::C_ARGB8888,
						m_host.intf().read_word(addr() + (offs << 1)));
					break;
				case color_format_t::C_ARGB8888:
				default:
					ret = m_host.intf().read_word(addr() + (offs << 2));
					break;
			}
			m_host.add_cycle(1);
			return ret;
		}
		return m_host.bgcolor();
	} // jkmv32b_t<BigEndian, T>::bitmap_t::get_pixel

	template<bool BigEndian, typename T>
	void jkmv32b_t<BigEndian, T>::bitmap_t::write_pixel(const u16 x, const u16 y, const u32 argb)
	{
		const u16 dx = x + xoffset();
		const u16 dy = y + yoffset();
		if (enable() &&
			((dx < (1 << width())) &&
			(dy < (1 << height()))) &&
			(inner_clip().check(dx, dy) &&
			outer_clip().check(dx, dy)))
		{
			const u32 offs = m_swap_xy ? (dy | (dx << stride())) : (dx | (dy << stride()));
			switch (format())
			{
				case color_format_t::C_ARGB4444:
					m_host.intf().write_word(addr() + (offs << 1), 
						convert_argb(color_format_t::C_ARGB8888,
						color_format_t::C_ARGB4444,
						argb));
					break;
				case color_format_t::C_ARGB1555:
					m_host.intf().write_word(addr() + (offs << 1), 
						convert_argb(color_format_t::C_ARGB8888,
						color_format_t::C_ARGB1555,
						argb));
					break;
				case color_format_t::C_RGB565:
					m_host.intf().write_word(addr() + (offs << 1), 
						convert_argb(color_format_t::C_ARGB8888,
						color_format_t::C_RGB565,
						argb));
					break;
				case color_format_t::C_ARGB8888:
				default:
					m_host.intf().write_dword(addr() + (offs << 2), argb);
					break;
			}
			m_host.add_cycle(1);
		}
	} // jkmv32b_t<BigEndian, T>::bitmap_t::write_pixel

} // namespace jkmv32b
