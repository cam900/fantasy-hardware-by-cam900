/*

==============================================================================

    JKMQUEUE
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMQUEUE

Fantasy Queue controller

Features:
- Double port queue
- Interrupt controller

	Host interface

	Address Description
	00      Sub Queue data (Read) / Host Queue data (Write)
	01      Queue status (Read)
	        Bit 0 Sub Queue empty
	        Bit 1 Host Queue full
	        Bit 2 Host Queue overflow
	        Bit 3 Sub Queue underflow
	        Bit 4 Sub Queue Half Full
	        Bit 5 Host Queue Half Full
	02      Host IRQ status (Read) / Host IRQ Acknowledge (Write)
	03      Sub IRQ status (Read) / Send Sub IRQ (Write)

	Sub interface

	Address Description
	00      Host Queue data (Read) / Sub Queue data (Write)
	01      Queue status (Read)
	        Bit 0 Host Queue empty
	        Bit 1 Sub Queue full
	        Bit 2 Sub Queue overflow
	        Bit 3 Host Queue underflow
	        Bit 4 Host Queue Half Full
	        Bit 5 Sub Queue Half Full
	02      Sub IRQ status (Read) / Sub IRQ Acknowledge (Write)
	03      Host IRQ status (Read) / Send Host IRQ (Write)
*/

#include "jkmqueue.hpp"

namespace jkmqueue
{
	template<typename T, std::size_t S>
	void jkmqueue_t<T, S>::irq_update()
	{
		m_intf.host_empty(m_s2h_queue.empty());
		m_intf.host_full(m_s2h_queue.full());
		m_intf.host_overflow(m_s2h_queue.overflow());
		m_intf.host_underflow(m_s2h_queue.underflow());
		m_intf.host_half(m_s2h_queue.half());
		m_intf.host_irq(m_host_irq);

		m_intf.sub_empty(m_h2s_queue.empty());
		m_intf.sub_full(m_h2s_queue.full());
		m_intf.sub_overflow(m_h2s_queue.overflow());
		m_intf.sub_underflow(m_h2s_queue.underflow());
		m_intf.sub_half(m_h2s_queue.half());
		m_intf.sub_irq(m_sub_irq);
	} // jkmqueue_t<T, S>::irq_update

	template<typename T, std::size_t S>
	void jkmqueue_t<T, S>::reset()
	{
		m_h2s_queue.reset();
		m_s2h_queue.reset();
		m_host_irq = false;
		m_sub_irq = false;
	} // jkmqueue_t<T, S>::reset

	template<typename T, std::size_t S>
	T jkmqueue_t<T, S>::host_r(const u8 addr) const
	{
		T ret = 0;
		switch (addr & 3)
		{
			case 0:
				ret = m_s2h_queue.dequeue();
				irq_update();
				break;
			case 1:
				ret = (m_s2h_queue.empty() ? 0x01 : 0x00)
					| (m_h2s_queue.full() ? 0x02 : 0x00)
					| (m_h2s_queue.overflow() ? 0x04 : 0x00)
					| (m_s2h_queue.underflow() ? 0x08 : 0x00)
					| (m_s2h_queue.half() ? 0x10 : 0x00)
					| (m_h2s_queue.half() ? 0x20 : 0x00);
				break;
			case 2:
				ret = m_host_irq ? 0x01 : 0x00;
				break;
			case 3:
				ret = m_sub_irq ? 0x01 : 0x00;
				break;
		}
		return ret;
	} // jkmqueue_t<T, S>::host_r

	template<typename T, std::size_t S>
	void jkmqueue_t<T, S>::host_w(const u8 addr, const T data, const T mask)
	{
		switch (addr & 3)
		{
			case 0:
				m_h2s_queue.enqueue(data);
				irq_update();
				break;
			case 1: // read only
				break;
			case 2:
				if (bitfield(mask, 0, 8))
				{
					if (bitfield(data, 0))
					{
						m_host_irq = false;
						irq_update();
					}
				}
				break;
			case 3:
				if (bitfield(mask, 0, 8))
				{
					if (bitfield(data, 0))
					{
						m_sub_irq = true;
						irq_update();
					}
				}
				break;
		}
	} // jkmqueue_t<T, S>::host_w

	template<typename T, std::size_t S>
	T jkmqueue_t<T, S>::sub_r(const u8 addr) const
	{
		T ret = 0;
		switch (addr & 3)
		{
			case 0:
				ret = m_h2s_queue.dequeue();
				irq_update();
				break;
			case 1:
				ret = (m_h2s_queue.empty() ? 0x01 : 0x00)
					| (m_s2h_queue.full() ? 0x02 : 0x00)
					| (m_s2h_queue.overflow() ? 0x04 : 0x00)
					| (m_h2s_queue.underflow() ? 0x08 : 0x00)
					| (m_h2s_queue.half() ? 0x10 : 0x00)
					| (m_s2h_queue.half() ? 0x20 : 0x00);
				break;
			case 2:
				ret = m_sub_irq ? 0x01 : 0x00;
				break;
			case 3:
				ret = m_host_irq ? 0x01 : 0x00;
				break;
		}
		return ret;
	} // jkmqueue_t<T, S>::sub_r

	template<typename T, std::size_t S>
	void jkmqueue_t<T, S>::sub_w(const u8 addr, const T data, const T mask)
	{
		switch (addr & 3)
		{
			case 0:
				m_s2h_queue.enqueue(data);
				irq_update();
				break;
			case 1: // read only
				break;
			case 2:
				if (bitfield(mask, 0, 8))
				{
					if (bitfield(data, 0))
					{
						m_sub_irq = false;
						irq_update();
					}
				}
				break;
			case 3:
				if (bitfield(mask, 0, 8))
				{
					if (bitfield(data, 0))
					{
						m_host_irq = true;
						irq_update();
					}
				}
				break;
		}
	} // jkmqueue_t<T, S>::sub_w

	template<typename T, std::size_t S>
	void jkmqueue_t<T, S>::queue_t::reset()
	{
		for (T &data : m_data)
		{
			data = 0;
		}
		m_read_ptr = 0;
		m_write_ptr = 0;
		m_count = 0;
		m_empty = true;
		m_full = false;
		m_overflow = false;
		m_underflow = false;
		m_half = false;
	} // jkmqueue_t<T, S>::queue_t::reset

	template<typename T, std::size_t S>
	T jkmqueue_t<T, S>::queue_t::dequeue()
	{
		if (m_empty)
		{
			m_underflow = true;
			return ~0;
		}
		else
		{
			const T ret = m_data[m_read_ptr];
			m_read_ptr = (m_read_ptr + 1) % S;
			m_count--;
			if (m_full)
			{
				m_full = false;
			}
			if (m_overflow)
			{
				m_overflow = false;
			}
			if (m_read_ptr == m_write_ptr)
			{
				m_empty = true;
			}
			if (m_half && (m_count < (S >> 1)))
			{
				m_half = false;
			}
			return ret;
		}
	} // jkmqueue_t<T, S>::queue_t::dequeue

	template<typename T, std::size_t S>
	void jkmqueue_t<T, S>::queue_t::enqueue(const T data)
	{
		if (m_full)
		{
			m_overflow = true;
			return;
		}
		else
		{
			const T ret = m_data[m_write_ptr];
			m_write_ptr = (m_write_ptr + 1) % S;
			m_count++;
			if (m_empty)
			{
				m_empty = false;
			}
			if (m_underflow)
			{
				m_underflow = false;
			}
			if (m_read_ptr == m_write_ptr)
			{
				m_full = true;
			}
			if (!m_half && (m_count >= (S >> 1)))
			{
				m_half = true;
			}
			return ret;
		}
	} // jkmqueue_t<T, S>::queue_t::enqueue

} // namespace jkmqueue
