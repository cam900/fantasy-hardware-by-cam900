/*

==============================================================================

    JKMQUEUE
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMQUEUE

Fantasy Queue controller

*/

#ifndef JKMQUEUE_HPP
#define JKMQUEUE_HPP

#pragma once

#include <array>

namespace jkmqueue
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	class jkmqueue_intf_t
	{
		public:
			jkmqueue_intf_t()
			{
			} // jkmqueue_intf_t

			virtual void host_empty(bool empty) {}
			virtual void host_full(bool full) {}
			virtual void host_overflow(bool overflow) {}
			virtual void host_underflow(bool underflow) {}
			virtual void host_half(bool half) {}
			virtual void host_irq(bool assert) {}

			virtual void sub_empty(bool empty) {}
			virtual void sub_full(bool full) {}
			virtual void sub_overflow(bool overflow) {}
			virtual void sub_underflow(bool underflow) {}
			virtual void sub_half(bool half) {}
			virtual void sub_irq(bool assert) {}
	}; // class jkmqueue_intf_t

	template<typename T, std::size_t S>
	class jkmqueue_t
	{
		private:
			class queue_t
			{
				public:
					queue_t()
						: m_data{0}
						, m_count(0)
						, m_read_ptr(0)
						, m_write_ptr(0)
						, m_empty(true)
						, m_full(false)
						, m_overflow(false)
						, m_underflow(false)
					{
					} // queue_t

					void reset();

					T dequeue();
					void enqueue(const T data);

					bool empty() const
					{
						return m_empty;
					} // empty

					bool full() const
					{
						return m_full;
					} // full

					bool overflow() const
					{
						return m_overflow;
					} // overflow

					bool underflow() const
					{
						return m_underflow;
					} // underflow

					bool half() const
					{
						return m_half;
					} // half

				private:
					std::array<T, S> m_data;
					u32 m_count = 0;
					u32 m_read_ptr = 0;
					u32 m_write_ptr = 0;
					bool m_empty = true;
					bool m_full = false;
					bool m_overflow = false;
					bool m_underflow = false;
					bool m_half = false;
			}; // class queue_t
		public:
			jkmqueue_t(jkmqueue_intf_t &intf)
				: m_intf(intf)
				, m_h2s_queue(queue_t())
				, m_s2h_queue(queue_t())
				, m_host_irq(false)
				, m_sub_irq(false)
			{
			} // jkmqueue_t

			void reset();

			T host_r(const u8 addr) const;
			void host_w(const u8 addr, const T data, const T mask);
			T sub_r(const u8 addr) const;
			void sub_w(const u8 addr, const T data, const T mask);

		private:
			void irq_update();

			jkmqueue_intf_t &m_intf; // Interface
			queue_t m_h2s_queue; // Host to Sub queue
			queue_t m_s2h_queue; // Sub to Host queue
			bool m_host_irq = false;
			bool m_sub_irq = false;
	}; // class jkmqueue_t
} // namespace jkmqueue

#endif // JKMQUEUE_HPP
