/*

==============================================================================

    JKMMMU
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMMMU

Fantasy MMU controller

Features:
- Paging hardware with protect/modify bit
- Configurable page size / frame size

	8 bit MMU host interface

	Address Description
	00      MMU bank select LSB
	01      MMU bank select MSB (MSB: Auto increment)
	04      MMU bank data bit 0-7
	05      MMU bank data bit 8-15
	06      MMU bank data bit 16-23
	07      MMU bank data bit 24-31
	08      MMU Error code word 0 bit 0-7
	09      MMU Error code word 0 bit 8-15
	0a      MMU Error code word 0 bit 16-23
	0b      MMU Error code word 0 bit 24-31
	0c      MMU Error code word 0 bit 32-39
	0d      MMU Error code word 0 bit 40-47
	0e      MMU Error code word 0 bit 48-55
	0f      MMU Error code word 0 bit 56-63
	10      MMU Error code word 1 bit 0-7
	11      MMU Error code word 1 bit 8-15
	12      MMU Error code word 1 bit 16-23
	13      MMU Error code word 1 bit 24-31
	14      MMU Error code word 1 bit 32-39
	15      MMU Error code word 1 bit 40-47
	16      MMU Error code word 1 bit 48-55
	17      MMU Error code word 1 bit 56-63
	20      MMU Page size (MSB: enable)

	16 bit MMU host interface

	Address Description
	00      MMU bank select (MSB: Auto increment)
	04      MMU bank data LSB
	06      MMU bank data MSB
	08      MMU Error code word 0 bit 0-15
	0a      MMU Error code word 0 bit 16-31
	0c      MMU Error code word 0 bit 32-47
	0e      MMU Error code word 0 bit 48-63
	10      MMU Error code word 1 bit 0-15
	12      MMU Error code word 1 bit 16-31
	14      MMU Error code word 1 bit 32-47
	16      MMU Error code word 1 bit 48-63
	20      MMU Page size (MSB: enable)

	32 bit MMU host interface

	Address Description
	00      MMU bank select (MSB: Auto increment)
	04      MMU bank data
	08      MMU Error code word 0 bit 0-31
	0c      MMU Error code word 0 bit 32-63
	10      MMU Error code word 1 bit 0-31
	14      MMU Error code word 1 bit 32-63
	20      MMU Page size (MSB: enable)

	64 bit MMU host interface

	Address Description
	00      MMU bank select (MSB: Auto increment)
	08      MMU bank data
	10      MMU Error code word 0
	18      MMU Error code word 1
	20      MMU Page size (MSB: enable)

	MMU Bank data format

	Bit 0 Valid(0)/Invalid(1)
	Bit 1 Read Enable(0)/Disable(1)
	Bit 2 Write Enable(0)/Disable(1)
	Bit 3 Execute Enable(0)/Disable(1)
	Bit 4 User(0)/Supervisor(1)
	Bit 5 Reference(1)
	Bit 6 Dirty(1)
	Bit 7 to 31 Page address

	MMU Error code format

	Word 0
	Bit 0 to 4 Error Code
	Bit 0 Invalid
	Bit 1 Read
	Bit 2 Write
	Bit 3 Execute
	Bit 4 Supervisor
	Bit 16 to 31 Page address
	Bit 32 to 63 Virtual address
	Word 1 Physical address

*/

#include "jkmmmu.hpp"

namespace jkmmmu
{
	template<bool BigEndian, typename T, std::size_t PageEntries>
	void jkmmmu_t<BigEndian, T, PageEntries>::reset()
	{
		for (page_t &page : m_page)
		{
			page.reset();
		}
		m_error.reset();
		m_page_addr = 0;
		m_page_size = 9;
		m_enable = false;
	}

	template<bool BigEndian, typename T, std::size_t PageEntries>
	T jkmmmu_t<BigEndian, T, PageEntries>::mmu_r(const bool execute, const u32 addr)
	{
		if (!m_enable)
		{
			switch (sizeof(T))
			{
				case 1: return m_intf.read_byte(m_intf.supervisor_flag(), execute, m_init_mask | addr);
				case 2: return m_intf.read_word(m_intf.supervisor_flag(), execute, m_init_mask | addr);
				case 4: return m_intf.read_dword(m_intf.supervisor_flag(), execute, m_init_mask | addr);
				case 8: return m_intf.read_qword(m_intf.supervisor_flag(), execute, m_init_mask | addr);
				default: return 0;
			}
		}
		else
		{
			const u16 page_addr = (addr >> m_page_size) % PageEntries;
			const page_t &page = m_page[page_addr];
			const u64 vaddr = addr * sizeof(T);
			const u64 paddr = page.get_addr(vaddr, m_page_size);
			if (page.valid())
			{
				if (m_intf.supervisor_flag() || (!page.supervisor()))
				{
					if (((!execute) && (!page.read_protect())) || (execute && (!page.execute_protect())))
					{
						switch (sizeof(T))
						{
							case 1: return m_intf.read_byte(m_intf.supervisor_flag(), execute, paddr);
							case 2: return m_intf.read_word(m_intf.supervisor_flag(), execute, paddr);
							case 4: return m_intf.read_dword(m_intf.supervisor_flag(), execute, paddr);
							case 8: return m_intf.read_qword(m_intf.supervisor_flag(), execute, paddr);
						}
						page.set_reference();
					}
					else
					{
						m_error.set_error(execute ? error_t::error_code_t::ERROR_EXECUTE_FAIL : error_t::error_code_t::ERROR_READ_FAIL, page_addr, vaddr, paddr);
					}
				}
				else
				{
					m_error.set_error((execute ? error_t::error_code_t::ERROR_EXECUTE_FAIL : error_t::error_code_t::ERROR_READ_FAIL) | error_t::error_code_t::ERROR_SUPERVISOR, page_addr, vaddr, paddr);
				}
			}
			else
			{
				m_error.set_error((execute ? error_t::error_code_t::ERROR_EXECUTE_FAIL : error_t::error_code_t::ERROR_READ_FAIL) | error_t::error_code_t::ERROR_INVALID, page_addr, vaddr, paddr);
			}
		}
		return ~0;
	}

	template<bool BigEndian, typename T, std::size_t PageEntries>
	void jkmmmu_t<BigEndian, T, PageEntries>::mmu_w(const u32 addr, const T data , const T mask)
	{
		if (!m_enable)
		{
			switch (sizeof(T))
			{
				case 1: m_intf.write_byte(m_intf.supervisor_flag(), m_init_mask | addr, data, mask); break;
				case 2: m_intf.write_word(m_intf.supervisor_flag(), m_init_mask | addr, data, mask); break;
				case 4: m_intf.write_dword(m_intf.supervisor_flag(), m_init_mask | addr, data, mask); break;
				case 8: m_intf.write_qword(m_intf.supervisor_flag(), m_init_mask | addr, data, mask); break;
			}
		}
		else
		{
			const u16 page_addr = (addr >> m_page_size) % PageEntries;
			const page_t &page = m_page[page_addr];
			const u64 vaddr = addr * sizeof(T);
			const u64 paddr = page.get_addr(vaddr, m_page_size);
			if (page.valid())
			{
				if (m_intf.supervisor_flag() || (!page.supervisor()))
				{
					if (!page.write_protect())
					{
						switch (sizeof(T))
						{
							case 1: m_intf.write_byte(m_intf.supervisor_flag(), paddr, data, mask); break;
							case 2: m_intf.write_word(m_intf.supervisor_flag(), paddr, data, mask); break;
							case 4: m_intf.write_dword(m_intf.supervisor_flag(), paddr, data, mask); break;
							case 8: m_intf.write_qword(m_intf.supervisor_flag(), paddr, data, mask); break;
						}
						page.set_dirty();
					}
					else
					{
						m_error.set_error(error_t::error_code_t::ERROR_WRITE_FAIL, page_addr, vaddr, paddr);
					}

				}
				else
				{
					m_error.set_error(error_t::error_code_t::ERROR_WRITE_FAIL | error_t::error_code_t::ERROR_SUPERVISOR, page_addr, vaddr, paddr);
				}
			}
			else
			{
				m_error.set_error(error_t::error_code_t::ERROR_WRITE_FAIL | error_t::error_code_t::ERROR_INVALID, page_addr, vaddr, paddr);
			}
		}
	}


	template<bool BigEndian, typename T, std::size_t PageEntries>
	T jkmmmu_t<BigEndian, T, PageEntries>::host_r(const u8 addr, const T mask)
	{
		T ret = 0;
		switch (sizeof(T))
		{
			case 1:
			{
				const u8 shift16 = bitfield(BigEndian ? ~addr : addr, 0) << 3;
				const u8 shift32 = bitfield(BigEndian ? ~addr : addr, 0, 2) << 3;
				const u8 shift64 = bitfield(BigEndian ? ~addr : addr, 0, 3) << 3;
				switch (addr & 0x3f)
				{
					case 0x00:
					case 0x01:
						ret = m_page_addr >> shift16;
						break;
					case 0x04:
					case 0x05:
					case 0x06:
					case 0x07:
						if (bitfield(m_page_addr, 0, 15) < PageEntries)
						{
							ret = m_page[bitfield(m_page_addr, 0, 15)].regs_r() >> shift32;
						}
						if ((m_page_addr & 0x8000) && (bitfield(addr, 0, 2) == 0b11))
						{
							m_page_addr = (m_page_addr & 0x8000) | ((m_page_addr + 1) & 0x7fff);
						}
						break;
					case 0x08:
					case 0x09:
					case 0x0a:
					case 0x0b:
					case 0x0c:
					case 0x0d:
					case 0x0e:
					case 0x0f:
						ret = m_error.regs_r() >> shift64;
						break;
					case 0x10:
					case 0x11:
					case 0x12:
					case 0x13:
					case 0x14:
					case 0x15:
					case 0x16:
					case 0x17:
						ret = m_error.paddr() >> shift64;
						break;
					case 0x20:
						ret = bitfield(m_page_size, 0, 5) | (m_enable ? 0x80 : 0);
						break;
				}
				break;
			}
			case 2:
			{
				const u8 shift32 = bitfield(BigEndian ? ~addr : addr, 0) << 4;
				const u8 shift64 = bitfield(BigEndian ? ~addr : addr, 0, 2) << 4;
				switch (addr & 0x1f)
				{
					case 0x00:
						ret = m_page_addr;
						break;
					case 0x02:
					case 0x03:
						if (bitfield(m_page_addr, 0, 15) < PageEntries)
						{
							ret = m_page[bitfield(m_page_addr, 0, 15)].regs_r() >> shift32;
						}
						if ((m_page_addr & 0x8000) && (bitfield(addr, 0) == 0b1))
						{
							m_page_addr = (m_page_addr & 0x8000) | ((m_page_addr + 1) & 0x7fff);
						}
						break;
					case 0x04:
					case 0x05:
					case 0x06:
					case 0x07:
						ret = m_error.regs_r() >> shift64;
						break;
					case 0x08:
					case 0x09:
					case 0x0a:
					case 0x0b:
						ret = m_error.paddr() >> shift64;
						break;
					case 0x10:
						ret = bitfield(m_page_size, 0, 5) | (m_enable ? 0x80 : 0);
						break;
				}
				break;
			}
			case 4:
			{
				const u8 shift64 = bitfield(BigEndian ? ~addr : addr, 0) << 5;
				switch (addr & 0x0f)
				{
					case 0x00:
						if (bitfield(mask, 0, 16))
						{
							ret = m_page_addr;
						}
						break;
					case 0x01:
						if (bitfield(m_page_addr, 0, 15) < PageEntries)
						{
							ret = m_page[bitfield(m_page_addr, 0, 15)].regs_r();
						}
						if (m_page_addr & 0x8000)
						{
							m_page_addr = (m_page_addr & 0x8000) | ((m_page_addr + 1) & 0x7fff);
						}
						break;
					case 0x02:
					case 0x03:
						ret = m_error.regs_r() >> shift64;
						break;
					case 0x04:
					case 0x05:
						ret = m_error.paddr() >> shift64;
						break;
					case 0x08:
						ret = bitfield(m_page_size, 0, 5) | (m_enable ? 0x80 : 0);
						break;
				}
				break;
			}
			case 8:
			{
				switch (addr & 0x07)
				{
					case 0x00:
						if (bitfield(mask, 0, 16))
						{
							ret = m_page_addr;
						}
						break;
					case 0x01:
						if (bitfield(mask, 0, 32))
						{
							if (bitfield(m_page_addr, 0, 15) < PageEntries)
							{
								ret = m_page[bitfield(m_page_addr, 0, 15)].regs_r();
							}
							if (m_page_addr & 0x8000)
							{
								m_page_addr = (m_page_addr & 0x8000) | ((m_page_addr + 1) & 0x7fff);
							}
						}
						break;
					case 0x02:
						ret = m_error.regs_r();
						break;
					case 0x03:
						ret = m_error.paddr();
						break;
					case 0x04:
						ret = bitfield(m_page_size, 0, 5) | (m_enable ? 0x80 : 0);
						break;
				}
				break;
			}
		}
		return ret;
	}

	template<bool BigEndian, typename T, std::size_t PageEntries>
	void jkmmmu_t<BigEndian, T, PageEntries>::host_w(const u8 addr, const T data, const T mask)
	{
		switch (sizeof(T))
		{
			case 1:
			{
				const u16 sdata16 = u16(data) << (bitfield(BigEndian ? ~addr : addr, 0) << 3);
				const u16 smask16 = u16(mask) << (bitfield(BigEndian ? ~addr : addr, 0) << 3);
				const u32 sdata32 = u32(data) << (bitfield(BigEndian ? ~addr : addr, 0, 2) << 3);
				const u32 smask32 = u32(mask) << (bitfield(BigEndian ? ~addr : addr, 0, 2) << 3);
				switch (addr & 0x3f)
				{
					case 0x00:
					case 0x01:
						if (bitfield(smask16, 0, 16))
						{
							m_page_addr = (m_page_addr & ~smask16) | (sdata16 & smask16);
						}
						break;
					case 0x04:
					case 0x05:
					case 0x06:
					case 0x07:
						if (bitfield(m_page_addr, 0, 15) < PageEntries)
						{
							m_page[bitfield(m_page_addr, 0, 15)].regs_w(sdata32, smask32);
						}
						if ((m_page_addr & 0x8000) && (bitfield(addr, 0, 2) == 0b11))
						{
							m_page_addr = (m_page_addr & 0x8000) | ((m_page_addr + 1) & 0x7fff);
						}
						break;
					case 0x08:
					case 0x09:
					case 0x0a:
					case 0x0b:
					case 0x0c:
					case 0x0d:
					case 0x0e:
					case 0x0f:
						if (bitfield(smask32, 0, 8))
						{
							m_error.regs_clr(bitfield(sdata32, 0, 8));
						}
						break;
					case 0x20:
						m_page_size = bitfield(data, 0, 5);
						m_enable = bitfield(data, 7);
						break;
				}
				break;
			}
			case 2:
			{
				const u32 sdata32 = u32(data) << (bitfield(BigEndian ? ~addr : addr, 0) << 4);
				const u32 smask32 = u32(mask) << (bitfield(BigEndian ? ~addr : addr, 0) << 4);
				switch (addr & 0x1f)
				{
					case 0x00:
						if (bitfield(mask, 0, 16))
						{
							m_page_addr = (m_page_addr & bitfield(~mask, 0, 16)) | (bitfield(data, 0, 16) & bitfield(mask, 0, 16));
						}
						break;
					case 0x02:
					case 0x03:
						if (bitfield(m_page_addr, 0, 15) < PageEntries)
						{
							m_page[bitfield(m_page_addr, 0, 15)].regs_w(sdata32, smask32);
						}
						if ((m_page_addr & 0x8000) && (bitfield(addr, 0) == 0b1))
						{
							m_page_addr = (m_page_addr & 0x8000) | ((m_page_addr + 1) & 0x7fff);
						}
						break;
					case 0x04:
					case 0x05:
					case 0x06:
					case 0x07:
						if (bitfield(smask32, 0, 8))
						{
							m_error.regs_clr(bitfield(sdata32, 0, 8));
						}
						break;
					case 0x10:
						if (bitfield(mask, 0, 8))
						{
							m_page_size = bitfield(data, 0, 5);
							m_enable = bitfield(data, 7);
						}
						break;
				}
				break;
			}
			case 4:
			{
				switch (addr & 0x0f)
				{
					case 0x00:
						if (bitfield(mask, 0, 16))
						{
							m_page_addr = (m_page_addr & bitfield(~mask, 0, 16)) | (bitfield(data, 0, 16) & bitfield(mask, 0, 16));
						}
						break;
					case 0x01:
						if (bitfield(m_page_addr, 0, 15) < PageEntries)
						{
							m_page[bitfield(m_page_addr, 0, 15)].regs_w(data, mask);
						}
						if (m_page_addr & 0x8000)
						{
							m_page_addr = (m_page_addr & 0x8000) | ((m_page_addr + 1) & 0x7fff);
						}
						break;
					case 0x02:
					case 0x03:
						if (bitfield(mask, 0, 8))
						{
							m_error.regs_clr(bitfield(data, 0, 8));
						}
					case 0x08:
						if (bitfield(mask, 0, 8))
						{
							m_page_size = bitfield(data, 0, 5);
							m_enable = bitfield(data, 7);
						}
						break;
				}
				break;
			}
			case 8:
			{
				switch (addr & 0x7)
				{
					case 0x00:
						if (bitfield(mask, 0, 16))
						{
							m_page_addr = (m_page_addr & bitfield(~mask, 0, 16)) | (bitfield(data, 0, 16) & bitfield(mask, 0, 16));
						}
						break;
					case 0x01:
						if (bitfield(mask, 0, 32))
						{
							if (bitfield(m_page_addr, 0, 15) < PageEntries)
							{
								m_page[bitfield(m_page_addr, 0, 15)].regs_w(bitfield(data, 0, 32), bitfield(mask, 0, 32));
							}
							if (m_page_addr & 0x8000)
							{
								m_page_addr = (m_page_addr & 0x8000) | ((m_page_addr + 1) & 0x7fff);
							}
						}
						break;
					case 0x02:
						if (bitfield(mask, 0, 8))
						{
							m_error.regs_clr(bitfield(data, 0, 8));
						}
						break;
					case 0x04:
						if (bitfield(mask, 0, 8))
						{
							m_page_size = bitfield(data, 0, 5);
							m_enable = bitfield(data, 7);
						}
						break;
				}
				break;
			}
		}
	}

} // namespace jkmmmu
