/*

==============================================================================

    JKMMMU
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMMMU

Fantasy MMU controller

*/

#ifndef JKMMMU_HPP
#define JKMMMU_HPP

#pragma once

#include <array>

namespace jkmmmu
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	template<bool BigEndian>
	class jkmmmu_intf_t
	{
		public:
			jkmmmu_intf_t()
			{
			} // jkmmmu_intf_t

			virtual void irq(bool assert) {}
			virtual bool supervisor_flag() { return true; }
			virtual void error_trap(bool valid, bool read, bool write, bool execute, bool supervisor);

			virtual u8 read_byte(bool supervisor, bool execute, u64 addr) { return 0; }
			u16 read_word(bool supervisor, bool execute, u64 addr)
			{
				return BigEndian ?
					(read_byte(supervisor, execute, addr + 1) | (u16(read_byte(supervisor, execute, addr)) << 8)) : 
					(read_byte(supervisor, execute, addr) | (u16(read_byte(supervisor, execute, addr + 1)) << 8));
			} // read_word
			u32 read_dword(bool supervisor, bool execute, u64 addr)
			{
				return BigEndian ?
					(read_word(supervisor, execute, addr + 2) | (u32(read_word(supervisor, execute, addr)) << 16)) : 
					(read_word(supervisor, execute, addr) | (u32(read_word(supervisor, execute, addr + 2)) << 16));
			} // read_dword
			u32 read_qword(bool supervisor, bool execute, u64 addr)
			{
				return BigEndian ?
					(read_dword(supervisor, execute, addr + 4) | (u64(read_dword(supervisor, execute, addr)) << 32)) : 
					(read_dword(supervisor, execute, addr) | (u64(read_dword(supervisor, execute, addr + 4)) << 32));
			} // read_qword
			virtual void write_byte(bool supervisor, u64 addr, u8 data, u8 mask = ~0) {}
			void write_word(bool supervisor, u64 addr, u16 data, u16 mask = ~0)
			{
				if (BigEndian)
				{
					write_byte(supervisor, addr + 1, bitfield(data, 0, 8), bitfield(mask, 0, 8));
					write_byte(supervisor, addr, bitfield(data, 8, 8), bitfield(mask, 8, 8));
				}
				else
				{
					write_byte(supervisor, addr, bitfield(data, 0, 8), bitfield(mask, 0, 8));
					write_byte(supervisor, addr + 1, bitfield(data, 8, 8), bitfield(mask, 8, 8));
				}
			} // write_word
			void write_dword(bool supervisor, u64 addr, u32 data, u32 mask = ~0)
			{
				if (BigEndian)
				{
					write_word(supervisor, addr + 2, bitfield(data, 0, 16), bitfield(mask, 0, 16));
					write_word(supervisor, addr, bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				else
				{
					write_word(supervisor, addr, bitfield(data, 0, 16), bitfield(mask, 0, 16));
					write_word(supervisor, addr + 2, bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
			} // write_dword
			void write_qword(bool supervisor, u64 addr, u64 data, u64 mask = ~0)
			{
				if (BigEndian)
				{
					write_dword(supervisor, addr + 4, bitfield(data, 0, 32), bitfield(mask, 0, 32));
					write_dword(supervisor, addr, bitfield(data, 32, 32), bitfield(mask, 32, 32));
				}
				else
				{
					write_dword(supervisor, addr, bitfield(data, 0, 32), bitfield(mask, 0, 32));
					write_dword(supervisor, addr + 4, bitfield(data, 32, 32), bitfield(mask, 32, 32));
				}
			} // write_qword

	}; // class jkmmmu_intf_t

	template<bool BigEndian, typename T, std::size_t PageEntries>
	class jkmmmu_t
	{
		private:
			class page_t
			{
				public:
					page_t()
						: m_valid(false)
						, m_read_protect(false)
						, m_write_protect(false)
						, m_execute_protect(false)
						, m_supervisor(false)
						, m_dirty(false)
						, m_reference(false)
						, m_addr(0)
					{
					} // page_t

					void reset()
					{
						m_valid = false;
						m_read_protect = false;
						m_write_protect = false;
						m_execute_protect = false;
						m_supervisor = false;
						m_dirty = false;
						m_reference = false;
						m_addr = 0;
					}

					u64 get_addr(const u32 vaddr, const u8 page_size) const
					{
						return (vaddr & ((1 << page_size) - 1)) | (m_addr << page_size);
					} // get_addr

					bool valid() const
					{
						return m_valid;
					} // valid

					bool read_protect() const
					{
						return m_read_protect;
					} // read_protect

					bool write_protect() const
					{
						return m_write_protect;
					} // write_protect

					bool execute_protect() const
					{
						return m_execute_protect;
					} // execute_protect

					bool supervisor() const
					{
						return m_supervisor;
					} // supervisor

					bool dirty() const
					{
						return m_dirty;
					} // dirty

					bool reference() const
					{
						return m_reference;
					} // reference

					u32 addr() const
					{
						return m_addr;
					} // addr

					void set_valid(const bool valid)
					{
						m_valid = valid;
					} // set_valid

					void set_read_protect(const bool read_protect)
					{
						m_read_protect = read_protect;
					} // set_read_protect

					void set_write_protect(const bool write_protect)
					{
						m_write_protect = write_protect;
					} // set_write_protect

					void set_execute_protect(const bool execute_protect)
					{
						m_execute_protect = execute_protect;
					} // set_execute_protect

					void set_supervisor(const bool supervisor)
					{
						m_supervisor = supervisor;
					} // set_supervisor

					void set_dirty()
					{
						m_dirty = true;
					} // set_dirty

					void set_reference()
					{
						m_reference = true;
					} // set_reference

					void clear_dirty()
					{
						m_dirty = false;
					} // clear_dirty

					void clear_reference()
					{
						m_reference = false;
					} // clear_reference

					void set_addr(const u32 addr, const u32 mask)
					{
						m_addr = (m_addr & ~mask) | (addr & mask);
					} // set_addr

					u32 regs_r() const
					{
						return (valid() ? 0x00000001 : 0)
							| (read_protect() ? 0x00000002 : 0)
							| (write_protect() ? 0x00000004 : 0)
							| (execute_protect() ? 0x00000008 : 0)
							| (supervisor() ? 0x00000010 : 0)
							| (dirty() ? 0x00000020 : 0)
							| (reference() ? 0x00000040 : 0)
							| (addr() << 7);
					}

					void regs_w(const u32 data, const u32 mask = ~0) const
					{
						if (bitfield(mask, 0, 8))
						{
							set_valid(bitfield(data, 0));
							set_read_protect(bitfield(data, 1));
							set_write_protect(bitfield(data, 2));
							set_execute_protect(bitfield(data, 3));
							set_supervisor(bitfield(data, 4));
							set_dirty(bitfield(data, 5));
							set_reference(bitfield(data, 6));
						}
						m_addr = (m_addr & bitfield(~mask, 7, 25))
							| (bitfield(data, 7, 25) & bitfield(mask, 7, 25));
					}

				private:
					bool m_valid = false;
					bool m_read_protect = false;
					bool m_write_protect = false;
					bool m_execute_protect = false;
					bool m_supervisor = false;
					bool m_dirty = false;
					bool m_reference = false;
					u32 m_addr = 0;
			}; // class page_t

			class error_t
			{
				public:
					enum error_code_t : u8
					{
						ERROR_NONE = 0,
						ERROR_INVALID = (1 << 0),
						ERROR_READ_FAIL = (1 << 1),
						ERROR_WRITE_FAIL = (1 << 2),
						ERROR_EXECUTE_FAIL = (1 << 3),
						ERROR_SUPERVISOR = (1 << 4)
					}; // enum error_code_t

					error_t(jkmmmu_t &host)
						: m_host(host)
						, m_invalid(false)
						, m_read_fail(false)
						, m_write_fail(false)
						, m_execute_fail(false)
						, m_supervisor_fail(false)
						, m_page(0)
						, m_vaddr(0)
						, m_paddr(0)
					{
					} // error_t

					void reset()
					{
						m_invalid = false;
						m_read_fail = false;
						m_write_fail = false;
						m_execute_fail = false;
						m_supervisor_fail = false;
						m_page = 0;
						m_vaddr = 0;
						m_paddr = 0;
					} // reset

					void set_error(const error_code_t code, const u16 page, const u32 vaddr, const u64 paddr)
					{
						m_page = page;
						m_vaddr = vaddr;
						m_paddr = paddr;
						m_invalid = (code & ERROR_INVALID);
						m_read_fail = (code & ERROR_READ_FAIL);
						m_write_fail = (code & ERROR_WRITE_FAIL);
						m_execute_fail = (code & ERROR_EXECUTE_FAIL);
						m_supervisor_fail = (code & ERROR_SUPERVISOR);
						m_host.m_intf.error_trap(m_invalid, m_read_fail, m_write_fail, m_execute_fail, m_supervisor_fail);
					} // set_error

					bool invalid() const
					{
						return m_invalid;
					} // invalid

					bool read_fail() const
					{
						return m_read_fail;
					} // read_fail

					bool write_fail() const
					{
						return m_write_fail;
					} // write_fail

					bool execute_fail() const
					{
						return m_execute_fail;
					} // execute_fail

					bool supervisor_fail() const
					{
						return m_supervisor_fail;
					} // supervisor_fail

					u16 page() const
					{
						return m_page;
					} // page

					u32 vaddr() const
					{
						return m_vaddr;
					} // vaddr

					u64 paddr() const
					{
						return m_paddr;
					} // paddr

					u64 regs_r() const
					{
						return (invalid() ? 0x00000001 : 0)
							| (read_fail() ? 0x00000002 : 0)
							| (write_fail() ? 0x00000004 : 0)
							| (execute_fail() ? 0x00000008 : 0)
							| (supervisor_fail() ? 0x00000010 : 0)
							| (page() << 16)
							| (vaddr() << 32);
					}

					void regs_clr(const u8 data) const
					{
						if (bitfield(data, 0))
						{
							m_invalid = false;
						}
						if (bitfield(data, 1))
						{
							m_read_fail = false;
						}
						if (bitfield(data, 2))
						{
							m_write_fail = false;
						}
						if (bitfield(data, 3))
						{
							m_execute_fail = false;
						}
						if (bitfield(data, 4))
						{
							m_supervisor_fail = false;
						}
						m_host.m_intf.error_trap(false, false, false, false, false);
					}

				private:
					jkmmmu_t &m_host;
					bool m_invalid = false;
					bool m_read_fail = false;
					bool m_write_fail = false;
					bool m_execute_fail = false;
					bool m_supervisor_fail = false;
					u16 m_page = 0;
					u32 m_vaddr = 0;
					u64 m_paddr = 0;
			}; // class error_t
		public:
			jkmmmu_t(jkmmmu_intf_t<BigEndian> &intf, const u64 init_mask)
				: m_intf(intf)
				, m_page{page_t()}
				, m_error(*this)
				, m_page_addr(0)
				, m_page_size(9)
				, m_enable(false)
				, m_init_mask(init_mask)
			{
			} // jkmmmu_t

			void reset();

			T mmu_r(const bool execute, const u32 addr);
			void mmu_w(const u32 addr, const T data, const T mask);

			T host_r(const u8 addr, const T mask = T(~0));
			void host_w(const u8 addr, const T data, const T mask = T(~0));

			u8 page_size() const
			{
				return m_page_size;
			} // page_size
		private:
			jkmmmu_intf_t<BigEndian> &m_intf; // Interface
			std::array<page_t, PageEntries> m_page;
			error_t m_error;
			u16 m_page_addr = 0;      // Page table address
			u8 m_page_size = 9;       // Page size
			bool m_enable = false;    // MMU translation enable
			const u64 m_init_mask = 0; // Initial address mask if mmu disabled
	}; // class jkmmmu_t
} // namespace jkmmmu

#endif // JKMMMU_HPP
