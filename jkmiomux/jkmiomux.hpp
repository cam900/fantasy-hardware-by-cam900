/*

==============================================================================

    JKMIOMUX
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMIOMUX

Fantasy I/O Multiplexer

*/

#ifndef JKMIOMUX_HPP
#define JKMIOMUX_HPP

#pragma once

#include <array>
#include <utility>

namespace jkmiomux
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	template<typename T>
	class jkmiomux_intf_t
	{
		public:
			jkmiomux_intf_t()
			{
			} // jkmiomux_intf_t

			virtual T io_in(u8 slot, T dir) { return 0; }
			virtual void io_out(u8 slot, T data, T dir) {}
	}; // class jkmiomux_intf_t

	template<typename T>
	class jkmiomux_t
	{
		private:
			class io_t
			{
				public:
					io_t(jkmiomux_t &host, const u8 slot, const T _3state)
						: m_host(host)
						, m_slot(slot)
						, m_3state(_3state)
						, m_dir(0)
						, m_data(0)
					{
					} // io_t

					void reset()
					{
						m_dir = 0;
						m_data = 0;
					} // reset

					T io_r(const bool is_dir) const
					{
						if (is_dir)
						{
							return m_dir;
						}
						else
						{
							T data = m_data;
							const T dir = m_dir;
							if (dir != 0xff)
							{
								data = (data & dir) | (m_host.m_intf.io_in(m_slot, T(~dir)) & ~dir);
							}
							return data;
						}
					} // io_r

					void io_w(const bool is_dir, const T data)
					{
						T dir = m_dir;
						if (is_dir)
						{
							if (dir == data)
							{
								return;
							}
							m_dir = dir = data;
						}
						else
						{
							T old_data = std::exchange<T>(m_data, data);
							if (((old_data ^ data) & dir) == 0)
							{
								return;
							}
						}

						m_host.m_intf.io_out(m_slot, (data & dir) | (m_3state & ~dir), dir);
					} // io_w

				private:
					jkmiomux_t &m_host; // Host
					const u8 m_slot;    // Slot number
					const T m_3state;   // 3 state
					T m_dir = 0;        // I/O Direction
					T m_data = 0;       // I/O Data
			}; // class io_t

		public:
			jkmiomux_t(jkmiomux_intf_t<T> &intf, std::array<T, 8> _3state)
				: m_intf(intf)
				, m_io{
					io_t(*this, 0, _3state[0]),
					io_t(*this, 1, _3state[1]),
					io_t(*this, 2, _3state[2]),
					io_t(*this, 3, _3state[3]),
					io_t(*this, 4, _3state[4]),
					io_t(*this, 5, _3state[5]),
					io_t(*this, 6, _3state[6]),
					io_t(*this, 7, _3state[7]),
				}
				, m_slot(0)
			{
			} // jkmiomux_t

			void reset();

			T host_r(const u8 addr) const;
			void host_w(const u8 addr, const T data);

		private:
			jkmiomux_intf_t<T> &m_intf; // Interface
			std::array<io_t, 8> m_io;   // I/O ports
			u8 m_slot = 0;              // Register slot
	}; // class jkmiomux_t
} // namespace jkmiomux

#endif // JKMIOMUX_HPP
