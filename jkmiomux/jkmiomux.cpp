/*

==============================================================================

    JKMIOMUX
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMIOMUX

Fantasy I/O Multiplexer

Features:
- 8 8/16/32/64 bit I/O slots, Configurable direction, 3 State

	Host interface

	Address Description
	00      I/O Port select (Bit 0 to 2), Direction/Data (Bit 7)
	01      Register data
*/

#include "jkmiomux.hpp"

namespace jkmiomux
{
	template<typename T>
	void jkmiomux_t<T>::reset()
	{
		for (io_t &io : m_io)
		{
			io.reset();
		}
		m_slot = 0;
	} // jkmiomux_t<T>::reset

	template<typename T>
	T jkmiomux_t<T>::host_r(const u8 addr) const
	{
		if (bitfield(addr, 0)) // Data
		{
			return m_io[bitfield(m_slot, 0, 3)].io_r(bitfield(m_slot, 7));
		}
		else // Slot
		{
			return m_slot;
		}
	} // jkmiomux_t<T>::host_r

	template<typename T>
	void jkmiomux_t<T>::host_w(const u8 addr, const T data)
	{
		if (bitfield(addr, 0)) // Data
		{
			m_io[bitfield(m_slot, 0, 3)].io_w(bitfield(m_slot, 7), data);
		}
		else // Slot
		{
			m_slot = bitfield(data, 0, 8);
		}
	} // jkmiomux_t<T>::host_w

} // namespace jkmiomux
