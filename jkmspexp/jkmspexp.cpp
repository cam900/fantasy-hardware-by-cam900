/*

==============================================================================

    JKMSPEXP
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMSPEXP

Fantasy Physical address expander

Features:
- 8 slot of Memory address controller
- Up to 64 bit physical address space
- DMA controller

	Host interface

	Address Description
	00      Register select (Bit 4 to 6), Bit select (Bit 0 to 2)
	01      Register data

	Register map
	Address Description
	00      Controller slot select (Bit 0 to 2), Enable (Bit 7)
	10-17   Address compare check mask
	20-27   Address compare result mask
	30-37   Address AND
	40-47   Address OR
	50      Slot control
	        Wait state (Bit 0 to 5)
	        Write enable (Bit 6)
	        Slot enable (Bit 7)
	60-67   DMA Source address
	70-77   DMA Target address
	80-87   DMA Counter
	90      DMA Control
	        Transfer size (Bit 0 to 1)
	        Fill (Bit 2)
	        Source address Increment/Decrement (Bit 3)
	        Source address Increment/Decrement enable (Bit 4)
	        Target address Increment/Decrement (Bit 5)
	        Target address Increment/Decrement enable (Bit 6)
	        Busy/Run (Bit 7)
	a0      DMA Source wait state
	b0      DMA Target wait state

	Address calculate formula:
	slot priority: 0 < 1 < 2 < ... < 7
	if ((Virtual address & Address compare check mask) == Address compare result mask)
		then Physical address = (Virtual address & Address AND) | Address OR
*/

#include "jkmspexp.hpp"

namespace jkmspexp
{
	template<bool BigEndian, typename T>
	void jkmspexp_t<BigEndian, T>::dma_t::run()
	{
		if (!m_busy)
		{
			m_host.m_intf.halt();
			m_buffer = 0;
			m_remain = m_swait;
			m_state = false;
			m_busy = true;
		}
	}

	template<bool BigEndian, typename T>
	void jkmspexp_t<BigEndian, T>::dma_t::tick()
	{
		if (m_busy)
		{
			if (m_remain-- == 0)
			{
				if (m_state)
				{
					if (m_counter == 0)
					{
						m_busy = false;
						m_host.m_intf.resume();
						m_host.m_intf.dma_irq(true);
						return;
					}
					switch (m_size)
					{
						case 0:
							m_buffer = m_fill ? (m_source & 0xff)
									: m_host.m_intf.read_byte(m_source);
							break;
						case 1:
							m_buffer = m_fill ? (m_source & 0xffff)
									: m_host.m_intf.read_word(m_source);
							break;
						case 2:
							m_buffer = m_fill ? (m_source & 0xffffffff)
									: m_host.m_intf.read_dword(m_source);
							break;
						case 3:
							m_buffer = m_fill ? (m_source & 0xffffffffffffffff)
									: m_host.m_intf.read_qword(m_source);
							break;
					}
					if (m_source_incdec_enable)
					{
						switch (m_size)
						{
							case 0:
								m_source = m_source + (m_source_incdec ? -1 : 1);
								break;
							case 1:
								m_source = m_source + (m_source_incdec ? -2 : 2);
								break;
							case 2:
								m_source = m_source + (m_source_incdec ? -4 : 4);
								break;
							case 3:
								m_source = m_source + (m_source_incdec ? -8 : 8);
								break;
						}
					}
					m_remain = m_swait;
				}
				else
				{
					switch (m_size)
					{
						case 0:
							m_host.m_intf.write_byte(m_target, m_buffer);
							break;
						case 1:
							m_host.m_intf.write_word(m_target, m_buffer);
							break;
						case 2:
							m_host.m_intf.write_dword(m_target, m_buffer);
							break;
						case 3:
							m_host.m_intf.write_qword(m_target, m_buffer);
							break;
					}
					if (m_target_incdec_enable)
					{
						switch (m_size)
						{
							case 0:
								m_target = m_target + (m_target_incdec ? -1 : 1);
								break;
							case 1:
								m_target = m_target + (m_target_incdec ? -2 : 2);
								break;
							case 2:
								m_target = m_target + (m_target_incdec ? -4 : 4);
								break;
							case 3:
								m_target = m_target + (m_target_incdec ? -8 : 8);
								break;
						}
					}
					m_remain = m_twait;
					m_counter--;
				}
			}
			m_state = !m_state;
		}
	}

	template<bool BigEndian, typename T>
	void jkmspexp_t<BigEndian, T>::reset()
	{
		for (regs_t &reg : m_regs)
		{
			reg.reset();
		}
		m_dma.reset();
		m_slot = 0;
		m_reg = 0;
		m_enable = false;
	} // jkmspexp_t::reset

	template<bool BigEndian, typename T>
	u64 jkmspexp_t<BigEndian, T>::get_addr(const u64 addr, bool &wp, u64 &wait, bool &exists)
	{
		u64 paddr = addr;
		wp = true;
		exists = false;
		wait = 0x3f;
		for (u8 i = 0; i < 8; i++)
		{
			regs_t &reg = m_regs[i];
			if (reg.enable())
			{
				if (reg.compare(addr))
				{
					paddr = reg.get_addr(addr);
					wp = reg.wp();
					wait = reg.wait();
					exists = true;
				}
			}
		}
		return paddr;
	} // jkmspexp_t<BigEndian, T>::get_addr

	template<bool BigEndian, typename T>
	T jkmspexp_t<BigEndian, T>::host_r(const u64 addr) const
	{
		T ret = 0;
		bool wp = true;
		bool exists = false;
		u64 wait = 0x3f;
		if (m_enable)
		{
			const u64 paddr = get_addr(addr * sizeof(T), wp, exists, wait);
			m_intf.wait_state(wait);
			if (exists)
			{
				switch (sizeof(T))
				{
					case 1:
						ret = m_intf.read_byte(paddr);
						break;
					case 2:
						ret = m_intf.read_word(paddr);
						break;
					case 4:
						ret = m_intf.read_dword(paddr);
						break;
					case 8:
						ret = m_intf.read_qword(paddr);
						break;
				}
			}
			else
			{
				m_intf.nonexists_trigger(addr);
			}
		}
		else
		{
			const u64 paddr = (addr * sizeof(T)) | m_init_mask;
			m_intf.wait_state(wait);
			switch (sizeof(T))
			{
				case 1:
					ret = m_intf.read_byte(paddr);
					break;
				case 2:
					ret = m_intf.read_word(paddr);
					break;
				case 4:
					ret = m_intf.read_dword(paddr);
					break;
				case 8:
					ret = m_intf.read_qword(paddr);
					break;
			}
		}
		return ret;
	} // jkmspexp_t<BigEndian, T>::host_r

	template<bool BigEndian, typename T>
	void jkmspexp_t<BigEndian, T>::host_w(const u64 addr, const T data, const T mask)
	{
		bool wp = true;
		bool exists = false;
		u64 wait = 0x3f;
		if (m_enable)
		{
			const u64 paddr = get_addr(addr * sizeof(T), wp, exists, wait);
			m_intf.wait_state(wait);
			if (exists)
			{
				if (!wp)
				{
					switch (sizeof(T))
					{
						case 1:
							m_intf.write_byte(paddr, data, mask);
							break;
						case 2:
							m_intf.write_word(paddr, data, mask);
							break;
						case 4:
							m_intf.write_dword(paddr, data, mask);
							break;
						case 8:
							m_intf.write_qword(paddr, data, mask);
							break;
					}
				}
				else
				{
					m_intf.wp_trigger(addr, paddr);
				}
			}
			else
			{
				m_intf.nonexists_trigger(addr);
			}
		}
		else
		{
			const u64 paddr = (addr * sizeof(T)) | m_init_mask;
			m_intf.wait_state(wait);
			switch (sizeof(T))
			{
				case 1:
					m_intf.write_byte(paddr, data, mask);
					break;
				case 2:
					m_intf.write_word(paddr, data, mask);
					break;
				case 4:
					m_intf.write_dword(paddr, data, mask);
					break;
				case 8:
					m_intf.write_qword(paddr, data, mask);
					break;
			}
		}
	} // jkmspexp_t<BigEndian, T>::host_w

	template<bool BigEndian, typename T>
	T jkmspexp_t<BigEndian, T>::regs_r(const u8 addr) const
	{
		if (bitfield(addr, 0)) // Data
		{
			const T rmask = T(~0);
			regs_t &reg = m_regs[bitfield(m_slot, 0, 3)];
			switch (sizeof(T))
			{
				case 1:
				{
					const u8 shift = (bitfield(BigEndian ? ~m_reg : m_reg, 0, 3) << 3);
					switch (m_reg)
					{
						case 0:
							return m_slot | (m_enable ? 0x80 : 0);
							break;
						case 0x10:
						case 0x11:
						case 0x12:
						case 0x13:
						case 0x14:
						case 0x15:
						case 0x16:
						case 0x17:
							return (reg.acomp() >> shift) & rmask;
							break;
						case 0x20:
						case 0x21:
						case 0x22:
						case 0x23:
						case 0x24:
						case 0x25:
						case 0x26:
						case 0x27:
							return (reg.amask() >> shift) & rmask;
							break;
						case 0x30:
						case 0x31:
						case 0x32:
						case 0x33:
						case 0x34:
						case 0x35:
						case 0x36:
						case 0x37:
							return (reg.aand() >> shift) & rmask;
							break;
						case 0x40:
						case 0x41:
						case 0x42:
						case 0x43:
						case 0x44:
						case 0x45:
						case 0x46:
						case 0x47:
							return (reg.aor() >> shift) & rmask;
							break;
						case 0x50:
							return reg.wait() | (reg.wp() ? 0x40 : 0) | (reg.enable() ? 0x80 : 0);
							break;
						case 0x60:
						case 0x61:
						case 0x62:
						case 0x63:
						case 0x64:
						case 0x65:
						case 0x66:
						case 0x67:
							return (m_dma.source() >> shift) & rmask;
							break;
						case 0x70:
						case 0x71:
						case 0x72:
						case 0x73:
						case 0x74:
						case 0x75:
						case 0x76:
						case 0x77:
							return (m_dma.target() >> shift) & rmask;
							break;
						case 0x80:
						case 0x81:
						case 0x82:
						case 0x83:
						case 0x84:
						case 0x85:
						case 0x86:
						case 0x87:
							return (m_dma.counter() >> shift) & rmask;
							break;
						case 0x90:
							return m_dma.size()
								| (m_dma.fill() ? 0x04 : 0x00)
								| (m_dma.source_incdec() ? 0x08 : 0x00)
								| (m_dma.source_incdec_enable() ? 0x10 : 0x00)
								| (m_dma.target_incdec() ? 0x20 : 0x00)
								| (m_dma.target_incdec_enable() ? 0x40 : 0x00)
								| (m_dma.busy() ? 0x80 : 0x00);
							break;
						case 0xa0:
							return m_dma.swait();
							break;
						case 0xb0:
							return m_dma.twait();
							break;
					}
					break;
				}
				case 2:
				{
					const u8 shift = bitfield(BigEndian ? ~m_reg : m_reg, 0, 2) << 4;
					switch (m_reg)
					{
						case 0:
							return m_slot | (m_enable ? 0x80 : 0);
							break;
						case 0x10:
						case 0x11:
						case 0x12:
						case 0x13:
							return (reg.acomp() >> shift) & rmask;
							break;
						case 0x20:
						case 0x21:
						case 0x22:
						case 0x23:
							return (reg.amask() >> shift) & rmask;
							break;
						case 0x30:
						case 0x31:
						case 0x32:
						case 0x33:
							return (reg.aand() >> shift) & rmask;
							break;
						case 0x40:
						case 0x41:
						case 0x42:
						case 0x43:
							return (reg.aor() >> shift) & rmask;
							break;
						case 0x50:
							return reg.wait() | (reg.wp() ? 0x40 : 0) | (reg.enable() ? 0x80 : 0);
							break;
						case 0x60:
						case 0x61:
						case 0x62:
						case 0x63:
							return (m_dma.source() >> shift) & rmask;
							break;
						case 0x70:
						case 0x71:
						case 0x72:
						case 0x73:
							return (m_dma.target() >> shift) & rmask;
							break;
						case 0x80:
						case 0x81:
						case 0x82:
						case 0x83:
							return (m_dma.counter() >> shift) & rmask;
							break;
						case 0x90:
							return m_dma.size()
								| (m_dma.fill() ? 0x04 : 0x00)
								| (m_dma.source_incdec() ? 0x08 : 0x00)
								| (m_dma.source_incdec_enable() ? 0x10 : 0x00)
								| (m_dma.target_incdec() ? 0x20 : 0x00)
								| (m_dma.target_incdec_enable() ? 0x40 : 0x00)
								| (m_dma.busy() ? 0x80 : 0x00);
							break;
						case 0xa0:
							return m_dma.swait();
							break;
						case 0xb0:
							return m_dma.twait();
							break;
					}
					break;
				}
				case 4:
				{
					const u8 shift = bitfield(BigEndian ? ~m_reg : m_reg, 0, 1) << 5;
					switch (m_reg)
					{
						case 0:
							return m_slot | (m_enable ? 0x80 : 0);
							break;
						case 0x10:
						case 0x11:
							return (reg.acomp() >> shift) & rmask;
							break;
						case 0x20:
						case 0x21:
							return (reg.amask() >> shift) & rmask;
							break;
						case 0x30:
						case 0x31:
							return (reg.aand() >> shift) & rmask;
							break;
						case 0x40:
						case 0x41:
							return (reg.aor() >> shift) & rmask;
							break;
						case 0x50:
							return reg.wait() | (reg.wp() ? 0x40 : 0) | (reg.enable() ? 0x80 : 0);
							break;
						case 0x60:
						case 0x61:
							return (m_dma.source() >> shift) & rmask;
							break;
						case 0x70:
						case 0x71:
							return (m_dma.target() >> shift) & rmask;
							break;
						case 0x80:
						case 0x81:
							return (m_dma.counter() >> shift) & rmask;
							break;
						case 0x90:
							return m_dma.size()
								| (m_dma.fill() ? 0x04 : 0x00)
								| (m_dma.source_incdec() ? 0x08 : 0x00)
								| (m_dma.source_incdec_enable() ? 0x10 : 0x00)
								| (m_dma.target_incdec() ? 0x20 : 0x00)
								| (m_dma.target_incdec_enable() ? 0x40 : 0x00)
								| (m_dma.busy() ? 0x80 : 0x00);
							break;
						case 0xa0:
							return m_dma.swait();
							break;
						case 0xb0:
							return m_dma.twait();
							break;
					}
					break;
				}
				case 8:
					switch (m_reg)
					{
						case 0:
							return m_slot | (m_enable ? 0x80 : 0);
							break;
						case 0x10:
							return reg.acomp();
							break;
						case 0x20:
							return reg.amask();
							break;
						case 0x30:
							return reg.aand();
							break;
						case 0x40:
							return reg.aor();
							break;
						case 0x50:
							return reg.wait() | (reg.wp() ? 0x40 : 0) | (reg.enable() ? 0x80 : 0);
							break;
						case 0x60:
							return m_dma.source();
							break;
						case 0x70:
							return m_dma.target();
							break;
						case 0x80:
							return m_dma.counter();
							break;
						case 0x90:
							return m_dma.size()
								| (m_dma.fill() ? 0x04 : 0x00)
								| (m_dma.source_incdec() ? 0x08 : 0x00)
								| (m_dma.source_incdec_enable() ? 0x10 : 0x00)
								| (m_dma.target_incdec() ? 0x20 : 0x00)
								| (m_dma.target_incdec_enable() ? 0x40 : 0x00)
								| (m_dma.busy() ? 0x80 : 0x00);
							break;
						case 0xa0:
							return m_dma.swait();
							break;
						case 0xb0:
							return m_dma.twait();
							break;
					}
					break;
			}
			return 0;
		}
		else // Register
		{
			return m_reg;
		}
	} // jkmspexp_t<BigEndian, T>::regs_r

	template<bool BigEndian, typename T>
	void jkmspexp_t<BigEndian, T>::regs_w(const u8 addr, const T data, const T mask)
	{
		if (bitfield(addr, 0)) // Data
		{
			regs_t &reg = m_regs[bitfield(m_slot, 0, 3)];
			switch (sizeof(T))
			{
				case 1:
				{
					const u64 sdata = u64(data) << (bitfield(BigEndian ? ~m_reg : m_reg, 0, 3) << 3);
					const u64 smask = u64(mask) << (bitfield(BigEndian ? ~m_reg : m_reg, 0, 3) << 3);
					switch (m_reg)
					{
						case 0:
							if (bitfield(mask, 0, 8))
							{
								m_slot = bitfield(data, 0, 3);
								m_enable = bitfield(data, 7);
							}
							break;
						case 0x10:
						case 0x11:
						case 0x12:
						case 0x13:
						case 0x14:
						case 0x15:
						case 0x16:
						case 0x17:
							reg.set_acomp(sdata, smask);
							break;
						case 0x20:
						case 0x21:
						case 0x22:
						case 0x23:
						case 0x24:
						case 0x25:
						case 0x26:
						case 0x27:
							reg.set_amask(sdata, smask);
							break;
						case 0x30:
						case 0x31:
						case 0x32:
						case 0x33:
						case 0x34:
						case 0x35:
						case 0x36:
						case 0x37:
							reg.set_aand(sdata, smask);
							break;
						case 0x40:
						case 0x41:
						case 0x42:
						case 0x43:
						case 0x44:
						case 0x45:
						case 0x46:
						case 0x47:
							reg.set_aor(sdata, smask);
							break;
						case 0x50:
							if (bitfield(mask, 0, 8))
							{
								reg.set_wait(bitfield(data, 0, 6));
								reg.set_wp(bitfield(data, 6));
								reg.set_enable(bitfield(data, 7));
							}
							break;
						case 0x60:
						case 0x61:
						case 0x62:
						case 0x63:
						case 0x64:
						case 0x65:
						case 0x66:
						case 0x67:
							m_dma.set_source(sdata, smask);
							break;
						case 0x70:
						case 0x71:
						case 0x72:
						case 0x73:
						case 0x74:
						case 0x75:
						case 0x76:
						case 0x77:
							m_dma.set_target(sdata, smask);
							break;
						case 0x80:
						case 0x81:
						case 0x82:
						case 0x83:
						case 0x84:
						case 0x85:
						case 0x86:
						case 0x87:
							m_dma.set_counter(sdata, smask);
							break;
						case 0x90:
							if (bitfield(mask, 0, 8))
							{
								m_dma.set_size(bitfield(data, 0, 2));
								m_dma.set_fill(bitfield(data, 2));
								m_dma.set_source_incdec(bitfield(data, 3));
								m_dma.set_source_incdec_enable(bitfield(data, 4));
								m_dma.set_target_incdec(bitfield(data, 5));
								m_dma.set_target_incdec_enable(bitfield(data, 6));
								if (bitfield(data, 7))
								{
									m_dma.run();
								}
								else
								{
									m_intf.dma_irq(false);
								}
							}
							break;
						case 0xa0:
							if (bitfield(mask, 0, 8))
							{
								m_dma.set_swait(bitfield(data, 0, 6));
							}
							break;
						case 0xb0:
							if (bitfield(mask, 0, 8))
							{
								m_dma.set_twait(bitfield(data, 0, 6));
							}
							break;
					}
					break;
				}
				case 2:
				{
					const u64 sdata = u64(data) << (bitfield(BigEndian ? ~m_reg : m_reg, 0, 2) << 4);
					const u64 smask = u64(mask) << (bitfield(BigEndian ? ~m_reg : m_reg, 0, 2) << 4);
					switch (m_reg)
					{
						case 0:
							if (bitfield(mask, 0, 8))
							{
								m_slot = bitfield(data, 0, 3);
								m_enable = bitfield(data, 7);
							}
							break;
						case 0x10:
						case 0x11:
						case 0x12:
						case 0x13:
							reg.set_acomp(sdata, smask);
							break;
						case 0x20:
						case 0x21:
						case 0x22:
						case 0x23:
							reg.set_amask(sdata, smask);
							break;
						case 0x30:
						case 0x31:
						case 0x32:
						case 0x33:
							reg.set_aand(sdata, smask);
							break;
						case 0x40:
						case 0x41:
						case 0x42:
						case 0x43:
							reg.set_aor(sdata, smask);
							break;
						case 0x50:
							if (bitfield(mask, 0, 8))
							{
								reg.set_wait(bitfield(data, 0, 6));
								reg.set_wp(bitfield(data, 6));
								reg.set_enable(bitfield(data, 7));
							}
							break;
						case 0x60:
						case 0x61:
						case 0x62:
						case 0x63:
							m_dma.set_source(sdata, smask);
							break;
						case 0x70:
						case 0x71:
						case 0x72:
						case 0x73:
							m_dma.set_target(sdata, smask);
							break;
						case 0x80:
						case 0x81:
						case 0x82:
						case 0x83:
							m_dma.set_counter(sdata, smask);
							break;
						case 0x90:
							if (bitfield(mask, 0, 8))
							{
								m_dma.set_size(bitfield(data, 0, 2));
								m_dma.set_fill(bitfield(data, 2));
								m_dma.set_source_incdec(bitfield(data, 3));
								m_dma.set_source_incdec_enable(bitfield(data, 4));
								m_dma.set_target_incdec(bitfield(data, 5));
								m_dma.set_target_incdec_enable(bitfield(data, 6));
								if (bitfield(data, 7))
								{
									m_dma.run();
								}
								else
								{
									m_intf.dma_irq(false);
								}
							}
							break;
						case 0xa0:
							if (bitfield(mask, 0, 8))
							{
								m_dma.set_swait(bitfield(data, 0, 6));
							}
							break;
						case 0xb0:
							if (bitfield(mask, 0, 8))
							{
								m_dma.set_twait(bitfield(data, 0, 6));
							}
							break;
					}
					break;
				}
				case 4:
				{
					const u64 sdata = u64(data) << (bitfield(BigEndian ? ~m_reg : m_reg, 0, 1) << 5);
					const u64 smask = u64(mask) << (bitfield(BigEndian ? ~m_reg : m_reg, 0, 1) << 5);
					switch (m_reg)
					{
						case 0:
							if (bitfield(mask, 0, 8))
							{
								m_slot = bitfield(data, 0, 3);
								m_enable = bitfield(data, 7);
							}
							break;
						case 0x10:
						case 0x11:
							reg.set_acomp(sdata, smask);
							break;
						case 0x20:
						case 0x21:
							reg.set_amask(sdata, smask);
							break;
						case 0x30:
						case 0x31:
							reg.set_aand(sdata, smask);
							break;
						case 0x40:
						case 0x41:
							reg.set_aor(sdata, smask);
							break;
						case 0x50:
							if (bitfield(mask, 0, 8))
							{
								reg.set_wait(bitfield(data, 0, 6));
								reg.set_wp(bitfield(data, 6));
								reg.set_enable(bitfield(data, 7));
							}
							break;
						case 0x60:
						case 0x61:
							m_dma.set_source(sdata, smask);
							break;
						case 0x70:
						case 0x71:
							m_dma.set_target(sdata, smask);
							break;
						case 0x80:
						case 0x81:
							m_dma.set_counter(sdata, smask);
							break;
						case 0x90:
							if (bitfield(mask, 0, 8))
							{
								m_dma.set_size(bitfield(data, 0, 2));
								m_dma.set_fill(bitfield(data, 2));
								m_dma.set_source_incdec(bitfield(data, 3));
								m_dma.set_source_incdec_enable(bitfield(data, 4));
								m_dma.set_target_incdec(bitfield(data, 5));
								m_dma.set_target_incdec_enable(bitfield(data, 6));
								if (bitfield(data, 7))
								{
									m_dma.run();
								}
								else
								{
									m_intf.dma_irq(false);
								}
							}
							break;
						case 0xa0:
							if (bitfield(mask, 0, 8))
							{
								m_dma.set_swait(bitfield(data, 0, 6));
							}
							break;
						case 0xb0:
							if (bitfield(mask, 0, 8))
							{
								m_dma.set_twait(bitfield(data, 0, 6));
							}
							break;
					}
					break;
				}
				case 8:
				{
					switch (m_reg)
					{
						case 0:
							if (bitfield(mask, 0, 8))
							{
								m_slot = bitfield(data, 0, 3);
								m_enable = bitfield(data, 7);
							}
							break;
						case 0x10:
							reg.set_acomp(data, mask);
							break;
						case 0x20:
							reg.set_amask(data, mask);
							break;
						case 0x30:
							reg.set_aand(data, mask);
							break;
						case 0x40:
							reg.set_aor(data, mask);
							break;
						case 0x50:
							if (bitfield(mask, 0, 8))
							{
								reg.set_wait(bitfield(data, 0, 6));
								reg.set_wp(bitfield(data, 6));
								reg.set_enable(bitfield(data, 7));
							}
							break;
						case 0x60:
							m_dma.set_source(data, mask);
							break;
						case 0x70:
							m_dma.set_target(data, mask);
							break;
						case 0x80:
							m_dma.set_counter(data, mask);
							break;
						case 0x90:
							if (bitfield(mask, 0, 8))
							{
								m_dma.set_size(bitfield(data, 0, 2));
								m_dma.set_fill(bitfield(data, 2));
								m_dma.set_source_incdec(bitfield(data, 3));
								m_dma.set_source_incdec_enable(bitfield(data, 4));
								m_dma.set_target_incdec(bitfield(data, 5));
								m_dma.set_target_incdec_enable(bitfield(data, 6));
								if (bitfield(data, 7))
								{
									m_dma.run();
								}
								else
								{
									m_intf.dma_irq(false);
								}
							}
							break;
						case 0xa0:
							if (bitfield(mask, 0, 8))
							{
								m_dma.set_swait(bitfield(data, 0, 6));
							}
							break;
						case 0xb0:
							if (bitfield(mask, 0, 8))
							{
								m_dma.set_twait(bitfield(data, 0, 6));
							}
							break;
					}
					break;
				}
			}
		}
		else // Register
		{
			if (bitfield(mask, 0, 8))
			{
				m_reg = bitfield(data, 0, 8);
			}
		}
	} // jkmspexp_t<BigEndian, T>::regs_w

} // namespace jkmspexp
