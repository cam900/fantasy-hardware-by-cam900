/*

==============================================================================

    JKMSPEXP
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMSPEXP

Fantasy Physical address expander

*/

#ifndef JKMSPEXP_HPP
#define JKMSPEXP_HPP

#pragma once

#include <array>

namespace jkmspexp
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	template<bool BigEndian>
	class jkmspexp_intf_t
	{
		public:
			jkmspexp_intf_t()
			{
			} // jkmspexp_intf_t

			virtual void wp_trigger(u64 addr, u64 paddr) {}
			virtual void nonexists_trigger(u64 addr) {}
			virtual void wait_state(u64 cycle) {}
			virtual void halt() {}
			virtual void resume() {}
			virtual void dma_irq(bool assert) {}

			virtual u8 read_byte(u64 addr) { return 0; }
			u16 read_word(u64 addr)
			{
				return BigEndian ?
					(read_byte(addr + 1) | (u16(read_byte(addr)) << 8)) : 
					(read_byte(addr) | (u16(read_byte(addr + 1)) << 8));
			} // read_word
			u32 read_dword(u64 addr)
			{
				return BigEndian ?
					(read_word(addr + 2) | (u32(read_word(addr)) << 16)) : 
					(read_word(addr) | (u32(read_word(addr + 2)) << 16));
			} // read_dword
			u32 read_qword(u64 addr)
			{
				return BigEndian ?
					(read_dword(addr + 4) | (u64(read_dword(addr)) << 32)) : 
					(read_dword(addr) | (u64(read_dword(addr + 4)) << 32));
			} // read_qword
			virtual void write_byte(u64 addr, u8 data, u8 mask = ~0) {}
			void write_word(u64 addr, u16 data, u16 mask = ~0)
			{
				if (BigEndian)
				{
					write_byte(addr + 1, bitfield(data, 0, 8), bitfield(mask, 0, 8));
					write_byte(addr, bitfield(data, 8, 8), bitfield(mask, 8, 8));
				}
				else
				{
					write_byte(addr, bitfield(data, 0, 8), bitfield(mask, 0, 8));
					write_byte(addr + 1, bitfield(data, 8, 8), bitfield(mask, 8, 8));
				}
			} // write_word
			void write_dword(u64 addr, u32 data, u32 mask = ~0)
			{
				if (BigEndian)
				{
					write_word(addr + 2, bitfield(data, 0, 16), bitfield(mask, 0, 16));
					write_word(addr, bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
				else
				{
					write_word(addr, bitfield(data, 0, 16), bitfield(mask, 0, 16));
					write_word(addr + 2, bitfield(data, 16, 16), bitfield(mask, 16, 16));
				}
			} // write_dword
			void write_qword(u64 addr, u64 data, u64 mask = ~0)
			{
				if (BigEndian)
				{
					write_dword(addr + 4, bitfield(data, 0, 32), bitfield(mask, 0, 32));
					write_dword(addr, bitfield(data, 32, 32), bitfield(mask, 32, 32));
				}
				else
				{
					write_dword(addr, bitfield(data, 0, 32), bitfield(mask, 0, 32));
					write_dword(addr + 4, bitfield(data, 32, 32), bitfield(mask, 32, 32));
				}
			} // write_qword

	}; // class jkmspexp_intf_t

	template<bool BigEndian, typename T>
	class jkmspexp_t
	{
		private:
			class regs_t
			{
				public:
					regs_t()
						: m_acomp(0)
						, m_amask(0)
						, m_aand(~0)
						, m_aor(0)
						, m_wait(0x3f)
						, m_wp(false)
						, m_enable(false)
					{
					} // regs_t

					void reset()
					{
						m_acomp = 0;
						m_amask = 0;
						m_aand = ~0;
						m_aor = 0;
						m_wait = 0x3f;
						m_wp = false;
						m_enable = true;
					} // reset

					bool compare(const u64 addr) const
					{
						return (addr & m_acomp) == m_amask;
					} // compare

					u64 get_addr(const u64 addr) const
					{
						return (addr & m_aand) | m_aor;
					} // get_addr

					u64 acomp() const
					{
						return m_acomp;
					} // acomp

					u64 amask() const
					{
						return m_amask;
					} // amask

					u64 aand() const
					{
						return m_aand;
					} // aand

					u64 aor() const
					{
						return m_aor;
					} // aor

					u8 wait() const
					{
						return m_wait;
					} // wait

					bool wp() const
					{
						return m_wp;
					} // wp

					bool enable() const
					{
						return m_enable;
					} // enable

					void set_acomp(const u64 acomp, const u64 mask)
					{
						m_acomp = (m_acomp & ~mask) | (acomp & mask);
					} // set_acomp

					void set_amask(const u64 amask, const u64 mask)
					{
						m_amask = (m_amask & ~mask) | (amask & mask);
					} // set_amask

					void set_aand(const u64 aand, const u64 mask)
					{
						m_aand = (m_aand & ~mask) | (aand & mask);
					} // set_aand

					void set_aor(const u64 aor, const u64 mask)
					{
						m_aor = (m_aor & ~mask) | (aor & mask);
					} // set_aor

					void set_wait(const bool wait)
					{
						m_wait = wait;
					} // set_wait

					void set_wp(const bool wp)
					{
						m_wp = wp;
					} // set_wp

					void set_enable(const bool enable)
					{
						m_enable = enable;
					} // set_enable

				private:
					u64 m_acomp = 0; // Address compare
					u64 m_amask = 0; // Address mask
					u64 m_aand = ~0; // Address AND
					u64 m_aor = 0; // Address OR
					u8 m_wait = 0x3f; // Wait state
					bool m_wp = false; // Write protect
					bool m_enable = false; // Enable
			}; // class regs_t

			class dma_t
			{
				public:
					dma_t(jkmspexp_t &host)
						: m_host(host)
						, m_source(0)
						, m_target(0)
						, m_counter(0)
						, m_buffer(0)
						, m_remain(0)
						, m_swait(0x3f)
						, m_twait(0x3f)
						, m_size(0)
						, m_source_incdec_enable(false)
						, m_target_incdec_enable(false)
						, m_source_incdec(false)
						, m_target_incdec(false)
						, m_fill(false)
						, m_busy(false)
						, m_state(false)
					{
					} // dma_t

					void reset()
					{
						m_source = 0;
						m_target = 0;
						m_counter = 0;
						m_buffer = 0;
						m_remain = 0;
						m_swait = 0x3f;
						m_twait = 0x3f;
						m_size = 0;
						m_source_incdec_enable = false;
						m_target_incdec_enable = false;
						m_source_incdec = false;
						m_target_incdec = false;
						m_fill = false;
						m_busy = false;
						m_state = false;
					}

					void run();

					void tick();

					u64 source() const
					{
						return m_source;
					} // source

					u64 target() const
					{
						return m_target;
					} // target

					u64 counter() const
					{
						return m_counter;
					} // counter

					u8 swait() const
					{
						return m_swait;
					} // wait

					u8 twait() const
					{
						return m_twait;
					} // wait

					u8 size() const
					{
						return m_size;
					} // size

					bool source_incdec_enable() const
					{
						return m_source_incdec_enable;
					} // source_incdec_enable

					bool target_incdec_enable() const
					{
						return m_target_incdec_enable;
					} // target_incdec_enable

					bool source_incdec() const
					{
						return m_source_incdec;
					} // source_incdec

					bool target_incdec() const
					{
						return m_target_incdec;
					} // target_incdec

					bool fill() const
					{
						return m_fill;
					} // fill

					bool busy() const
					{
						return m_busy;
					} // busy

					void set_source(const u64 source, const u64 mask)
					{
						m_source = (m_source & ~mask) | (source & mask);
					} // set_source

					void set_target(const u64 target, const u64 mask)
					{
						m_target = (m_target & ~mask) | (target & mask);
					} // set_target

					void set_counter(const u64 counter, const u64 mask)
					{
						m_counter = (m_counter & ~mask) | (counter & mask);
					} // set_counter

					void set_swait(const u8 wait)
					{
						m_swait = bitfield(wait, 0, 6);
					} // set_wait

					void set_twait(const u8 wait)
					{
						m_twait = bitfield(wait, 0, 6);
					} // set_wait

					void set_size(const u8 size)
					{
						m_size = bitfield(size, 0, 2);
					} // set_size

					void set_source_incdec_enable(const bool enable)
					{
						m_source_incdec_enable = enable;
					} // set_source_incdec_enable

					void set_target_incdec_enable(const bool enable)
					{
						m_target_incdec_enable = enable;
					} // set_target_incdec_enable

					void set_source_incdec(const bool incdec)
					{
						m_source_incdec = incdec;
					} // set_source_incdec

					void set_target_incdec(const bool incdec)
					{
						m_target_incdec = incdec;
					} // set_target_incdec

					void set_fill(const bool fill)
					{
						m_fill = fill;
					} // set_fill

				private:
					jkmspexp_t &m_host; // Host interface
					u64 m_source = 0; // Source address
					u64 m_target = 0; // Target address
					u64 m_counter = 0; // Counter
					u64 m_buffer = 0; // Read/Write buffer
					s8 m_remain = 0; // Transfer wait remains
					u8 m_swait = 0x3f; // Wait state
					u8 m_twait = 0x3f; // Wait state
					u8 m_size = 0; // Transfer size
					bool m_source_incdec_enable = false; // Source address Increment/Decrement enable flag
					bool m_target_incdec_enable = false; // Target address Increment/Decrement enable flag
					bool m_source_incdec = false; // Source address Increment/Decrement flag
					bool m_target_incdec = false; // Target address Increment/Decrement flag
					bool m_fill = false; // Fill flag
					bool m_busy = false; // Busy flag
					bool m_state = false; // Read/Write state

			}; // class dma_t

		public:
			jkmspexp_t(jkmspexp_intf_t<BigEndian> &intf, u64 init_mask)
				: m_intf(intf)
				, m_init_mask(init_mask)
				, m_regs{
					regs_t(), regs_t(), regs_t(), regs_t(),
					regs_t(), regs_t(), regs_t(), regs_t(),
				}
				, m_dma(*this)
				, m_slot(0)
				, m_reg(0)
				, m_enable(false)
			{
			} // jkmspexp_t

			void reset();

			T regs_r(const u8 addr) const;
			void regs_w(const u8 addr, const T data, const T mask);

			T host_r(const u64 addr) const;
			void host_w(const u64 addr, const T data, const T mask);

		private:
			u64 get_addr(const u64 addr, bool &wp, u64 &wait, bool &exists);

			jkmspexp_intf_t<BigEndian> &m_intf;
			const u64 m_init_mask = 0;    // Initial address base
			std::array<regs_t, 8> m_regs; // Control register
			dma_t m_dma;                  // DMA register
			u8 m_slot = 0;                // Slot select
			u8 m_reg = 0;                 // Register select
			bool m_enable = false;        // Enable
	}; // class jkmspexp_t
} // namespace jkmspexp

#endif // JKMSPEXP_HPP
