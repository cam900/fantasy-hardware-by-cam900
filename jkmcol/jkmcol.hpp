/*

==============================================================================

    JKMCOL
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMCOL

Fantasy Collision helper

*/

#ifndef JKMCOL_HPP
#define JKMCOL_HPP

#pragma once

#include <array>

namespace jkmcol
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	template<typename T>
	class jkmcol_intf_t
	{
		public:
			jkmcol_intf_t()
			{
			} // jkmcol_intf_t

			virtual T io_in(u8 slot, T dir) { return 0; }
			virtual void io_out(u8 slot, T data, T dir) {}
	}; // class jkmcol_intf_t

	template<bool BigEndian, typename T>
	class jkmcol_t
	{
		private:
			class pos_t
			{
				public:
					pos_t()
						: m_origin(0)
						, m_offset(0)
						, m_calc(0)
					{
					} // pos_t

					void reset()
					{
						m_origin = 0;
						m_offset = 0;
						m_calc = 0;
					}

					u16 origin() const
					{
						return m_origin;
					} // origin

					u16 offset() const
					{
						return m_offset;
					} // offset

					u8 calc() const
					{
						return m_calc;
					} // calc

					void set_origin(const u16 origin, const u16 mask)
					{
						m_origin = (m_origin & ~mask) | (origin & mask);
					} // set_origin

					void set_offset(const u16 offset, const u16 mask)
					{
						m_offset = (m_offset & ~mask) | (offset & mask);
					} // set_offset

					void set_calc(const u8 calc, const u8 mask)
					{
						m_calc = (m_calc & ~mask) | (calc & mask);
					} // set_calc

				private:
					u16 m_origin; // Origin point
					u16 m_offset; // Point offset
					u8 m_calc; // Calculate method
			}; // class pos_t

		public:
			jkmcol_t(jkmcol_intf_t<T> &intf, std::array<T, 8> _3state)
				: m_intf(intf)
				, m_x{pos_t(), pos_t()}
				, m_y{pos_t(), pos_t()}
				, m_z{pos_t(), pos_t()}
				, m_slot(0)
				, m_status(0)
			{
			} // jkmcol_t

			void reset();

			T host_r(const u8 addr) const;
			void host_w(const u8 addr, const T data, const T mask);

		private:
			s16 calc_point(const u8 calc, const s16 origin, const s16 offset) const;
			void coll_check();

			jkmcol_intf_t<T> &m_intf; // Interface
			std::array<pos_t, 2> m_x; // X position
			std::array<pos_t, 2> m_y; // Y position
			std::array<pos_t, 2> m_z; // Z position
			u8 m_slot = 0;            // Register slot
			u8 m_status = 0;          // Collision Status
	}; // class jkmcol_t
} // namespace jkmcol

#endif // JKMCOL_HPP
