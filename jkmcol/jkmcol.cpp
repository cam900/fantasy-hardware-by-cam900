/*

==============================================================================

    JKMCOL
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMCOL

Fantasy Collision helper

Features:
- X, Y, Z Collision check, Variable offset calculation support

	Host interface

	Address Description
	00      Register select
	01      Register data

	8 bit Register map

	Register Description
	00       Status (Read) / Reset (Write)
	02       X1 Origin
	03       ""
	04       X1 Offset
	05       ""
	06       X1 Control
	08       X2 Origin
	09       ""
	0a       X2 Offset
	0b       ""
	0c       X2 Control
	0e       Y1 Origin
	0f       ""
	10       Y1 Offset
	11       ""
	12       Y1 Control
	14       Y2 Origin
	15       ""
	16       Y2 Offset
	17       ""
	18       Y2 Control
	1a       Z1 Origin
	1b       ""
	1c       Z1 Offset
	1d       ""
	1e       Z1 Control
	20       Z2 Origin
	21       ""
	22       Z2 Offset
	23       ""
	24       Z2 Control

	16 bit Register map

	Register Description
	00       Status (Read) / Reset (Write)
	01       X1 Origin
	02       X1 Offset
	03       X1 Control
	04       X2 Origin
	05       X2 Offset
	06       X2 Control
	07       Y1 Origin
	08       Y1 Offset
	09       Y1 Control
	0a       Y2 Origin
	0b       Y2 Offset
	0c       Y2 Control
	0d       Z1 Origin
	0e       Z1 Offset
	0f       Z1 Control
	10       Z2 Origin
	11       Z2 Offset
	12       Z2 Control

	Control register format
	x--- ---- Enable/Disable boundary check
	-xxx ---- Calculate method of right point
	---- -xxx Calculate method of left point

	Status flag
	x--- ---- X1 left >= X2 left
	-x-- ---- X1 right <= X2 right
	--x- ---- Y1 top >= Y2 top
	---x ---- Y1 bottom <= Y2 bottom
	---- x--- Z1 front >= Z2 front
	---- -x-- Z1 back <= Z2 back
*/

#include "jkmcol.hpp"

namespace jkmcol
{
	template<bool BigEndian, typename T>
	s16 jkmcol_t<BigEndian, T>::calc_point(const u8 calc, const s16 origin, const s16 offset) const
	{
		switch (calc & 7)
		{
			case 0: return origin;
			case 1: return offset;
			case 2: return origin - offset;
			case 3: return origin + offset;
			case 4: return origin - (offset >> 1);
			case 5: return origin + ((offset + 1) >> 1);
			default: return 0;
		}
	} // jkmcol_t<BigEndian, T>::calc_point

	template<bool BigEndian, typename T>
	void jkmcol_t<BigEndian, T>::coll_check()
	{
		// calculate point
		const s16 ax1 = calc_point(bitfield(m_x[0].calc(), 0, 3), s16(m_x[0].origin()), s16(m_x[0].offset()));
		const s16 ax2 = calc_point(bitfield(m_x[0].calc(), 4, 3), s16(m_x[0].origin()), s16(m_x[0].offset()));
		const s16 bx1 = calc_point(bitfield(m_x[1].calc(), 0, 3), s16(m_x[1].origin()), s16(m_x[1].offset()));
		const s16 bx2 = calc_point(bitfield(m_x[1].calc(), 4, 3), s16(m_x[1].origin()), s16(m_x[1].offset()));

		const s16 ay1 = calc_point(bitfield(m_y[0].calc(), 0, 3), s16(m_y[0].origin()), s16(m_y[0].offset()));
		const s16 ay2 = calc_point(bitfield(m_y[0].calc(), 4, 3), s16(m_y[0].origin()), s16(m_y[0].offset()));
		const s16 by1 = calc_point(bitfield(m_y[1].calc(), 0, 3), s16(m_y[1].origin()), s16(m_y[1].offset()));
		const s16 by2 = calc_point(bitfield(m_y[1].calc(), 4, 3), s16(m_y[1].origin()), s16(m_y[1].offset()));

		const s16 az1 = calc_point(bitfield(m_z[0].calc(), 0, 3), s16(m_z[0].origin()), s16(m_z[0].offset()));
		const s16 az2 = calc_point(bitfield(m_z[0].calc(), 4, 3), s16(m_z[0].origin()), s16(m_z[0].offset()));
		const s16 bz1 = calc_point(bitfield(m_z[1].calc(), 0, 3), s16(m_z[1].origin()), s16(m_z[1].offset()));
		const s16 bz2 = calc_point(bitfield(m_z[1].calc(), 4, 3), s16(m_z[1].origin()), s16(m_z[1].offset()));

		m_status = 0;
		if (bitfield(m_x[0].calc(), 7) && (ax1 >= bx1))
		{
			m_status |= 0x80;
		}
		if (bitfield(m_x[1].calc(), 7) && (ax2 <= bx2))
		{
			m_status |= 0x40;
		}
		if (bitfield(m_y[0].calc(), 7) && (ay1 >= by1))
		{
			m_status |= 0x20;
		}
		if (bitfield(m_y[1].calc(), 7) && (ay2 <= by2))
		{
			m_status |= 0x10;
		}
		if (bitfield(m_z[0].calc(), 7) && (az1 >= bz1))
		{
			m_status |= 0x08;
		}
		if (bitfield(m_z[1].calc(), 7) && (az2 <= bz2))
		{
			m_status |= 0x04;
		}
	} // jkmcol_t<BigEndian, T>::coll_check

	template<bool BigEndian, typename T>
	void jkmcol_t<BigEndian, T>::reset()
	{
		for (pos_t &x : m_x)
		{
			x.reset();
		}
		for (pos_t &y : m_y)
		{
			y.reset();
		}
		for (pos_t &z : m_z)
		{
			z.reset();
		}
		m_slot = 0;
		m_status = 0;
	} // jkmcol_t<BigEndian, T>::reset


	template<bool BigEndian, typename T>
	T jkmcol_t<BigEndian, T>::host_r(const u8 addr) const
	{
		if (bitfield(addr, 0)) // Data
		{
			switch (sizeof(T))
			{
				case 1:
				{
					const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 3;
					switch (m_slot)
					{
						case 0x00:
							return m_status;
							break;
						case 0x02:
						case 0x03:
							return m_x[0].origin() >> shift;
							break;
						case 0x04:
						case 0x05:
							return m_x[0].offset() >> shift;
							break;
						case 0x06:
							return m_x[0].calc();
							break;
						case 0x08:
						case 0x09:
							return m_x[1].origin() >> shift;
							break;
						case 0x0a:
						case 0x0b:
							return m_x[1].offset() >> shift;
							break;
						case 0x0c:
							return m_x[1].calc();
							break;
						case 0x0e:
						case 0x0f:
							return m_y[0].origin() >> shift;
							break;
						case 0x10:
						case 0x11:
							return m_y[0].offset() >> shift;
							break;
						case 0x12:
							return m_y[0].calc();
							break;
						case 0x14:
						case 0x15:
							return m_y[1].origin() >> shift;
							break;
						case 0x16:
						case 0x17:
							return m_y[1].offset() >> shift;
							break;
						case 0x18:
							return m_y[1].calc();
							break;
						case 0x1a:
						case 0x1b:
							return m_z[0].origin() >> shift;
							break;
						case 0x1c:
						case 0x1d:
							return m_z[0].offset() >> shift;
							break;
						case 0x1e:
							return m_z[0].calc();
							break;
						case 0x20:
						case 0x21:
							return m_z[1].origin() >> shift;
							break;
						case 0x22:
						case 0x23:
							return m_z[1].offset() >> shift;
							break;
						case 0x24:
							return m_z[1].calc();
							break;
					}
					break;
				}
				case 2:
					switch (m_slot)
					{
						case 0x00:
							return m_status;
							break;
						case 0x01:
							return m_x[0].origin();
							break;
						case 0x02:
							return m_x[0].offset();
							break;
						case 0x03:
							return m_x[0].calc();
							break;
						case 0x04:
							return m_x[1].origin();
							break;
						case 0x05:
							return m_x[1].offset();
							break;
						case 0x06:
							return m_x[1].calc();
							break;
						case 0x07:
							return m_y[0].origin();
							break;
						case 0x08:
							return m_y[0].offset();
							break;
						case 0x09:
							return m_y[0].calc();
							break;
						case 0x0a:
							return m_y[1].origin();
							break;
						case 0x0b:
							return m_y[1].offset();
							break;
						case 0x0c:
							return m_y[1].calc();
							break;
						case 0x0d:
							return m_z[0].origin();
							break;
						case 0x0e:
							return m_z[0].offset();
							break;
						case 0x0f:
							return m_z[0].calc();
							break;
						case 0x10:
							return m_z[1].origin();
							break;
						case 0x11:
							return m_z[1].offset();
							break;
						case 0x12:
							return m_z[1].calc();
							break;
					}
					break;
				default:
					return 0;
					break;
			}
		}
		else // Slot
		{
			return m_slot;
		}
	} // jkmcol_t<BigEndian, T>::host_r
	
	template<bool BigEndian, typename T>
	void jkmcol_t<BigEndian, T>::host_w(const u8 addr, const T data, const T mask)
	{
		if (bitfield(addr, 0)) // Data
		{
			switch (sizeof(T))
			{
				case 1:
				{
					const u16 sdata = u16(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 3);
					const u16 smask = u16(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 3);
					switch (m_slot)
					{
						case 0x00:
							if (bitfield(mask, 0, 8))
							{
								m_status = 0; // Reset
							}
							break;
						case 0x02:
						case 0x03:
							m_x[0].set_origin(sdata, smask);
							coll_check();
							break;
						case 0x04:
						case 0x05:
							m_x[0].set_offset(sdata, smask);
							coll_check();
							break;
						case 0x06:
							if (bitfield(mask, 0, 8))
							{
								m_x[0].set_calc(data);
								coll_check();
							}
							break;
						case 0x08:
						case 0x09:
							m_x[1].set_origin(sdata, smask);
							coll_check();
							break;
						case 0x0a:
						case 0x0b:
							m_x[1].set_offset(sdata, smask);
							coll_check();
							break;
						case 0x0c:
							if (bitfield(mask, 0, 8))
							{
								m_x[1].set_calc(data);
								coll_check();
							}
							break;
						case 0x0e:
						case 0x0f:
							m_y[0].set_origin(sdata, smask);
							coll_check();
							break;
						case 0x10:
						case 0x11:
							m_y[0].set_offset(sdata, smask);
							coll_check();
							break;
						case 0x12:
							if (bitfield(mask, 0, 8))
							{
								m_y[0].set_calc(data);
								coll_check();
							}
							break;
						case 0x14:
						case 0x15:
							m_y[1].set_origin(sdata, smask);
							coll_check();
							break;
						case 0x16:
						case 0x17:
							m_y[1].set_offset(sdata, smask);
							coll_check();
							break;
						case 0x18:
							if (bitfield(mask, 0, 8))
							{
								m_y[1].set_calc(data);
								coll_check();
							}
							break;
						case 0x1a:
						case 0x1b:
							m_z[0].set_origin(sdata, smask);
							coll_check();
							break;
						case 0x1c:
						case 0x1d:
							m_z[0].set_offset(sdata, smask);
							coll_check();
							break;
						case 0x1e:
							if (bitfield(mask, 0, 8))
							{
								m_z[0].set_calc(data);
								coll_check();
							}
							break;
						case 0x20:
						case 0x21:
							m_z[1].set_origin(sdata, smask);
							coll_check();
							break;
						case 0x22:
						case 0x23:
							m_z[1].set_offset(sdata, smask);
							coll_check();
							break;
						case 0x24:
							if (bitfield(mask, 0, 8))
							{
								m_z[1].set_calc(data);
								coll_check();
							}
							break;
					}
					break;
				}
				case 2:
					switch (m_slot)
					{
						case 0x00:
							if (bitfield(mask, 0, 8))
							{
								m_status = 0; // Reset
							}
							break;
						case 0x01:
							m_x[0].set_origin(data, mask);
							coll_check();
							break;
						case 0x02:
							m_x[0].set_offset(data, mask);
							coll_check();
							break;
						case 0x03:
							if (bitfield(mask, 0, 8))
							{
								m_x[0].set_calc(bitfield(data, 0, 8));
								coll_check();
							}
							break;
						case 0x04:
							m_x[1].set_origin(data, mask);
							coll_check();
							break;
						case 0x05:
							m_x[1].set_offset(data, mask);
							coll_check();
							break;
						case 0x06:
							if (bitfield(mask, 0, 8))
							{
								m_x[1].set_calc(bitfield(data, 0, 8));
								coll_check();
							}
							break;
						case 0x07:
							m_y[0].set_origin(data, mask);
							coll_check();
							break;
						case 0x08:
							m_y[0].set_offset(data, mask);
							coll_check();
							break;
						case 0x09:
							if (bitfield(mask, 0, 8))
							{
								m_y[0].set_calc(bitfield(data, 0, 8));
								coll_check();
							}
							break;
						case 0x0a:
							m_y[1].set_origin(data, mask);
							coll_check();
							break;
						case 0x0b:
							m_y[1].set_offset(data, mask);
							coll_check();
							break;
						case 0x0c:
							if (bitfield(mask, 0, 8))
							{
								m_y[1].set_calc(bitfield(data, 0, 8));
								coll_check();
							}
							break;
						case 0x0d:
							m_z[0].set_origin(data, mask);
							coll_check();
							break;
						case 0x0e:
							m_z[0].set_offset(data, mask);
							coll_check();
							break;
						case 0x0f:
							if (bitfield(mask, 0, 8))
							{
								m_z[0].set_calc(bitfield(data, 0, 8));
								coll_check();
							}
							break;
						case 0x10:
							m_z[1].set_origin(data, mask);
							coll_check();
							break;
						case 0x11:
							m_z[1].set_offset(data, mask);
							coll_check();
							break;
						case 0x12:
							if (bitfield(mask, 0, 8))
							{
								m_z[1].set_calc(bitfield(data, 0, 8));
								coll_check();
							}
							break;
					}
					break;
			}
		}
		else // Slot
		{
			if (bitfield(mask, 0, 8))
			{
				m_slot = bitfield(data, 0, 8);
			}
		}
	} // jkmcol_t<BigEndian, T>::host_w

} // namespace jkmcol
