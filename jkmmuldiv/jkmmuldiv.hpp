/*

==============================================================================

    JKMMULDIV
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMMULDIV

Fantasy I/O Multiplexer

*/

#ifndef JKMMULDIV_HPP
#define JKMMULDIV_HPP

#pragma once

#include <algorithm>
#include <array>

namespace jkmmuldiv
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	template<bool BigEndian, typename T>
	class jkmmuldiv_t
	{
		public:
			jkmmuldiv_t()
				: m_mul_counter(0)
				, m_div_counter(0)
				, m_busy_mul(false)
				, m_busy_div(false)
				, m_overflow(false)
				, m_accumulate(false)
				, m_signed(false)
				, m_sign_srca(1)
				, m_sign_srcb(1)
				, m_mul_srca(0)
				, m_mul_srcb(0)
				, m_mul_res(0)
				, m_dividend(0)
				, m_divisor(0)
				, m_quotient(0)
				, m_remainder(0)
				, m_slot(0)
			{
			} // jkmmuldiv_t

			void reset();
			void tick();

			T host_r(const u8 addr) const;
			void host_w(const u8 addr, const T data, const T mask);

		private:
			void multiply();
			void divide();

			s64 m_mul_counter = 0;
			s64 m_div_counter = 0;
			bool m_busy_mul = false; // Multiplier busy flag
			bool m_busy_div = false; // Divider busy flag
			bool m_overflow = false; // Overflow/Divided by zero flag
			bool m_accumulate = false; // Accumulate flag
			bool m_signed = false; // Signed math flag
			s8 m_sign_srca = 1; // Multiplier source A sign flag
			s8 m_sign_srcb = 1; // Multiplier source B sign flag
			u16 m_mul_srca = 0; // Multiplier source A
			u16 m_mul_srcb = 0; // Multiplier source B
			u64 m_mul_res = 0; // Multiplier result

			u32 m_dividend = 0; // Divider dividend
			u16 m_divisor = 0; // Divider divisor
			u32 m_quotient = 0; // Divider quotient
			u16 m_remainder = 0; // Divider remainder

			u8 m_slot = 0;              // Register slot
	}; // class jkmmuldiv_t
} // namespace jkmmuldiv

#endif // JKMMULDIV_HPP
