/*

==============================================================================

    JKMMULDIV
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMMULDIV

Fantasy Multiplier/Divider

Features:
- 16x16 Multiplier, 32/16 Divider
- 48 bit Multiply result, Accumulate support
- 32 bit Quotient, 16 bit Remainder
- Signed/Unsigned math support

	Host interface

	Address Description
	00      Register select
	01      Register data

	8 bit Register map

	Register Description
	00       Control / Status
	         Bit 0 Clear
	         Bit 1 Accumulate
	         Bit 2 Signed/Unsigned
	         Bit 6 Overflow/Divided by zero flag
	         Bit 7 Busy
	02-03    Multiply source A
	04-05    Multiply source B
	05       Trigger multiplier
	08-0f    Multiply result
	10-13    Divider dividend
	14-15    Divider divisor
	15       Trigger divider
	18-1b    Divider quotient
	1c-1d    Divider remainder

	16 bit Register map

	Register Description
	00       Control / Status
	         Bit 0 Clear
	         Bit 1 Accumulate
	         Bit 2 Signed/Unsigned
	         Bit 6 Overflow/Divided by zero flag
	         Bit 7 Busy
	01       Multiply source A
	02       Multiply source B
	02       Trigger multiplier
	04-07    Multiply result
	08-09    Divider dividend
	0a       Divider divisor
	0a       Trigger divider
	1c-1d    Divider quotient
	1e       Divider remainder
*/

#include "jkmmuldiv.hpp"

namespace jkmmuldiv
{
	template<bool BigEndian, typename T>
	void jkmmuldiv_t<BigEndian, T>::multiply()
	{
		if (m_busy_mul)
		{
			m_overflow = false;

			u64 res = m_mul_srca * m_mul_srcb;
			const u64 mask = 0xffffffffffffULL;

			if (m_signed)
			{
				if (!(m_sign_srca + m_sign_srcb)) // Different sign
				{
					res = (res ^ mask) + 1;
				}
			}

			if (m_accumulate)
			{
				m_mul_res += bitfield(res, 0, 48);

				if (m_mul_res > mask)
				{
					m_overflow = true;
				}

				m_mul_res &= mask;
			}
			else
			{
				m_mul_res = res;
			}
			m_busy_mul = false;
		}
	}

	template<bool BigEndian, typename T>
	void jkmmuldiv_t<BigEndian, T>::divide()
	{
		if (m_busy_div)
		{
			m_overflow = false;
			u32 res = ~0;
			u16 mod = 0;
			if (m_divisor == 0)
			{
				m_overflow = true; // Divided by zero
				res = ~0;
				mod = 0;
			}
			else
			{
				res = m_dividend / m_divisor;
				mod = m_dividend % m_divisor;
			}
			m_quotient = res;
			m_remainder = mod;
			m_busy_div = false;
		}
	}

	template<bool BigEndian, typename T>
	void jkmmuldiv_t<BigEndian, T>::tick()
	{
		if (m_busy_mul)
		{
			if (m_mul_counter-- == 0)
			{
				multiply();
				m_mul_counter = 0;
			}
		}
		if (m_busy_div)
		{
			if (m_div_counter-- == 0)
			{
				divide();
				m_div_counter = 0;
			}
		}
	} // jkmmuldiv_t<BigEndian, T>::tick

	template<bool BigEndian, typename T>
	void jkmmuldiv_t<BigEndian, T>::reset()
	{
		m_mul_counter = 0;
		m_div_counter = 0;
		m_busy_mul = false;
		m_busy_div = false;
		m_overflow = false;
		m_accumulate = false;
		m_signed = false;
		m_sign_srca = 1;
		m_sign_srcb = 1;
		m_mul_srca = 0;
		m_mul_srcb = 0;
		m_mul_res = 0;

		m_dividend = 0;
		m_divisor = 0;
		m_quotient = 0;
		m_remainder = 0;

		m_slot = 0;
	} // jkmmuldiv_t<BigEndian, T>::reset

	template<bool BigEndian, typename T>
	T jkmmuldiv_t<BigEndian, T>::host_r(const u8 addr) const
	{
		if (bitfield(addr, 0)) // Data
		{
			switch (sizeof(T))
			{
				case 1:
				{
					const u16 shift16 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 3;
					const u16 shift32 = bitfield(BigEndian ? ~m_slot : m_slot, 0, 2) << 3;
					const u16 shift64 = bitfield(BigEndian ? ~m_slot : m_slot, 0, 3) << 3;
					switch (m_slot)
					{
						case 0x00:
							return (m_accumulate ? 0x02 : 0x00)
									| (m_signed ? 0x04 : 0x00)
									| (m_overflow ? 0x40 : 0x00)
									| ((m_busy_div || m_busy_mul) ? 0x80 : 0x00);
							break;
						case 0x02:
						case 0x03:
							return m_mul_srca >> shift16;
							break;
						case 0x04:
						case 0x05:
							return m_mul_srcb >> shift16;
							break;
						case 0x08:
						case 0x09:
						case 0x0a:
						case 0x0b:
						case 0x0c:
						case 0x0d:
						case 0x0e:
						case 0x0f:
							return m_mul_res >> shift64;
							break;
						case 0x10:
						case 0x11:
						case 0x12:
						case 0x13:
							return m_dividend >> shift32;
							break;
						case 0x14:
						case 0x15:
							return m_divisor >> shift16;
							break;
						case 0x18:
						case 0x19:
						case 0x1a:
						case 0x1b:
							return m_quotient >> shift32;
							break;
						case 0x1c:
						case 0x1d:
							return m_remainder >> shift16;
							break;
					}
					break;
				}
				case 2:
				{
					const u16 shift32 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
					const u16 shift64 = bitfield(BigEndian ? ~m_slot : m_slot, 0, 2) << 4;
					switch (m_slot)
					{
						case 0x00:
							return (m_accumulate ? 0x02 : 0x00)
									| (m_signed ? 0x04 : 0x00)
									| (m_overflow ? 0x40 : 0x00)
									| ((m_busy_div || m_busy_mul) ? 0x80 : 0x00);
							break;
						case 0x01:
							return m_mul_srca;
							break;
						case 0x02:
							return m_mul_srcb;
							break;
						case 0x04:
						case 0x05:
						case 0x06:
						case 0x07:
							return m_mul_res >> shift64;
							break;
						case 0x08:
						case 0x09:
							return m_dividend >> shift32;
							break;
						case 0x0a:
							return m_divisor;
							break;
						case 0x0c:
						case 0x0d:
							return m_quotient >> shift32;
							break;
						case 0x0e:
							return m_remainder;
							break;
					}
					break;
				}
			}
		}
		else // Slot
		{
			return m_slot;
		}
		return 0;
	} // jkmmuldiv_t<BigEndian, T>::host_r

	template<bool BigEndian, typename T>
	void jkmmuldiv_t<BigEndian, T>::host_w(const u8 addr, const T data, const T mask)
	{
		if (bitfield(addr, 0) && ((!m_busy_mul) && (!m_busy_div))) // Data
		{
			switch (sizeof(T))
			{
				case 1:
				{
					const u16 sdata16 = (u16(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 3));
					const u16 smask16 = (u16(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 3));
					const u16 sdata32 = (u32(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0, 2) << 3));
					const u16 smask32 = (u32(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0, 2) << 3));
					switch (m_slot)
					{
						case 0x00:
							if (bitfield(mask, 0, 8))
							{
								if (bitfield(data, 0))
								{
									m_sign_srca = 1;
									m_sign_srcb = 1;
									m_mul_srca = 0;
									m_mul_srcb = 0;
									m_mul_res = 0;
									m_dividend = 0;
									m_divisor = 0;
									m_quotient = 0;
									m_remainder = 0;
								}
								m_accumulate = bitfield(data, 1);
								m_signed = bitfield(data, 2);
							}
							break;
						case 0x02:
						case 0x03:
						{
							m_mul_srca = (m_mul_srca & ~smask16) | (sdata16 & smask16);
							if (m_signed && bitfield(m_slot, 0))
							{
								if (bitfield(m_mul_srca, 15))
								{
									const u16 temp = (m_mul_srca ^ 0xffff) + 1;
									m_sign_srca = -1;
									m_mul_srca = temp;
								}
								else
								{
									m_sign_srca = 1;
								}
							}
							break;
						}
						case 0x04:
						case 0x05:
						{
							m_mul_srcb = (m_mul_srcb & ~smask16) | (sdata16 & smask16);
							if (m_signed && bitfield(m_slot, 0))
							{
								if (bitfield(m_mul_srcb, 15))
								{
									const u16 temp = (m_mul_srcb ^ 0xffff) + 1;
									m_sign_srcb = -1;
									m_mul_srcb = temp;
								}
								else
								{
									m_sign_srcb = 1;
								}
							}
							if (bitfield(m_slot, 0))
							{
								m_busy_mul = true;
								m_mul_counter = m_accumulate ? 6 : 5;
							}
							break;
						}
						case 0x10:
						case 0x11:
						case 0x12:
						case 0x13:
							m_dividend = (m_dividend & ~smask32) | (sdata32 & smask32);
							break;
						case 0x14:
						case 0x15:
							m_divisor = (m_divisor & ~smask16) | (sdata16 & smask16);
							if (bitfield(m_slot, 0))
							{
								m_busy_div = true;
								m_div_counter = 6;
							}
							break;
					}
					break;
				}
				case 2:
				{
					const u16 sdata32 = (u32(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4));
					const u16 smask32 = (u32(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4));
					switch (m_slot)
					{
						case 0x00:
							if (bitfield(mask, 0, 8))
							{
								if (bitfield(data, 0))
								{
									m_sign_srca = 1;
									m_sign_srcb = 1;
									m_mul_srca = 0;
									m_mul_srcb = 0;
									m_mul_res = 0;
									m_dividend = 0;
									m_divisor = 0;
									m_quotient = 0;
									m_remainder = 0;
								}
								m_accumulate = bitfield(data, 1);
								m_signed = bitfield(data, 2);
							}
							break;
						case 0x01:
						{
							m_mul_srca = (m_mul_srca & ~mask) | (data & mask);
							if (m_signed)
							{
								if (bitfield(m_mul_srca, 15))
								{
									const u16 temp = (m_mul_srca ^ 0xffff) + 1;
									m_sign_srca = -1;
									m_mul_srca = temp;
								}
								else
								{
									m_sign_srca = 1;
								}
							}
							break;
						}
						case 0x02:
						{
							m_mul_srcb = (m_mul_srcb & ~mask) | (data & mask);
							if (m_signed)
							{
								if (bitfield(m_mul_srcb, 15))
								{
									const u16 temp = (m_mul_srcb ^ 0xffff) + 1;
									m_sign_srcb = -1;
									m_mul_srcb = temp;
								}
								else
								{
									m_sign_srcb = 1;
								}
							}
							m_busy_mul = true;
							m_mul_counter = m_accumulate ? 6 : 5;
							break;
						}
						case 0x08:
						case 0x09:
							m_dividend = (m_dividend & ~smask32) | (sdata32 & smask32);
							break;
						case 0x0a:
							m_divisor = (m_divisor & ~mask) | (data & mask);
							m_busy_div = true;
							m_div_counter = 6;
							break;
					}
					break;
				}
			}
		}
		else // Slot
		{
			if (bitfield(mask, 0, 8))
			{
				m_slot = bitfield(data, 0, 8);
			}
		}
	} // jkmmuldiv_t<BigEndian, T>::host_w

} // namespace jkmmuldiv
