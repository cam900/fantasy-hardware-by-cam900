/*

==============================================================================

    JKMPAL
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMPAL

Fantasy palette interface

Features:
- Max 65536 palette entry, 8 bit data per color channel (24 bit True color)

	Host interface

	Address Description
	00      Register select
	01      Register data

	8 bit Register map

	00 Color RAM address LSB
	01 Color RAM address MSB
	04 Red
	05 Green
	06 Blue

	16 bit Register map

	00 Color RAM address
	04 Red
	05 Green
	06 Blue
*/

#include "jkmpal.hpp"

namespace jkmpal
{
	template<bool BigEndian, typename T>
	void jkmpal_t<BigEndian, T>::reset()
	{
		m_pal_addr = 0;
		m_slot = 0;
	} // jkmpal_t::reset

	template<bool BigEndian, typename T>
	T jkmpal_t<BigEndian, T>::host_r(const u8 addr)
	{
		if (bitfield(addr, 0)) // Data
		{
			switch (sizeof(T))
			{
				case 1:
					switch (m_slot)
					{
						case 0:
						case 1:
						{
							const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 3;
							return m_pal_addr >> shift;
						}
						case 4:
							return m_intf.read_red(m_pal_addr);
						case 5:
							return m_intf.read_green(m_pal_addr);
						case 6:
							return m_intf.read_blue(m_pal_addr++);
					}
					break;
				case 2:
					switch (m_slot)
					{
						case 0:
							return m_pal_addr;
						case 4:
							return m_intf.read_red(m_pal_addr);
						case 5:
							return m_intf.read_green(m_pal_addr);
						case 6:
							return m_intf.read_blue(m_pal_addr++);
					}
					break;
			}
		}
		else // Slot
		{
			return m_slot;
		}
		return 0;
	} // jkmpal_t::host_r

	template<bool BigEndian, typename T>
	void jkmpal_t<BigEndian, T>::host_w(const u8 addr, const T data, const T mask)
	{
		if (bitfield(addr, 0)) // Data
		{
			switch (sizeof(T))
			{
				case 1:
					switch (m_slot)
					{
						case 0:
						case 1:
						{
							const u16 sdata = u16(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 3);
							const u16 smask = u16(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 3);
							m_pal_addr = (m_pal_addr & ~smask) | (sdata & smask);
							break;
						}
						case 4:
							if (bitfield(mask, 0, 8))
							{
								m_intf.write_red(m_pal_addr, bitfield(data, 0, 8));
							}
							break;
						case 5:
							if (bitfield(mask, 0, 8))
							{
								m_intf.write_green(m_pal_addr, bitfield(data, 0, 8));
							}
							break;
						case 6:
							if (bitfield(mask, 0, 8))
							{
								m_intf.write_blue(m_pal_addr, bitfield(data, 0, 8));
								m_pal_addr++;
							}
							break;
					}
					break;
				case 2:
					switch (m_slot)
					{
						case 0:
							m_pal_addr = (m_pal_addr & ~mask) | (data & mask);
							break;
						case 4:
							if (bitfield(mask, 0, 8))
							{
								m_intf.write_red(m_pal_addr, bitfield(data, 0, 8));
							}
							break;
						case 5:
							if (bitfield(mask, 0, 8))
							{
								m_intf.write_green(m_pal_addr, bitfield(data, 0, 8));
							}
							break;
						case 6:
							if (bitfield(mask, 0, 8))
							{
								m_intf.write_blue(m_pal_addr, bitfield(data, 0, 8));
								m_pal_addr++;
							}
							break;
					}
					break;
			}
		}
		else // Slot
		{
			m_slot = bitfield(data, 0, 3);
		}
	} // jkmpal_t::host_w

} // namespace jkmpal
