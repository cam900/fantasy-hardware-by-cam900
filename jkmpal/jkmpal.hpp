/*

==============================================================================

    JKMPAL
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMPAL

Fantasy palette interface

*/

#ifndef JKMPAL_HPP
#define JKMPAL_HPP

#pragma once

namespace jkmpal
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	class jkmpal_intf_t
	{
		public:
			jkmpal_intf_t()
			{
			} // jkmpal_intf_t

			virtual u8 read_red(u16 color) { return 0; }
			virtual u8 read_green(u16 color) { return 0; }
			virtual u8 read_blue(u16 color) { return 0; }
			virtual void write_red(u16 color, u8 red) {}
			virtual void write_green(u16 color, u8 green) {}
			virtual void write_blue(u16 color, u8 blue) {}
	}; // class jkmpal_intf_t

	template<bool BigEndian, typename T>
	class jkmpal_t
	{
		public:
			jkmpal_t(jkmpal_intf_t &intf)
				: m_intf(intf)
				, m_pal_addr(0)
				, m_slot(0)
			{
			} // jkmpal_t

			void reset();

			T host_r(const u8 addr);
			void host_w(const u8 addr, const T data, const T mask = T(~0));

		private:
			jkmpal_intf_t &m_intf; // Interface
			u16 m_pal_addr = 0;    // Palette address
			u8 m_slot = 0;         // Register slot
	}; // class jkmpal_t
} // namespace jkmpal

#endif // JKMPAL_HPP
