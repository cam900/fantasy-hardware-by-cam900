/*

==============================================================================

    JKM-WS7
    Copyright (C) 2023-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKM-WS7

Fantasy sound generator by cam900

*/

#ifndef JKM_WS7_HPP
#define JKM_WS7_HPP

#pragma once

#include <algorithm>
#include <array>

namespace jkm_ws7
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = unsigned char;
	using s16 = unsigned short;
	using s32 = unsigned int;
	using s64 = unsigned long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	}

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	}

	template<typename T>
	static const inline T clamp(const T in, const T min, const T max)
	{
		return (in < min) ? min : ((in > max) ? max : in);
	}

	class jkm_ws7_core
	{
		private:
			class jkm_ws7_voice
			{
				public:
					jkm_ws7_voice()
						: m_waveform{0}
						, m_enable(false)
						, m_noise(false)
						, m_direct_output(false)
						, m_lvol(0)
						, m_rvol(0)
						, m_lout(0)
						, m_rout(0)
						, m_pitch(0)
						, m_counter(0)
						, m_pos(0)
						, m_lfsr(1)
					{
					} // jkm_ws7_voice()

					void tick()
					{
						m_lout = m_rout = 0;
						if (m_enable)
						{
							if (m_direct_output)
							{
								m_lout = m_lvol;
								m_rout = m_rvol;
							}
							else
							{
								if (m_counter >= m_pitch)
								{
									m_pos = bitfield(m_pos + 1, 0, 5);
									m_lfsr = (m_lfsr >> 1) | ((bitfield(m_lfsr, 0) ^ bitfield(m_lfsr, 2)) << 16);
									m_counter = 0;
								}
								else
									m_counter++;

								m_lout = m_noise ? (bitfield(m_lfsr, 0) ? m_lvol : 0) : (m_waveform[m_pos] * m_lvol) >> 8;
								m_rout = m_noise ? (bitfield(m_lfsr, 0) ? m_rvol : 0) : (m_waveform[m_pos] * m_rvol) >> 8;
							}
						}
					} // void tick()

					void reset()
					{
						m_enable = false;
						m_noise = false;
						m_direct_output = false;
						m_lvol = 0;
						m_rvol = 0;
						m_pitch = 0;
						m_counter = 0;
						m_pos = 0;
						m_lfsr = 1;
					} // void reset()

					// getters/setters
					inline s8 waveform_r(const u8 pos) const
					{
						return m_waveform[bitfield(pos, 0, 5)];
					} // waveform_r

					inline void waveform_w(const u8 pos, const s8 data)
					{
						m_waveform[bitfield(pos, 0, 5)] = data;
					} // void waveform_w

					inline bool enable() const { return m_enable; }
					inline void set_enable(const bool enable) { m_enable = enable; }

					inline bool noise() const { return m_noise; }
					inline void set_noise(const bool noise) { m_noise = noise; }

					inline bool direct_output() const { return m_direct_output; }
					inline void set_direct_output(const bool direct_output) { m_direct_output = direct_output; }

					inline s8 lvol() const { return m_lvol; }
					inline void set_lvol(const s8 lvol) { m_lvol = lvol; }

					inline s8 rvol() const { return m_rvol; }
					inline void set_rvol(const s8 rvol) { m_rvol = rvol; }

					inline u16 pitch() const { return m_pitch; }
					inline void set_pitch(const u16 pitch, const u16 mask) { m_pitch = (m_pitch & ~mask) | (pitch & mask); }

					inline s16 lout() const { return m_lout; }
					inline s16 rout() const { return m_rout; }
				private:
					std::array<s8, 32> m_waveform = {0};
					bool m_enable = false;
					bool m_noise = false;
					bool m_direct_output = false;
					s8 m_lvol = 0;
					s8 m_rvol = 0;
					s16 m_lout = 0;
					s16 m_rout = 0;
					u16 m_pitch = 0;
					u16 m_counter = 0;
					u8 m_pos = 0;
					u32 m_lfsr = 1;
			}; // class jkm_ws7_voice

		public:
			jkm_ws7_core()
				: m_voice{
					jkm_ws7_voice(), jkm_ws7_voice(), jkm_ws7_voice(), jkm_ws7_voice(),
					jkm_ws7_voice(), jkm_ws7_voice(), jkm_ws7_voice()
				}
				, m_lout(0)
				, m_rout(0)
			{
			} // jkm_ws7_core()

			void tick();
			void reset();

			// register read/write handler
			u8 read8(const u8 reg) const;
			void write8(const u8 reg, const u8 data);

			// output
			s16 lout() const { return m_lout; }
			s16 rout() const { return m_rout; }
		private:
			std::array<jkm_ws7_voice, 7> m_voice;
			s16 m_lout = 0;
			s16 m_rout = 0;
	}; // class jkm_ws7_core

}; // namespace jkm_ws7

#endif // JKM_WS7_HPP