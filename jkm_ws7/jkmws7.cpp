/*

==============================================================================

    JKM-WS7
    Copyright (C) 2023-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKM-WS7

Fantasy sound generator by cam900

Specifications:
- 7 channels
- waveform/noise/direct output, configured at each channels
- 8 bit signed volume

*/

#include "jkmws7.hpp"

namespace jkm_ws7
{
	/*

		Register map

		Register  Description
		0x00-0xdf Waveform (32 byte per channel, 8 bit signed)
		0x00-0x1f Channel 0 Waveform
		0x20-0x3f Channel 1 Waveform
		0x40-0x5f Channel 2 Waveform
		0x60-0x7f Channel 3 Waveform
		0x80-0x9f Channel 4 Waveform
		0xa0-0xbf Channel 5 Waveform
		0xc0-0xdf Channel 6 Waveform

		0xe0-0xed Volume/Direct output (Stereo, 8 bit signed)
		0xe0      Channel 0 Left volume/direct output
		0xe1      Channel 0 Right volume/direct output
		0xe2      Channel 1 Left volume/direct output
		0xe3      Channel 1 Right volume/direct output
		0xe4      Channel 2 Left volume/direct output
		0xe5      Channel 2 Right volume/direct output
		0xe6      Channel 3 Left volume/direct output
		0xe7      Channel 3 Right volume/direct output
		0xe8      Channel 4 Left volume/direct output
		0xe9      Channel 4 Right volume/direct output
		0xea      Channel 5 Left volume/direct output
		0xeb      Channel 5 Right volume/direct output
		0xec      Channel 6 Left volume/direct output
		0xed      Channel 6 Right volume/direct output

		0xee      Direct output, enable(1) / disable(0)
		          Bit 0...6 is Channel 0...6

		0xef      Output mode, noise(1) / waveform(0)
		          Bit 0...6 is Channel 0...6

		0xf0-0xfd Pitch (16 bit, Frequency : input clock / (pitch + 1))
		0xf0      Channel 0 Pitch (bit 0 to 7)
		0xf1      Channel 0 Pitch (bit 8 to 15)
		0xf2      Channel 1 Pitch (bit 0 to 7)
		0xf3      Channel 1 Pitch (bit 8 to 15)
		0xf4      Channel 2 Pitch (bit 0 to 7)
		0xf5      Channel 2 Pitch (bit 8 to 15)
		0xf6      Channel 3 Pitch (bit 0 to 7)
		0xf7      Channel 3 Pitch (bit 8 to 15)
		0xf8      Channel 4 Pitch (bit 0 to 7)
		0xf9      Channel 4 Pitch (bit 8 to 15)
		0xfa      Channel 5 Pitch (bit 0 to 7)
		0xfb      Channel 5 Pitch (bit 8 to 15)
		0xfc      Channel 6 Pitch (bit 0 to 7)
		0xfd      Channel 6 Pitch (bit 8 to 15)

		0xff      Sound output enable, on(1) / off(0)
		          Bit 0...6 is Channel 0...6

	*/
	void jkm_ws7_core::write8(const u8 reg, const u8 data)
	{
		switch (bitfield(reg, 4, 4))
		{
			case 0x0:
			case 0x1:
			case 0x2:
			case 0x3:
			case 0x4:
			case 0x5:
			case 0x6:
			case 0x7:
			case 0x8:
			case 0x9:
			case 0xa:
			case 0xb:
			case 0xc:
			case 0xd:
				m_voice[bitfield(reg, 5, 3)].waveform_w(bitfield(reg, 0, 5), data);
				break;
			case 0xe:
				switch (bitfield(reg, 0, 4))
				{
					case 0xe:
						{
							for (int i = 0; i < 7; i++)
								m_voice[i].set_direct_output(bitfield(data, i));
							break;
						}
					case 0xf:
						{
							for (int i = 0; i < 7; i++)
								m_voice[i].set_noise(bitfield(data, i));
							break;
						}
					default:
						if (bitfield(reg, 0))
							m_voice[bitfield(reg, 1, 3)].set_rvol(data);
						else
							m_voice[bitfield(reg, 1, 3)].set_lvol(data);
						break;
				}
				break;
			case 0xf:
				switch (bitfield(reg, 0, 4))
				{
					case 0xe: // not used
						break;
					case 0xf:
						{
							for (int i = 0; i < 7; i++)
								m_voice[i].set_enable(bitfield(data, i));
							break;
						}
					default:
						if (bitfield(reg, 0))
							m_voice[bitfield(reg, 1, 3)].set_pitch(u16(data) << 8, 0xff00);
						else
							m_voice[bitfield(reg, 1, 3)].set_pitch(data, 0x00ff);
						break;
				}
				break;
		}
	} // jkm_ws7_core::write8

	u8 jkm_ws7_core::read8(const u8 reg) const
	{
		switch (bitfield(reg, 4, 4))
		{
			case 0x0:
			case 0x1:
			case 0x2:
			case 0x3:
			case 0x4:
			case 0x5:
			case 0x6:
			case 0x7:
			case 0x8:
			case 0x9:
			case 0xa:
			case 0xb:
			case 0xc:
			case 0xd:
				return m_voice[bitfield(reg, 5, 3)].waveform_r(bitfield(reg, 0, 5));
			case 0xe:
				switch (bitfield(reg, 0, 4))
				{
					case 0xe:
						{
							u8 ret = 0;
							for (int i = 0; i < 7; i++)
								ret |= m_voice[i].direct_output() << i;
							return ret;
						}
					case 0xf:
						{
							u8 ret = 0;
							for (int i = 0; i < 7; i++)
								ret |= m_voice[i].noise() << i;
							return ret;
						}
					default:
						if (bitfield(reg, 0))
							return m_voice[bitfield(reg, 1, 3)].rvol();
						else
							return m_voice[bitfield(reg, 1, 3)].lvol();
						break;
				}
				break;
			case 0xf:
				switch (bitfield(reg, 0, 4))
				{
					case 0xe: // not used
						return 0xff;
					case 0xf:
						{
							u8 ret = 0;
							for (int i = 0; i < 7; i++)
								ret |= m_voice[i].enable() << i;
							return ret;
						}
					default:
						if (bitfield(reg, 0))
							return bitfield(m_voice[bitfield(reg, 1, 3)].pitch(), 8, 8);
						else
							return bitfield(m_voice[bitfield(reg, 1, 3)].pitch(), 0, 8);
				}
				break;
		}
		return 0xff;
	} // jkm_ws7_core::read8

	void jkm_ws7_core::tick()
	{
		m_lout = m_rout = 0;
		for (auto &v : m_voice)
		{
			v.tick();
			m_lout += v.lout();
			m_rout += v.rout();
		}
	} // jkm_ws7_core::tick

	void jkm_ws7_core::reset()
	{
		for (auto &v : m_voice)
			v.reset();

		m_lout = 0;
		m_rout = 0;
	} // jkm_ws7_core::reset
} // namespace jkm_ws7