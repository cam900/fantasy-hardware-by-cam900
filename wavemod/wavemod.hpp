/*

==============================================================================

    Wavemod
    Copyright (C) 2023-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

Wavemod

Fantasy sound generator by cam900

*/

#ifndef WAVEMOD_HPP
#define WAVEMOD_HPP

#pragma once

#include <algorithm>
#include <array>

namespace wavemod
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = unsigned char;
	using s16 = unsigned short;
	using s32 = unsigned int;
	using s64 = unsigned long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	}

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	}

	template<typename T>
	static const inline T clamp(const T in, const T min, const T max)
	{
		return (in < min) ? min : ((in > max) ? max : in);
	}

	class wavemod_intf
	{
		public:
			// constructor
			wavemod_intf()
			{
			} // wavemod_intf()

			virtual s16 read_sample(u16 addr)
			{
				return 0;
			} // s16 read_sample(u16 addr)
	}; // class wavemod_intf

	class wavemod_core
	{
		private:
			class wavemod_voice
			{
				private:
					class wavemod_extmod
					{
						public:
							// constructor
							wavemod_extmod()
								: m_enable(false)
								, m_position(1)
							{
							} // wavemod_extmod()

							inline bool enable() const { return m_enable; }
							inline void set_enable(const bool enable) { m_enable = enable; }

							inline u16 position() const { return m_position; }
							inline void set_position(const u8 bit) { m_position = (bit & 0xf); }

							inline u16 shifted_position() { return (1 << m_position); }

						private:
							bool m_enable = false;
							u16 m_position = 1;
					}; // class wavemod_extmod

					class wavemod_filter
					{
						public:
							wavemod_filter()
								: m_enable(false)
								, m_mode{false}
								, m_prev_pole{0}
								, m_pole{0}
								, m_coeff{0}
							{
							} // wavemod_filter

							void filter_apply(s32 &in);

							inline bool enable() const { return m_enable; }
							inline void set_enable(const bool enable) { m_enable = enable; }

							inline s16 coeff(const u8 slot) const { return m_coeff[slot & 3]; }
							inline void set_coeff(const u8 slot, const s16 coeff) { m_coeff[slot & 3] = coeff; }

							inline bool mode(const u8 slot) { return ((slot & 3) == 3) ? 0 : m_mode[slot & 3]; }
							inline void set_mode(const u8 slot, const bool mode) { if ((slot & 3) != 3) { m_mode[slot & 3] = mode; } }

						private:
							inline void lp_exec(const u8 in, const u8 out);
							inline void hp_exec(const u8 in, const u8 out);

							bool m_enable = false;
							bool m_mode[3] = {false};
							s32 m_prev_pole[5] = {0};
							s32 m_pole[5] = {0};
							s16 m_coeff[4] = {0};
					}; // class wavemod_filter

					class wavemod_postmod
					{
						public:
							wavemod_postmod()
								: m_enable(false)
								, m_mod_ch{0}
								, m_shift(0)
							{
							} // wavemod_postmod()

							inline bool enable() const { return m_enable; }
							inline void set_enable(const bool enable) { m_enable = enable; }

							inline u8 mod_ch(const u8 slot) const { return m_mod_ch[slot & 1]; }
							inline void set_mod_ch(const u8 slot, const u8 ch) { m_mod_ch[slot & 1] = ch & 0x3f; }

							inline u8 shift() const { return m_shift; }
							inline void set_shift(const u8 shift) { m_shift = shift; }

						private:
							bool m_enable = false;
							u8 m_mod_ch[2] = {0};
							u8 m_shift = 0;
					}; // class wavemod_postmod

					class wavemod_eg
					{
						private:
							enum wavemod_eg_status : u8
							{
								EG_IDLE = 0,
								EG_ATTACK,
								EG_DECAY,
								EG_SUSTAIN,
								EG_RELEASE
							}; // enum wavemod_eg_status

							class wavemod_eg_params
							{
								public:
									wavemod_eg_params()
										: m_reg_speed(0)
										, m_reg_target(0)
										, m_speed(0)
										, m_target(0)
									{
									} // wavemod_eg_params()

									inline u16 reg_speed() const { return m_reg_speed; }
									inline void set_speed(const u16 speed) { m_reg_speed = speed; m_speed = get_envelope(speed); }
									inline u16 reg_target() const { return m_reg_target; }
									inline void set_target(const u16 target) { m_reg_target = target; m_target = get_envelope(target); }

									inline s32 speed() const { return m_speed; }
									inline s32 target() const { return clamp<s32>(m_target, -0x2000000, 0x2000000); }
								private:
									s32 get_envelope(u16 in)
									{
										s32 exp = bitfield(in, 11, 4);
										s32 out = 0;
										if (exp > 1)
										{
											out = (0x800 | (in & 0x7ff)) << (exp - 1);
										}
										else
										{
											out = in & 0xfff;
										}
										if (bitfield(in, 15))
										{
											out = -out;
										}
										return out;
									}
									u16 m_reg_speed = 0;
									u16 m_reg_target = 0;
									s32 m_speed = 0;
									s32 m_target = 0;

							}; // class wavemod_eg_params
						public:
							wavemod_eg(wavemod_voice &host)
								: m_host(host)
								, m_attack(wavemod_eg_params())
								, m_decay(wavemod_eg_params())
								, m_sustain(wavemod_eg_params())
								, m_release(wavemod_eg_params())
								, m_status(EG_IDLE)
								, m_enable(false)
								, m_vol(0)
								, m_speed(0)
								, m_target(0)
							{
							} // wavemod_eg()

							void keyon();
							void keyoff();
							void update();

							wavemod_eg_params &attack() { return m_attack; }
							wavemod_eg_params &decay() { return m_decay; }
							wavemod_eg_params &sustain() { return m_sustain; }
							wavemod_eg_params &release() { return m_release; }
							wavemod_eg_status status() { return m_status; }

							inline bool enable() const { return m_enable; }
							inline void set_enable(const bool enable) { m_enable = enable; }

							inline s32 vol() const { return m_vol; }
						private:
							wavemod_voice &m_host;
							wavemod_eg_params m_attack;
							wavemod_eg_params m_decay;
							wavemod_eg_params m_sustain;
							wavemod_eg_params m_release;
							wavemod_eg_status m_status;
							bool m_enable = false;
							s32 m_vol = 0;
							s32 m_speed = 0;
							s32 m_target = 0;
							u16 m_reg_speed = 0;
					}; // class wavemod_eg
				public:
					// constructor
					wavemod_voice(wavemod_core &host)
						: m_host(host)
						, m_filter(wavemod_filter())
						, m_eg(wavemod_eg(*this))
						, m_mute(wavemod_extmod())
						, m_hinv(wavemod_extmod())
						, m_vinv(wavemod_extmod())
						, m_am(wavemod_postmod())
						, m_fm(wavemod_postmod())
						, m_pm(wavemod_postmod())
						, m_busy(false)
						, m_pause(false)
						, m_loop(false)
						, m_ringmod(false)
						, m_ext_wave(false)
						, m_pulse(false)
						, m_triangle(false)
						, m_sawtooth(false)
						, m_white_noise(false)
						, m_ipulse(false)
						, m_itriangle(false)
						, m_isawtooth(false)
						, m_peri_noise(false)
						, m_ringmod_ch(0)
						, m_base(0)
						, m_addr(0)
						, m_wavelen(1)
						, m_intlen(0)
						, m_pulse_duty(0)
						, m_period(0)
						, m_counter(0)
						, m_noise_seed(1)
						, m_lfsr(1)
						, m_lvol(0)
						, m_rvol(0)
						, m_mvol(0)
						, m_output(0)
						, m_loutput(0)
						, m_routput(0)
					{
					}
					void tick(const u8 c);
					void set_keyon();
					void set_keyoff();

					wavemod_filter &filter() { return m_filter; }

					wavemod_eg &eg() { return m_eg; }

					wavemod_extmod &mute() { return m_mute; }

					wavemod_extmod &hinv() { return m_hinv; }

					wavemod_extmod &vinv() { return m_vinv; }

					wavemod_postmod &am() { return m_am; }

					wavemod_postmod &fm() { return m_fm; }

					wavemod_postmod &pm() { return m_pm; }

					inline bool keyon() const { return m_keyon; }

					inline bool busy() const { return m_busy; }
					inline void set_busy(const bool busy) { m_busy = busy; }

					inline bool pause() const { return m_pause; }
					inline void set_pause(const bool pause) { m_pause = pause; }

					inline bool loop() const { return m_loop; }
					inline void set_loop(const bool loop) { m_loop = loop; }

					inline bool ringmod() const { return m_ringmod; }
					inline void set_ringmod(const bool ringmod) { m_ringmod = ringmod; }

					inline bool ext_wave() const { return m_ext_wave; }
					inline void set_ext_wave(const bool ext_wave) { m_ext_wave = ext_wave; }

					inline bool pulse() const { return m_pulse; }
					inline void set_pulse(const bool pulse) { m_pulse = pulse; }

					inline bool triangle() const { return m_triangle; }
					inline void set_triangle(const bool triangle) { m_triangle = triangle; }

					inline bool sawtooth() const { return m_sawtooth; }
					inline void set_sawtooth(const bool sawtooth) { m_sawtooth = sawtooth; }

					inline bool white_noise() const { return m_white_noise; }
					inline void set_white_noise(const bool white_noise) { m_white_noise = white_noise; }

					inline bool ipulse() const { return m_ipulse; }
					inline void set_ipulse(const bool ipulse) { m_ipulse = ipulse; }

					inline bool itriangle() const { return m_itriangle; }
					inline void set_itriangle(const bool itriangle) { m_itriangle = itriangle; }

					inline bool isawtooth() const { return m_isawtooth; }
					inline void set_isawtooth(const bool isawtooth) { m_isawtooth = isawtooth; }

					inline bool periodic_noise() const { return m_peri_noise; }
					inline void set_periodic_noise(const bool peri_noise) { m_peri_noise = peri_noise; }

					inline u8 ringmod_ch() { return m_ringmod_ch; }
					inline void set_ringmod_ch(const u8 ch) { m_ringmod_ch = ch; }

					inline u16 base() const { return m_base; }
					inline void set_base(const u16 base) { m_base = base; }
					
					inline u16 addr() const { return m_addr; }
					inline void set_addr(const u16 addr) { m_addr = addr; }
					
					inline u16 wavelen() const { return m_wavelen; }
					inline void set_wavelen(const u8 bit) { m_wavelen = (bit & 0xf); }
					
					inline u16 intlen() const { return m_intlen; }
					inline void set_intlen(const u8 bit) { m_intlen = (bit & 0xf); }
					
					inline u16 pulse_duty() const { return m_pulse_duty; }
					inline void set_pulse_duty(const u16 pulse_duty) { m_pulse_duty = pulse_duty; }
					
					inline u16 period() const { return m_period; }
					inline void set_period(const u16 period) { m_period = period; }
					
					inline u16 noise_seed() const { return m_noise_seed; }
					inline void set_noise_seed(const u16 noise_seed) { m_noise_seed = noise_seed; }
					
					inline u16 lvol() const { return m_lvol; }
					inline void set_lvol(const u16 lvol) { m_lvol = lvol; }
					
					inline u16 rvol() const { return m_rvol; }
					inline void set_rvol(const u16 rvol) { m_rvol = rvol; }
					
					inline u16 mvol() const { return m_mvol; }
					inline void set_mvol(const u16 mvol) { m_mvol = mvol; }
					
					inline s32 output() const { return m_output; }
					inline s32 loutput() const { return m_loutput; }
					inline s32 routput() const { return m_routput; }

				private:
					wavemod_core &m_host;
					wavemod_filter m_filter;    // Filter
					wavemod_eg m_eg;            // Envelope Generator
					wavemod_extmod m_mute;      // mute position
					wavemod_extmod m_hinv;      // horizontal invert position
					wavemod_extmod m_vinv;      // vertical invert position
					wavemod_postmod m_am;       // AM
					wavemod_postmod m_fm;       // FM
					wavemod_postmod m_pm;       // PM
					bool m_keyon = false;       // Key on flag
					bool m_busy = false;        // busy flag
					bool m_pause = false;       // pause flag
					bool m_loop = false;        // loop flag
					bool m_ringmod = false;     // ringmod flag
					bool m_ext_wave = false;    // External waveform enable
					bool m_pulse = false;       // Pulse wave
					bool m_triangle = false;    // Triangle wave
					bool m_sawtooth = false;    // Sawtooth wave
					bool m_white_noise = false; // White noise
					bool m_ipulse = false;      // Inverted Pulse wave
					bool m_itriangle = false;   // Inverted Triangle wave
					bool m_isawtooth = false;   // Inverted Sawtooth wave
					bool m_peri_noise = false;  // periodic noise
					u8 m_ringmod_ch = 0;             // Ring mod input channel
					u16 m_base = 0;             // external waveform base address
					u16 m_addr = 0;             // address counter
					u16 m_wavelen = 1;          // waveform length
					u16 m_intlen = 0;          // waveform length
					u16 m_pulse_duty = 0;       // Pulse duty
					u32 m_period = 0;           // clock period
					u32 m_counter = 0;          // clock counter
					u32 m_noise_seed = 1;       // noise seed
					u32 m_lfsr = 1;             // LFSR
					s16 m_lvol = 0;             // Left volume
					s16 m_rvol = 0;             // Right volume
					s16 m_mvol = 0;             // Master volume
					s32 m_output = 0;           // output
					s32 m_loutput = 0;          // Left output
					s32 m_routput = 0;          // Right output
			}; // class wavemod_voice

		public:
			// constructor
			wavemod_core(wavemod_intf &intf)
				: m_intf(intf)
				, m_voice{
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
				  }
				, m_lout(0)
				, m_rout(0)
				, m_channel_select(0)
			{
			} // wavemod_core()

			void tick();

			void write16(const u8 addr, const u16 data);

		private:
			wavemod_intf &m_intf;
			std::array<wavemod_voice, 64> m_voice;
			s32 m_lout = 0;
			s32 m_rout = 0;
			u16 m_channel_select = 0;
	}; // class wavemod_core

}; // namespace wavemod

#endif // WAVEMOD_HPP