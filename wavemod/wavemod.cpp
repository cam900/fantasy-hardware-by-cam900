/*

==============================================================================

    Wavemod
    Copyright (C) 2023-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

Wavemod

Fantasy sound generator by cam900

Specifications:

AM, FM, PM, Ring modulation
4 pole digital filter (user configurable)
internal & external waveform generator
Hardware envelope generator

*/

#include "wavemod.hpp"

namespace wavemod
{
	/*

		Register map

		Register Bit                 Description
		         fedc ba98 7654 3210
		00       Playback
		         ---- ---- ---- x--- Envelope enable/disable
		         ---- ---- ---- -x-- One-shot(0)/Loop(1) waveform
		         ---- ---- ---- --x- Pause
		         ---- ---- ---- ---x Busy
		Waveform configuration
		01       x--- ---- ---- ---- White noise
		         -x-- ---- ---- ---- Pulse wave
		         --x- ---- ---- ---- Triangle wave
		         ---x ---- ---- ---- Sawtooth wave
		         ---- x--- ---- ---- Periodic noise
		         ---- -x-- ---- ---- Inverted Pulse wave
		         ---- --x- ---- ---- Inverted Triangle wave
		         ---- ---x ---- ---- Inverted Sawtooth wave
		         ---- ---- xxxx ---- External waveform size (1 << x)
		         ---- ---- ---- xxxx Internal waveform size (1 << x)
		External waveform modification
		02       xxxx ---- ---- ---- Horizontal invert position (1 << x)
		         ---- xxxx ---- ---- Vertical invert position (1 << x)
		         ---- ---- xxxx ---- Mute position (1 << x)
		         ---- ---- ---- x--- Horizontal invert enable
		         ---- ---- ---- -x-- Vertical invert enable
		         ---- ---- ---- --x- Mute enable
		         ---- ---- ---- ---x External waveform enable
		03       xxxx xxxx xxxx xxxx Period
		04       xxxx xxxx xxxx xxxx External waveform base address
		05       xxxx xxxx xxxx xxxx External waveform address accumulator
		06       xxxx xxxx xxxx xxxx Pulse duty
		07       xxxx xxxx xxxx xxxx Noise seed
		08       xxxx xxxx xxxx xxxx Left volume
		09       xxxx xxxx xxxx xxxx Right volume
		0A       xxxx xxxx xxxx xxxx Master volume
		Post processing
		0C       x--- ---- ---- ---- AM enable
		         -x-- ---- ---- ---- FM enable
		         --x- ---- ---- ---- PM enable
		         ---x ---- ---- ---- Ring mod enable
		         ---- xxxx xx-- ---- Ring mod input channel
		         ---- ---- ---- x--- Filter mode D
		         ---- ---- ---- -x-- Filter mode C
		         ---- ---- ---- --x- Filter mode B
		         ---- ---- ---- ---x Filter enable
		0D       xxxx ---- ---- ---- AM shift
		         ---- xxxx xx-- ---- AM input channel A
		         ---- ---- --xx xxxx AM input channel B
		0E       xxxx ---- ---- ---- FM shift
		         ---- xxxx xx-- ---- FM input channel A
		         ---- ---- --xx xxxx FM input channel B
		0F       xxxx ---- ---- ---- PM shift
		         ---- xxxx xx-- ---- PM input channel A
		         ---- ---- --xx xxxx PM input channel B
		10       xxxx xxxx xxxx xxxx Filter coefficient A
		11       xxxx xxxx xxxx xxxx Filter coefficient B
		12       xxxx xxxx xxxx xxxx Filter coefficient C
		13       xxxx xxxx xxxx xxxx Filter coefficient D
		Volume envelope
		14       xxxx xxxx xxxx xxxx Attack speed
		         x--- ---- ---- ---- Sign
		         -xxx x--- ---- ---- Exponent
		         ---- -xxx xxxx xxxx Mantissa
		15       xxxx xxxx xxxx xxxx Attack target
		         x--- ---- ---- ---- Sign
		         -xxx x--- ---- ---- Exponent
		         ---- -xxx xxxx xxxx Mantissa
		         -111 1--- ---- ---- Max value
		16       xxxx xxxx xxxx xxxx Decay speed
		17       xxxx xxxx xxxx xxxx Decay target
		18       xxxx xxxx xxxx xxxx Sustain speed
		19       xxxx xxxx xxxx xxxx Sustain target
		1A       xxxx xxxx xxxx xxxx Release speed
		3F       ---- ---- --xx xxxx Channel select

	*/
	void wavemod_core::write16(const u8 addr, const u16 data)
	{
		wavemod_voice &voice = m_voice[m_channel_select & 0x3f];
		switch (addr & 0x3f)
		{
			case 0x00:
				voice.set_pause(bitfield(data, 1));
				voice.set_loop(bitfield(data, 2));
				voice.eg().set_enable(bitfield(data, 3));
				if ((!voice.keyon()) && (bitfield(data, 0) == 1))
				{
					voice.set_keyon();
				}
				else if (voice.keyon() && (bitfield(data, 0) == 0))
				{
					voice.set_keyoff();
				}
				break;
			case 0x01:
				voice.set_white_noise(bitfield(data, 15));
				voice.set_sawtooth(bitfield(data, 14));
				voice.set_triangle(bitfield(data, 13));
				voice.set_pulse(bitfield(data, 12));
				voice.set_periodic_noise(bitfield(data, 11));
				voice.set_isawtooth(bitfield(data, 10));
				voice.set_itriangle(bitfield(data, 9));
				voice.set_ipulse(bitfield(data, 8));
				voice.set_wavelen(bitfield(data, 4, 4));
				voice.set_intlen(bitfield(data, 0, 4));
				break;
			case 0x02:
				voice.hinv().set_position(bitfield(data, 12, 4));
				voice.vinv().set_position(bitfield(data, 8, 4));
				voice.mute().set_position(bitfield(data, 4, 4));
				voice.hinv().set_enable(bitfield(data, 3));
				voice.vinv().set_enable(bitfield(data, 2));
				voice.mute().set_enable(bitfield(data, 1));
				voice.set_ext_wave(bitfield(data, 0));
				break;
			case 0x03:
				voice.set_period(data);
				break;
			case 0x04:
				voice.set_base(data);
				break;
			case 0x05:
				voice.set_addr(data);
				break;
			case 0x06:
				voice.set_pulse_duty(data);
				break;
			case 0x07:
				voice.set_noise_seed(data);
				break;
			case 0x08:
				voice.set_lvol(data);
				break;
			case 0x09:
				voice.set_rvol(data);
				break;
			case 0x0a:
				voice.set_mvol(data);
				break;
			case 0x0c:
				voice.am().set_enable(bitfield(data, 15));
				voice.fm().set_enable(bitfield(data, 14));
				voice.pm().set_enable(bitfield(data, 13));
				voice.set_ringmod(bitfield(data, 12));
				voice.set_ringmod_ch(bitfield(data, 6, 6));
				voice.filter().set_mode(2, bitfield(data, 3));
				voice.filter().set_mode(1, bitfield(data, 2));
				voice.filter().set_mode(0, bitfield(data, 1));
				voice.filter().set_enable(bitfield(data, 0));
				break;
			case 0x0d:
				voice.am().set_shift(bitfield(data, 12, 4));
				voice.am().set_mod_ch(0, bitfield(data, 6, 6));
				voice.am().set_mod_ch(1, bitfield(data, 0, 6));
				break;
			case 0x0e:
				voice.fm().set_shift(bitfield(data, 12, 4));
				voice.fm().set_mod_ch(0, bitfield(data, 6, 6));
				voice.fm().set_mod_ch(1, bitfield(data, 0, 6));
				break;
			case 0x0f:
				voice.pm().set_shift(bitfield(data, 12, 4));
				voice.pm().set_mod_ch(0, bitfield(data, 6, 6));
				voice.pm().set_mod_ch(1, bitfield(data, 0, 6));
				break;
			case 0x10:
				voice.filter().set_coeff(0, data);
				break;
			case 0x11:
				voice.filter().set_coeff(1, data);
				break;
			case 0x12:
				voice.filter().set_coeff(2, data);
				break;
			case 0x13:
				voice.filter().set_coeff(3, data);
				break;
			case 0x14:
				voice.eg().attack().set_speed(data);
				break;
			case 0x15:
				voice.eg().attack().set_target(data);
				break;
			case 0x16:
				voice.eg().decay().set_speed(data);
				break;
			case 0x17:
				voice.eg().decay().set_target(data);
				break;
			case 0x18:
				voice.eg().sustain().set_speed(data);
				break;
			case 0x19:
				voice.eg().sustain().set_target(data);
				break;
			case 0x1a:
				voice.eg().release().set_speed(data);
				break;
			case 0x3f:
				m_channel_select = bitfield(data, 0, 6);
				break;
			default:
				break;
		}
	};

	void wavemod_core::tick()
	{
		for (int i = 0; i < 64; i++)
		{
			m_voice[i].tick(i);
		}
	} // wavemod_core::tick()

	void wavemod_core::wavemod_voice::tick(const u8 c)
	{
		if (m_busy && !m_pause)
		{
			// phase modulation
			u16 addr = m_addr;
			if (m_pm.enable())
			{
				s32 addr_mod = m_host.m_voice[u8(c + m_pm.mod_ch(0)) & 0x3f].output();
				addr_mod += m_host.m_voice[u8(c + m_pm.mod_ch(1)) & 0x3f].output();
				addr_mod >>= m_pm.shift() + 1;
				addr += addr_mod;
			}
			// external waveform generate
			s16 ext_wave = 0;
			if (m_ext_wave)
			{
				u16 wave_addr = addr & ((1 << m_wavelen) - 1);
				// horizontal invert
				if (m_hinv.enable())
				{
					if (addr & m_hinv.shifted_position())
					{
						wave_addr = (1 << m_wavelen) - 1 - wave_addr;
					}
				}
				// fetch external wave
				ext_wave = m_host.m_intf.read_sample(m_base + wave_addr);
				// vertical invert
				if (m_vinv.enable())
				{
					if (addr & m_vinv.shifted_position())
					{
						ext_wave = -ext_wave;
					}
				}
				// mute
				if (m_mute.enable())
				{
					if (addr & m_mute.shifted_position())
					{
						ext_wave = 0;
					}
				}
			}
			// internal waveform generate
			u16 int_addr = m_addr << m_intlen;
			s16 int_wave = 0;
			if (m_pulse)
			{
				int_wave += (int_addr > m_pulse_duty) ? 0x7fff : -0x8000;
			}
			if (m_ipulse)
			{
				int_wave += (int_addr < m_pulse_duty) ? 0x7fff : -0x8000;
			}
			if (m_triangle)
			{
				int_wave += -0x8000 + ((int_addr & 0x8000) ? (0xffff - ((int_addr & 0x7fff) << 1)) : (m_addr << 1));
			}
			if (m_itriangle)
			{
				int_wave += 0x7fff - ((int_addr & 0x8000) ? (0xffff - ((int_addr & 0x7fff) << 1)) : (m_addr << 1));
			}
			if (m_sawtooth)
			{
				int_wave += -0x8000 + (int_addr);
			}
			if (m_isawtooth)
			{
				int_wave += 0x7fff - (int_addr);
			}
			if (m_white_noise)
			{
				int_wave += s16(m_lfsr);
			}
			if (m_peri_noise)
			{
				int_wave += (m_lfsr & 1) ? 0x7fff : -0x8000;
			}
			s32 out = s32(s16(ext_wave + int_wave));
			m_filter.filter_apply(out);
			// Ring modulation
			if (m_ringmod)
			{
				out = clamp<s32>((out * m_host.m_voice[u8(c + m_ringmod_ch) & 0x3f].output()) >> 15, -0x8000, 0x7fff);
			}
			// Amplitude modulation
			if (m_am.enable())
			{
				s32 out_mod = m_host.m_voice[u8(c + m_am.mod_ch(0)) & 0x3f].output();
				out_mod += m_host.m_voice[u8(c + m_am.mod_ch(1)) & 0x3f].output();
				out_mod >>= m_am.shift() + 1;
				out = clamp<s32>(out + out_mod, -0x8000, 0x7fff);
			}
			// Frequency modulation
			s32 period = m_period;
			if (m_fm.enable())
			{
				s32 period_mod = m_host.m_voice[u8(c + m_fm.mod_ch(0)) & 0x3f].output();
				period_mod += m_host.m_voice[u8(c + m_fm.mod_ch(1)) & 0x3f].output();
				period_mod >>= m_fm.shift() + 1;
				period = clamp<s32>(period - period_mod, 0, 0xffff);
			}
			m_output = (out * m_mvol) >> 15;
			if (m_eg.enable())
			{
				m_eg.update();
				m_output = s64(m_output) * s64(m_eg.vol()) >> 25;
			}
			m_loutput = (m_output * m_lvol) >> 15;
			m_routput = (m_output * m_rvol) >> 15;
			// clock tick
			if (++m_counter > period)
			{
				// apply noise seed
				for (int i = 0; i < 16; i++)
				{
					if (bitfield(m_noise_seed, i))
						m_lfsr ^= bitfield(m_lfsr, i) << 16;
				}
				m_lfsr >>= 1;
				if (m_lfsr == 0)
				{
					m_lfsr = 1;
				}
				m_addr++;
				// one shot waveform
				if (!m_loop && (m_addr >= m_wavelen))
				{
					m_busy = false;
				}
				m_counter = 0;
			}
        }
		else
		{
			m_output = 0;
			m_loutput = 0;
			m_routput = 0;
		}
	} // wavemod_core::wavemod_voice::tick(const u8 c)

	void wavemod_core::wavemod_voice::set_keyon()
	{
		m_keyon = true;
		m_busy = true;
		m_addr = 0;
		m_lfsr = 1;
		if (m_eg.enable())
		{
			m_eg.keyon();
		}
	} // wavemod_core::wavemod_voice::set_keyon()

	void wavemod_core::wavemod_voice::set_keyoff()
	{
		m_keyon = false;
		if (m_eg.enable())
		{
			m_eg.keyoff();
		}
		else
		{
			m_busy = false;
		}
	} // wavemod_core::wavemod_voice::set_keyoff()

	// Filter
	void wavemod_core::wavemod_voice::wavemod_filter::filter_apply(s32 &in)
	{
		m_pole[0] = in;
		if (m_enable)
		{
			// First pole is low pass
			lp_exec(0, 1);
			for (int i = 1; i < 4; i++)
			{
				if (m_mode[i - 1]) // High pass
				{
					hp_exec(i, i + 1);
				}
				else // Low pass
				{
					lp_exec(i, i + 1);
				}
			}
		}
		else
		{
			for (int i = 0; i < 4; i++)
			{
				m_prev_pole[i + 1] = m_pole[i + 1];
				m_pole[i + 1] = m_pole[i];
			}
		}
		in = m_pole[4];
	} // wavemod_core::wavemod_voice::wavemod_filter::filter_apply(const s32 in)

	inline void wavemod_core::wavemod_voice::wavemod_filter::lp_exec(const u8 in, const u8 out)
	{
		// Yn = K*(Xn - Yn-1) + Yn-1
		m_prev_pole[out] = m_pole[out];
  		m_pole[out] = ((m_coeff[in] * (m_pole[in] - m_pole[out])) >> 15) + m_pole[out];
	} // wavemod_core::wavemod_voice::wavemod_filter::lp_exec(const u8 in, const u8 out)

	inline void wavemod_core::wavemod_voice::wavemod_filter::hp_exec(const u8 in, const u8 out)
	{
		// Yn = Xn - Xn-1 + K*Yn-1
		m_prev_pole[out] = m_pole[out]; \
		m_pole[out] = m_pole[in] - m_prev_pole[in] + ((m_coeff[in] * m_pole[out]) >> 16) + (m_pole[out] >> 1);
	} // wavemod_core::wavemod_voice::wavemod_filter::hp_exec(const u8 in, const u8 out)

	// Envelope generator
	void wavemod_core::wavemod_voice::wavemod_eg::update()
	{
		if ((m_status != EG_IDLE) && ((m_reg_speed & 0x7fff) != 0))
		{
			m_vol += m_speed;
			if ((m_speed > 0 && m_vol >= m_target) || (m_speed < 0 && m_vol <= m_target))
			{
				m_vol = m_target;
				switch (m_status)
				{
					case EG_ATTACK:
						m_reg_speed = decay().reg_speed();
						m_speed = decay().speed();
						m_target = decay().target();
						m_status = EG_DECAY;
						break;
					case EG_DECAY:
						m_reg_speed = sustain().reg_speed();
						m_speed = sustain().speed();
						m_target = sustain().target();
						m_status = EG_SUSTAIN;
						break;
					case EG_SUSTAIN:
						m_reg_speed = 0;
						m_speed = 0;
						m_target = sustain().target();
						m_status = EG_IDLE;
						break;
					case EG_RELEASE:
						m_reg_speed = 0;
						m_speed = 0;
						m_target = 0;
						m_status = EG_IDLE;
						m_host.set_busy(false);
						break;
				}
			}
		}
	} // wavemod_core::wavemod_voice::wavemod_eg::update()

	void wavemod_core::wavemod_voice::wavemod_eg::keyon()
	{
		m_reg_speed = attack().reg_speed();
		m_speed = attack().speed();
		m_target = attack().target();
		m_vol = 0;
		m_status = EG_ATTACK;
	} // wavemod_core::wavemod_voice::wavemod_eg::keyon()

	void wavemod_core::wavemod_voice::wavemod_eg::keyoff()
	{
		m_reg_speed = release().reg_speed();
		m_speed = release().speed();
		m_target = 0;
		m_status = EG_RELEASE;
	} // wavemod_core::wavemod_voice::wavemod_eg::keyon()

} // namespace wavemod