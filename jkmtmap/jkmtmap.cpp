
/*

==============================================================================

    JKMTMAP
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMTMAP

Fantasy tilemap generator hardware

	Features:
	- size: 8x8x8bpp per tiles, 64x64 tiles per page (512x512 pixel)
	- Single tilemap per chip, with RotoZoom support
	- Column scroll/select, Row scroll/select support

	8 bit Host interface

	Address Description
	00      Register select
	02      Register data LSB
	03      Register data MSB

	16 bit Host interface

	Address Description
	00      Register select
	01      Register data

	Register map

	Register Description
	00       VRAM address MSB
	01       VRAM address LSB
	02       VRAM data MSB
	03       VRAM data LSB
	04       CRAM address MSB
	05       CRAM address LSB
	06       CRAM data MSB
	07       CRAM data LSB
	08       Control
	         Start position scale (Bit 0 to 3)
	         Increment scale (Bit 4 to 7)
	         Accumulator shift (Bit 8 to 11)
	         Number of horizontal page (Bit 12 to 13)
	         Number of vertical page (Bit 14 to 15)
	09       Control #2
	         Flip X (Bit 0)
	         Flip Y (Bit 1)
	         Wraparound X (Bit 2)
	         Wraparound Y (Bit 3)
	         Rowscroll enable (Bit 4)
	         Columnscroll enable (Bit 5)
	         Tilemap enable (Bit 15)
	0a       Start X
	0b       Start Y
	0c       Increment X per X
	0d       Increment X per Y
	0e       Increment Y per X
	0f       Increment Y per Y
	10       Rowscroll base address LSB (Bit 0 to 15)
	11       Rowscroll base address MSB (Bit 0 to 15)
	12       Columnscroll base address LSB (Bit 0 to 15)
	13       Columnscroll base address MSB (Bit 0 to 15)
	14       Page base address LSB (Bit 0 to 15)
	15       Page base address MSB (Bit 0 to 15)

	Tile format

	xxxx xxxx ---- ---- ---- ---- ---- ---- Color/Priority
	---- ---- x--- ---- ---- ---- ---- ---- Flip X
	---- ---- -x-- ---- ---- ---- ---- ---- Flip Y
	---- ---- --xx xxxx xxxx xxxx xxxx xxxx Tile index

	Rowscroll format

	Address Description
	0       Start X
	1       Start Y
	2       Increment X per X
	3       Increment Y per X

	Columnscroll format

	Address Description
	0       Start X
	1       Start Y

*/

#include "jkmtmap.hpp"

namespace jkmtmap
{
	template<bool BigEndian, typename T>
	void jkmtmap_t<BigEndian, T>::reset()
	{
		m_enable = false;
		m_flipx = false;
		m_flipy = false;
		m_wrap_x = false;
		m_wrap_y = false;
		m_rowscroll = false;
		m_colscroll = false;
		m_startx = 0;
		m_starty = 0;
		m_incxx = 0;
		m_incxy = 0;
		m_incyx = 0;
		m_incyy = 0;
		m_rowscroll_base = 0;
		m_colscroll_base = 0;
		m_page_base = 0;
		m_start_scale = 0;
		m_inc_scale = 0;
		m_accum_shift = 0;
		m_page_width = 0;
		m_page_height = 0;

		m_attr_ram_addr = 0;
		m_char_ram_addr = 0;
		m_slot = 0;
	}

	template<bool BigEndian, typename T>
	void jkmtmap_t<BigEndian, T>::draw_pixel(const s32 x, const s32 y, const u32 rx, const u32 ry)
	{
		u32 cx = rx;
		u32 cy = ry;
		if (m_colscroll)
		{
			cx += s32(s16(m_intf.read_attr_word(((m_colscroll_base + x) << 2) + 0))) << (16 - m_start_scale);
			cy += s32(s16(m_intf.read_attr_word(((m_colscroll_base + x) << 2) + 2))) << (16 - m_start_scale);
		}
		cx >>= m_accum_shift;
		cy >>= m_accum_shift;

		const u32 tilemap_width = ((1 << m_page_width) << 9);
		const u32 tilemap_height = ((1 << m_page_height) << 9);
		if (m_flipx)
		{
			cx = tilemap_width - cx - 1;
		}
		if (m_flipy)
		{
			cy = tilemap_width - cy - 1;
		}

		if ((m_wrap_x || (cx < tilemap_width)) && (m_wrap_y || (cy < tilemap_height)))
		{
			if (m_wrap_x)
			{
				cx &= tilemap_width - 1;
			}
			if (m_wrap_y)
			{
				cy &= tilemap_height - 1;
			}
			const u32 page_x = bitfield(cx, 9, 2);
			const u32 page_y = bitfield(cy, 9, 2);
			const u32 px = bitfield(cx, 3, 6);
			const u32 py = bitfield(cy, 3, 6);
			const u32 tx = bitfield(cx, 0, 3);
			const u32 ty = bitfield(cy, 0, 3);
			const u32 page_addr = m_intf.read_attr_byte(m_page_base + (page_x | (page_y << m_page_width))) << 14;
			const u32 tilemap_addr = page_addr + ((px | (py << 6)) << 2);

			const u32 tile_attr = m_intf.read_attr_dword(tilemap_addr);
			const u32 tile_index = bitfield(tile_attr, 0, 22);
			const bool flipx = bitfield(tile_attr, 22);
			const bool flipy = bitfield(tile_attr, 23);
			const u8 colpri = bitfield(tile_attr, 24, 8);

			const u32 tile_addr = (flipx ? (7 - tx) : tx) | ((flipy ? (7 - ty) : ty) << 3);
			const u8 pixel = m_intf.read_char_byte((tile_index << 6) | tile_addr);
			if ((pixel != 0))
			{
				m_intf.write_pixel(x, y, pixel | (u16(colpri) << 8));
			}
			else
			{
				m_intf.write_pixel(x, y, 0);
			}
		}
	} // jkmtmap_t<BigEndian, T>::draw_pixel

	template<bool BigEndian, typename T>
	void jkmtmap_t<BigEndian, T>::draw_row(const s32 y, const s32 left, const s32 right, const u32 sx, const u32 sy)
	{
		u32 rx = m_rowscroll ? (m_intf.read_attr_word(((m_rowscroll_base + y) << 3) + 0) << (16 - m_start_scale)) : sx;
		u32 ry = m_rowscroll ? (m_intf.read_attr_word(((m_rowscroll_base + y) << 3) + 2) << (16 - m_start_scale)) : sy;
		const s32 incxx = s32(s16(m_rowscroll ? m_intf.read_attr_word(((m_rowscroll_base + y) << 3) + 4) : m_incxx)) << (16 - m_inc_scale);
		const s32 incyx = s32(s16(m_rowscroll ? m_intf.read_attr_word(((m_rowscroll_base + y) << 3) + 6) : m_incyx)) << (16 - m_inc_scale);
		for (s32 x = left; x <= right; x++)
		{
			draw_pixel(x, y, rx, ry);
			rx += incxx;
			ry += incyx;
		}
	} // jkmtmap_t<BigEndian, T>::draw_row

	template<bool BigEndian, typename T>
	void jkmtmap_t<BigEndian, T>::draw_tilemap(const s32 left, const s32 right, const s32 top, const s32 bottom)
	{
		if (m_enable)
		{
			const s32 incxx = s32(s16(m_incxx)) << (16 - m_inc_scale);
			const s32 incxy = s32(s16(m_incxy)) << (16 - m_inc_scale);
			const s32 incyx = s32(s16(m_incyx)) << (16 - m_inc_scale);
			const s32 incyy = s32(s16(m_incyy)) << (16 - m_inc_scale);

			u32 sx = m_startx << (16 - m_start_scale);
			u32 sy = m_starty << (16 - m_start_scale);

			sx += (incxx * left) + (incxy * top);
			sy += (incyx * left) + (incyy * top);
			for (s32 y = top; y <= bottom; y++)
			{
				draw_row(y, sx, sy);
				sx += incxy;
				sy += incyy;
			}
		}
		else
		{
			for (s32 y = top; y <= bottom; y++)
			{
				for (s32 x = left; x <= right; x++)
				{
					m_intf.write_pixel(x, y, 0);
				}
			}
		}
	} // jkmtmap_t<BigEndian, T>::draw_tilemap

	template<bool BigEndian, typename T>
	T jkmtmap_t<BigEndian, T>::host_r(const u8 addr)
	{
		T ret = 0;

		switch (sizeof(T))
		{
			case 1:
			{
				if (bitfield(addr, 1))
				{
					const u8 shift = bitfield(BigEndian ? ~addr : addr, 0) << 3;
					const u8 shift2 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
					switch (m_slot)
					{
						case 0x00:
						case 0x01:
							ret = (m_attr_ram_addr >> shift2) >> shift;
							break;
						case 0x02:
						case 0x03:
							ret = (m_intf.read_attr_dword(m_attr_ram_addr << 2) >> shift2) >> shift;
							if (bitfield(addr, 0) && bitfield(m_slot, 0))
							{
								m_attr_ram_addr++;
							}
							break;
						case 0x04:
						case 0x05:
							ret = (m_char_ram_addr >> shift2) >> shift;
							break;
						case 0x06:
						case 0x07:
							ret = (m_intf.read_char_dword(m_char_ram_addr << 2) >> shift2) >> shift;
							if (bitfield(addr, 0) && bitfield(m_slot, 0))
							{
								m_char_ram_addr++;
							}
							break;
						case 0x08:
							ret = (m_start_scale
								| (m_inc_scale << 4)
								| (m_accum_shift << 8)
								| (m_page_width << 12)
								| (m_page_height << 14)) >> shift;
							break;
						case 0x09:
							ret = ((m_flipx ? 0x0001 : 0x0000)
								| (m_flipy ? 0x0002 : 0x0000)
								| (m_wrap_x ? 0x0004 : 0x0000)
								| (m_wrap_y ? 0x0008 : 0x0000)
								| (m_rowscroll ? 0x0010 : 0x0000)
								| (m_colscroll ? 0x0020 : 0x0000)
								| (m_enable ? 0x8000 : 0x0000)) >> shift;
							break;
						case 0x0a:
							ret = m_startx >> shift;
							break;
						case 0x0b:
							ret = m_starty >> shift;
							break;
						case 0x0c:
							ret = m_incxx >> shift;
							break;
						case 0x0d:
							ret = m_incxy >> shift;
							break;
						case 0x0e:
							ret = m_incyx >> shift;
							break;
						case 0x0f:
							ret = m_incyy >> shift;
							break;
						case 0x10:
						case 0x11:
							ret = (m_rowscroll_base >> shift2) >> shift;
							break;
						case 0x12:
						case 0x13:
							ret = (m_colscroll_base >> shift2) >> shift;
							break;
						case 0x14:
						case 0x15:
							ret = (m_page_base >> shift2) >> shift;
							break;
					}
				}
				else if (bitfield(~addr, 0))
				{
					ret = m_slot;
				}
				break;
			}
			case 2:
			{
				if (bitfield(addr, 0))
				{
					const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
					switch (m_slot)
					{
						case 0x00:
						case 0x01:
							ret = m_attr_ram_addr >> shift;
							break;
						case 0x02:
						case 0x03:
							ret = m_intf.read_attr_dword(m_attr_ram_addr << 2) >> shift;
							if (bitfield(m_slot, 0))
							{
								m_attr_ram_addr++;
							}
							break;
						case 0x04:
						case 0x05:
							ret = m_char_ram_addr >> shift;
							break;
						case 0x06:
						case 0x07:
							ret = m_intf.read_char_dword(m_char_ram_addr << 2) >> shift;
							if (bitfield(m_slot, 0))
							{
								m_char_ram_addr++;
							}
							break;
						case 0x08:
							ret = m_start_scale
								| (m_inc_scale << 4)
								| (m_accum_shift << 8)
								| (m_page_width << 12)
								| (m_page_height << 14);
							break;
						case 0x09:
							ret = (m_flipx ? 0x0001 : 0x0000)
								| (m_flipy ? 0x0002 : 0x0000)
								| (m_wrap_x ? 0x0004 : 0x0000)
								| (m_wrap_y ? 0x0008 : 0x0000)
								| (m_rowscroll ? 0x0010 : 0x0000)
								| (m_colscroll ? 0x0020 : 0x0000)
								| (m_enable ? 0x8000 : 0x0000);
							break;
						case 0x0a:
							ret = m_startx;
							break;
						case 0x0b:
							ret = m_starty;
							break;
						case 0x0c:
							ret = m_incxx;
							break;
						case 0x0d:
							ret = m_incxy;
							break;
						case 0x0e:
							ret = m_incyx;
							break;
						case 0x0f:
							ret = m_incyy;
							break;
						case 0x10:
						case 0x11:
							ret = m_rowscroll_base >> shift;
							break;
						case 0x12:
						case 0x13:
							ret = m_colscroll_base >> shift;
							break;
						case 0x14:
						case 0x15:
							ret = m_page_base >> shift;
							break;
					}
				}
				else
				{
					ret = m_slot;
				}
				break;
			}
		}
		return ret;
	}

	template<bool BigEndian, typename T>
	void jkmtmap_t<BigEndian, T>::host_w(const u8 addr, const T data, const T mask)
	{
		switch (sizeof(T))
		{
			case 1:
			{
				if (bitfield(addr, 1))
				{
					const u16 sdata = u16(data) << (bitfield(BigEndian ? ~addr : addr, 0) << 3);
					const u16 smask = u16(mask) << (bitfield(BigEndian ? ~addr : addr, 0) << 3);
					const u32 sdata2 = u32(sdata) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
					const u32 smask2 = u32(smask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
					switch (m_slot)
					{
						case 0x00:
						case 0x01:
							m_attr_ram_addr = (m_attr_ram_addr & ~smask2) | (sdata2 & smask2);
							break;
						case 0x02:
						case 0x03:
							m_intf.write_attr_dword(m_attr_ram_addr << 2, sdata2, smask2);
							if (bitfield(addr, 0) && bitfield(m_slot, 0))
							{
								m_attr_ram_addr++;
							}
							break;
						case 0x04:
						case 0x05:
							m_char_ram_addr = (m_char_ram_addr & ~smask2) | (sdata2 & smask2);
							break;
						case 0x06:
						case 0x07:
							m_intf.write_char_dword(m_char_ram_addr << 2, sdata2, smask2);
							if (bitfield(addr, 0) && bitfield(m_slot, 0))
							{
								m_char_ram_addr++;
							}
							break;
						case 0x08:
							if (bitfield(smask, 0, 8))
							{
								m_start_scale = bitfield(sdata, 0, 4);
								m_inc_scale = bitfield(sdata, 4, 4);
							}
							if (bitfield(smask, 8, 8))
							{
								m_accum_shift = bitfield(sdata, 8, 4);
								m_page_width = bitfield(sdata, 12, 2);
								m_page_height = bitfield(sdata, 14, 2);
							}
							break;
						case 0x09:
							if (bitfield(smask, 0, 8))
							{
								m_flipx = bitfield(sdata, 0);
								m_flipy = bitfield(sdata, 1);
								m_wrap_x = bitfield(sdata, 2);
								m_wrap_y = bitfield(sdata, 3);
								m_rowscroll = bitfield(sdata, 4);
								m_colscroll = bitfield(sdata, 5);
							}
							if (bitfield(smask, 8, 8))
							{
								m_enable = bitfield(sdata, 15);
							}
							break;
						case 0x0a:
							m_startx = (m_startx & ~smask) | (sdata & smask);
							break;
						case 0x0b:
							m_starty = (m_starty & ~smask) | (sdata & smask);
							break;
						case 0x0c:
							m_incxx = (m_incxx & ~smask) | (sdata & smask);
							break;
						case 0x0d:
							m_incxy = (m_incxy & ~smask) | (sdata & smask);
							break;
						case 0x0e:
							m_incyx = (m_incyx & ~smask) | (sdata & smask);
							break;
						case 0x0f:
							m_incyy = (m_incyy & ~smask) | (sdata & smask);
							break;
						case 0x10:
						case 0x11:
							m_rowscroll_base = (m_rowscroll_base & ~smask2) | (sdata2 & smask2);
							break;
						case 0x12:
						case 0x13:
							m_colscroll_base = (m_colscroll_base & ~smask2) | (sdata2 & smask2);
							break;
						case 0x14:
						case 0x15:
							m_page_base = (m_page_base & ~smask2) | (sdata2 & smask2);
							break;
					}
				}
				else if (bitfield(~addr, 0))
				{
					if (bitfield(mask, 0, 8))
					{
						m_slot = bitfield(data, 0, 6);
					}
				}
				break;
			}
			case 2:
			{
				if (bitfield(addr, 0))
				{
					const u32 sdata = u32(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
					const u32 smask = u32(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
					switch (m_slot)
					{
						case 0x00:
						case 0x01:
							m_attr_ram_addr = (m_attr_ram_addr & ~smask) | (sdata & smask);
							break;
						case 0x02:
						case 0x03:
							m_intf.write_attr_dword(m_attr_ram_addr << 2, sdata, smask);
							if (bitfield(m_slot, 0))
							{
								m_attr_ram_addr++;
							}
							break;
						case 0x04:
						case 0x05:
							m_char_ram_addr = (m_char_ram_addr & ~smask) | (sdata & smask);
							break;
						case 0x06:
						case 0x07:
							m_intf.write_char_dword(m_char_ram_addr << 2, sdata, smask);
							if (bitfield(m_slot, 0))
							{
								m_char_ram_addr++;
							}
							break;
						case 0x08:
							if (bitfield(mask, 0, 8))
							{
								m_start_scale = bitfield(data, 0, 4);
								m_inc_scale = bitfield(data, 4, 4);
							}
							if (bitfield(mask, 8, 8))
							{
								m_accum_shift = bitfield(data, 8, 4);
								m_page_width = bitfield(data, 12, 2);
								m_page_height = bitfield(data, 14, 2);
							}
							break;
						case 0x09:
							if (bitfield(mask, 0, 8))
							{
								m_flipx = bitfield(data, 0);
								m_flipy = bitfield(data, 1);
								m_wrap_x = bitfield(data, 2);
								m_wrap_y = bitfield(data, 3);
								m_rowscroll = bitfield(data, 4);
								m_colscroll = bitfield(data, 5);
							}
							if (bitfield(mask, 8, 8))
							{
								m_enable = bitfield(data, 15);
							}
							break;
						case 0x0a:
							m_startx = (m_startx & ~mask) | (data & mask);
							break;
						case 0x0b:
							m_starty = (m_starty & ~mask) | (data & mask);
							break;
						case 0x0c:
							m_incxx = (m_incxx & ~mask) | (data & mask);
							break;
						case 0x0d:
							m_incxy = (m_incxy & ~mask) | (data & mask);
							break;
						case 0x0e:
							m_incyx = (m_incyx & ~mask) | (data & mask);
							break;
						case 0x0f:
							m_incyy = (m_incyy & ~mask) | (data & mask);
							break;
						case 0x10:
						case 0x11:
							m_rowscroll_base = (m_rowscroll_base & ~smask) | (sdata & smask);
							break;
						case 0x12:
						case 0x13:
							m_colscroll_base = (m_colscroll_base & ~smask) | (sdata & smask);
							break;
						case 0x14:
						case 0x15:
							m_page_base = (m_page_base & ~smask) | (sdata & smask);
							break;
					}
				}
				else
				{
					if (bitfield(mask, 0, 8))
					{
						m_slot = bitfield(data, 0, 6);
					}
				}
				break;
			}
		}
	}

} // namespace jkmtmap