/*

==============================================================================

    JKMTMAP
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMTMAP

Fantasy tilemap generator hardware

*/

#ifndef JKMTMAP_HPP
#define JKMTMAP_HPP

#pragma once

#include <array>

namespace jkmtmap
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	template<bool BigEndian>
	class jkmtmap_intf_t
	{
		public:
			jkmtmap_intf_t()
			{
			} // jkmtmap_intf_t

			virtual u16 read_attr_word(u32 offs) { return 0; }
			u32 read_attr_dword(u32 offs)
			{
				return BigEndian ?
					((u32(read_attr_word(offs)) << 16) | read_attr_word(offs + 1))
					: ((u32(read_attr_word(offs + 1)) << 16) | read_attr_word(offs));
			} // read_attr_dword
			virtual void write_attr_byte(u32 offs, u8 data, u8 mask = ~0) {}
			void write_attr_word(u32 offs, u16 data, u16 mask = ~0)
			{
				if (BigEndian)
				{
					write_attr_byte(offs, bitfield(data, 8, 8), bitfield(mask, 8, 8));
					write_attr_byte(offs + 1, bitfield(data, 0, 8), bitfield(mask, 0, 8));
				}
				else
				{
					write_attr_byte(offs + 1, bitfield(data, 8, 8), bitfield(mask, 8, 8));
					write_attr_byte(offs, bitfield(data, 0, 8), bitfield(mask, 0, 8));
				}
			} // write_attr_word
			void write_attr_dword(u32 offs, u32 data, u32 mask = ~0)
			{
				if (BigEndian)
				{
					write_attr_word(offs, bitfield(data, 16, 16), bitfield(mask, 16, 16));
					write_attr_word(offs + 2, bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				else
				{
					write_attr_word(offs + 2, bitfield(data, 16, 16), bitfield(mask, 16, 16));
					write_attr_word(offs, bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
			} // write_attr_dword

			virtual u8 read_char_byte(u32 offs) { return 0; }
			u16 read_char_word(u32 offs)
			{
				return BigEndian ?
					(u16(read_char_byte(offs)) << 8) | read_char_byte(offs + 1)
					: (u16(read_char_byte(offs + 1)) << 8) | read_char_byte(offs);
			} // read_char_word
			u32 read_char_dword(u32 offs)
			{
				return BigEndian ?
					(u32(read_char_word(offs)) << 16) | read_char_word(offs + 2)
					: (u32(read_char_word(offs + 2)) << 16) | read_char_word(offs);
			} // read_char_dword
			virtual void write_char_byte(u32 offs, u8 data, u8 mask = ~0) {}
			void write_char_word(u32 offs, u16 data, u16 mask = ~0)
			{
				if (BigEndian)
				{
					write_char_byte(offs, bitfield(data, 8, 8), bitfield(mask, 8, 8));
					write_char_byte(offs + 1, bitfield(data, 0, 8), bitfield(mask, 0, 8));
				}
				else
				{
					write_char_byte(offs + 1, bitfield(data, 8, 8), bitfield(mask, 8, 8));
					write_char_byte(offs, bitfield(data, 0, 8), bitfield(mask, 0, 8));
				}
			} // write_char_word
			void write_char_dword(u32 offs, u32 data, u32 mask = ~0)
			{
				if (BigEndian)
				{
					write_char_word(offs, bitfield(data, 16, 16), bitfield(mask, 16, 16));
					write_char_word(offs + 2, bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				else
				{
					write_char_word(offs + 2, bitfield(data, 16, 16), bitfield(mask, 16, 16));
					write_char_word(offs, bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
			} // write_char_dword

			virtual void write_pixel(u32 x, u32 y, u16 pixel) {}
	}; // class jkmtmap_intf_t

	template<bool BigEndian, typename T>
	class jkmtmap_t
	{
		private:
		public:
			jkmtmap_t(jkmtmap_intf_t<BigEndian> &intf)
				: m_intf(intf)
				, m_enable(false)
				, m_flipx(false)
				, m_flipy(false)
				, m_wrap_x(false)
				, m_wrap_y(false)
				, m_rowscroll(false)
				, m_colscroll(false)
				, m_startx(0)
				, m_starty(0)
				, m_incxx(0)
				, m_incxy(0)
				, m_incyx(0)
				, m_incyy(0)
				, m_rowscroll_base(0)
				, m_colscroll_base(0)
				, m_page_base(0)
				, m_start_scale(0)
				, m_inc_scale(0)
				, m_accum_shift(0)
				, m_page_width(0)
				, m_page_height(0)

				, m_attr_ram_addr(0)
				, m_char_ram_addr(0)
				, m_slot(0)
			{
			} // jkmtmap_t

			void reset();

			void draw_tilemap(const s32 left, const s32 right, const s32 top, const s32 bottom);

			T host_r(const u8 addr);
			void host_w(const u8 addr, const T data, const T mask);

		private:
			void draw_pixel(const s32 x, const s32 y, const u32 rx, const u32 ry);
			void draw_row(const s32 y, const s32 left, const s32 right, const u32 sx, const u32 sy);

			jkmtmap_intf_t<BigEndian> &m_intf; // Interface

			// tilemap registers
			bool m_enable = false;           // Tilemap enable
			bool m_flipx = false;            // Tilemap flip X
			bool m_flipy = false;            // Tilemap flip Y
			bool m_wrap_x = false;           // Wraparound X
			bool m_wrap_y = false;           // Wraparound Y
			bool m_rowscroll = false;        // Per-row scroll enable
			bool m_colscroll = false;        // Per-column scroll enable
			u16 m_startx = 0;                // Tilemap base X position
			u16 m_starty = 0;                // Tilemap base Y position
			u16 m_incxx = 0;                 // Tilemap X increment per column
			u16 m_incxy = 0;                 // Tilemap X increment per row
			u16 m_incyx = 0;                 // Tilemap Y increment per column
			u16 m_incyy = 0;                 // Tilemap Y increment per row
			u16 m_rowscroll_base = 0;        // Per-row scroll attribute address
			u16 m_colscroll_base = 0;        // Per-column scroll attribute address
			u16 m_page_base = 0;             // Base address for each page
			u8 m_start_scale = 0;            // Scale for base positions
			u8 m_inc_scale = 0;              // Scale for increment parameters
			u8 m_accum_shift = 0;            // Accumulator right shift
			u8 m_page_width = 0;             // Tilemap Page width
			u8 m_page_height = 0;            // Tilemap page height

			// misc
			u32 m_attr_ram_addr = 0;  // Attribute RAM address
			u32 m_char_ram_addr = 0;  // Character RAM address
			u8 m_slot = 0;            // Register slot
	}; // class jkmtmap_t
} // namespace jkmtmap

#endif // JKMTMAP_HPP
