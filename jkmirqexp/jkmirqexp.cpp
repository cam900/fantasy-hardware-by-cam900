/*

==============================================================================

    JKMIRQEXP
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMIRQEXP

Fantasy IRQ expander

Features:
- 8 Interrupt slots, Configurable status

	Host interface

	Address Description
	00      Register select
	01      Register data

	Register map

	Register Description
	00       Interrupt status (Read) / Interrupt acknowledge (Write)
	01       Interrupt pending (Read)
	02       Interrupt enable
*/

#include "jkmirqexp.hpp"

namespace jkmirqexp
{
	void jkmirqexp_t::irq_clear(const u8 num)
	{
		if (num < 8)
		{
			const u8 old = m_int_state;
			m_int_state &= ~(1 << num);
			if (m_int_state != old)
			{
				irq_update();
			}
		}
	} // jkmirqexp_t::irq_clear

	void jkmirqexp_t::irq_set(const u8 num)
	{
		if (num < 8)
		{
			const u8 old = m_int_state;
			m_int_state |= (1 << num);
			if (m_int_state != old)
			{
				irq_update();
			}
		}
	} // jkmirqexp_t::irq_set

	void jkmirqexp_t::irq_update()
	{
		if ((m_int_state & m_int_enable) != 0)
		{
			m_intf.irq(true);
		}
		else
		{
			m_intf.irq(false);
		}
	} // jkmirqexp_t::irq_update

	void jkmirqexp_t::reset()
	{
		m_int_state = 0;
		m_int_enable = 0;
		m_slot = 0;
	} // jkmirqexp_t::reset

	u8 jkmirqexp_t::host_r(const u8 addr) const
	{
		if (bitfield(addr, 0)) // Data
		{
			switch (m_slot)
			{
				case 0:
					return m_int_state;
					break;
				case 1:
					return m_int_state & m_int_enable;
					break;
				case 2:
					return m_int_enable;
					break;
			}
			return 0;
		}
		else // Slot
		{
			return m_slot;
		}
	} // jkmirqexp_t::host_r

	void jkmirqexp_t::host_w(const u8 addr, const u8 data)
	{
		if (bitfield(addr, 0)) // Data
		{
			switch (m_slot)
			{
				case 0:
				{
					const u8 old = m_int_state;
					m_int_state &= ~data;
					if (m_int_state != old)
					{
						irq_update();
					}
					break;
				}
				case 2:
				{
					const u8 old = m_int_enable;
					m_int_enable = data;
					if (m_int_enable != old)
					{
						irq_update();
					}
					break;
				}
			}
		}
		else // Slot
		{
			m_slot = bitfield(data, 0, 2);
		}
	} // jkmirqexp_t::host_w

} // namespace jkmirqexp
