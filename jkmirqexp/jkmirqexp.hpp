/*

==============================================================================

    JKMIRQEXP
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMIRQEXP

Fantasy IRQ expander

*/

#ifndef JKMIRQEXP_HPP
#define JKMIRQEXP_HPP

#pragma once

namespace jkmirqexp
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	class jkmirqexp_intf_t
	{
		public:
			jkmirqexp_intf_t()
			{
			} // jkmirqexp_intf_t

			virtual void irq(bool assert) {}
	}; // class jkmirqexp_intf_t

	class jkmirqexp_t
	{
		public:
			jkmirqexp_t(jkmirqexp_intf_t &intf)
				: m_intf(intf)
				, m_int_state(0)
				, m_int_enable(0)
				, m_slot(0)
			{
			} // jkmirqexp_t

			void reset();

			u8 host_r(const u8 addr) const;
			void host_w(const u8 addr, const u8 data);

			void irq_clear(const u8 num);
			void irq_set(const u8 num);

		private:
			void irq_update();

			jkmirqexp_intf_t &m_intf; // Interface
			u8 m_int_state = 0;       // Interrupt state
			u8 m_int_enable = 0;      // Interrupt enable
			u8 m_slot = 0;            // Register slot
	}; // class jkmirqexp_t
} // namespace jkmirqexp

#endif // JKMIRQEXP_HPP
