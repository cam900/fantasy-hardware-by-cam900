/*

==============================================================================

    JKMSPR
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMSPR

Fantasy sprite generator hardware

*/

#ifndef JKMSPR_HPP
#define JKMSPR_HPP

#pragma once

namespace jkmspr
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	template<bool BigEndian>
	class jkmspr_intf_t
	{
		public:
			jkmspr_intf_t()
			{
			} // jkmspr_intf_t

			virtual void irq(bool assert) {}

			virtual u16 read_attr_word(u8 bank, u32 offs) { return 0; }
			u32 read_attr_dword(u8 bank, u32 offs)
			{
				return BigEndian ?
					((u32(read_attr_word(bank, offs)) << 16) | read_attr_word(bank, offs + 1))
					: ((u32(read_attr_word(bank, offs + 1)) << 16) | read_attr_word(bank, offs));
			} // read_attr_dword
			virtual void write_attr_word(u8 bank, u32 offs, u16 data, u16 mask) {}

			virtual u32 read_char_dword(u32 offs) { return 0; }
			virtual void write_char_dword(u32 offs, u32 data, u32 mask) {}

			virtual void write_pixel(u8 bank, u32 x, u32 y, u16 pixel) {}
	}; // class jkmspr_intf_t

	template<bool BigEndian, typename T>
	class jkmspr_t
	{
		private:
			class sprite_t
			{
				public:
					sprite_t(jkmspr_t &host)
						: m_host(host)

						, m_sprite_start(0)
						, m_sprite_num(0)
						, m_global_x(0)
						, m_global_y(0)
						, m_clear_x(0)
						, m_clear_y(0)
						, m_clear_width(0)
						, m_clear_height(0)
						, m_global_flipx(false)
						, m_global_flipy(false)

						, m_busy(false)
						, m_enable(false)
						, m_bpp(false)
						, m_flipx(false)
						, m_flipy(false)
						, m_colpri(0)
						, m_base(0)
						, m_x(0)
						, m_y(0)
						, m_zoomx(0)
						, m_zoomy(0)
						, m_width(0)
						, m_height(0)
					{
					} // sprite_t

					void reset();

					s64 get_single_sprite(const u8 bank, const u32 offs);

					s64 write_pixel(const u8 bank, const s32 dx, const s32 dy, const u8 pixel);
					s64 draw_single_pixel(const u8 bank, const s32 dx, const s32 dy, const s32 dy_inc, const u8 pixel);
					s64 draw_zoom_pixel(const u8 bank, u32 &dx_inc, s32 &dx, const s32 dy, const s32 dy_inc, const u8 pixel);
					s64 draw_row(const u8 bank, const u32 base, const s32 dy, const s32 dy_inc);
					s64 draw_sprite(const u8 bank);

					bool busy() const
					{
						return m_busy;
					} // busy

					u16 sprite_start() const
					{
						return m_sprite_start;
					} // sprite_start

					u16 sprite_num() const
					{
						return m_sprite_num;
					} // sprite_num

					s32 global_x() const
					{
						return m_global_x;
					} // global_x

					s32 global_y() const
					{
						return m_global_y;
					} // global_y

					u16 clear_x() const
					{
						return m_clear_x;
					} // clear_x

					u16 clear_y() const
					{
						return m_clear_y;
					} // clear_y

					u16 clear_width() const
					{
						return m_clear_width;
					} // clear_width

					u16 clear_height() const
					{
						return m_clear_height;
					} // clear_height

					bool global_flipx() const
					{
						return m_global_flipx;
					} // global_flipx

					bool global_flipy() const
					{
						return m_global_flipy;
					} // global_flipy

					void set_sprite_start(const u16 start, const u16 mask = ~0)
					{
						m_sprite_start = (m_sprite_start & ~mask) | (start & mask);
					} // set_sprite_start

					void set_sprite_num(const u16 num, const u16 mask = ~0)
					{
						m_sprite_num = (m_sprite_num & ~mask) | (num & mask);
					} // set_sprite_num

					void set_global_x(const s16 x, const u16 mask = ~0)
					{
						m_global_x = (m_global_x & ~mask) | (x & mask);
					} // set_global_x

					void set_global_y(const s16 y, const u16 mask = ~0)
					{
						m_global_y = (m_global_y & ~mask) | (y & mask);
					} // set_global_y

					void set_clear_x(const u16 x, const u16 mask = ~0)
					{
						m_clear_x = (m_clear_x & ~mask) | (x & mask);
					} // set_clear_x

					void set_clear_y(const u16 y, const u16 mask = ~0)
					{
						m_clear_y = (m_clear_y & ~mask) | (y & mask);
					} // set_clear_y

					void set_clear_width(const u16 width, const u16 mask = ~0)
					{
						m_clear_width = (m_clear_width & ~mask) | (width & mask);
					} // set_clear_width

					void set_clear_height(const u16 height, const u16 mask = ~0)
					{
						m_clear_height = (m_clear_height & ~mask) | (height & mask);
					} // set_clear_height

					void set_global_flipx(const bool flipx)
					{
						m_global_flipx = flipx;
					} // set_global_flipx

					void set_global_flipy(const bool flipy)
					{
						m_global_flipy = flipy;
					} // set_global_flipy

					void set_busy(const bool busy)
					{
						m_busy = busy;
					} // set_busy

				private:
					jkmspr_t &m_host;

					// register
					u16 m_sprite_start = 0;
					u16 m_sprite_num = 0;
					s16 m_global_x = 0;
					s16 m_global_y = 0;
					u16 m_clear_x = 0;
					u16 m_clear_y = 0;
					u16 m_clear_width = 0;
					u16 m_clear_height = 0;
					bool m_global_flipx = false;
					bool m_global_flipy = false;

					// internal state
					bool m_busy = false;
					bool m_enable = false;
					bool m_bpp = false;
					bool m_flipx = false;
					bool m_flipy = false;
					u16 m_colpri = 0;
					u32 m_base = 0;
					s16 m_x = 0;
					s16 m_y = 0;
					u16 m_zoomx = 0;
					u16 m_zoomy = 0;
					u16 m_width = 0;
					u16 m_height = 0;
			}; // class sprite_t
		public:
			jkmspr_t(jkmspr_intf_t<BigEndian> &intf)
				: m_intf(intf)
				, m_sprite(sprite_t())
				, m_irq(false)
				, m_cycle(0)
				, m_attr_ram_addr(0)
				, m_char_ram_addr(0)
				, m_bank(0)
				, m_slot(0)
			{
			} // jkmspr_t

			void reset();
			void tick();

			T host_r(const u8 addr);
			void host_w(const u8 addr, const T data, const T mask);

		private:
			jkmspr_intf_t<BigEndian> &m_intf; // Interface
			sprite_t m_sprite;
			bool m_irq = false;       // IRQ flag
			s64 m_cycle = 0;          // DMA cycle
			u16 m_attr_ram_addr = 0;  // Attribute RAM address
			u32 m_char_ram_addr = 0;  // Character RAM address
			u8 m_bank = 0;            // Bank select
			u8 m_slot = 0;            // Register slot
	}; // class jkmspr_t
} // namespace jkmspr

#endif // JKMSPR_HPP
