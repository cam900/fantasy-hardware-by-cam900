/*

==============================================================================

    JKMSPR
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMSPR

Fantasy sprite generator hardware

Features:
- 1 Banks per 1 attribute/1 framebuffer RAMs, 2 Banks total
- DMA done IRQ
- Up to 16384 sprites on screen
- 4 bpp or 8 bpp
- Flip, Zoom support
- 16 bit pixel output

	8 bit Host interface

	Address Description
	00      Register select
	02      Register data LSB
	03      Register data MSB

	16 bit Host interface

	Address Description
	00      Register select
	01      Register data

	Register map

	Register Description
	00       Attribute RAM address
	01       Attribute RAM data
	02       Tile memory address MSB
	03       Tile memory address LSB
	04       Tile memory data MSB
	05       Tile memory data LSB
	06       Global X offset
	07       Global Y offset
	08       Sprite start offset
	09       Number of sprites
	0a       Display/Draw Bank select (Bit 0), DMA IRQ(Bit 1), Global Flip X/Y (Bit 6, 7)
	0b       DMA Run (Bit 0)
	0c       Clear X
	0d       Clear Y
	0e       Clear width
	0f       Clear height


	4 bit Pixel chunk address format

			Bit
			11111111 11111111 00000000 00000000
	Address fedcba98 76543210 fedcba98 76543210
	Data    xxxxxxxx xxxxxxxx xxxxxxxx xxxxx---
	Y       -------- -------- -------- -----210

	8 bit Pixel chunk address format

			Bit
			11111111 11111111 00000000 00000000
	Address fedcba98 76543210 fedcba98 76543210
	Data    xxxxxxxx xxxxxxxx xxxxxxxx xxxx----
	X       -------- -------- -------- ----0---
	Y       -------- -------- -------- -----210


	4 bit Pixel chunk data format

			Bit
			11111111 11111111 00000000 00000000
	Data    fedcba98 76543210 fedcba98 76543210
	Pixel   32103210 32103210 32103210 32103210
	X       00001111 22223333 44445555 66667777

	8 bit Pixel chunk data format

			Bit
			11111111 11111111 00000000 00000000
	Data    fedcba98 76543210 fedcba98 76543210
	Pixel   76543210 76543210 76543210 76543210
	X       00000000 11111111 22222222 33333333


	Sprite format (8 words per sprite)

	Address Bit                 Description
	        fedc ba98 7654 3210
	00      x--- ---- ---- ---- Enable
	        -x-- ---- ---- ---- Bit per pixel (4bit(0) or 8bit(1))
	        --x- ---- ---- ---- Flip Y
	        ---x ---- ---- ---- Flip X
	        ---- xxxx xxxx xxxx Color/Priority (16 color boundary)
	01      xxxx xxxx ---- ---- Height (1 tile each)
	        ---- ---- xxxx xxxx Width (1 tile each)
	02      xxxx xxxx xxxx xxxx Tile index (32 byte boundary) MSB
	03      xxxx xxxx xxxx xxxx "" LSB
	04      sxxx xxxx xxxx xxxx X
	05      sxxx xxxx xxxx xxxx Y
	06      iiii ifff ffff ffff Zoom X (i: Integer, f: Fraction)
	07      iiii ifff ffff ffff Zoom Y (i: Integer, f: Fraction)
*/

#include "jkmspr.hpp"

namespace jkmspr
{
	template<bool BigEndian, typename T>
	T jkmspr_t<BigEndian, T>::host_r(const u8 addr)
	{
		T ret = 0;
		if (bitfield(addr, (sizeof(T) > 1) ? 0 : 1)) // Data
		{
			switch (sizeof(T))
			{
				case 1:
				{
					const u8 shift = bitfield(BigEndian ? ~addr : addr, 0) << 3;
					switch (m_slot)
					{
						case 0:
							ret = m_attr_ram_addr >> shift;
							break;
						case 1:
							ret = m_intf.read_attr_word(m_bank, m_attr_ram_addr) >> shift;
							if (bitfield(addr, 0))
							{
								m_attr_ram_addr++;
							}
							break;
						case 2:
						case 3:
						{
							const u8 shift2 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = (m_char_ram_addr >> shift2) >> shift;
							break;
						}
						case 4:
						case 5:
						{
							const u8 shift2 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = (m_intf.read_char_dword(m_char_ram_addr) >> shift2) >> shift;
							if (bitfield(m_slot, 0) && bitfield(addr, 0))
							{
								m_char_ram_addr++;
							}
							break;
						}
						case 6:
							ret = m_sprite.global_x() >> shift;
							break;
						case 7:
							ret = m_sprite.global_y() >> shift;
							break;
						case 8:
							ret = m_sprite.sprite_start() >> shift;
							break;
						case 9:
							ret = m_sprite.sprite_num() >> shift;
							break;
						case 0x0a:
							if (!shift)
							{
								ret = m_bank
								| (m_irq ? 0x02 : 0x00)
								| (m_sprite.global_flipx() ? 0x40 : 0x00)
								| (m_sprite.global_flipy() ? 0x80 : 0x00);
							}
							break;
						case 0x0b:
							if (!shift)
							{
								ret = m_sprite.busy() ? 0x01 : 0x00;
							}
							break;
						case 0x0c:
							ret = m_sprite.clear_x() >> shift;
							break;
						case 0x0d:
							ret = m_sprite.clear_y() >> shift;
							break;
						case 0x0e:
							ret = m_sprite.clear_width() >> shift;
							break;
						case 0x0f:
							ret = m_sprite.clear_height() >> shift;
							break;
					}
					return 0;
					break;
				}
				case 2:
				{
					switch (m_slot)
					{
						case 0:
							ret = m_attr_ram_addr;
							break;
						case 1:
							ret = m_intf.read_attr_word(m_attr_ram_addr++);
							break;
						case 2:
						case 3:
						{
							const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = m_char_ram_addr >> shift;
							break;
						}
						case 4:
						case 5:
						{
							const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = m_intf.read_char_dword(m_char_ram_addr) >> shift;
							if (bitfield(m_slot, 0))
							{
								m_char_ram_addr++;
							}
							break;
						}
						case 6:
							ret = m_sprite.global_x();
							break;
						case 7:
							ret = m_sprite.global_y();
							break;
						case 8:
							ret = m_sprite.sprite_start();
							break;
						case 9:
							ret = m_sprite.sprite_num();
							break;
						case 0x0a:
							ret = m_bank
								| (m_irq ? 0x02 : 0x00)
								| (m_sprite.global_flipx() ? 0x40 : 0x00)
								| (m_sprite.global_flipy() ? 0x80 : 0x00);
							break;
						case 0x0b:
							ret = m_sprite.busy() ? 0x01 : 0x00;
							break;
						case 0x0c:
							ret = m_sprite.clear_x();
							break;
						case 0x0d:
							ret = m_sprite.clear_y();
							break;
						case 0x0e:
							ret = m_sprite.clear_width();
							break;
						case 0x0f:
							ret = m_sprite.clear_height();
							break;
					}
					break;
				}
			}
		}
		else // Slot
		{
			ret = m_slot;
		}
		return ret;
	} // jkmspr_t::host_r

	template<bool BigEndian, typename T>
	void jkmspr_t<BigEndian, T>::host_w(const u8 addr, const T data, const T mask)
	{
		if (bitfield(addr, (sizeof(T) > 1) ? 0 : 1)) // Data
		{
			switch (sizeof(T))
			{
				case 1:
				{
					const u16 sdata = u16(data) << (bitfield(BigEndian ? ~addr : addr, 0) << 3);
					const u16 smask = u16(mask) << (bitfield(BigEndian ? ~addr : addr, 0) << 3);
					switch (m_slot)
					{
						case 0:
							m_attr_ram_addr = (m_attr_ram_addr & ~smask) | (sdata & smask);
							break;
						case 1:
							m_intf.write_attr_word(m_bank, m_attr_ram_addr, sdata, smask);
							if (bitfield(addr, 0))
							{
								m_attr_ram_addr++;
							}
							break;
						case 2:
						case 3:
						{
							if (!m_sprite.busy())
							{
								const u32 sdata2 = u32(sdata) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(smask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_char_ram_addr = (m_char_ram_addr & ~smask2) | (sdata2 & smask2);
							}
							break;
						}
						case 4:
						case 5:
						{
							if (!m_sprite.busy())
							{
								const u32 sdata2 = u32(sdata) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(smask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_intf.write_char_dword(m_char_ram_addr, sdata2, smask2);
								if (bitfield(m_slot, 0) && bitfield(addr, 0))
								{
									m_char_ram_addr++;
								}
							}
							break;
						}
						case 6:
							m_sprite.set_global_x(sdata, smask);
							break;
						case 7:
							m_sprite.set_global_y(sdata, smask);
							break;
						case 8:
							m_sprite.set_sprite_start(sdata, smask);
							break;
						case 9:
							m_sprite.set_sprite_num(sdata, smask);
							break;
						case 0x0a:
							if (bitfield(smask, 0, 8))
							{
								if (!m_sprite.busy())
								{
									m_bank = bitfield(sdata, 0);
									m_sprite.set_global_flipx(bitfield(sdata, 6));
									m_sprite.set_global_flipy(bitfield(sdata, 7));
								}
								if (bitfield(sdata, 1))
								{
									m_irq = false;
									m_intf.irq(m_irq);
								}
							}
							break;
						case 0x0b:
							if (bitfield(smask, 0, 8))
							{
								if (!m_sprite.busy())
								{
									if (bitfield(sdata, 0))
									{
										m_sprite.set_busy(true);
										m_cycle = m_sprite.draw_sprite(m_bank ^ 1);
									}
								}
							}
							break;
						case 0x0c:
							m_sprite.set_clear_x(sdata, smask);
							break;
						case 0x0d:
							m_sprite.set_clear_y(sdata, smask);
							break;
						case 0x0e:
							m_sprite.set_clear_width(sdata, smask);
							break;
						case 0x0f:
							m_sprite.set_clear_height(sdata, smask);
							break;
					}
					break;
				}
				case 2:
				{
					switch (m_slot)
					{
						case 0:
							m_attr_ram_addr = (m_attr_ram_addr & ~mask) | (data & mask);
							break;
						case 1:
							m_intf.write_attr_word(m_bank, m_attr_ram_addr++, data, mask);
							break;
						case 2:
						case 3:
						{
							if (!m_sprite.busy())
							{
								const u32 sdata2 = u32(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_char_ram_addr = (m_char_ram_addr & ~smask2) | (sdata2 & smask2);
							}
							break;
						}
						case 4:
						case 5:
						{
							if (!m_sprite.busy())
							{
								const u32 sdata2 = u32(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_intf.write_char_dword(m_char_ram_addr, sdata2, smask2);
								if (bitfield(m_slot, 0))
								{
									m_char_ram_addr++;
								}
							}
							break;
						}
						case 6:
							m_sprite.set_global_x(data, mask);
							break;
						case 7:
							m_sprite.set_global_y(data, mask);
							break;
						case 8:
							m_sprite.set_sprite_start(data, mask);
							break;
						case 9:
							m_sprite.set_sprite_num(data, mask);
							break;
						case 0x0a:
							if (bitfield(mask, 0, 8))
							{
								if (!m_sprite.busy())
								{
									m_bank = bitfield(data, 0);
									m_sprite.set_global_flipx(bitfield(data, 6));
									m_sprite.set_global_flipy(bitfield(data, 7));
								}
								if (bitfield(data, 1))
								{
									m_irq = false;
									m_intf.irq(m_irq);
								}
							}
							break;
						case 0x0b:
							if (bitfield(mask, 0, 8))
							{
								if (!m_sprite.busy())
								{
									if (bitfield(data, 0))
									{
										m_sprite.set_busy(true);
										m_cycle = m_sprite.draw_sprite(m_bank ^ 1);
									}
								}
							}
							break;
						case 0x0c:
							m_sprite.set_clear_x(data, mask);
							break;
						case 0x0d:
							m_sprite.set_clear_y(data, mask);
							break;
						case 0x0e:
							m_sprite.set_clear_width(data, mask);
							break;
						case 0x0f:
							m_sprite.set_clear_height(data, mask);
							break;
					}
					break;
				}
			}
		}
		else // Slot
		{
			if (bitfield(mask, 0, 8))
			{
				m_slot = data & 0xf;
			}
		}
	} // jkmspr_t::host_w

	template<bool BigEndian, typename T>
	void jkmspr_t<BigEndian, T>::sprite_t::reset()
	{
		m_sprite_start = 0;
		m_sprite_num = 0;
		m_global_x = 0;
		m_global_y = 0;
		m_clear_x = 0;
		m_clear_y = 0;
		m_clear_width = 0;
		m_clear_height = 0;
		m_global_flipx = false;
		m_global_flipy = false;

		m_busy = false;
		m_enable = false;
		m_bpp = false;
		m_flipx = false;
		m_flipy = false;
		m_colpri = 0;
		m_base = 0;
		m_x = 0;
		m_y = 0;
		m_zoomx = 0;
		m_zoomy = 0;
		m_width = 0;
		m_height = 0;
	}

	template<bool BigEndian, typename T>
	s64 jkmspr_t<BigEndian, T>::sprite_t::get_single_sprite(const u8 bank, const u32 offs)
	{
		const u16 attr = m_host.m_intf.read_attr_word(bank, offs + 0x0);
		const u16 size = m_host.m_intf.read_attr_word(bank, offs + 0x1);
		m_base = m_host.m_intf.read_attr_dword(bank, offs + 0x2);
		m_x = m_host.m_intf.read_attr_word(bank, offs + 0x4);
		m_y = m_host.m_intf.read_attr_word(bank, offs + 0x5);
		m_zoomx = m_host.m_intf.read_attr_word(bank, offs + 0x6);
		m_zoomy = m_host.m_intf.read_attr_word(bank, offs + 0x7);

		m_colpri = bitfield(attr, 0, 12);
		m_flipx = bitfield(attr, 12);
		m_flipy = bitfield(attr, 13);
		m_bpp = bitfield(attr, 14);
		m_enable = bitfield(attr, 15);
		m_width = bitfield(size, 0, 8) + 1;
		m_height = bitfield(size, 8, 8) + 1;

		return 8;
	}

	template<bool BigEndian, typename T>
	s64 jkmspr_t<BigEndian, T>::sprite_t::write_pixel(const u8 bank, const s32 dx, const s32 dy, const u8 pixel)
	{
		if (pixel != 0)
		{
			m_host.m_intf.write_pixel(bank, m_global_flipx ? (m_global_x - dx) : (m_global_x + dx), m_global_flipy ? (m_global_y - dy) : (m_global_y + dy), pixel + (m_colpri << 4));
		}
		return 1;
	}

	template<bool BigEndian, typename T>
	s64 jkmspr_t<BigEndian, T>::sprite_t::draw_single_pixel(const u8 bank, const s32 dx, const s32 dy, const s32 dy_inc, const u8 pixel)
	{
		s64 cycle = 0;
		if (m_zoomy > 0x800)
		{
			s32 ty = dy;
			s32 ty_inc = dy_inc;
			while (ty_inc >= 0x800)
			{
				cycle += write_pixel(bank, dx, ty, pixel);
				ty += m_flipy ? -1 : 1;
				ty_inc -= 0x800;
			}
		}
		else
		{
			cycle += write_pixel(bank, dx, dy, pixel);
		}
		return cycle;
	}

	template<bool BigEndian, typename T>
	s64 jkmspr_t<BigEndian, T>::sprite_t::draw_zoom_pixel(const u8 bank, u32 &dx_inc, s32 &dx, const s32 dy, const s32 dy_inc, const u8 pixel)
	{
		s64 cycle = 0;
		dx_inc += m_zoomx;
		if (m_zoomx > 0x800)
		{
			while (dx_inc >= 0x800)
			{
				cycle += draw_single_pixel(bank, dx, dy, dy_inc, pixel);
				dx += m_flipx ? -1 : 1;
				dx_inc -= 0x800;
			}
		}
		else
		{
			cycle += draw_single_pixel(bank, dx, dy, dy_inc, pixel);
			dx += (dx_inc >> 11) * (m_flipx ? -1 : 1);
			dx_inc &= 0x7ff;
		}
		return cycle;
	}

	template<bool BigEndian, typename T>
	s64 jkmspr_t<BigEndian, T>::sprite_t::draw_row(const u8 bank, const u32 base, const s32 dy, const s32 dy_inc)
	{
		s64 cycle = 0;
		u32 row = base;
		s32 dx = s32(m_x);
		s32 dx_inc = 0;
		for (int sx = 0; sx < (m_bpp ? (m_width << 1) : m_width); sx++)
		{
			const u8 chunk = m_host.m_intf.read_char_dword(row);
			cycle += 8;
			if (m_bpp)
			{
				for (int tx = 0; tx < 4; tx++)
				{
					const u8 pixel = bitfield(chunk, (~tx) << 3, 3);
					cycle += draw_zoom_pixel(bank, dx_inc, dx, dy, dy_inc, pixel);
				}
			}
			else
			{
				for (int tx = 0; tx < 8; tx++)
				{
					const u8 pixel = bitfield(chunk, (~tx) << 2, 2);
					cycle += draw_zoom_pixel(bank, dx_inc, dx, dy, dy_inc, pixel);
				}
			}
			row += (m_height << 3);
		}
		return cycle;
	}

	template<bool BigEndian, typename T>
	s64 jkmspr_t<BigEndian, T>::sprite_t::draw_sprite(const u8 bank)
	{
		s64 cycle = 0;
		for (u32 dy = m_clear_y; dy < m_clear_y + m_clear_height; dy++)
		{
			for (u32 dx = m_clear_x; dx < m_clear_x + m_clear_width; dx++)
			{
				m_host.m_intf.write_pixel(bank, dx, dy, 0);
			}
		}

		for (u32 offs = m_sprite_start; offs < m_sprite_start + m_sprite_num; offs++)
		{
			cycle += get_single_sprite(offs * 8);
			if (m_enable)
			{
				s32 dy = s32(m_y);
				s32 dy_inc = 0;
				u32 base = m_base << 3;
				for (int sy = 0; sy < m_height; sy++)
				{
					for (int ty = 0; ty < 8; ty++)
					{
						dy_inc += m_zoomy;
						cycle += draw_row(bank, base, dy, dy_inc);
						dy += (dy_inc >> 11) * (m_flipy ? -1 : 1);
						dy_inc &= 0x7ff;
						base++;
					}
				}
			}
		}
		return cycle;
	}

	template<bool BigEndian, typename T>
	void jkmspr_t<BigEndian, T>::reset()
	{
		m_sprite.reset();
		m_irq = false;
		m_cycle = 0;
		m_attr_ram_addr = 0;
		m_char_ram_addr = 0;
		m_bank = 0;
		m_slot = 0;
	}

	template<bool BigEndian, typename T>
	void jkmspr_t<BigEndian, T>::tick()
	{
		if (m_sprite.busy())
		{
			if (m_cycle-- <= 0)
			{
				m_sprite.set_busy(false);
				m_irq = true;
				m_intf.irq(m_irq);
			}
		}
	}
} // namespace jkmspr
