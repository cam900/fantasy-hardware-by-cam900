/*

==============================================================================

    JKMBLIT
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMBLIT

Fantasy blitter hardware

Features:
- 1 Banks per framebuffer RAMs, 2 Banks total
- Blitter done IRQ
- 1 bpp, 2 bpp, 4 bpp or 8 bpp and RLE Compressed source support
- Zero, Non-zero draw mode support (Single color or Pixel)
- Flip, Zoom, Tilt, Stretch support
- 16 bit pixel output

	8 bit Host interface

	Address Description
	00      Register select
	02      Register data LSB
	03      Register data MSB

	16 bit Host interface

	Address Description
	00      Register select
	01      Register data

	Register map

	Register Description
	00       Source memory address MSB
	01       Source memory address LSB
	02       Source memory data MSB
	03       Source memory data LSB
	04       Global X offset
	05       Global Y offset
	06       Display/Draw Bank select (Bit 0), Blitter IRQ(Bit 1), Global Flip X/Y (Bit 6, 7)
	07       Blitter Run (Bit 0)
	10       Blitter control
	Bit 0 Flip X
	Bit 1 Flip Y
	Bit 2 to 3 Draw method - Zero pixel case
	- 0- Don't draw
	- 10 Draw pixel
	- 11 Draw color
	Bit 4 to 5 Draw method - Non-Zero pixel case
	- 0- Don't draw
	- 10 Draw pixel
	- 11 Draw color
	Bit 6 to 7 Bit per pixel
	- 00 1bpp
	- 01 2bpp
	- 10 4bpp
	- 11 8bpp
	Bit 8 Clip
	Bit 9 RLE compressed
	11       Draw color
	12       Source base address LSB
	13       Source base address MSB
	14       Source width
	15       Source height
	16       X position
	17       Y position
	18       X zoom fraction
	19       X zoom integer
	1a       Y zoom fraction
	1b       Y zoom integer
	1c       Tilt fraction
	1d       Tilt integer
	1e       Stretch fraction
	1f       Stretch integer
	20       Clip left
	21       Clip right
	22       Clip top
	23       Clip bottom
	24       Palette index
*/

#include "jkmblit.hpp"

namespace jkmblit
{
	template<bool BigEndian, typename T>
	T jkmblit_t<BigEndian, T>::host_r(const u8 addr)
	{
		T ret = 0;
		if (bitfield(addr, (sizeof(T) > 1) ? 0 : 1)) // Data
		{
			switch (sizeof(T))
			{
				case 1:
				{
					const u8 shift = bitfield(BigEndian ? ~addr : addr, 0) << 3;
					switch (m_slot)
					{
						case 0x00:
						case 0x01:
						{
							const u8 shift2 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = (m_char_ram_addr >> shift2) >> shift;
							break;
						}
						case 0x02:
						case 0x03:
						{
							const u8 shift2 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = (m_intf.read_char_dword(m_char_ram_addr) >> shift2) >> shift;
							if (bitfield(m_slot, 0) && bitfield(addr, 0))
							{
								m_char_ram_addr++;
							}
							break;
						}
						case 0x04:
							ret = m_blitter.global_x() >> shift;
							break;
						case 0x05:
							ret = m_blitter.global_y() >> shift;
							break;
						case 0x06:
							if (!shift)
							{
								ret = m_bank
								| (m_irq ? 0x02 : 0x00)
								| (m_blitter.global_flipx() ? 0x40 : 0x00)
								| (m_blitter.global_flipy() ? 0x80 : 0x00);
							}
							break;
						case 0x07:
							if (!shift)
							{
								ret = m_blitter.busy() ? 0x01 : 0x00;
							}
							break;
						case 0x10:
							ret = ((m_blitter.flipx() ? 0x0001 : 0x0000)
								| (m_blitter.flipy() ? 0x0002 : 0x0000)
								| (m_blitter.zero_draw() << 2)
								| (m_blitter.nonzero_draw() << 4)
								| (m_blitter.bpp() << 6)
								| (m_blitter.clip() ? 0x0100 : 0x0000)
								| (m_blitter.rle() ? 0x0200 : 0x0000)) >> shift;
							break;
						case 0x11:
							ret = m_blitter.color() >> shift;
							break;
						case 0x12:
						case 0x13:
						{
							const u8 shift2 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = (m_blitter.base() >> shift2) >> shift;
							break;
						}
						case 0x14:
							ret = m_blitter.width() >> shift;
							break;
						case 0x15:
							ret = m_blitter.height() >> shift;
							break;
						case 0x16:
							ret = m_blitter.x() >> shift;
							break;
						case 0x17:
							ret = m_blitter.y() >> shift;
							break;
						case 0x18:
						case 0x19:
						{
							const u8 shift2 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = (m_blitter.zoomx() >> shift2) >> shift;
							break;
						}
						case 0x1a:
						case 0x1b:
						{
							const u8 shift2 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = (m_blitter.zoomy() >> shift2) >> shift;
							break;
						}
						case 0x1c:
						case 0x1d:
						{
							const u8 shift2 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = (m_blitter.tilt() >> shift2) >> shift;
							break;
						}
						case 0x1e:
						case 0x1f:
						{
							const u8 shift2 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = (m_blitter.stretch() >> shift2) >> shift;
							break;
						}
						case 0x20:
							ret = m_blitter.clip_left() >> shift;
							break;
						case 0x21:
							ret = m_blitter.clip_right() >> shift;
							break;
						case 0x22:
							ret = m_blitter.clip_top() >> shift;
							break;
						case 0x23:
							ret = m_blitter.clip_bottom() >> shift;
							break;
						case 0x24:
							ret = m_blitter.palindex() >> shift;
							break;
					}
					return 0;
					break;
				}
				case 2:
				{
					switch (m_slot)
					{
						case 0x00:
						case 0x01:
						{
							const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = m_char_ram_addr >> shift;
							break;
						}
						case 0x02:
						case 0x03:
						{
							const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = m_intf.read_char_dword(m_char_ram_addr) >> shift;
							if (bitfield(m_slot, 0))
							{
								m_char_ram_addr++;
							}
							break;
						}
						case 0x04:
							ret = m_blitter.global_x();
							break;
						case 0x05:
							ret = m_blitter.global_y();
							break;
						case 0x06:
							ret = m_bank
								| (m_irq ? 0x02 : 0x00)
								| (m_blitter.global_flipx() ? 0x40 : 0x00)
								| (m_blitter.global_flipy() ? 0x80 : 0x00);
							break;
						case 0x07:
							ret = m_blitter.busy() ? 0x01 : 0x00;
							break;
						case 0x10:
							ret = (m_blitter.flipx() ? 0x0001 : 0x0000)
								| (m_blitter.flipy() ? 0x0002 : 0x0000)
								| (m_blitter.zero_draw() << 2)
								| (m_blitter.nonzero_draw() << 4)
								| (m_blitter.bpp() << 6)
								| (m_blitter.clip() ? 0x0100 : 0x0000)
								| (m_blitter.rle() ? 0x0200 : 0x0000);
							break;
						case 0x11:
							ret = m_blitter.color();
							break;
						case 0x12:
						case 0x13:
						{
							const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = m_blitter.base() >> shift;
							break;
						}
						case 0x14:
							ret = m_blitter.width();
							break;
						case 0x15:
							ret = m_blitter.height();
							break;
						case 0x16:
							ret = m_blitter.x();
							break;
						case 0x17:
							ret = m_blitter.y();
							break;
						case 0x18:
						case 0x19:
						{
							const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = m_blitter.zoomx() >> shift;
							break;
						}
						case 0x1a:
						case 0x1b:
						{
							const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = m_blitter.zoomy() >> shift;
							break;
						}
						case 0x1c:
						case 0x1d:
						{
							const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = m_blitter.tilt() >> shift;
							break;
						}
						case 0x1e:
						case 0x1f:
						{
							const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = m_blitter.stretch() >> shift;
							break;
						}
						case 0x20:
							ret = m_blitter.clip_left();
							break;
						case 0x21:
							ret = m_blitter.clip_right();
							break;
						case 0x22:
							ret = m_blitter.clip_top();
							break;
						case 0x23:
							ret = m_blitter.clip_bottom();
							break;
						case 0x24:
							ret = m_blitter.palindex();
							break;
					}
					break;
				}
			}
		}
		else // Slot
		{
			ret = m_slot;
		}
		return ret;
	} // jkmblit_t::host_r

	template<bool BigEndian, typename T>
	void jkmblit_t<BigEndian, T>::host_w(const u8 addr, const T data, const T mask)
	{
		if (bitfield(addr, (sizeof(T) > 1) ? 0 : 1)) // Data
		{
			switch (sizeof(T))
			{
				case 1:
				{
					const u16 sdata = u16(data) << (bitfield(BigEndian ? ~addr : addr, 0) << 3);
					const u16 smask = u16(mask) << (bitfield(BigEndian ? ~addr : addr, 0) << 3);
					switch (m_slot)
					{
						case 0x00:
						case 0x01:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(sdata) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(smask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_char_ram_addr = (m_char_ram_addr & ~smask2) | (sdata2 & smask2);
							}
							break;
						}
						case 0x02:
						case 0x03:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(sdata) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(smask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_intf.write_char_dword(m_char_ram_addr << 2, sdata2, smask2);
								if (bitfield(m_slot, 0) && bitfield(addr, 0))
								{
									m_char_ram_addr++;
								}
							}
							break;
						}
						case 0x04:
							m_blitter.set_global_x(sdata, smask);
							break;
						case 0x05:
							m_blitter.set_global_y(sdata, smask);
							break;
						case 0x06:
							if (bitfield(smask, 0, 8))
							{
								if (!m_blitter.busy())
								{
									m_bank = bitfield(sdata, 0);
									m_blitter.set_global_flipx(bitfield(sdata, 6));
									m_blitter.set_global_flipy(bitfield(sdata, 7));
								}
								if (bitfield(sdata, 1))
								{
									m_irq = false;
									m_intf.irq(m_irq);
								}
							}
							break;
						case 0x07:
							if (bitfield(smask, 0, 8))
							{
								if (!m_blitter.busy())
								{
									if (bitfield(sdata, 0))
									{
										m_blitter.set_busy(true);
										m_cycle = m_blitter.blit(m_bank ^ 1);
									}
								}
							}
							break;
						case 0x10:
							if (!m_blitter.busy())
							{
								if (bitfield(smask, 0, 8))
								{
									m_blitter.set_flipx(bitfield(sdata, 0));
									m_blitter.set_flipy(bitfield(sdata, 1));
									m_blitter.set_zero_draw(bitfield(sdata, 2, 2));
									m_blitter.set_nonzero_draw(bitfield(sdata, 4, 2));
									m_blitter.set_bpp(bitfield(sdata, 6, 2));
								}
								if (bitfield(smask, 8, 8))
								{
									m_blitter.set_clip(bitfield(sdata, 8));
									m_blitter.set_rle(bitfield(sdata, 9));
								}
							}
							break;
						case 0x11:
							if (!m_blitter.busy())
							{
								m_blitter.set_color(sdata, smask);
							}
							break;
						case 0x12:
						case 0x13:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(sdata) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(smask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_blitter.set_base(sdata2, smask2);
							}
							break;
						}
						case 0x14:
							if (!m_blitter.busy())
							{
								m_blitter.set_width(sdata, smask);
							}
							break;
						case 0x15:
							if (!m_blitter.busy())
							{
								m_blitter.set_height(sdata, smask);
							}
							break;
						case 0x16:
							if (!m_blitter.busy())
							{
								m_blitter.set_x(sdata, smask);
							}
							break;
						case 0x17:
							if (!m_blitter.busy())
							{
								m_blitter.set_y(sdata, smask);
							}
							break;
						case 0x18:
						case 0x19:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(sdata) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(smask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_blitter.set_zoomx(sdata2, smask2);
							}
							break;
						}
						case 0x1a:
						case 0x1b:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(sdata) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(smask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_blitter.set_zoomy(sdata2, smask2);
							}
							break;
						}
						case 0x1c:
						case 0x1d:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(sdata) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(smask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_blitter.set_tilt(sdata2, smask2);
							}
							break;
						}
						case 0x1e:
						case 0x1f:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(sdata) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(smask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_blitter.set_stretch(sdata2, smask2);
							}
							break;
						}
						case 0x20:
							if (!m_blitter.busy())
							{
								m_blitter.set_clip_left(sdata, smask);
							}
							break;
						case 0x21:
							if (!m_blitter.busy())
							{
								m_blitter.set_clip_right(sdata, smask);
							}
							break;
						case 0x22:
							if (!m_blitter.busy())
							{
								m_blitter.set_clip_top(sdata, smask);
							}
							break;
						case 0x23:
							if (!m_blitter.busy())
							{
								m_blitter.set_clip_bottom(sdata, smask);
							}
							break;
						case 0x24:
							if (!m_blitter.busy())
							{
								m_blitter.set_palindex(sdata, smask);
							}
							break;
					}
					break;
				}
				case 2:
				{
					switch (m_slot)
					{
						case 0x00:
						case 0x01:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_char_ram_addr = (m_char_ram_addr & ~smask2) | (sdata2 & smask2);
							}
							break;
						}
						case 0x02:
						case 0x03:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_intf.write_char_dword(m_char_ram_addr << 2, sdata2, smask2);
								if (bitfield(m_slot, 0))
								{
									m_char_ram_addr++;
								}
							}
							break;
						}
						case 0x04:
							m_blitter.set_global_x(data, mask);
							break;
						case 0x05:
							m_blitter.set_global_y(data, mask);
							break;
						case 0x06:
							if (bitfield(mask, 0, 8))
							{
								if (!m_blitter.busy())
								{
									m_bank = bitfield(data, 0);
									m_blitter.set_global_flipx(bitfield(data, 6));
									m_blitter.set_global_flipy(bitfield(data, 7));
								}
								if (bitfield(data, 1))
								{
									m_irq = false;
									m_intf.irq(m_irq);
								}
							}
							break;
						case 0x07:
							if (bitfield(mask, 0, 8))
							{
								if (!m_blitter.busy())
								{
									if (bitfield(data, 0))
									{
										m_blitter.set_busy(true);
										m_cycle = m_blitter.blit(m_bank ^ 1);
									}
								}
							}
							break;
						
						case 0x10:
							if (!m_blitter.busy())
							{
								if (bitfield(mask, 0, 8))
								{
									m_blitter.set_flipx(bitfield(data, 0));
									m_blitter.set_flipy(bitfield(data, 1));
									m_blitter.set_zero_draw(bitfield(data, 2, 2));
									m_blitter.set_nonzero_draw(bitfield(data, 4, 2));
									m_blitter.set_bpp(bitfield(data, 6, 2));
								}
								if (bitfield(mask, 8, 8))
								{
									m_blitter.set_clip(bitfield(data, 8));
									m_blitter.set_rle(bitfield(data, 9));
								}
							}
							break;
						case 0x11:
							if (!m_blitter.busy())
							{
								m_blitter.set_color(data, mask);
							}
							break;
						case 0x12:
						case 0x13:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_blitter.set_base(sdata2, smask2);
							}
							break;
						}
						case 0x14:
							if (!m_blitter.busy())
							{
								m_blitter.set_width(data, mask);
							}
							break;
						case 0x15:
							if (!m_blitter.busy())
							{
								m_blitter.set_height(data, mask);
							}
							break;
						case 0x16:
							if (!m_blitter.busy())
							{
								m_blitter.set_x(data, mask);
							}
							break;
						case 0x17:
							if (!m_blitter.busy())
							{
								m_blitter.set_y(data, mask);
							}
							break;
						case 0x18:
						case 0x19:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_blitter.set_zoomx(sdata2, smask2);
							}
							break;
						}
						case 0x1a:
						case 0x1b:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_blitter.set_zoomy(sdata2, smask2);
							}
							break;
						}
						case 0x1c:
						case 0x1d:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_blitter.set_tilt(sdata2, smask2);
							}
							break;
						}
						case 0x1e:
						case 0x1f:
						{
							if (!m_blitter.busy())
							{
								const u32 sdata2 = u32(data) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								const u32 smask2 = u32(mask) << (bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4);
								m_blitter.set_stretch(sdata2, smask2);
							}
							break;
						}
						case 0x20:
							if (!m_blitter.busy())
							{
								m_blitter.set_clip_left(data, mask);
							}
							break;
						case 0x21:
							if (!m_blitter.busy())
							{
								m_blitter.set_clip_right(data, mask);
							}
							break;
						case 0x22:
							if (!m_blitter.busy())
							{
								m_blitter.set_clip_top(data, mask);
							}
							break;
						case 0x23:
							if (!m_blitter.busy())
							{
								m_blitter.set_clip_bottom(data, mask);
							}
							break;
						case 0x24:
							if (!m_blitter.busy())
							{
								m_blitter.set_palindex(data, mask);
							}
							break;
					}
					break;
				}
			}
		}
		else // Slot
		{
			if (bitfield(mask, 0, 8))
			{
				m_slot = bitfield(data, 0, 8);
			}
		}
	} // jkmblit_t::host_w

	template<bool BigEndian, typename T>
	void jkmblit_t<BigEndian, T>::blitter_t::reset()
	{
		m_global_x = 0;
		m_global_y = 0;
		m_global_flipx = false;
		m_global_flipy = false;

		m_bpp = 0;
		m_nonzero_draw = 0;
		m_zero_draw = 0;
		m_flipx = false;
		m_flipy = false;
		m_color = 0;
		m_palindex = 0;
		m_base = 0;
		m_width = 0;
		m_height = 0;
		m_x = 0;
		m_y = 0;
		m_zoomx = 0;
		m_zoomy = 0;
		m_tilt = 0;
		m_stretch = 0;

		m_clip = false;
		m_clip_left = 0;
		m_clip_right = 0;
		m_clip_top = 0;
		m_clip_bottom = 0;

		m_busy = false;
		m_last_dy = 0;
		m_last_dy_inc = 0;
		m_pixel_bits = 0;
		m_pixel = 0;
		m_cycle = 0;

		m_rle = false;
		m_rle_count = -1;
		m_rle_literal = true;
	}

	template<bool BigEndian, typename T>
	u8 jkmblit_t<BigEndian, T>::blitter_t::get_pixel(u32 &base)
	{
		const u8 bpp = (1 << m_bpp);
		if (m_rle)
		{
			if (m_pixel_bits < bpp)
			{
				if (m_rle_count < 0)
				{
					m_rle_count = m_host.m_intf.read_char_byte(base++);
					m_cycle += 8;
					m_rle_literal = bitfield(m_rle_count, 7);
					m_rle_count &= 0x7f;
					if (!m_rle_literal)
					{
						m_pixel = (m_pixel << 8) | m_host.m_intf.read_char_byte(base++);
						m_cycle += 8;
					}
				}
				if (m_rle_literal)
				{
					m_pixel = (m_pixel << 8) | m_host.m_intf.read_char_byte(base++);
					m_cycle += 8;
					m_rle_count--;
				}
				else
				{
					m_rle_count--;
				}
				m_pixel_bits += 8;
			}
			else
			{
				m_cycle++;
			}
			m_pixel_bits -= bpp;
			return bitfield(m_pixel, m_pixel_bits, bpp);
		}
		else
		{
			if (m_pixel_bits < bpp)
			{
				m_pixel = (m_pixel << 8) | m_host.m_intf.read_char_byte(base++);
				m_cycle += 8;
				m_pixel_bits += 8;
			}
			else
			{
				m_cycle++;
			}
			m_pixel_bits -= bpp;
			return bitfield(m_pixel, m_pixel_bits, bpp);
		}
	}

	template<bool BigEndian, typename T>
	void jkmblit_t<BigEndian, T>::blitter_t::write_pixel(const u8 bank, const s32 dx, const s32 dy, const u8 pixel)
	{
		const s32 xpos = m_global_flipx ? (m_global_x - dx) : (m_global_x + dx);
		const s32 ypos = m_global_flipy ? (m_global_y - dy) : (m_global_y + dy);
		if ((!m_clip)
			|| (((xpos >= m_clip_left) && (xpos <= m_clip_right))
			&& ((ypos >= m_clip_top) && (ypos <= m_clip_bottom))))
		{
			if (pixel == 0)
			{
				switch (m_zero_draw)
				{
					case 0x02:
						m_host.m_intf.write_pixel(bank, xpos, ypos, pixel + m_palindex);
						break;
					case 0x03:
						m_host.m_intf.write_pixel(bank, xpos, ypos, m_color);
						break;
				}
			}
			else
			{
				switch (m_nonzero_draw)
				{
					case 0x02:
						m_host.m_intf.write_pixel(bank, xpos, ypos, pixel + m_palindex);
						break;
					case 0x03:
						m_host.m_intf.write_pixel(bank, xpos, ypos, m_color);
						break;
				}
			}
		}
		m_cycle++;
	}

	template<bool BigEndian, typename T>
	void jkmblit_t<BigEndian, T>::blitter_t::draw_single_pixel(const u8 bank, const s32 dx, const s32 dy, const s32 dy_inc, const u8 pixel)
	{
		m_last_dy = dy;
		m_last_dy_inc = dy_inc;
		if (m_zoomy > 0x10000)
		{
			while (m_last_dy_inc >= 0x10000)
			{
				write_pixel(bank, dx, m_last_dy, pixel);
				m_last_dy += m_flipy ? -1 : 1;
				m_last_dy_inc -= 0x10000;
			}
		}
		else
		{
			write_pixel(bank, dx, dy, pixel);
			m_last_dy += (m_last_dy_inc >> 16) * (m_flipy ? -1 : 1);
			m_last_dy_inc &= 0xffff;
		}
	}

	template<bool BigEndian, typename T>
	void jkmblit_t<BigEndian, T>::blitter_t::draw_zoom_pixel(const u8 bank, u32 &dx_inc, s32 &dx, const s32 dy, const s32 dy_inc, const u8 pixel)
	{
		dx_inc += s32(m_zoomx) + s32(dy * m_stretch);
		if (m_zoomx > 0x10000)
		{
			while (dx_inc >= 0x10000)
			{
				draw_single_pixel(bank, dx, dy, dy_inc, pixel);
				dx += m_flipx ? -1 : 1;
				dx_inc -= 0x10000;
			}
		}
		else
		{
			draw_single_pixel(bank, dx, dy, dy_inc, pixel);
			dx += (dx_inc >> 16) * (m_flipx ? -1 : 1);
			dx_inc &= 0xffff;
		}
	}

	template<bool BigEndian, typename T>
	void jkmblit_t<BigEndian, T>::blitter_t::draw_row(const u8 bank, u32 &base, const s32 dy, const s32 dy_inc)
	{
		s32 dx = s32(m_x) + s32(dy * m_tilt);
		s32 dx_inc = 0;
		for (int sx = 0; sx < m_width; sx++)
		{
			const u8 pixel = get_pixel(base);
			draw_zoom_pixel(bank, dx_inc, dx, dy, dy_inc, pixel);
		}
	}

	template<bool BigEndian, typename T>
	s64 jkmblit_t<BigEndian, T>::blitter_t::blit(const u8 bank)
	{
		m_last_dy = 0;
		m_last_dy_inc = 0;
		m_pixel_bits = 0;
		m_pixel = 0;
		m_cycle = 0;

		m_rle_count = -1;
		m_rle_literal = true;

		s32 dy = s32(m_y);
		s32 dy_inc = 0;
		u32 base = m_base;
		for (int sy = 0; sy < m_height; sy++)
		{
			dy_inc += m_zoomy;
			draw_row(bank, base, dy, dy_inc);
			dy = m_last_dy;
			dy_inc = m_last_dy_inc;
		}
		return m_cycle;
	}

	template<bool BigEndian, typename T>
	void jkmblit_t<BigEndian, T>::reset()
	{
		m_blitter.reset();
		m_irq = false;
		m_cycle = 0;
		m_char_ram_addr = 0;
		m_bank = 0;
		m_slot = 0;
	}

	template<bool BigEndian, typename T>
	void jkmblit_t<BigEndian, T>::tick()
	{
		if (m_blitter.busy())
		{
			if (m_cycle-- <= 0)
			{
				m_blitter.set_busy(false);
				m_irq = true;
				m_intf.irq(m_irq);
			}
		}
	}
} // namespace jkmblit
