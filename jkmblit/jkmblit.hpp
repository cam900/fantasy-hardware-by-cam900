/*

==============================================================================

    JKMBLIT
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMBLIT

Fantasy blitter hardware

*/

#ifndef JKMBLIT_HPP
#define JKMBLIT_HPP

#pragma once

namespace jkmblit
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	template<bool BigEndian>
	class jkmblit_intf_t
	{
		public:
			jkmblit_intf_t()
			{
			} // jkmblit_intf_t

			virtual void irq(bool assert) {}

			virtual u8 read_char_byte(u8 offs) { return 0; }
			u16 read_char_word(u16 offs)
			{
				return BigEndian ?
					((u16(read_char_byte(offs)) << 8) | read_char_byte(offs + 1))
					: ((u16(read_char_byte(offs + 1)) << 8) | read_char_byte(offs));
			} // read_char_word
			u32 read_char_dword(u32 offs)
			{
				return BigEndian ?
					((u32(read_char_byte(offs)) << 16) | read_char_byte(offs + 2))
					: ((u32(read_char_byte(offs + 2)) << 16) | read_char_byte(offs));
			} // read_char_word
			virtual void write_char_byte(u32 offs, u8 data, u8 mask) {}
			void write_char_word(u32 offs, u16 data, u16 mask)
			{
				if (BigEndian)
				{
					write_char_byte(offs, bitfield(data, 8, 8), bitfield(mask, 8, 8));
					write_char_byte(offs + 1, bitfield(data, 0, 8), bitfield(mask, 0, 8));
				}
				else
				{
					write_char_byte(offs + 1, bitfield(data, 8, 8), bitfield(mask, 8, 8));
					write_char_byte(offs, bitfield(data, 0, 8), bitfield(mask, 0, 8));
				}
			}
			void write_char_dword(u32 offs, u32 data, u32 mask)
			{
				if (BigEndian)
				{
					write_char_word(offs, bitfield(data, 16, 16), bitfield(mask, 16, 16));
					write_char_word(offs + 2, bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
				else
				{
					write_char_word(offs + 2, bitfield(data, 16, 16), bitfield(mask, 16, 16));
					write_char_word(offs, bitfield(data, 0, 16), bitfield(mask, 0, 16));
				}
			}

			virtual void write_pixel(u8 bank, u32 x, u32 y, u16 pixel) {}
	}; // class jkmblit_intf_t

	template<bool BigEndian, typename T>
	class jkmblit_t
	{
		private:
			class blitter_t
			{
				public:
					blitter_t(jkmblit_t &host)
						: m_host(host)

						, m_global_x(0)
						, m_global_y(0)
						, m_global_flipx(false)
						, m_global_flipy(false)

						, m_bpp(0)
						, m_nonzero_draw(0)
						, m_zero_draw(0)
						, m_flipx(false)
						, m_flipy(false)
						, m_color(0)
						, m_palindex(0)
						, m_base(0)
						, m_width(0)
						, m_height(0)
						, m_x(0)
						, m_y(0)
						, m_zoomx(0)
						, m_zoomy(0)
						, m_tilt(0)
						, m_stretch(0)

						, m_clip(false)
						, m_clip_left(0)
						, m_clip_right(0)
						, m_clip_top(0)
						, m_clip_bottom(0)

						, m_busy(false)
						, m_last_dy(0)
						, m_last_dy_inc(0)
						, m_pixel_bits(0)
						, m_pixel(0)
						, m_cycle(0)

						, m_rle(false)
						, m_rle_count(-1)
						, m_rle_literal(true)
					{
					} // blitter_t

					void reset();

					u8 get_pixel(u32 &base);
					void write_pixel(const u8 bank, const s32 dx, const s32 dy, const u8 pixel);
					void draw_single_pixel(const u8 bank, const s32 dx, const s32 dy, const s32 dy_inc, const u8 pixel);
					void draw_zoom_pixel(const u8 bank, u32 &dx_inc, s32 &dx, const s32 dy, const s32 dy_inc, const u8 pixel);
					void draw_row(const u8 bank, u32 &base, const s32 dy, const s32 dy_inc);
					s64 blit(const u8 bank);

					bool busy() const
					{
						return m_busy;
					} // busy

					s32 global_x() const
					{
						return m_global_x;
					} // global_x

					s32 global_y() const
					{
						return m_global_y;
					} // global_y

					bool global_flipx() const
					{
						return m_global_flipx;
					} // global_flipx

					bool global_flipy() const
					{
						return m_global_flipy;
					} // global_flipy

					u8 bpp() const
					{
						return m_bpp;
					} // bpp

					u8 nonzero_draw() const
					{
						return m_nonzero_draw;
					} // nonzero_draw

					u8 zero_draw() const
					{
						return m_zero_draw;
					} // zero_draw

					bool flipx() const
					{
						return m_flipx;
					} // flipx

					bool flipy() const
					{
						return m_flipy;
					} // flipy

					u16 color() const
					{
						return m_color;
					} // color

					u16 palindex() const
					{
						return m_palindex;
					} // palindex

					u32 base() const
					{
						return m_base;
					} // base

					u16 width() const
					{
						return m_width;
					} // width

					u16 height() const
					{
						return m_height;
					} // height

					s16 x() const
					{
						return m_x;
					} // x

					s16 y() const
					{
						return m_y;
					} // y

					u32 zoomx() const
					{
						return m_zoomx;
					} // zoomx

					u32 zoomy() const
					{
						return m_zoomy;
					} // zoomy

					u32 tilt() const
					{
						return m_tilt;
					} // tilt

					u32 stretch() const
					{
						return m_stretch;
					} // stretch

					bool clip() const
					{
						return m_clip;
					} // clip

					u16 clip_left() const
					{
						return m_clip_left;
					} // clip_left

					u16 clip_right() const
					{
						return m_clip_right;
					} // clip_right

					u16 clip_top() const
					{
						return m_clip_top;
					} // clip_top

					u16 clip_bottom() const
					{
						return m_clip_bottom;
					} // clip_bottom

					bool rle() const
					{
						return m_rle;
					} // rle

					void set_global_x(const s16 x, const u16 mask = ~0)
					{
						m_global_x = (m_global_x & ~mask) | (x & mask);
					} // set_global_x

					void set_global_y(const s16 y, const u16 mask = ~0)
					{
						m_global_y = (m_global_y & ~mask) | (y & mask);
					} // set_global_y

					void set_global_flipx(const bool flipx)
					{
						m_global_flipx = flipx;
					} // set_global_flipx

					void set_global_flipy(const bool flipy)
					{
						m_global_flipy = flipy;
					} // set_global_flipy

					void set_bpp(const u8 bpp)
					{
						m_bpp = bpp;
					} // set_bpp

					void set_nonzero_draw(const u8 nonzero_draw)
					{
						m_nonzero_draw = nonzero_draw;
					} // set_nonzero_draw

					void set_zero_draw(const u8 zero_draw)
					{
						m_zero_draw = zero_draw;
					} // set_zero_draw

					void set_flipx(const bool flipx)
					{
						m_flipx = flipx;
					} // set_flipx

					void set_flipy(const bool flipy)
					{
						m_flipy = flipy;
					} // set_flipy

					void set_color(const u16 color, const u16 mask = ~0)
					{
						m_color = (m_color & ~mask) | (color & mask);
					} // set_color

					void set_palindex(const u16 palindex, const u16 mask = ~0)
					{
						m_palindex = (m_palindex & ~mask) | (palindex & mask);
					} // set_palindex

					void set_base(const u32 base, const u32 mask = ~0)
					{
						m_base = (m_base & ~mask) | (base & mask);
					} // set_base

					void set_width(const u16 width, const u16 mask = ~0)
					{
						m_width = (m_width & ~mask) | (width & mask);
					} // set_width

					void set_height(const u16 height, const u16 mask = ~0)
					{
						m_height = (m_height & ~mask) | (height & mask);
					} // set_height

					void set_x(const s16 x, const u16 mask = ~0)
					{
						m_x = (m_x & ~mask) | (x & mask);
					} // set_x

					void set_y(const s16 y, const u16 mask = ~0)
					{
						m_y = (m_y & ~mask) | (y & mask);
					} // set_y

					void set_zoomx(const u32 zoomx, const u32 mask = ~0)
					{
						m_zoomx = (m_zoomx & ~mask) | (zoomx & mask);
					} // set_zoomx

					void set_zoomy(const u32 zoomy, const u32 mask = ~0)
					{
						m_zoomy = (m_zoomy & ~mask) | (zoomy & mask);
					} // set_zoomy

					void set_tilt(const u32 tilt, const u32 mask = ~0)
					{
						m_tilt = (m_tilt & ~mask) | (tilt & mask);
					} // set_tilt

					void set_stretch(const u32 stretch, const u32 mask = ~0)
					{
						m_stretch = (m_stretch & ~mask) | (stretch & mask);
					} // set_stretch

					void set_clip(const bool clip)
					{
						m_clip = clip;
					} // set_clip

					void set_clip_left(const u16 clip_left, const u16 mask = ~0)
					{
						m_clip_left = (m_clip_left & ~mask) | (clip_left & mask);
					} // set_clip_left

					void set_clip_right(const u16 clip_right, const u16 mask = ~0)
					{
						m_clip_right = (m_clip_right & ~mask) | (clip_right & mask);
					} // set_clip_right

					void set_clip_top(const u16 clip_top, const u16 mask = ~0)
					{
						m_clip_top = (m_clip_top & ~mask) | (clip_top & mask);
					} // set_clip_top

					void set_clip_bottom(const u16 clip_bottom, const u16 mask = ~0)
					{
						m_clip_bottom = (m_clip_bottom & ~mask) | (clip_bottom & mask);
					} // set_clip_bottom

					void set_rle(const bool rle)
					{
						m_rle = rle;
					} // set_rle

					void set_busy(const bool busy)
					{
						m_busy = busy;
					} // set_busy

				private:
					jkmblit_t &m_host;

					// register
					s16 m_global_x = 0;
					s16 m_global_y = 0;
					bool m_global_flipx = false;
					bool m_global_flipy = false;

					// blitter parameters
					u8 m_bpp = 0;
					u8 m_nonzero_draw = 0;
					u8 m_zero_draw = 0;
					bool m_flipx = false;
					bool m_flipy = false;
					u16 m_color = 0;
					u16 m_palindex = 0;
					u32 m_base = 0;
					u16 m_width = 0;
					u16 m_height = 0;
					s16 m_x = 0;
					s16 m_y = 0;
					u32 m_zoomx = 0;
					u32 m_zoomy = 0;
					u32 m_tilt = 0;
					u32 m_stretch = 0;

					// clip parameters
					bool m_clip = false;
					u16 m_clip_left = 0;
					u16 m_clip_right = 0;
					u16 m_clip_top = 0;
					u16 m_clip_bottom = 0;

					// internal state
					bool m_busy = false;
					s32 m_last_dy = 0;
					s32 m_last_dy_inc = 0;
					s8 m_pixel_bits = 0;
					u16 m_pixel = 0;
					s64 m_cycle = 0;

					// RLE status
					bool m_rle = false;
					s16 m_rle_count = -1;
					bool m_rle_literal = true;
			}; // class blitter_t
		public:
			jkmblit_t(jkmblit_intf_t<BigEndian> &intf)
				: m_intf(intf)
				, m_blitter(blitter_t())
				, m_irq(false)
				, m_cycle(0)
				, m_char_ram_addr(0)
				, m_bank(0)
				, m_slot(0)
			{
			} // jkmblit_t

			void reset();
			void tick();

			T host_r(const u8 addr);
			void host_w(const u8 addr, const T data, const T mask);

		private:
			jkmblit_intf_t<BigEndian> &m_intf; // Interface
			blitter_t m_blitter;
			bool m_irq = false;       // IRQ flag
			s64 m_cycle = 0;          // Blitter cycle
			u32 m_char_ram_addr = 0;  // Character RAM address
			u8 m_bank = 0;            // Bank select
			u8 m_slot = 0;            // Register slot
	}; // class jkmblit_t
} // namespace jkmblit

#endif // JKMBLIT_HPP
