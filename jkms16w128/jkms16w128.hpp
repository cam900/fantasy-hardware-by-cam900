/*

==============================================================================

    JKMS16W128
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMS16W128

Fantasy sound generator by cam900

*/

#ifndef JKMS16W128_HPP
#define JKMS16W128_HPP

#pragma once

#include <algorithm>
#include <array>

namespace jkms16w128
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	template<typename T>
	static const inline T clamp(const T in, const T min, const T max)
	{
		return (in < min) ? min : ((in > max) ? max : in);
	} // clamp

	class jkms16w128_intf_t
	{
		public:
			jkms16w128_intf_t()
			{
			} // jkms16w128_intf_t

			virtual u16 read_word(u16 addr) { return 0; }
			virtual void write_word(u16 addr, u16 data, u16 mask = ~0) {}
	}; // jkms16w128_intf_t

	class jkms16w128_t
	{
		private:
			class voice_t
			{
				private:
					class envelope_t
					{
						public:
							enum env_state_t : u8
							{
								ENV_ATTACK = 0,
								ENV_DECAY,
								ENV_SUSTAIN,
								ENV_IDLE,
								ENV_RELEASE,
								ENV_RELEND,
								ENV_MAX
							}; // env_state_t

							envelope_t()
								: m_enable(false)
								, m_env_delay_time(0)
								, m_env_level_init(0)
								, m_env_rate_val{0}
								, m_env_target_val{0}
								, m_loop(false)
								, m_env_state(ENV_IDLE)
								, m_env_level(0)
								, m_env_rate(0)
								, m_env_target(0)
								, m_env_delay_counter(0)
							{
							} // envelope_t

							void reset();

							void init();

							void release();

							s32 tick();

							inline void set_enable(const bool enable)
							{
								m_enable = enable;
							} // set_env_level_init

							inline bool enable() const
							{
								return m_enable;
							} // get_env_level_init

							inline void set_env_delay_time(const u16 val, const u16 mask = ~0)
							{
								m_env_delay_time = (m_env_delay_time & ~mask) | (val & mask);
							} // set_env_delay_time

							inline u16 env_delay_time() const
							{
								return m_env_delay_time;
							} // env_delay_time

							inline void set_env_level_init(const u16 val, const u16 mask = ~0)
							{
								m_env_level_init = (m_env_level_init & ~mask) | (val & mask);
							} // set_env_level_init

							inline u16 env_level_init() const
							{
								return m_env_level_init;
							} // env_level_init

							inline void set_env_rate(const env_state_t state, const u16 val, const u16 mask = ~0)
							{
								m_env_rate_val[state] = (m_env_rate_val[state] & ~mask) | (val & mask);
							} // set_env_rate

							inline u16 env_rate(const env_state_t state) const
							{
								return m_env_rate_val[state];
							} // env_rate

							inline void set_env_target(const env_state_t state, const u16 val, const u16 mask = ~0)
							{
								m_env_target_val[state] = (m_env_target_val[state] & ~mask) | (val & mask);
							} // set_env_target

							inline u16 env_target(const env_state_t state) const
							{
								return m_env_target_val[state];
							} // env_target

							inline void set_loop(const bool loop)
							{
								m_loop = loop;
							} // set_loop

							inline bool loop()
							{
								return m_loop;
							} // loop

						private:
							// internal helper functions
							inline s32 get_env_delay(const u16 envval) const
							{
								// 4 bit exponent, 12 bit mantissa
								// eeee mmmm mmmm mmmm
								// 0000 0000 0000 0000 Ignore

								const u8 exponent = bitfield(envval, 12, 4);
								const u16 mantissa = bitfield(envval, 0, 12);

								if (exponent == 0)
									return mantissa;
								else
									return (0x1000 | mantissa) << (exponent - 1);
							} // get_env_delay

							inline s32 get_env_rate(const u16 envval) const
							{
								// 4 bit exponent, 12 bit mantissa
								// eeee mmmm mmmm mmmm
								// 1111 1111 1111 1111 Skip
								// 0000 0000 0000 0000 Ignore

								if (envval == 0xffff)
									return -1; // skip

								const u8 exponent = bitfield(envval, 12, 4);
								const u16 mantissa = bitfield(envval, 0, 12);

								if (exponent == 0)
									return mantissa;
								else
									return (0x1000 | mantissa) << (exponent - 1);
							} // get_env_rate

							inline s32 get_env_target(const u16 envval) const
							{
								// 1 bit sign, 4 bit exponent, 11 bit mantissa
								// seee emmm mmmm mmmm
								// -111 1--- ---- ---- max value
								// -000 0000 0000 0000 Ignore

								const bool sign = bitfield(envval, 15);
								const u8 exponent = bitfield(envval, 11, 4);
								const u16 mantissa = bitfield(envval, 0, 11);

								if (exponent == 0xf)
									return (1 << 25) * (sign ? -1 : 1);
								if (exponent == 0)
									return mantissa * (sign ? -1 : 1);
								else
									return ((0x800 | mantissa) << (exponent - 1)) * (sign ? -1 : 1);
							} // get_env_target

							// registers
							bool m_enable = false;
							u16 m_env_delay_time = 0;
							u16 m_env_level_init = 0;
							std::array<u16, ENV_MAX> m_env_rate_val = {0};
							std::array<u16, ENV_MAX> m_env_target_val = {0};
							bool m_loop = false;

							// internal states
							u8 m_env_state = ENV_IDLE;
							s32 m_env_level = 0;
							s32 m_env_rate = 0;
							s32 m_env_target = 0;
							s32 m_env_delay_counter = 0;
					}; // class envelope_t

					class wave_t
					{
						private:
							class lfo_t
							{
								public:
									lfo_t()
										: m_enable(false)
										, m_delay(0)
										, m_rate(0)
										, m_scale(0)
										, m_wave(0)
										, m_depth(0)

										, m_delay_counter(0)
										, m_level(0)
										, m_target(0)
										, m_temp_rate(0)
										, m_lfsr(1)
									{
									} // lfo_t

									void reset();

									void init();

									s32 tick();

									void set_enable(const bool enable)
									{
										m_enable = enable;
									} // set_enable

									bool enable() const
									{
										return m_enable;
									} // enable

									void set_delay(const u16 delay, const u16 mask = ~0)
									{
										m_delay = (m_delay & ~mask) | (delay & mask);
									} // set_delay

									u16 delay() const
									{
										return m_delay;
									} // delay

									void set_rate(const u16 rate, const u16 mask = ~0)
									{
										m_rate = (m_rate & ~mask) | (rate & mask);
									} // set_rate

									u16 rate() const
									{
										return m_rate;
									} // rate

									void set_scale(const u16 scale, const u16 mask = ~0)
									{
										m_scale = (m_scale & ~mask) | (scale & mask);
									} // set_scale

									u16 scale() const
									{
										return m_scale;
									} // scale

									void set_wave(const u8 wave)
									{
										m_wave = wave;
									} // set_wave

									u8 wave() const
									{
										return m_wave;
									} // wave

									void set_depth(const u16 depth, const u16 mask = ~0)
									{
										m_depth = (m_depth & ~mask) | (depth & mask);
									} // set_depth

									u16 depth() const
									{
										return m_depth;
									} // depth

								private:
									inline s32 get_lfo_rate(const u16 lfoval) const
									{
										// 4 bit exponent, 12 bit mantissa
										// eeee mmmm mmmm mmmm
										// 0000 0000 0000 0000 Ignore

										const u8 exponent = bitfield(lfoval, 12, 4);
										const u16 mantissa = bitfield(lfoval, 0, 12);

										if (exponent == 0)
											return mantissa;
										else
											return (0x1000 | mantissa) << (exponent - 1);
									} // get_lfo_rate

									inline s32 get_lfo_target(const u16 lfoval) const
									{
										// 1 bit sign, 4 bit exponent, 11 bit mantissa
										// seee emmm mmmm mmmm
										// -111 1--- ---- ---- max value
										// -000 0000 0000 0000 Ignore

										const bool sign = bitfield(lfoval, 15);
										const u8 exponent = bitfield(lfoval, 11, 4);
										const u16 mantissa = bitfield(lfoval, 0, 11);

										if (exponent == 0xf)
											return (1 << 25) * (sign ? -1 : 1);
										if (exponent == 0)
											return mantissa * (sign ? -1 : 1);
										else
											return ((0x800 | mantissa) << (exponent - 1)) * (sign ? -1 : 1);
									} // get_lfo_target

									// registers
									bool m_enable = false;
									u16 m_delay = 0;
									u16 m_rate = 0;
									u16 m_scale = 0;
									u8 m_wave = 0;
									s16 m_depth = 0;

									// internal states
									s32 m_delay_counter = 0;
									s32 m_level = 0;
									s32 m_target = 0;
									s32 m_temp_rate = 0;
									u32 m_lfsr = 1;

							}; // class lfo_t

						public:
							wave_t(jkms16w128_t &host)
								: m_host(host)
								, m_vibrato(lfo_t())
								, m_tremolo(lfo_t())
								, m_pulse(false)
								, m_sawtooth(false)
								, m_triangle(false)
								, m_inv_pulse(false)
								, m_inv_sawtooth(false)
								, m_inv_triangle(false)
								, m_periodic_noise(false)
								, m_white_noise(false)

								, m_int_wave_size(0)
								, m_pulse_duty(0)
								, m_lfsr_tab(1)
								, m_lfsr_init(1)

								, m_ext_wave_enable(false)
								, m_base(0)
								, m_addr_size(0)

								, m_pitch(0)

								, m_pm_input_en(false)
								, m_pm_input_scale(0)

								, m_am_input_en(false)
								, m_am_input_scale(0)

								, m_fm_input_en(false)
								, m_fm_input_scale(0)

								, m_mute_enable(false)
								, m_mute_pos(0)

								, m_hinv_enable(false)
								, m_hinv_pos(0)

								, m_vinv_enable(false)
								, m_vinv_pos(0)

								, m_addr(0)
								, m_addr_mask(0)
								, m_lfsr(1)
								, m_frac(0)
								, m_noise_frac(0)

								, m_pm_input(0)
								, m_am_input(0)
								, m_fm_input(0)
							{
							} // wave_t

							inline u16 read_word(u16 addr) const
							{
								return m_host.m_intf.read_word(addr);
							} // read_word

							void add_pm_input(const s64 in)
							{
								m_pm_input = clamp<s32>(m_pm_input + in, -0x8000, 0x7fff);
							} // add_pm_input

							void add_am_input(const s64 in)
							{
								m_am_input = clamp<s32>(m_am_input + in, -0x8000, 0x7fff);
							} // add_am_input

							void add_fm_input(const s64 in)
							{
								m_fm_input = clamp<s32>(m_fm_input + in, -0x8000, 0x7fff);
							} // add_fm_input

							void reset();

							void init();

							s32 wave_get();

							void tick();

							// getter/setters
							lfo_t &vibrato()
							{
								return m_vibrato;
							} // vibrato

							lfo_t &tremolo()
							{
								return m_tremolo;
							} // tremolo

							inline void set_pulse(const bool pulse)
							{
								m_pulse = pulse;
							} // set_pulse

							inline bool pulse() const
							{
								return m_pulse;
							} // pulse

							inline void set_sawtooth(const bool sawtooth)
							{
								m_sawtooth = sawtooth;
							} // set_sawtooth

							inline bool sawtooth() const
							{
								return m_sawtooth;
							} // sawtooth

							inline void set_triangle(const bool triangle)
							{
								m_triangle = triangle;
							} // set_triangle

							inline bool triangle() const
							{
								return m_triangle;
							} // triangle

							inline void set_inv_pulse(const bool inv_pulse)
							{
								m_inv_pulse = inv_pulse;
							} // set_inv_pulse

							inline bool inv_pulse() const
							{
								return m_inv_pulse;
							} // inv_pulse

							inline void set_inv_sawtooth(const bool inv_sawtooth)
							{
								m_inv_sawtooth = inv_sawtooth;
							} // set_inv_sawtooth

							inline bool inv_sawtooth() const
							{
								return m_inv_sawtooth;
							} // inv_sawtooth

							inline void set_inv_triangle(const bool inv_triangle)
							{
								m_inv_triangle = inv_triangle;
							} // set_inv_triangle

							inline bool inv_triangle() const
							{
								return m_inv_triangle;
							} // inv_triangle

							inline void set_periodic_noise(const bool periodic_noise)
							{
								m_periodic_noise = periodic_noise;
							} // set_periodic_noise

							inline bool periodic_noise() const
							{
								return m_periodic_noise;
							} // periodic_noise

							inline void set_white_noise(const bool white_noise)
							{
								m_white_noise = white_noise;
							} // set_white_noise

							inline bool white_noise() const
							{
								return m_white_noise;
							} // white_noise

							inline void set_int_wave_size(const u8 size)
							{
								m_int_wave_size = size;
							}

							inline u8 int_wave_size() const
							{
								return m_int_wave_size;
							}

							inline void set_pulse_duty(const u16 pulse_duty, const u16 mask = ~0)
							{
								m_pulse_duty = (m_pulse_duty & ~mask) | (pulse_duty & mask);
							} // set_pulse_duty

							inline u16 pulse_duty() const
							{
								return m_pulse_duty;
							} // pulse_duty

							inline void set_lfsr_tab(const u16 lfsr_tab, const u16 mask = ~0)
							{
								m_lfsr_tab = (m_lfsr_tab & ~mask) | (lfsr_tab & mask);
							} // set_lfsr_tab

							inline u16 lfsr_tab() const
							{
								return m_lfsr_tab;
							} // lfsr_tab

							inline void set_lfsr_init(const u16 lfsr_init, const u16 mask = ~0)
							{
								m_lfsr_init = (m_lfsr_init & ~mask) | (lfsr_init & mask);
							} // set_lfsr_init

							inline u16 lfsr_init() const
							{
								return m_lfsr_init;
							} // lfsr_init

							inline void set_ext_wave_enable(const bool ext_wave_enable)
							{
								m_ext_wave_enable = ext_wave_enable;
							} // set_ext_wave_enable

							inline bool ext_wave_enable() const
							{
								return m_ext_wave_enable;
							} // ext_wave_enable

							inline void set_base(const u16 base, const u16 mask = ~0)
							{
								m_base = (m_base & ~mask) | (base & mask);
							} // set_base

							inline u16 base() const
							{
								return m_base;
							} // base

							inline void set_addr_size(const u8 addr_size)
							{
								m_addr_size = addr_size;
								m_addr_mask = (1 << m_addr_size) - 1;
							} // set_addr_size

							inline u16 addr_size() const
							{
								return m_addr_size;
							} // addr_size

							inline void set_pitch(const u32 pitch, const u32 mask)
							{
								m_pitch = (m_pitch & ~mask) | (pitch & mask);
							} // set_pitch

							inline u32 pitch() const
							{
								return m_pitch;
							} // pitch

							inline void set_pm_input_en(const bool enable)
							{
								m_pm_input_en = enable;
							} // set_pm_input_en

							inline bool pm_input_en() const
							{
								return m_pm_input_en;
							} // pm_input_en

							inline void set_pm_input_scale(const s16 scale, const u16 mask = ~0)
							{
								m_pm_input_scale = (m_pm_input_scale & ~mask) | (scale & mask);
							} // set_pm_input_scale

							inline s16 pm_input_scale() const
							{
								return m_pm_input_scale;
							} // pm_input_scale

							inline void set_am_input_en(const bool enable)
							{
								m_am_input_en = enable;
							} // set_am_input_en

							inline bool am_input_en() const
							{
								return m_am_input_en;
							} // am_input_en

							inline void set_am_input_scale(const s16 scale, const u16 mask = ~0)
							{
								m_am_input_scale = (m_am_input_scale & ~mask) | (scale & mask);
							} // set_am_input_scale

							inline s16 am_input_scale() const
							{
								return m_am_input_scale;
							} // am_input_scale

							inline void set_fm_input_en(const bool enable)
							{
								m_fm_input_en = enable;
							} // set_fm_input_en

							inline bool fm_input_en() const
							{
								return m_fm_input_en;
							} // fm_input_en

							inline void set_fm_input_scale(const s16 scale, const u16 mask = ~0)
							{
								m_fm_input_scale = (m_fm_input_scale & ~mask) | (scale & mask);
							} // set_fm_input_scale

							inline s16 fm_input_scale() const
							{
								return m_fm_input_scale;
							} // fm_input_scale

							inline void set_mute_enable(const bool enable)
							{
								m_mute_enable = enable;
							} // set_mute_enable

							inline bool mute_enable() const
							{
								return m_mute_enable;
							} // mute_enable

							inline void set_mute_pos(const u8 pos)
							{
								m_mute_pos = pos;
							} // set_mute_pos

							inline u8 mute_pos() const
							{
								return m_mute_pos;
							} // mute_pos

							inline void set_hinv_enable(const bool enable)
							{
								m_hinv_enable = enable;
							} // set_hinv_enable

							inline bool hinv_enable() const
							{
								return m_hinv_enable;
							} // hinv_enable

							inline void set_hinv_pos(const u8 pos)
							{
								m_hinv_pos = pos;
							} // set_hinv_pos

							inline u8 hinv_pos() const
							{
								return m_hinv_pos;
							} // hinv_pos

							inline void set_vinv_enable(const bool enable)
							{
								m_vinv_enable = enable;
							} // set_vinv_enable

							inline bool vinv_enable() const
							{
								return m_vinv_enable;
							} // vinv_enable

							inline void set_vinv_pos(const u8 pos)
							{
								m_vinv_pos = pos;
							} // set_vinv_pos

							inline u8 vinv_pos() const
							{
								return m_vinv_pos;
							} // vinv_pos

							inline void set_addr(const u16 addr, const u16 mask = ~0)
							{
								m_addr = (m_addr & ~mask) | (addr & mask);
							} // set_addr

							inline u16 addr() const
							{
								return m_addr;
							} // addr

						private:
							// classes
							jkms16w128_t &m_host;
							lfo_t m_vibrato;
							lfo_t m_tremolo;

							// registers
							// internal waveforms
							bool m_pulse = false;
							bool m_sawtooth = false;
							bool m_triangle = false;
							bool m_inv_pulse = false;
							bool m_inv_sawtooth = false;
							bool m_inv_triangle = false;
							bool m_periodic_noise = false;
							bool m_white_noise = false;

							u8 m_int_wave_size = 0;
							u16 m_pulse_duty = 0;
							u16 m_lfsr_tab = 1;
							u16 m_lfsr_init = 1;

							// external waveforms
							bool m_ext_wave_enable = false;
							u16 m_base = 0;
							u8 m_addr_size = 0;

							// playback
							u32 m_pitch = 0;

							// modulations
							// phase modulations
							bool m_pm_input_en = false;
							s16 m_pm_input_scale = 0;

							// amplitude modulations
							bool m_am_input_en = false;
							s16 m_am_input_scale = 0;

							// frequency modulations
							bool m_fm_input_en = false;
							s16 m_fm_input_scale = 0;

							// mute bit
							bool m_mute_enable = false;
							u8 m_mute_pos = 0;

							// horizontal invert bit
							bool m_hinv_enable = false;
							u8 m_hinv_pos = 0;

							// vertical invert bit
							bool m_vinv_enable = false;
							u8 m_vinv_pos = 0;

							// internal states
							// counters
							u16 m_addr = 0;
							u16 m_addr_mask = 0;
							u32 m_lfsr = 1;
							u32 m_frac = 0;
							u32 m_noise_frac = 0;

							// modulation
							s32 m_pm_input = 0;
							s32 m_am_input = 0;
							s32 m_fm_input = 0;

					}; // class wave_t

					class filter_t
					{
						private:
							class filter_pole_t
							{
								public:
									filter_pole_t()
										: m_enable(false)
										, m_lowpass(false)
										, m_highpass(false)
										, m_bandpass(false)
										, m_cutoff(0)
										, m_resonance(0)
										, m_lowres(0)
										, m_highres(0)
										, m_bandres(0)
									{
									} // filter_pole_t

									void reset();

									s32 tick(const s32 input);

									inline void set_enable(const bool enable)
									{
										m_enable = enable;
									} // set_enable

									inline bool enable() const
									{
										return m_enable;
									} // enable

									inline void set_lowpass(const bool lowpass)
									{
										m_lowpass = lowpass;
									} // set_lowpass

									inline bool lowpass() const
									{
										return m_lowpass;
									} // lowpass

									inline void set_highpass(const bool highpass)
									{
										m_highpass = highpass;
									} // set_lowpass

									inline bool highpass() const
									{
										return m_highpass;
									} // lowpass

									inline void set_bandpass(const bool bandpass)
									{
										m_bandpass = bandpass;
									} // set_lowpass

									inline bool bandpass() const
									{
										return m_bandpass;
									} // lowpass

									inline void set_cutoff(const u16 cutoff, const u16 mask = ~0)
									{
										m_cutoff = (m_cutoff & ~mask) | (cutoff & mask);
									} // set_cutoff

									inline u16 cutoff() const
									{
										return m_cutoff;
									} // cutoff

									inline void set_resonance(const u16 resonance, const u16 mask = ~0)
									{
										m_resonance = (m_resonance & ~mask) | (resonance & mask);
									} // set_cutoff

									inline u16 resonance() const
									{
										return m_resonance;
									} // resonance

								private:
									// registers
									bool m_enable = false;
									bool m_lowpass = false;
									bool m_highpass = false;
									bool m_bandpass = false;
									u16 m_cutoff = 0;
									u16 m_resonance = 0;

									// internal states
									s32 m_lowres = 0;
									s32 m_highres = 0;
									s32 m_bandres = 0;
							}; // class filter_pole_t

						public:
							filter_t()
								: m_pole{
									filter_pole_t(), filter_pole_t(), filter_pole_t(), filter_pole_t()
								}
								, m_enable(false)
							{
							} // filter_t

							void reset();

							s32 tick(const s32 input);

							filter_pole_t &pole(u8 slot)
							{
								return m_pole[slot & 3];
							}

							inline void set_enable(const bool enable)
							{
								m_enable = enable;
							} // set enable

							inline bool enable() const
							{
								return m_enable;
							} // enable

						private:
							// classes
							std::array<filter_pole_t, 4> m_pole;

							// registers
							bool m_enable;
					}; // class filter_t

					class mod_t
					{
						private:
							class mod_slot_t
							{
								public:
									mod_slot_t()
										: m_enable(false)
										, m_slot(0)
										, m_scale(0)
									{
									} // mod_slot_t

									void reset()
									{
										m_enable = false;
										m_slot = 0;
										m_scale = 0;
									} // reset

									inline void set_enable(const bool enable)
									{
										m_enable = enable;
									} // set_enable

									inline bool enable() const
									{
										return m_enable;
									} // enable

									inline void set_slot(const u8 slot)
									{
										m_slot = slot;
									} // set_enable

									inline u8 slot() const
									{
										return m_slot;
									} // enable

									inline void set_scale(const s8 scale)
									{
										m_scale = scale;
									} // set_enable

									inline s8 scale() const
									{
										return m_scale;
									} // enable

								private:
									bool m_enable = false;
									u8 m_slot = 0;
									s8 m_scale = 0;
							}; // class mod_slot_t

						public:
							mod_t()
								: m_mod_slot{
									mod_slot_t(), mod_slot_t(), mod_slot_t(), mod_slot_t(),
									mod_slot_t(), mod_slot_t(), mod_slot_t(), mod_slot_t()
								}
								, m_enable(false)
							{
							} // mod_t

							void reset()
							{
								for (mod_slot_t &s : m_mod_slot)
									s.reset();
							} // reset

							mod_slot_t &mod_slot(u8 slot)
							{
								return m_mod_slot[slot & 7];
							} // mod_slot

							inline void set_enable(const bool enable)
							{
								m_enable = enable;
							} // set_enable

							inline bool enable() const
							{
								return m_enable;
							} // enable

						private:
							// classes
							std::array<mod_slot_t, 8> m_mod_slot;

							// registers
							bool m_enable = false;
					}; // class mod_t

				public:
					voice_t(jkms16w128_t &host)
						: m_host(host)
						, m_envelope(envelope_t())
						, m_wave(wave_t(host))
						, m_filter(filter_t())
						, m_pm(mod_t())
						, m_am(mod_t())
						, m_fm(mod_t())
						, m_keyon(false)
						, m_busy(false)
						, m_direct_output(false)
						, m_lvol(0)
						, m_rvol(0)
						, m_mvol(0)
						, m_lout(0)
						, m_rout(0)
					{
					} // voice_t

					void reset();

					void keyon();

					void keyoff();

					void tick();

					envelope_t &envelope()
					{
						return m_envelope;
					} // envelope

					wave_t &wave()
					{
						return m_wave;
					} // wave

					filter_t &filter()
					{
						return m_filter;
					} // filter

					mod_t &pm()
					{
						return m_pm;
					} // pm

					mod_t &am()
					{
						return m_am;
					} // am

					mod_t &fm()
					{
						return m_fm;
					} // fm

					inline void set_keyon(const bool state)
					{
						if (m_keyon && !state)
							keyoff();
						else if (!m_keyon && state)
							keyon();
					} // set_keyon

					inline bool get_keyon() const
					{
						return m_keyon;
					} // get_keyon

					inline void set_direct_output(const bool enable)
					{
						m_direct_output = enable;
					} // set_direct_output

					inline bool direct_output() const
					{
						return m_direct_output;
					} // direct_output

					inline void set_lvol(const s16 lvol, const u16 mask = ~0)
					{
						m_lvol = (m_lvol & ~mask) | (lvol & mask);
					} // set_lvol

					inline s16 lvol()
					{
						return m_lvol;
					} // lvol

					inline void set_rvol(const s16 rvol, const u16 mask = ~0)
					{
						m_rvol = (m_rvol & ~mask) | (rvol & mask);
					} // set_rvol

					inline s16 rvol()
					{
						return m_rvol;
					} // rvol

					inline void set_mvol(const s16 mvol, const u16 mask = ~0)
					{
						m_mvol = (m_mvol & ~mask) | (mvol & mask);
					} // set_mvol

					inline s16 mvol()
					{
						return m_mvol;
					} // mvol

					inline s64 lout() const
					{
						return m_lout;
					} // lout

					inline s64 rout() const
					{
						return m_rout;
					} // rout;

				private:
					// classes
					jkms16w128_t &m_host;
					envelope_t m_envelope;
					wave_t m_wave;
					filter_t m_filter;
					mod_t m_pm;
					mod_t m_am;
					mod_t m_fm;

					// registers
					bool m_keyon = false;
					bool m_busy = false;
					bool m_direct_output = false;
					s16 m_lvol = 0;
					s16 m_rvol = 0;
					s16 m_mvol = 0;

					// outputs
					s64 m_lout = 0;
					s64 m_rout = 0;
			}; // class voice_t

		public:
			jkms16w128_t(jkms16w128_intf_t &intf)
				: m_intf(intf)
				, m_voice{
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
					*this, *this, *this, *this, *this, *this, *this, *this,
				}
				, m_reg(0)
				, m_voice_select(0)
				, m_mem_addr(0)
				, m_lout(0)
				, m_rout(0)
			{
			} // jkms16w128_t

			// host interface
			u16 read16(const u8 addr);
			void write16(const u8 addr, const u16 data);

			u8 read8(const u8 addr);
			void write8(const u8 addr, const u8 data);

			void reset();

			void tick();

			inline s64 lout() const
			{
				return m_lout;
			} // lout

			inline s64 rout() const
			{
				return m_rout;
			} // rout

		private:
			// register access handler
			u16 reg_r(const u8 reg);
			void reg_w(const u8 reg, const u16 data, const u16 mask = ~0);

			// classes
			jkms16w128_intf_t &m_intf;
			std::array<voice_t, 128> m_voice;

			// register
			u8 m_reg = 0;
			u8 m_voice_select = 0;

			// external memory access
			u16 m_mem_addr = 0;

			// outputs
			s64 m_lout = 0;
			s64 m_rout = 0;

	}; // class jkms16w128_t
} // namespace jkms16w128

#endif // JKMS16W128_HPP