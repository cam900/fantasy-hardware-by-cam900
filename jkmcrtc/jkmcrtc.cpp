/*

==============================================================================

    JKMCRTC
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMCRTC

Fantasy CRT controller

Features:
- Vertical scanline interrupt
- Blank, Sync pin
- Interlace and Non-interlace output support

	8 bit Host interface

	Address Description
	00      Register select
	02      Register data LSB
	03      Register data MSB


	16/32 bit Host interface

	Address Description
	00      Register select
	01      Register data


	8/16 bit Register map

	Register Description
	00       Control, Status
	Bit 15 Video enable (R/W)
	Bit 14 Interlace/Non-interlace (R/W)
	Bit 13 Scanline interrupt enable (R/W)
	Bit 12 Current frame (Read)
	Bit 11 Horizontal blank status (Read)
	Bit 10 Horizontal sync status (Read)
	Bit 9 Vertical blank status (Read)
	Bit 8 Vertical sync status (Read)
	Bit 7 to 0 Pixel clock divider (R/W)
	01       Horizontal visible width (R/W)
	02       Horizontal front porch (R/W)
	03       Horizontal synch width (R/W)
	04       Horizontal back porch (R/W)
	05       Vertical visible width (R/W)
	06       Vertical front porch (R/W)
	07       Vertical synch width (R/W)
	08       Vertical back porch (R/W)
	09       Scanline interrupt position (R/W)
	0c       Horizontal pixel position LSB (Read)
	0d       Horizontal pixel position MSB (Read)
	0e       Vertical scanline position LSB (Read)
	0f       Vertical scanline position MSB (Read)


	32 bit Register map

	Register Description
	00       Control, Status
	Bit 15 Video enable (R/W)
	Bit 14 Interlace/Non-interlace (R/W)
	Bit 13 Scanline interrupt enable (R/W)
	Bit 12 Current frame (Read)
	Bit 11 Horizontal blank status (Read)
	Bit 10 Horizontal sync status (Read)
	Bit 9 Vertical blank status (Read)
	Bit 8 Vertical sync status (Read)
	Bit 7 to 0 Pixel clock divider (R/W)
	01       Horizontal visible width (R/W)
	02       Horizontal front porch (R/W)
	03       Horizontal synch width (R/W)
	04       Horizontal back porch (R/W)
	05       Vertical visible width (R/W)
	06       Vertical front porch (R/W)
	07       Vertical synch width (R/W)
	08       Vertical back porch (R/W)
	09       Scanline interrupt position (R/W)
	0a       Horizontal pixel position (Read)
	0b       Vertical scanline position (Read)
*/

#include "jkmcrtc.hpp"

namespace jkmcrtc
{
	template<bool BigEndian, typename T>
	void jkmcrtc_t<BigEndian, T>::reset()
	{
		m_hstate = crtc_state::CRTC_BACK_PORCH;
		m_vstate = crtc_state::CRTC_BACK_PORCH;
		m_hparam.reset();
		m_vparam.reset();
		m_hparam_latch.reset();
		m_vparam_latch.reset();
		m_hpos = 0;
		m_hcounter = 0;
		m_vpos = 0;
		m_vcounter = 0;
		m_scanline_int = 0;
		m_scanline_int_latch = 0;
		m_scanline_int_counter = 0;
		m_scanline_int_enable = false;
		m_pix_clock_counter = 0;
		m_pix_clock_divider = 0;
		m_pix_clock_divider_latch = 0;
		m_interlace = false;
		m_interlace_latch = false;
		m_frame = false;
		m_hblank_pulse = true;
		m_hsync_pulse = false;
		m_vblank_pulse = true;
		m_vsync_pulse = false;
		m_video_enable = false;
		m_slot = 0;
	} // jkmcrtc_t<BigEndian, T>::reset

	template<bool BigEndian, typename T>
	void jkmcrtc_t<BigEndian, T>::tick()
	{
		if (m_pix_clock_counter++ == m_pix_clock_divider)
		{
			update_horizontal();
			m_pix_clock_counter = 0;
		}
	} // jkmcrtc_t<BigEndian, T>::tick

	template<bool BigEndian, typename T>
	void jkmcrtc_t<BigEndian, T>::update_horizontal()
	{
		if (m_video_enable && ((m_hstate == crtc_state::CRTC_VISIBLE) && (m_vstate == crtc_state::CRTC_VISIBLE)))
		{
			m_intf.write_pixel(m_hpos, m_vpos, m_intf.get_pixel(m_hpos, m_vpos));
		}
		else
		{
			m_intf.write_pixel(m_hpos, m_vpos, 0);
		}
		m_hpos++;
		switch (m_hstate)
		{
			case crtc_state::CRTC_BACK_PORCH:
				if (m_hcounter++ == m_hparam.back_porch())
				{
					m_intf.hblank(false);
					m_hblank_pulse = false;
					m_hparam.set_back_porch(m_hparam_latch.back_porch());
					m_hcounter = 0;
				}
				break;
			case crtc_state::CRTC_VISIBLE:
				if (m_hcounter++ == m_hparam.visible_width())
				{
					m_intf.hblank(true);
					m_hblank_pulse = true;
					m_hparam.set_visible_width(m_hparam_latch.visible_width());
					m_hcounter = 0;
				}
				break;
			case crtc_state::CRTC_FRONT_PORCH:
				if (m_hcounter++ == m_hparam.front_porch())
				{
					m_intf.hsync(true);
					m_hsync_pulse = true;
					m_hparam.set_front_porch(m_hparam_latch.front_porch());
					m_hcounter = 0;
				}
				break;
			case crtc_state::CRTC_SYNC_PULSE:
				if (m_hcounter++ == m_hparam.sync_width())
				{
					m_intf.hsync(false);
					m_hsync_pulse = false;
					update_vertical();
					m_hparam.set_sync_width(m_hparam_latch.sync_width());
					m_hpos = 0;
					m_hcounter = 0;
				}
				break;
		}
	} // jkmcrtc_t<BigEndian, T>::update_horizontal

	template<bool BigEndian, typename T>
	void jkmcrtc_t<BigEndian, T>::update_vertical()
	{
		m_vpos += (m_interlace ? 2 : 1);
		m_vcounter += (m_interlace ? 2 : 1);
		switch (m_vstate)
		{
			case crtc_state::CRTC_BACK_PORCH:
				if (m_vcounter > m_vparam.back_porch())
				{
					m_intf.vblank(false);
					m_vblank_pulse = false;
					m_vparam.set_back_porch(m_vparam_latch.back_porch());
					m_vcounter -= m_vparam.back_porch() + 1;
					m_scanline_int_counter = 0;
				}
				break;
			case crtc_state::CRTC_VISIBLE:
				if (m_scanline_int_counter++ == m_scanline_int)
				{
					if (m_scanline_int_enable)
					{
						m_intf.scanline_int(true);
					}
					m_scanline_int = m_scanline_int_latch;
				}
				if (m_vcounter > m_vparam.visible_width())
				{
					m_intf.vblank(true);
					m_vblank_pulse = true;
					m_vparam.set_visible_width(m_vparam_latch.visible_width());
					m_vcounter -= m_vparam.visible_width() + 1;
				}
				break;
			case crtc_state::CRTC_FRONT_PORCH:
				if (m_vcounter > m_vparam.front_porch())
				{
					m_intf.vsync(true);
					m_vsync_pulse = true;
					m_vparam.set_front_porch(m_vparam_latch.front_porch());
					m_vcounter -= m_vparam.front_porch() + 1;
				}
				break;
			case crtc_state::CRTC_SYNC_PULSE:
				if (m_vcounter > m_vparam.sync_width())
				{
					m_intf.vsync(false);
					m_vsync_pulse = false;
					m_interlace = m_interlace_latch;
					m_pix_clock_divider = m_pix_clock_divider_latch;
					m_frame = !m_frame;
					m_vparam.set_sync_width(m_vparam_latch.sync_width());
					m_vpos = (m_interlace && m_frame) ? 1 : 0;
					m_vcounter = (m_interlace && m_frame) ? 1 : 0;
				}
				break;
		}
	} // jkmcrtc_t<BigEndian, T>::update_vertical

	template<bool BigEndian, typename T>
	void jkmcrtc_t<BigEndian, T>::reg_w(const u16 data, const u16 mask)
	{
		switch (m_slot)
		{
			case 0:
				if (bitfield(mask, 8, 8))
				{
					m_video_enable = bitfield(data, 15);
					m_interlace_latch = bitfield(data, 14);
					m_scanline_int_enable = bitfield(data, 13);
				}
				if (bitfield(mask, 0, 8))
				{
					m_pix_clock_divider_latch = bitfield(data, 0, 8);
				}
				break;
			case 1:
				m_hparam_latch.set_visible_width(data, mask);
				break;
			case 2:
				m_hparam_latch.set_front_porch(data, mask);
				break;
			case 3:
				m_hparam_latch.set_sync_width(data, mask);
				break;
			case 4:
				m_hparam_latch.set_back_porch(data, mask);
				break;
			case 5:
				m_vparam_latch.set_visible_width(data, mask);
				break;
			case 6:
				m_vparam_latch.set_front_porch(data, mask);
				break;
			case 7:
				m_vparam_latch.set_sync_width(data, mask);
				break;
			case 8:
				m_vparam_latch.set_back_porch(data, mask);
				break;
			case 9:
				m_scanline_int_latch = (m_scanline_int_latch & ~mask) | (data & mask);
				break;
		}
	} // jkmcrtc_t<BigEndian, T>::reg_w

	template<bool BigEndian, typename T>
	void jkmcrtc_t<BigEndian, T>::host_w(const u8 addr, const T data, const T mask)
	{
		switch (sizeof(T))
		{
			case 1:
			{
				if (bitfield(addr, 1))
				{
					const u16 sdata = u16(data) << (bitfield(BigEndian ? ~addr : addr, 0) << 3);
					const u16 smask = u16(mask) << (bitfield(BigEndian ? ~addr : addr, 0) << 3);
					reg_w(sdata, smask);
				}
				else if (bitfield(addr, 0) == 0)
				{
					if (bitfield(mask, 0, 8))
					{
						m_slot = bitfield(data, 0, 4);
					}
				}
				break;
			}
			case 2:
			{
				if (bitfield(addr, 0))
				{
					reg_w(data, mask);
				}
				else
				{
					if (bitfield(mask, 0, 8))
					{
						m_slot = bitfield(data, 0, 4);
					}
				}
				break;
			}
			case 4:
			{
				if (bitfield(addr, 0))
				{
					if (bitfield(mask, 0, 16))
					{
						reg_w(bitfield(data, 0, 16), bitfield(mask, 0, 16));
					}
				}
				else
				{
					if (bitfield(mask, 0, 8))
					{
						m_slot = bitfield(data, 0, 4);
					}
				}
				break;
			}
		}
	} // jkmcrtc_t<BigEndian, T>::host_w

	template<bool BigEndian, typename T>
	T jkmcrtc_t<BigEndian, T>::host_r(const u8 addr) const
	{
		T ret = 0;
		switch (sizeof(T))
		{
			case 1:
			{
				if (bitfield(addr, 1))
				{
					const u8 shift = bitfield(BigEndian ? ~addr : addr, 0) << 3;
					switch (m_slot)
					{
						case 0:
							ret = ((m_video_enable ? 0x8000 : 0)
								| (m_interlace_latch ? 0x4000 : 0)
								| (m_scanline_int_enable ? 0x2000 : 0)
								| (m_frame ? 0x1000 : 0)
								| (m_hblank_pulse ? 0x0800 : 0)
								| (m_hsync_pulse ? 0x0400 : 0)
								| (m_vblank_pulse ? 0x0200 : 0)
								| (m_vsync_pulse ? 0x0100 : 0)
								| m_pix_clock_divider) >> shift;
							break;
						case 1:
							ret = m_hparam.visible_width() >> shift;
							break;
						case 2:
							ret = m_hparam.front_porch() >> shift;
							break;
						case 3:
							ret = m_hparam.sync_width() >> shift;
							break;
						case 4:
							ret = m_hparam.back_porch() >> shift;
							break;
						case 5:
							ret = m_vparam.visible_width() >> shift;
							break;
						case 6:
							ret = m_vparam.front_porch() >> shift;
							break;
						case 7:
							ret = m_vparam.sync_width() >> shift;
							break;
						case 8:
							ret = m_vparam.back_porch() >> shift;
							break;
						case 9:
							ret = m_scanline_int >> shift;
							break;
						case 0x0c:
						case 0x0d:
						{
							const u8 shift2 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = (m_hpos >> shift2) >> shift;
							break;
						}
						case 0x0e:
						case 0x0f:
						{
							const u8 shift2 = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = (m_vpos >> shift2) >> shift;
							break;
						}
					}
				}
				else if (bitfield(addr, 0) == 0)
				{
					ret = m_slot;
				}
				break;
			}
			case 2:
			{
				if (bitfield(addr, 0))
				{
					switch (m_slot)
					{
						case 0:
							ret = ((m_video_enable ? 0x8000 : 0)
								| (m_interlace_latch ? 0x4000 : 0)
								| (m_scanline_int_enable ? 0x2000 : 0)
								| (m_frame ? 0x1000 : 0)
								| (m_hblank_pulse ? 0x0800 : 0)
								| (m_hsync_pulse ? 0x0400 : 0)
								| (m_vblank_pulse ? 0x0200 : 0)
								| (m_vsync_pulse ? 0x0100 : 0)
								| m_pix_clock_divider);
							break;
						case 1:
							ret = m_hparam.visible_width();
							break;
						case 2:
							ret = m_hparam.front_porch();
							break;
						case 3:
							ret = m_hparam.sync_width();
							break;
						case 4:
							ret = m_hparam.back_porch();
							break;
						case 5:
							ret = m_vparam.visible_width();
							break;
						case 6:
							ret = m_vparam.front_porch();
							break;
						case 7:
							ret = m_vparam.sync_width();
							break;
						case 8:
							ret = m_vparam.back_porch();
							break;
						case 9:
							ret = m_scanline_int;
							break;
						case 0x0c:
						case 0x0d:
						{
							const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = m_hpos >> shift;
							break;
						}
						case 0x0e:
						case 0x0f:
						{
							const u8 shift = bitfield(BigEndian ? ~m_slot : m_slot, 0) << 4;
							ret = m_vpos >> shift;
							break;
						}
					}
				}
				else
				{
					ret = m_slot;
				}
				break;
			}
			case 4:
			{
				if (bitfield(addr, 0))
				{
					switch (m_slot)
					{
						case 0:
							ret = ((m_video_enable ? 0x8000 : 0)
								| (m_interlace_latch ? 0x4000 : 0)
								| (m_scanline_int_enable ? 0x2000 : 0)
								| (m_frame ? 0x1000 : 0)
								| (m_hblank_pulse ? 0x0800 : 0)
								| (m_hsync_pulse ? 0x0400 : 0)
								| (m_vblank_pulse ? 0x0200 : 0)
								| (m_vsync_pulse ? 0x0100 : 0)
								| m_pix_clock_divider);
							break;
						case 1:
							ret = m_hparam.visible_width();
							break;
						case 2:
							ret = m_hparam.front_porch();
							break;
						case 3:
							ret = m_hparam.sync_width();
							break;
						case 4:
							ret = m_hparam.back_porch();
							break;
						case 5:
							ret = m_vparam.visible_width();
							break;
						case 6:
							ret = m_vparam.front_porch();
							break;
						case 7:
							ret = m_vparam.sync_width();
							break;
						case 8:
							ret = m_vparam.back_porch();
							break;
						case 9:
							ret = m_scanline_int;
							break;
						case 0x0a:
							ret = m_hpos;
							break;
						case 0x0b:
							ret = m_vpos;
							break;
					}
				}
				else
				{
					ret = m_slot;
				}
				break;
			}
		}
	}
} // namespace jkmcrtc
