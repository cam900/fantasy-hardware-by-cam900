/*

==============================================================================

    JKMCRTC
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMCRTC

Fantasy CRT controller

*/

#ifndef JKMCRTC_HPP
#define JKMCRTC_HPP

#pragma once

namespace jkmcrtc
{
	using u8 = unsigned char;
	using u16 = unsigned short;
	using u32 = unsigned int;
	using u64 = unsigned long long;
	using s8 = signed char;
	using s16 = signed short;
	using s32 = signed int;
	using s64 = signed long long;

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos)
	{
		return (in >> pos) & 1;
	} // bitfield

	template<typename T>
	static const inline T bitfield(const T in, const u8 pos, const u8 len)
	{
		return (in >> pos) & ((1 << len) - 1);
	} // bitfield

	class jkmcrtc_intf_t
	{
		public:
			jkmcrtc_intf_t()
			{
			} // jkmcrtc_intf_t

			virtual void hblank(bool assert) {}
			virtual void hsync(bool assert) {}
			virtual void vblank(bool assert) {}
			virtual void vsync(bool assert) {}
			virtual void scanline_int(bool assert) {}

			virtual u32 get_pixel(u32 x, u32 y) { return 0; }
			virtual void write_pixel(u32 x, u32 y, u32 pixel) {}
	}; // class jkmcrtc_intf_t

	template<bool BigEndian, typename T>
	class jkmcrtc_t
	{
		private:
			enum crtc_state : u8
			{
				CRTC_BACK_PORCH = 0,
				CRTC_VISIBLE,
				CRTC_FRONT_PORCH,
				CRTC_SYNC_PULSE
			}; // enum crtc_state

			class params_t
			{
				public:
					params_t()
						: m_visible_width(0)
						, m_front_porch(0)
						, m_sync_width(0)
						, m_back_porch(0)
					{
					} // params_t

					void reset()
					{
						m_visible_width = 0;
						m_front_porch = 0;
						m_sync_width = 0;
						m_back_porch = 0;
					} // reset

					u16 visible_width() const
					{
						return m_visible_width;
					} // visible_width

					u16 front_porch() const
					{
						return m_front_porch;
					} // front_porch

					u16 sync_width() const
					{
						return m_sync_width;
					} // sync_width

					u16 back_porch() const
					{
						return m_back_porch;
					} // back_porch

					void set_visible_width(u16 width, u16 mask = ~0)
					{
						m_visible_width = (m_visible_width & ~mask) | (width & mask);
					} // set_visible_width

					void set_front_porch(u16 width, u16 mask = ~0)
					{
						m_front_porch = (m_front_porch & ~mask) | (width & mask);
					} // set_front_porch

					void set_sync_width(u16 width, u16 mask = ~0)
					{
						m_sync_width = (m_sync_width & ~mask) | (width & mask);
					} // set_sync_width

					void set_back_porch(u16 width, u16 mask = ~0)
					{
						m_back_porch = (m_back_porch & ~mask) | (width & mask);
					} // set_back_porch

				private:
					u16 m_visible_width = 0;
					u16 m_front_porch = 0;
					u16 m_sync_width = 0;
					u16 m_back_porch = 0;
			}; // class params_t

		public:
			jkmcrtc_t(jkmcrtc_intf_t &intf)
				: m_intf(intf)
				, m_hstate(crtc_state::CRTC_BACK_PORCH)
				, m_vstate(crtc_state::CRTC_BACK_PORCH)
				, m_hparam(params_t())
				, m_vparam(params_t())
				, m_hparam_latch(params_t())
				, m_vparam_latch(params_t())
				, m_hpos(0)
				, m_hcounter(0)
				, m_vpos(0)
				, m_vcounter(0)
				, m_scanline_int(0)
				, m_scanline_int_latch(0)
				, m_scanline_int_counter(0)
				, m_scanline_int_enable(false)
				, m_pix_clock_counter(0)
				, m_pix_clock_divider(0)
				, m_pix_clock_divider_latch(0)
				, m_interlace(false)
				, m_interlace_latch(false)
				, m_frame(false)
				, m_hblank_pulse(true)
				, m_hsync_pulse(false)
				, m_vblank_pulse(true)
				, m_vsync_pulse(false)
				, m_video_enable(false)
				, m_slot(0)
			{
			} // jkmcrtc_t

			void reset();

			void tick();

			T host_r(const u8 addr) const;
			void host_w(const u8 addr, const T data, const T mask = T(~0));

			s32 hpos() const
			{
				return m_hpos;
			} // hpos

			s32 vpos() const
			{
				return m_vpos;
			} // vpos

			bool frame() const
			{
				return m_frame;
			} // frame

			bool hblank_pulse() const
			{
				return m_hblank_pulse;
			} // hblank_pulse

			bool hsync_pulse() const
			{
				return m_hsync_pulse;
			} // hsync_pulse

			bool vblank_pulse() const
			{
				return m_vblank_pulse;
			} // vblank_pulse

			bool vsync_pulse() const
			{
				return m_vsync_pulse;
			} // vsync_pulse

		private:
			void update_horizontal();
			void update_vertical();

			void reg_w(const u16 data, const u16 mask = ~0);

			jkmcrtc_intf_t &m_intf;                            // Interface
			crtc_state m_hstate = crtc_state::CRTC_BACK_PORCH; // Horizontal state
			crtc_state m_vstate = crtc_state::CRTC_BACK_PORCH; // Vertical state
			params_t m_hparam;                                 // Horizontal parameters
			params_t m_vparam;                                 // Vertical parameters
			params_t m_hparam_latch;                           // Horizontal parameters latch
			params_t m_vparam_latch;                           // Vertical parameters latch
			s32 m_hpos = 0;                                    // Horizontal position
			s32 m_hcounter = 0;                                // Horizontal counter
			s32 m_vpos = 0;                                    // Vertical position
			s32 m_vcounter = 0;                                // Vertical counter
			u16 m_scanline_int = 0;                                    // Vertical interrupt position
			u16 m_scanline_int_latch = 0;                              // Vertical interrupt position latch
			u16 m_scanline_int_counter = 0;                            // Vertical interrupt position counter
			bool m_scanline_int_enable = false;                        // Vertical interrupt position enable
			u16 m_pix_clock_counter = 0;                       // Pixel clock counter
			u16 m_pix_clock_divider = 0;                       // Pixel clock divider
			u16 m_pix_clock_divider_latch = 0;                 // Pixel clock divider latch
			bool m_interlace = false;                          // Interlace flag
			bool m_interlace_latch = false;                    // Interlace flag latch
			bool m_frame = false;                              // Current frame
			bool m_hblank_pulse = true;                        // Horizontal blank pulse
			bool m_hsync_pulse = false;                        // Horizontal sync pulse
			bool m_vblank_pulse = true;                        // Vertical blank pulse
			bool m_vsync_pulse = false;                        // Vertical sync pulse
			bool m_video_enable = false;                       // Video enable
			u8 m_slot = 0;                                     // Register slot
	}; // class jkmcrtc_t
} // namespace jkmcrtc

#endif // JKMCRTC_HPP
