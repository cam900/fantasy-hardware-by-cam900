/*

==============================================================================

    JKMS16M32
    Copyright (C) 2024-present  cam900

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

==============================================================================

JKMS16M32

Fantasy sound generator by cam900

FM/PM/AM sound generator

Features:
- 32 voices
- 8 operators per each voices
- Configurable user waveform and internal waveform (pulse, triangle, sawtooth)
- Configurable digital filter (4 pole)
- User waveform RAM: 64 KWord x 16 bit (128 KBytes)

Tick rate: input clock / 8

*/

#include "jkms16m32.hpp"

namespace jkms16m32
{
	/*
		host interface

		8 bit:
		Address Description
		00      Register select
		02      Register data LSB
		03      Register data MSB

		16 bit:
		Address Description
		00      Register select
		02      Register data

		Register map

		Register Bit                 Description
		         fedc ba98 7654 3210 
		Channel register:
		00       ---- ---- 7654 3210 Operator on/off
		01       xxxx xxxx xxxx xxxx Left volume
		02       xxxx xxxx xxxx xxxx Right volume
		Operator register:
		Playback
		03       x--- ---- ---- ---- Direct output
		         -x-- ---- ---- ---- Frequency modulation input enable
		         --x- ---- ---- ---- Amplitude modulation input enable
		         ---x ---- ---- ---- Phase modulation input enable
		         ---- -x-- ---- ---- Frequency modulation output enable
		         ---- --x- ---- ---- Amplitude modulation output enable
		         ---- ---x ---- ---- Phase modulation output enable
		         ---- ---- x--- ---- Vibrato enable
		         ---- ---- -x-- ---- Tremolo enable
		         ---- ---- ---- x--- Filter enable
		         ---- ---- ---- -x-- Envelope loop
		         ---- ---- ---- --x- Envelope enable
		Waveform
		04       xxxx ---- ---- ---- Internal waveform size (65536 >> x)
		         ---- x--- ---- ---- Mute bit enable
		         ---- -x-- ---- ---- Horizontal Invert bit enable
		         ---- --x- ---- ---- Vertical Invert bit enable
		         ---- ---x ---- ---- External waveform enable
		         ---- ---- x--- ---- Periodic noise
		         ---- ---- -x-- ---- Inverted Triangle wave
		         ---- ---- --x- ---- Inverted Sawtooth wave
		         ---- ---- ---x ---- Inverted Pulse wave
		         ---- ---- ---- x--- White noise
		         ---- ---- ---- -x-- Triangle wave
		         ---- ---- ---- --x- Sawtooth wave
		         ---- ---- ---- ---x Pulse wave
		05       xxxx ---- ---- ---- Mute bit position
		         ---- xxxx ---- ---- Horizontal bit position
		         ---- ---- xxxx ---- Vertical invert bit position
		         ---- ---- ---- xxxx External waveform size (1 << x)
		06       xxxx xxxx xxxx xxxx Pulse duty
		07       xxxx xxxx xxxx xxxx Initial LFSR value
		08       xxxx xxxx xxxx xxxx LFSR Tab
		09       xxxx xxxx xxxx xxxx External waveform base address
		0a       xxxx xxxx xxxx xxxx Waveform position
		0b       xxxx xxxx xxxx xxxx Pitch (Frequency: Tick rate / (65536 - Pitch))

		Volume
		0c       sxxx xxxx xxxx xxxx Left volume
		0d       sxxx xxxx xxxx xxxx Right volume
		0e       sxxx xxxx xxxx xxxx Master volume

		Envelope
		0f       eeee mmmm mmmm mmmm Delay (4 bit exponent, 12 bit mantissa)
		10       eeee mmmm mmmm mmmm Attack rate (4 bit exponent, 12 bit mantissa)
		11       seee emmm mmmm mmmm Attack target volume (1 bit sign, 4 bit exponent, 11 bit mantissa)
		12       eeee mmmm mmmm mmmm Decay rate
		13       seee emmm mmmm mmmm Decay target volume
		14       eeee mmmm mmmm mmmm Sustain rate
		15       seee emmm mmmm mmmm Sustain target volume
		16       eeee mmmm mmmm mmmm Release rate
		17       seee emmm mmmm mmmm Initial envelope volume

		Filter
		18       x--- ---- ---- ---- Filter 1 enable
		         -x-- ---- ---- ---- Filter 1 Lowpass
		         --x- ---- ---- ---- Filter 1 Highpass
		         ---x ---- ---- ---- Filter 1 Bandpass
		         ---- xxxx xxxx xxxx Filter 1 resonance
		19       xxxx xxxx xxxx xxxx Filter 1 cutoff
		1a       x--- ---- ---- ---- Filter 2 enable
		         -x-- ---- ---- ---- Filter 2 Lowpass
		         --x- ---- ---- ---- Filter 2 Highpass
		         ---x ---- ---- ---- Filter 2 Bandpass
		         ---- xxxx xxxx xxxx Filter 2 resonance
		1b       xxxx xxxx xxxx xxxx Filter 2 cutoff
		1c       x--- ---- ---- ---- Filter 3 enable
		         -x-- ---- ---- ---- Filter 3 Lowpass
		         --x- ---- ---- ---- Filter 3 Highpass
		         ---x ---- ---- ---- Filter 3 Bandpass
		         ---- xxxx xxxx xxxx Filter 3 resonance
		1d       xxxx xxxx xxxx xxxx Filter 3 cutoff
		1e       x--- ---- ---- ---- Filter 4 enable
		         -x-- ---- ---- ---- Filter 4 Lowpass
		         --x- ---- ---- ---- Filter 4 Highpass
		         ---x ---- ---- ---- Filter 4 Bandpass
		         ---- xxxx xxxx xxxx Filter 4 resonance
		1f       xxxx xxxx xxxx xxxx Filter 4 cutoff

		Modulation
		20       7654 3210 ---- ---- PM output matrix
		         ---- ---- 7654 3210 FM output matrix
		21       ---- ---- 7654 3210 AM output matrix
		22       sxxx xxxx xxxx xxxx PM output scale
		23       sxxx xxxx xxxx xxxx AM output scale
		24       sxxx xxxx xxxx xxxx FM output scale
		25       sxxx xxxx xxxx xxxx PM input scale
		26       sxxx xxxx xxxx xxxx AM input scale
		27       sxxx xxxx xxxx xxxx FM input scale

		LFO
		30       eeee mmmm mmmm mmmm Vibrato Delay (4 bit exponent, 12 bit mantissa)
		31       eeee mmmm mmmm mmmm Vibrato rate (4 bit exponent, 12 bit mantissa)
		32       seee emmm mmmm mmmm Vibrato scale (1 bit sign, 4 bit exponent, 11 bit mantissa)
		33       ---- ---- ---- --xx Vibrato wave
		         ---- ---- ---- --00 Triangle
		         ---- ---- ---- --01 Sawtooth
		         ---- ---- ---- --10 Square
		         ---- ---- ---- --11 Noise
		34       sxxx xxxx xxxx xxxx Vibrato depth

		35       eeee mmmm mmmm mmmm Tremolo Delay (4 bit exponent, 12 bit mantissa)
		36       eeee mmmm mmmm mmmm Tremolo rate (4 bit exponent, 12 bit mantissa)
		37       seee emmm mmmm mmmm Tremolo scale (1 bit sign, 4 bit exponent, 11 bit mantissa)
		38       ---- ---- ---- --xx Tremolo wave
		         ---- ---- ---- --00 Triangle
		         ---- ---- ---- --01 Sawtooth
		         ---- ---- ---- --10 Square
		         ---- ---- ---- --11 Noise
		39       sxxx xxxx xxxx xxxx Tremolo depth

		Memory access
		fc       xxxx xxxx xxxx xxxx External wave memory address
		fd       xxxx xxxx xxxx xxxx External wave memory data
		ff       ---- ---- xxx- ---- Select operator
		         ---- ---- ---x xxxx Select voice
	*/

	// Access
	void jkms16m32_t::reg_w(const u8 reg, const u16 data, const u16 mask)
	{
		voice_t &voice = m_voice[m_voice_select];
		const bool msb = bitfield(mask, 8, 8);
		const bool lsb = bitfield(mask, 0, 8);
		const u8 filter_pole = (reg >> 1) & 3;
		switch (reg)
		{
			case 0x00:
				if (lsb)
				{
					for (u8 slot = 0; slot < 8; slot++)
					{
						voice.op(slot).set_keyon(bitfield(data, slot));
					}
				}
				break;

			case 0x01:
					voice.set_lvol(data, mask);
					break;

			case 0x02:
					voice.set_rvol(data, mask);
					break;

			case 0x03:
				if (msb)
				{
					voice.op(m_op_select).set_direct_output(bitfield(data, 15));
					voice.op(m_op_select).wave().set_fm_input_en(bitfield(data, 14));
					voice.op(m_op_select).wave().set_am_input_en(bitfield(data, 13));
					voice.op(m_op_select).wave().set_pm_input_en(bitfield(data, 12));
					voice.op(m_op_select).pm().set_enable(bitfield(data, 10));
					voice.op(m_op_select).am().set_enable(bitfield(data, 9));
					voice.op(m_op_select).fm().set_enable(bitfield(data, 8));
				}
				if (lsb)
				{
					voice.op(m_op_select).wave().vibrato().set_enable(bitfield(data, 7));
					voice.op(m_op_select).wave().tremolo().set_enable(bitfield(data, 6));
					voice.op(m_op_select).filter().set_enable(bitfield(data, 3));
					voice.op(m_op_select).envelope().set_loop(bitfield(data, 2));
					voice.op(m_op_select).envelope().set_enable(bitfield(data, 1));
				}
				break;

			case 0x04:
				if (msb)
				{
					voice.op(m_op_select).wave().set_int_wave_size(bitfield(data, 12, 4));
					voice.op(m_op_select).wave().set_mute_enable(bitfield(data, 11));
					voice.op(m_op_select).wave().set_hinv_enable(bitfield(data, 10));
					voice.op(m_op_select).wave().set_vinv_enable(bitfield(data, 9));
					voice.op(m_op_select).wave().set_ext_wave_enable(bitfield(data, 8));
				}
				if (lsb)
				{
					voice.op(m_op_select).wave().set_periodic_noise(bitfield(data, 7));
					voice.op(m_op_select).wave().set_inv_triangle(bitfield(data, 6));
					voice.op(m_op_select).wave().set_inv_sawtooth(bitfield(data, 5));
					voice.op(m_op_select).wave().set_inv_pulse(bitfield(data, 4));
					voice.op(m_op_select).wave().set_white_noise(bitfield(data, 3));
					voice.op(m_op_select).wave().set_triangle(bitfield(data, 2));
					voice.op(m_op_select).wave().set_sawtooth(bitfield(data, 1));
					voice.op(m_op_select).wave().set_pulse(bitfield(data, 0));
				}
				break;
			case 0x05:
				if (msb)
				{
					voice.op(m_op_select).wave().set_mute_pos(bitfield(data, 12, 4));
					voice.op(m_op_select).wave().set_hinv_pos(bitfield(data, 8, 4));
				}
				if (lsb)
				{
					voice.op(m_op_select).wave().set_vinv_pos(bitfield(data, 4, 4));
					voice.op(m_op_select).wave().set_addr_size(bitfield(data, 0, 4));
				}
				break;
			case 0x06:
				voice.op(m_op_select).wave().set_pulse_duty(data, mask);
				break;
			case 0x07:
				voice.op(m_op_select).wave().set_lfsr_init(data, mask);
				break;
			case 0x08:
				voice.op(m_op_select).wave().set_lfsr_tab(data, mask);
				break;
			case 0x09:
				voice.op(m_op_select).wave().set_base(data, mask);
				break;
			case 0x0a:
				voice.op(m_op_select).wave().set_addr(data, mask);
				break;
			case 0x0b:
				voice.op(m_op_select).wave().set_pitch(data, mask);
				break;
			case 0x0c:
				voice.op(m_op_select).set_lvol(data, mask);
				break;
			case 0x0d:
				voice.op(m_op_select).set_rvol(data, mask);
				break;
			case 0x0e:
				voice.op(m_op_select).set_mvol(data, mask);
				break;

			case 0x0f:
				voice.op(m_op_select).envelope().set_env_delay_time(data, mask);
				break;
			case 0x10:
				voice.op(m_op_select).envelope().set_env_rate(voice.op(m_op_select).envelope().ENV_ATTACK, data, mask);
				break;
			case 0x11:
				voice.op(m_op_select).envelope().set_env_target(voice.op(m_op_select).envelope().ENV_ATTACK, data, mask);
				break;
			case 0x12:
				voice.op(m_op_select).envelope().set_env_rate(voice.op(m_op_select).envelope().ENV_DECAY, data, mask);
				break;
			case 0x13:
				voice.op(m_op_select).envelope().set_env_target(voice.op(m_op_select).envelope().ENV_DECAY, data, mask);
				break;
			case 0x14:
				voice.op(m_op_select).envelope().set_env_rate(voice.op(m_op_select).envelope().ENV_SUSTAIN, data, mask);
				break;
			case 0x15:
				voice.op(m_op_select).envelope().set_env_target(voice.op(m_op_select).envelope().ENV_SUSTAIN, data, mask);
				break;
			case 0x16:
				voice.op(m_op_select).envelope().set_env_rate(voice.op(m_op_select).envelope().ENV_RELEASE, data, mask);
				break;
			case 0x17:
				voice.op(m_op_select).envelope().set_env_level_init(data, mask);
				break;

			case 0x18:
			case 0x1a:
			case 0x1c:
			case 0x1e:
				if (msb)
				{
					voice.op(m_op_select).filter().pole(filter_pole).set_enable(bitfield(data, 15));
					voice.op(m_op_select).filter().pole(filter_pole).set_lowpass(bitfield(data, 14));
					voice.op(m_op_select).filter().pole(filter_pole).set_highpass(bitfield(data, 13));
					voice.op(m_op_select).filter().pole(filter_pole).set_bandpass(bitfield(data, 12));
				}
				voice.op(m_op_select).filter().pole(filter_pole).set_resonance(bitfield(data, 0, 12), bitfield(mask, 0, 12));
				break;
			case 0x19:
			case 0x1b:
			case 0x1d:
			case 0x1f:
				voice.op(m_op_select).filter().pole(filter_pole).set_cutoff(data, mask);
				break;

			case 0x20:
				if (msb)
				{
					voice.op(m_op_select).pm().set_matrix(bitfield(data, 8, 8));
				}
				if (lsb)
				{
					voice.op(m_op_select).fm().set_matrix(bitfield(data, 0, 8));
				}
				break;
			case 0x21:
				if (lsb)
				{
					voice.op(m_op_select).am().set_matrix(bitfield(data, 0, 8));
				}
				break;
			
			case 0x22:
				voice.op(m_op_select).pm().set_scale(data, mask);
				break;
			case 0x23:
				voice.op(m_op_select).fm().set_scale(data, mask);
				break;
			case 0x24:
				voice.op(m_op_select).am().set_scale(data, mask);
				break;

			case 0x25:
				voice.op(m_op_select).wave().set_pm_input_scale(data, mask);
				break;
			case 0x26:
				voice.op(m_op_select).wave().set_am_input_scale(data, mask);
				break;
			case 0x27:
				voice.op(m_op_select).wave().set_fm_input_scale(data, mask);
				break;

			case 0x30:
				voice.op(m_op_select).wave().vibrato().set_delay(data, mask);
				break;
			case 0x31:
				voice.op(m_op_select).wave().vibrato().set_rate(data, mask);
				break;
			case 0x32:
				voice.op(m_op_select).wave().vibrato().set_scale(data, mask);
				break;
			case 0x33:
				if (lsb)
				{
					voice.op(m_op_select).wave().vibrato().set_wave(bitfield(data, 0, 2));
				}
				break;
			case 0x34:
				voice.op(m_op_select).wave().vibrato().set_depth(data, mask);
				break;

			case 0x35:
				voice.op(m_op_select).wave().tremolo().set_delay(data, mask);
				break;
			case 0x36:
				voice.op(m_op_select).wave().tremolo().set_rate(data, mask);
				break;
			case 0x37:
				voice.op(m_op_select).wave().tremolo().set_scale(data, mask);
				break;
			case 0x38:
				if (lsb)
				{
					voice.op(m_op_select).wave().tremolo().set_wave(bitfield(data, 0, 2));
				}
				break;
			case 0x39:
				voice.op(m_op_select).wave().tremolo().set_depth(data, mask);
				break;

			case 0xfc:
				m_mem_addr = (m_mem_addr & ~mask) | data & mask;
				break;

			case 0xfd:
				m_intf.write_word(m_mem_addr, data, mask);
				break;

			case 0xff:
				if (lsb)
				{
					m_voice_select = bitfield(data, 0, 5);
					m_op_select = bitfield(data, 5, 3);
				}
				break;
		}
	} // reg_w

	u16 jkms16m32_t::reg_r(const u8 reg)
	{
		u16 ret = 0;
		
		voice_t &voice = m_voice[m_voice_select];
		const u8 filter_pole = (reg >> 1) & 3;
		switch (reg)
		{
			case 0x00:
				for (u8 slot = 0; slot < 8; slot++)
				{
					ret |= voice.op(slot).get_keyon() ? (1 << slot) : 0;
				}
				break;
			
			case 0x01:
				ret |= bitfield(voice.lvol(), 0, 16);
				break;

			case 0x02:
				ret |= bitfield(voice.rvol(), 0, 16);
				break;

			case 0x03:
				ret |= voice.op(m_op_select).direct_output() ? (1 << 15) : 0;
				ret |= voice.op(m_op_select).wave().fm_input_en() ? (1 << 14) : 0;
				ret |= voice.op(m_op_select).wave().am_input_en() ? (1 << 13) : 0;
				ret |= voice.op(m_op_select).wave().pm_input_en() ? (1 << 12) : 0;
				ret |= voice.op(m_op_select).pm().enable() ? (1 << 10) : 0;
				ret |= voice.op(m_op_select).fm().enable() ? (1 << 9) : 0;
				ret |= voice.op(m_op_select).am().enable() ? (1 << 8) : 0;
				ret |= voice.op(m_op_select).wave().vibrato().enable() ? (1 << 7) : 0;
				ret |= voice.op(m_op_select).wave().tremolo().enable() ? (1 << 6) : 0;
				ret |= voice.op(m_op_select).filter().enable() ? (1 << 3) : 0;
				ret |= voice.op(m_op_select).envelope().loop() ? (1 << 2) : 0;
				ret |= voice.op(m_op_select).envelope().enable() ? (1 << 1) : 0;
				break;

			case 0x04:
				ret |= bitfield(voice.op(m_op_select).wave().int_wave_size(), 0, 4) << 12;
				ret |= voice.op(m_op_select).wave().mute_enable() ? (1 << 11) : 0;
				ret |= voice.op(m_op_select).wave().hinv_enable() ? (1 << 10) : 0;
				ret |= voice.op(m_op_select).wave().vinv_enable() ? (1 << 9) : 0;
				ret |= voice.op(m_op_select).wave().ext_wave_enable() ? (1 << 8) : 0;
				ret |= voice.op(m_op_select).wave().periodic_noise() ? (1 << 7) : 0;
				ret |= voice.op(m_op_select).wave().inv_triangle() ? (1 << 6) : 0;
				ret |= voice.op(m_op_select).wave().inv_sawtooth() ? (1 << 5) : 0;
				ret |= voice.op(m_op_select).wave().inv_pulse() ? (1 << 4) : 0;
				ret |= voice.op(m_op_select).wave().white_noise() ? (1 << 3) : 0;
				ret |= voice.op(m_op_select).wave().triangle() ? (1 << 2) : 0;
				ret |= voice.op(m_op_select).wave().sawtooth() ? (1 << 1) : 0;
				ret |= voice.op(m_op_select).wave().pulse() ? (1 << 0) : 0;
				break;
			case 0x05:
				ret |= bitfield(voice.op(m_op_select).wave().mute_pos(), 0, 4) << 12;
				ret |= bitfield(voice.op(m_op_select).wave().hinv_pos(), 0, 4) << 8;
				ret |= bitfield(voice.op(m_op_select).wave().vinv_pos(), 0, 4) << 4;
				ret |= bitfield(voice.op(m_op_select).wave().addr_size(), 0, 4) << 0;
				break;
			case 0x06:
				ret = voice.op(m_op_select).wave().pulse_duty();
				break;
			case 0x07:
				ret = voice.op(m_op_select).wave().lfsr_init();
				break;
			case 0x08:
				ret = voice.op(m_op_select).wave().lfsr_tab();
				break;
			case 0x09:
				ret = voice.op(m_op_select).wave().base();
				break;
			case 0x0a:
				ret = voice.op(m_op_select).wave().addr();
				break;
			case 0x0b:
				ret = voice.op(m_op_select).wave().pitch();
				break;
			case 0x0c:
				ret = bitfield(voice.op(m_op_select).lvol(), 0, 16);
				break;
			case 0x0d:
				ret = bitfield(voice.op(m_op_select).rvol(), 0, 16);
				break;
			case 0x0e:
				ret = bitfield(voice.op(m_op_select).mvol(), 0, 16);
				break;

			case 0x0f:
				ret = voice.op(m_op_select).envelope().env_delay_time();
				break;
			case 0x10:
				ret = voice.op(m_op_select).envelope().env_rate(voice.op(m_op_select).envelope().ENV_ATTACK);
				break;
			case 0x11:
				ret = voice.op(m_op_select).envelope().env_target(voice.op(m_op_select).envelope().ENV_ATTACK);
				break;
			case 0x12:
				ret = voice.op(m_op_select).envelope().env_rate(voice.op(m_op_select).envelope().ENV_DECAY);
				break;
			case 0x13:
				ret = voice.op(m_op_select).envelope().env_target(voice.op(m_op_select).envelope().ENV_DECAY);
				break;
			case 0x14:
				ret = voice.op(m_op_select).envelope().env_rate(voice.op(m_op_select).envelope().ENV_SUSTAIN);
				break;
			case 0x15:
				ret = voice.op(m_op_select).envelope().env_target(voice.op(m_op_select).envelope().ENV_SUSTAIN);
				break;
			case 0x16:
				ret = voice.op(m_op_select).envelope().env_rate(voice.op(m_op_select).envelope().ENV_RELEASE);
				break;
			case 0x17:
				ret = voice.op(m_op_select).envelope().env_level_init();
				break;

			case 0x18:
			case 0x1a:
			case 0x1c:
			case 0x1e:
				ret |= voice.op(m_op_select).filter().pole(filter_pole).enable() ? (1 << 15) : 0;
				ret |= voice.op(m_op_select).filter().pole(filter_pole).lowpass() ? (1 << 14) : 0;
				ret |= voice.op(m_op_select).filter().pole(filter_pole).highpass() ? (1 << 13) : 0;
				ret |= voice.op(m_op_select).filter().pole(filter_pole).bandpass() ? (1 << 12) : 0;
				ret |= bitfield(voice.op(m_op_select).filter().pole(filter_pole).resonance(), 0, 12);
				break;
			case 0x19:
			case 0x1b:
			case 0x1d:
			case 0x1f:
				ret = voice.op(m_op_select).filter().pole(filter_pole).cutoff();
				break;

			case 0x20:
				ret |= voice.op(m_op_select).pm().matrix() << 8;
				ret |= voice.op(m_op_select).fm().matrix();
				break;

			case 0x21:
				ret |= voice.op(m_op_select).am().matrix();
				break;

			case 0x22:
				ret |= bitfield(voice.op(m_op_select).pm().scale(), 0, 16);
				break;
			case 0x23:
				ret |= bitfield(voice.op(m_op_select).fm().scale(), 0, 16);
				break;
			case 0x24:
				ret |= bitfield(voice.op(m_op_select).am().scale(), 0, 16);
				break;

			case 0x25:
				ret = bitfield(voice.op(m_op_select).wave().pm_input_scale(), 0, 16);
				break;
			case 0x26:
				ret = bitfield(voice.op(m_op_select).wave().am_input_scale(), 0, 16);
				break;
			case 0x27:
				ret = bitfield(voice.op(m_op_select).wave().fm_input_scale(), 0, 16);
				break;

			case 0x30:
				ret = voice.op(m_op_select).wave().vibrato().delay();
				break;
			case 0x31:
				ret = voice.op(m_op_select).wave().vibrato().rate();
				break;
			case 0x32:
				ret = voice.op(m_op_select).wave().vibrato().scale();
				break;
			case 0x33:
				ret = bitfield(voice.op(m_op_select).wave().vibrato().wave(), 0, 2);
				break;
			case 0x34:
				ret = voice.op(m_op_select).wave().vibrato().depth();
				break;

			case 0x35:
				ret = voice.op(m_op_select).wave().tremolo().delay();
				break;
			case 0x36:
				ret = voice.op(m_op_select).wave().tremolo().rate();
				break;
			case 0x37:
				ret = voice.op(m_op_select).wave().tremolo().scale();
				break;
			case 0x38:
				ret = bitfield(voice.op(m_op_select).wave().tremolo().wave(), 0, 2);
				break;
			case 0x39:
				ret = voice.op(m_op_select).wave().tremolo().depth();
				break;

			case 0xfc:
				ret = m_mem_addr;
				break;

			case 0xfd:
				ret = m_intf.read_word(m_mem_addr);
				break;

			case 0xff:
				ret = bitfield(m_voice_select, 0, 5) | (bitfield(m_op_select, 0, 3) << 5);
				break;
		}
		return ret;
	}

	// host interface
	void jkms16m32_t::write16(const u8 addr, const u16 data)
	{
		switch (addr & 1)
		{
			case 0:
				m_reg = bitfield(data, 0, 8);
				break;
			case 1:
				reg_w(m_reg, data, ~0);
				break;
		}
	}

	u16 jkms16m32_t::read16(const u8 addr)
	{
		switch (addr & 1)
		{
			case 0:
				return m_reg;
			case 1:
				return reg_r(m_reg);
			default:
				return 0;
		}
	}

	void jkms16m32_t::write8(const u8 addr, const u8 data)
	{
		switch (addr & 1)
		{
			case 0:
				m_reg = data;
				break;
			case 2:
				reg_w(m_reg, data, 0x00ff);
				break;
			case 3:
				reg_w(m_reg, u16(data) << 8, 0xff00);
				break;
		}
	}

	u8 jkms16m32_t::read8(const u8 addr)
	{
		switch (addr & 3)
		{
			case 0:
				return m_reg;
			case 2:
				return reg_r(m_reg) & 0xff;
			case 3:
				return reg_r(m_reg) >> 8;
			default:
				return 0;
		}
	}

	// Core
	void jkms16m32_t::reset()
	{
		for (voice_t &v : m_voice)
			v.reset();

		m_reg = m_voice_select = 0;
		m_lout = m_rout = 0;
	} // jkms16m32_t::reset

	void jkms16m32_t::tick()
	{
		m_lout = m_rout = 0;
		for (voice_t &v : m_voice)
		{
			v.tick();
			m_lout += v.lout();
			m_rout += v.rout();
		}
	} // jkms16m32_t::tick

	// Voice
	void jkms16m32_t::voice_t::reset()
	{
		for (operator_t &op : m_op)
			op.reset();

		m_lvol = m_rvol = 0;
		m_op_ctr = 0;
		m_lout = m_rout = 0;
		m_lout_temp = m_rout_temp = 0;
	} // jkms16m32_t::reset

	void jkms16m32_t::voice_t::tick()
	{
		m_op[m_op_ctr].tick();
		m_lout_temp += m_op[m_op_ctr].lout();
		m_rout_temp += m_op[m_op_ctr].rout();
		m_op_ctr++;
		if ((m_op_ctr & 7) == 0)
		{
			m_lout = (m_lout_temp * m_lvol) >> 15;
			m_rout = (m_rout_temp * m_rvol) >> 15;
			m_lout_temp = 0;
			m_rout_temp = 0;
			m_op_ctr &= 7;
		}
	} // jkms16m32_t::tick

	// Operator
	void jkms16m32_t::voice_t::operator_t::reset()
	{
		m_envelope.reset();
		m_wave.reset();
		m_filter.reset();

		m_keyon = false;
		m_busy = false;
		m_direct_output = false;
		m_lvol = 0;
		m_rvol = 0;
		m_mvol = 0;

		m_lout = 0;
		m_rout = 0;
	} // jkms16m32_t::voice_t::operator_t::reset

	void jkms16m32_t::voice_t::operator_t::keyon()
	{
		m_envelope.init();
		m_wave.init();
		m_keyon = true;
		m_busy = true;
	} // jkms16m32_t::voice_t::operator_t::keyon

	void jkms16m32_t::voice_t::operator_t::keyoff()
	{
		m_keyon = false;
		if (m_envelope.enable())
			m_envelope.release();
		else
			m_busy = false;
	}

	void jkms16m32_t::voice_t::operator_t::tick()
	{
		m_lout = m_rout = 0;
		if (m_busy)
		{
			if (m_direct_output)
			{
				m_lout = m_lvol;
				m_rout = m_rvol;
			}
			else
			{
				s64 out = m_wave.wave_get();
				if (m_filter.enable())
					out = m_filter.tick(out);
				if (m_envelope.enable())
					out = (out * m_envelope.tick()) >> 25;
				m_wave.tick();

				out = (out * m_mvol) >> 15;
				m_lout = (out * m_lvol) >> 15;
				m_rout = (out * m_rvol) >> 15;

				for (u8 slot = 0; slot < 8; slot++)
				{
					if (m_pm.enable())
					{
						if (bitfield(m_pm.matrix(), slot))
						{
							m_voice.op(slot).wave().add_pm_input((out * m_pm.scale()) >> 15);
						}
					}
					if (m_am.enable())
					{
						if (bitfield(m_am.matrix(), slot))
						{
							m_voice.op(slot).wave().add_am_input((out * m_am.scale()) >> 15);
						}
					}
					if (m_fm.enable())
					{
						if (bitfield(m_fm.matrix(), slot))
						{
							m_voice.op(slot).wave().add_fm_input((out * m_fm.scale()) >> 15);
						}
					}
				}
			}
		}
	} // jkms16m32_t::voice_t::operator_t::tick

	// Envelope
	void jkms16m32_t::voice_t::operator_t::envelope_t::reset()
	{
		m_enable = false;
		m_env_delay_time = 0;
		m_env_level_init = 0;
		std::fill(m_env_rate_val.begin(), m_env_rate_val.end(), 0);
		std::fill(m_env_target_val.begin(), m_env_target_val.end(), 0);
		m_loop = false;
		m_env_state = ENV_IDLE;
		m_env_level = 0;
		m_env_rate = 0;
		m_env_target = 0;
		m_env_delay_counter = 0;
	} // jkms16m32_t::voice_t::operator_t::envelope_t::reset

	void jkms16m32_t::voice_t::operator_t::envelope_t::init()
	{
		m_env_delay_counter = get_env_delay(m_env_delay_time);
		m_env_state = ENV_ATTACK;
		m_env_rate = get_env_rate(m_env_rate_val[m_env_state]);
		m_env_target = get_env_target(m_env_target_val[m_env_state]);
		if (m_env_rate == -1)
			m_env_level = m_env_target;
		else
			m_env_level = get_env_target(m_env_level_init);
	} // jkms16m32_t::voice_t::operator_t::envelope_t::init

	void jkms16m32_t::voice_t::operator_t::envelope_t::release()
	{
		if (m_env_state != ENV_RELEASE && m_env_state != ENV_RELEND)
		{
			m_env_state = ENV_RELEASE;
			m_env_rate = get_env_rate(m_env_rate_val[m_env_state]);
			if (m_env_rate == -1)
				m_env_level = 0;
		}
	} // jkms16m32_t::voice_t::operator_t::envelope_t::release

	s32 jkms16m32_t::voice_t::operator_t::envelope_t::tick()
	{
		if (m_env_state == ENV_RELEASE)
		{
			if (m_env_rate == 0)
				return 0;
			else if (m_env_level > 0)
				m_env_level = std::min<s32>(m_env_level - m_env_rate, 0);
			else if (m_env_level < 0)
				m_env_level = std::max<s32>(m_env_level + m_env_rate, 0);
			else if (m_env_level == 0)
				m_env_state++;
		}
		else if (m_env_state != ENV_IDLE && m_env_state != ENV_RELEND)
		{
			if (m_env_delay_counter > 0)
			{
				m_env_delay_counter--;
				return m_env_level;
			}
			if (m_env_rate == 0)
				return m_env_level;
			else if (m_env_level > m_env_target)
				m_env_level = std::max<s32>(m_env_level - m_env_rate, m_env_target);
			else if (m_env_level < m_env_target)
				m_env_level = std::min<s32>(m_env_level + m_env_rate, m_env_target);
			else if (m_env_level == m_env_target)
			{
				m_env_state++;
				if (m_env_state == ENV_IDLE && m_loop)
					m_env_state = ENV_DECAY;
				if (m_env_state != ENV_IDLE && m_env_state != ENV_RELEND)
				{
					m_env_rate = get_env_rate(m_env_rate_val[m_env_state]);
					m_env_target = get_env_target(m_env_target_val[m_env_state]);
					if (m_env_rate == -1)
					{
						m_env_level = m_env_target;
					}
				}
			}
		}
		return m_env_level;
	} // jkms16m32_t::voice_t::operator_t::envelope_t::tick

	// Wave
	void jkms16m32_t::voice_t::operator_t::wave_t::reset()
	{
		m_vibrato.reset();
		m_tremolo.reset();
		m_pulse = false;
		m_sawtooth = false;
		m_triangle = false;
		m_inv_pulse = false;
		m_inv_sawtooth = false;
		m_inv_triangle = false;
		m_periodic_noise = false;
		m_white_noise = false;

		m_int_wave_size = 0;
		m_pulse_duty = 0;
		m_lfsr_tab = 1;
		m_lfsr_init = 1;

		m_ext_wave_enable = false;
		m_base = 0;
		m_addr_size = 0;

		m_pitch = 0;

		m_pm_input_en = false;
		m_pm_input_scale = 0;

		m_am_input_en = false;
		m_am_input_scale = 0;

		m_fm_input_en = false;
		m_fm_input_scale = 0;

		m_mute_enable = false;
		m_mute_pos = 0;

		m_hinv_enable = false;
		m_hinv_pos = 0;

		m_vinv_enable = false;
		m_vinv_pos = 0;

		m_addr = 0;
		m_addr_mask = 0;
		m_pitch_ctr = 0xffff;
		m_lfsr = 1;
	
		m_pm_input = 0;
		m_am_input = 0;
		m_fm_input = 0;
	} // jkms16m32_t::voice_t::operator_t::wave_t::reset

	void jkms16m32_t::voice_t::operator_t::wave_t::init()
	{
		m_vibrato.init();
		m_tremolo.init();
		m_addr = 0;
		m_lfsr = m_lfsr_init;
		m_pitch_ctr = 0xffff;
	} // jkms16m32_t::voice_t::operator_t::wave_t::init

	s32 jkms16m32_t::voice_t::operator_t::wave_t::wave_get()
	{
		u16 mod_addr = m_addr;
		if (m_pm_input_en)
			mod_addr += (m_pm_input * m_pm_input_scale) >> 15;

		s32 wave_output = 0;
		if (m_ext_wave_enable)
		{
			if (m_mute_enable && bitfield(mod_addr, m_mute_pos)) // mute
				wave_output = 0;
			else
			{
				u16 addr = mod_addr & m_addr_mask;
				if (m_hinv_enable && bitfield(mod_addr, m_hinv_pos)) // horizontal invert
					addr = m_addr_mask - addr;

				wave_output = s32(s16(read_word((m_base + addr) & 0xffff))); // signed to unsigned

				if (m_vinv_enable && bitfield(mod_addr, m_vinv_pos)) // vertical invert
					wave_output = -wave_output;
			}
		}

		const u16 int_addr = mod_addr << m_int_wave_size;
		s32 internal_output = 0;
		// Pulse wave
		if (m_pulse)
			internal_output += (int_addr < m_pulse_duty) ? -0x7fff : 0x7fff;

		// Inverted Pulse wave
		if (m_inv_pulse)
			internal_output += (int_addr > m_pulse_duty) ? -0x7fff : 0x7fff;

		// Sawtooth wave
		if (m_sawtooth)
			internal_output += s32(s16(int_addr));

		// Inverted Sawtooth wave
		if (m_inv_sawtooth)
			internal_output -= s32(s16(int_addr));

		// Triangle wave
		if (m_triangle)
		{
			const s16 out = bitfield(int_addr, 14) ? (0x7fff - ((int_addr & 0x3fff) << 1)) : ((int_addr & 0x3fff) << 1);
			internal_output += bitfield(int_addr, 15) ? -out : out;
		}

		// Inverted Triangle wave
		if (m_inv_triangle)
		{
			const s16 out = bitfield(int_addr, 14) ? (0x7fff - ((int_addr & 0x3fff) << 1)) : ((int_addr & 0x3fff) << 1);
			internal_output -= bitfield(int_addr, 15) ? -out : out;
		}

		// White noise
		if (m_white_noise)
			internal_output += s32(s16(m_lfsr));

		// Periodic noise
		if (m_periodic_noise)
			internal_output += bitfield(m_lfsr, 0) ? -0x7fff : 0x7fff;

		s32 output = clamp<s32>(internal_output + wave_output, -0x8000, 0x7fff);

		if (m_am_input_en)
			output = clamp<s32>(output + ((m_am_input * m_am_input_scale) >> 15), -0x8000, 0x7fff);
		if (m_tremolo.enable())
			output = clamp<s32>(output + ((s64(m_tremolo.tick()) * s64(m_tremolo.depth())) >> 25), -0x8000, 0x7fff);

		return output;
	} // jkms16m32_t::voice_t::operator_t::wave_t::wave_get

	void jkms16m32_t::voice_t::operator_t::wave_t::tick()
	{
		s64 pitch = m_pitch;
		if (m_fm_input_en)
			pitch = clamp<s64>(pitch + ((m_fm_input * m_fm_input_scale) >> 15), 0, 0xffff);
		if (m_vibrato.enable())
			pitch = clamp<s64>(pitch + ((m_vibrato.tick() * m_vibrato.depth()) >> 15), 0, 0xffff);

		if (m_pitch_ctr <= pitch)
		{
			m_addr++;

			u32 xor_tab = 0;
			for (int i = 0; i < 16; i++)
			{
				if (bitfield(m_lfsr_tab, i))
					xor_tab ^= bitfield(m_lfsr, i);
			}
			m_lfsr = (m_lfsr >> 1) | (xor_tab << 16);
			if (m_lfsr == 0)
				m_lfsr = 1 << 16;
			m_pitch_ctr = 0xffff;
		}
		else
		{
			m_pitch_ctr--;
		}

		m_pm_input = 0;
		m_am_input = 0;
		m_fm_input = 0;
	} // jkms16m32_t::voice_t::operator_t::wave_t::tick

	void jkms16m32_t::voice_t::operator_t::wave_t::lfo_t::reset()
	{
		m_enable = false;
		m_delay = 0;
		m_rate = 0;
		m_scale = 0;
		m_wave = 0;
		m_depth = 0;

		m_delay_counter = 0;
		m_level = 0;
		m_target = 0;
		m_temp_rate = 0;
		m_lfsr = 1;
	} // jkms16m32_t::voice_t::operator_t::wave_t::lfo_t::reset

	void jkms16m32_t::voice_t::operator_t::wave_t::lfo_t::init()
	{
		m_level = 0;
		m_target = get_lfo_target(m_scale);
		m_delay_counter = get_lfo_rate(m_delay);
		m_temp_rate = get_lfo_rate(m_rate);
		m_lfsr = 1;
	} // jkms16m32_t::voice_t::operator_t::wave_t::lfo_t::init

	s32 jkms16m32_t::voice_t::operator_t::wave_t::lfo_t::tick()
	{
		if (m_delay_counter > 0)
		{
			m_delay_counter--;
			return 0;
		}

		if (m_level > m_target)
			m_level = std::max<s32>(m_level - m_temp_rate, m_target);
		if (m_level < m_target)
			m_level = std::min<s32>(m_level + m_temp_rate, m_target);
		switch (m_wave)
		{
			default:
			case 0: // triangle
				if (m_level == m_target)
				{
					m_target = -m_target;
				}
				return m_level;
			case 1: // sawtooth
				if (m_level == m_target)
				{
					m_level = -m_level;
				}
				return m_level;
			case 2: // square wave
				if (m_level == m_target)
				{
					m_target = -m_target;
				}
				return m_target;
			case 3: // noise
				if (m_level == m_target)
				{
					m_lfsr = (m_lfsr >> 1) ^ ((bitfield(m_lfsr, 0) ^ bitfield(m_lfsr, 2)) << 15);
					m_level = -m_level;
				}
				return s64(s64(s32(s16(m_lfsr))) * s64(m_target)) >> 15;
		}
	} // jkms16m32_t::voice_t::operator_t::wave_t::lfo_t::tick

	void jkms16m32_t::voice_t::operator_t::filter_t::reset()
	{
		m_enable = false;
		for (filter_pole_t &p : m_pole)
			p.reset();
	} // jkms16m32_t::voice_t::operator_t::filter_t::reset

	s32 jkms16m32_t::voice_t::operator_t::filter_t::tick(const s32 input)
	{
		s32 output = input;
		for (filter_pole_t &p : m_pole)
		{
			if (p.enable())
				output = p.tick(output);
		}
		return output;
	} // jkms16m32_t::voice_t::operator_t::filter_t::tick

	void jkms16m32_t::voice_t::operator_t::filter_t::filter_pole_t::reset()
	{
		m_enable = false;
		m_lowpass = false;
		m_highpass = false;
		m_bandpass = false;
		m_cutoff = 0;
		m_resonance = 0;
		m_lowres = 0;
		m_highres = 0;
		m_bandres = 0;
	} // jkms16m32_t::voice_t::operator_t::filter_t::filter_pole_t::reset

	s32 jkms16m32_t::voice_t::operator_t::filter_t::filter_pole_t::tick(const s32 input)
	{
		m_lowres = m_lowres + ((m_cutoff * m_bandres) >> 16);
		m_highres = input - m_lowres - (((0x1000 - m_resonance) * m_bandres) >> 12);
		m_bandres = ((m_cutoff * m_highres) >> 16) + m_bandres;

		s32 out = 0;
		if (m_lowpass)
			out += m_lowres;
		if (m_highpass)
			out += m_highres;
		if (m_bandpass)
			out += m_bandpass;

		return out;
	} // jkms16m32_t::voice_t::operator_t::filter_t::filter_pole_t::tick
} // namespace jkms16m32